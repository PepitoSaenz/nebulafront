import { NgModule } from '@angular/core';
import { NgxSpinnerModule } from "ngx-spinner";
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MdePopoverModule } from '@material-extended/mde';
import { AppComponent } from './app.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { GlobalQueryComponent } from './global-query/global-query.component';
import { StartTableComponent } from './start-table/start-table.component';
import { GenerateNamingsComponent } from './generate-namings/generate-namings.component';
import { AuthComponent } from './auth/auth.component';
import { HomeComponent } from './home/home.component';
import { NotfoundpageComponent } from './notfoundpage/notfoundpage.component';
import { EditTableComponent } from './edit-table/edit-table.component';
import { CommentComponentComponent } from './comment-component/comment-component.component';
import { CommentHistorialComponent } from './comment-historial/comment-historial.component';
import { RequestTableComponent } from './request-table/request-table.component';
import { RequestUsecaseComponent } from './request-usecase/request-usecase.component';
import { SearchTableComponent } from './search-table/search-table.component';
import { SearchWordsComponent } from './search-words/search-words.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { StatisticsDialogComponent } from './statistics-dialog/statistics-dialog.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { LegacySearchComponent } from './legacy-search/legacy-search.component';
import { ErrorsComponent } from './errors/errors.component';
import { ViewTableComponent } from './view-table/view-table.component';
import { VoboDictionaryComponent } from './vobo-dictionary/vobo-dictionary.component';
import { GenerateReviewComponent } from './generate-review/generate-review.component';
import { ProfileComponent } from './profile/profile.component';
import { PasswordComponent } from './profile/password/password.component';
import { BacklogComponent } from './backlog/backlog.component';
import { StartTablonComponent } from './start-tablon/start-tablon.component';
import { RequestTablonComponent } from './request-tablon/request-tablon.component';
import { EditTablonComponent } from './edit-tablon/edit-tablon.component';
import { NamingInfoComponent } from './edit-tablon/naming-info/naming-info.component';
import { AclViewComponent } from './acl-view/acl-view.component';
import { CommentsDetailsComponent } from './acl-view/comments-details/comments-details.component';
import { RepoDatatableComponent } from './repo-datatable/repo-datatable.component';
import { RegisterUserComponent } from './auth/register-user/register-user.component';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { UserRequestComponent } from './admin-view/user-request/user-request.component';
import { LoadTableComponent } from './admin-view/load-table/load-table.component';
import { StartOpbaseComponent } from './start-opbase/start-opbase.component';
import { ComparisonInfoComponent } from './edit-tablon/comparison-info/comparison-info.component';
import { ViewDatatableComponent } from './view-datatable/view-datatable.component';
import { ViewTablonComponent } from './view-tablon/view-tablon.component';
import { EditOpbaseComponent } from './edit-opbase/edit-opbase.component';
import { HomeSearchComponent } from './home-search/home-search.component';
import { FunctionalGovComponent } from './functional-gov/functional-gov.component';
import { SearchOpbaseComponent } from './search-opbase/search-opbase.component';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { TableInfoComponent } from './control-panel/table-info/table-info.component';
import { ViewUuaasComponent } from './view-uuaas/view-uuaas.component';
import { UuaaDetailComponent } from './view-uuaas/uuaa-detail/uuaa-detail.component';
import { LoadInfoComponent } from './edit-table/load-info/load-info.component';
import { TablonInfoComponent } from './control-panel/tablon-info/tablon-info.component';
import { SearchTablonComponent } from './search-tablon/search-tablon.component';
import { LoadOpbaseComponent } from './load-opbase/load-opbase.component';
import { LoadSettingsComponent } from './load-opbase/load-settings/load-settings.component';
import { ProyectRequestComponent } from './admin-view/proyect-request/proyect-request.component';

const appRoutes: Routes = [
  {
    path: 'login',
    component: AuthComponent,
    data: { title: 'Login' }
  },
  {
    path: 'register',
    component: RegisterUserComponent,
    data: { title: 'Registro Usuarios' }
  },
  {
    path: 'profile',
    component: ProfileComponent,
    data: { title: 'profile' }
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { title: 'Home' }
  },
  {
    path: 'home/productowner',
    component: BacklogComponent,
    data: { title: 'Home Product Owner' }
  },
  {
    path: 'home/sproductowner',
    component: BacklogComponent,
    data: { title: 'Home Super PO' }
  },
  {
    path: 'statistics',
    component: StatisticsComponent,
    data: {title: 'statistics'}
  },
  {
    path: 'naming',
    component: GlobalQueryComponent,
    data: { title: 'Naming'}
  },
  {
    path: 'abbreviations',
    component: SearchWordsComponent,
    data: {title: 'Global abbreviations query'}
  },
  {
    path: 'legacy',
    component: LegacySearchComponent,
    data: {title: 'Legacy search'}
  },
  {
    path: 'opbases',
    component: StartOpbaseComponent,
    data: { title: 'Bases Op' }
  },
  {
    path: 'opbases/edit',
    component: EditOpbaseComponent,
    data: { title: 'Bases Op' }
  },
  {
    path: 'opbases/search',
    component: SearchOpbaseComponent,
    data: { title: 'OPBase Search'}
  },
  {
    path: 'tablones',
    component: StartTablonComponent,
    data: { title: 'Tablones' }
  },
  {
    path: 'tablon/edit',
    component: EditTablonComponent,
    data: { title: 'Tablones' }
  },
  {
    path: 'tables',
    component: StartTableComponent,
    data: { title: 'Tablas' }
  },
  {
    path: 'tables/vobo',
    component: VoboDictionaryComponent,
    data: { title: 'VoBo Negocio' }
  },
  {
    path: 'tables/view',
    component: ViewTableComponent,
    data: { title: 'Tablas read only' }
  },
  {
    path: 'tablon/view',
    component: ViewTablonComponent,
    data: { title: 'Tablones read only' }
  },
  {
    path: 'tables/search',
    component: HomeSearchComponent,
    data: { title: 'Update tablas' }
  },
  {
    path: 'table/gobierno/update',
    component: GenerateNamingsComponent,
    data: { title: 'Naming table' }
  },
  {
    path: 'table/ingesta/update',
    component: GenerateReviewComponent,
    data: { title: 'Naming table' }
  },
  {
    path: 'acl',
    component: AclViewComponent,
    data: { title: 'ACL' }
  },
  {
    path: 'admin',
    component: AdminViewComponent,
    data: { title: 'Admin View' }
  },
  {
    path: 'notfoundpage',
    component: NotfoundpageComponent,
    data: { title: 'Page Not Found'}
  },
  {
    path: 'tables/edit',
    component: EditTableComponent,
    data: { title: 'WORK EDIT TABLE'}
  },
  {
    path: 'funct',
    component: FunctionalGovComponent,
    data: { title: 'WORK GOV FUNC' }
  },
  {
    path: 'cpanel',
    component: ControlPanelComponent,
    data: { title: 'CPANEL' }
  },
  {
    path: 'view/uuaas',
    component: ViewUuaasComponent,
    data: { title: 'ua' }
  },
  {
    path: 'load/baseop',
    component: LoadOpbaseComponent,
    data: { title: 'ua' }
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'notfoundpage',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    GlobalQueryComponent,
    StartTableComponent,
    GenerateNamingsComponent,
    AuthComponent,
    HomeComponent,
    NotfoundpageComponent,
    EditTableComponent,
    CommentComponentComponent,
    CommentHistorialComponent,
    RequestTableComponent,
    SearchTableComponent,
    SearchWordsComponent,
    StatisticsComponent,
    ToolbarComponent,
    StatisticsDialogComponent,
    LegacySearchComponent,
    ErrorsComponent,
    ViewTableComponent,
    VoboDictionaryComponent,
    ProfileComponent,
    PasswordComponent,
    GenerateReviewComponent,
    BacklogComponent,
    RequestUsecaseComponent,
    StartTablonComponent,
    RequestTablonComponent,
    EditTablonComponent,
    NamingInfoComponent,
    AclViewComponent,
    CommentsDetailsComponent,
    RepoDatatableComponent,
    RegisterUserComponent,
    AdminViewComponent,
    UserRequestComponent,
    LoadTableComponent,
    StartOpbaseComponent,
    ComparisonInfoComponent,
    ViewDatatableComponent,
    ViewTablonComponent,
    EditOpbaseComponent,
    HomeSearchComponent,
    FunctionalGovComponent,
    SearchOpbaseComponent,
    ControlPanelComponent,
    TableInfoComponent,
    ViewUuaasComponent,
    UuaaDetailComponent,
    UuaaDetailComponent,
    LoadInfoComponent,
    TablonInfoComponent,
    SearchTablonComponent,
    LoadOpbaseComponent,
    LoadSettingsComponent,
    ProyectRequestComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    NgxDatatableModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    MdePopoverModule,
    NgxSpinnerModule
  ],
  exports: [RouterModule],
  providers: [DatePipe],
  bootstrap: [AppComponent],
  entryComponents: [
    CommentComponentComponent,
    CommentHistorialComponent,
    SearchTableComponent,
    RequestTableComponent,
    StatisticsDialogComponent,
    ErrorsComponent,
    RequestUsecaseComponent,
    RequestTablonComponent,
    PasswordComponent,
    NamingInfoComponent,
    CommentsDetailsComponent,
    RepoDatatableComponent,
    UserRequestComponent,
    ProyectRequestComponent,
    LoadTableComponent,
    ComparisonInfoComponent,
    TableInfoComponent,
    UuaaDetailComponent,
    LoadInfoComponent,
    TablonInfoComponent,
    SearchTablonComponent,
    LoadSettingsComponent,
    LoadOpbaseComponent
  ]
})
export class AppModule { }
