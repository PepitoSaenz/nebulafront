import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchOpbaseComponent } from './search-opbase.component';

describe('SearchOpbaseComponent', () => {
  let component: SearchOpbaseComponent;
  let fixture: ComponentFixture<SearchOpbaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchOpbaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchOpbaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
