import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Router } from "@angular/router";
import { RestService } from "../rest.service";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material";

@Component({
  selector: "app-search-opbase",
  templateUrl: "./search-opbase.component.html",
  styleUrls: ["./search-opbase.component.scss"],
})
export class SearchOpbaseComponent implements OnInit {

  projectOptions: any = [];
  selectedProject: any = {};
  uuaaOptions: any = [];

  activeHelp: string = "";

  nameQuery = "";
  logicNameQuery = "";
  descQuery = "";
  uuaaQuery = "";

  selectedAmbito = "";
  selectedUUAA = "";
  uuaaFull = false;

  basesQuery = [];

  loadingIndicator: boolean = false;
  expandIcon: string = "expand_more";

  @ViewChild("myTable") table: any;

  formControlAmbito = new FormControl('');
  formControlUUAA = new FormControl('');
  formControlProyecto = new FormControl('');

  states = [
    { value: "N", viewValue: "En propuesta" },
    { value: "G", viewValue: "Gobierno" },
    { value: "RN", viewValue: "VoBo Negocio" },
    { value: "Q", viewValue: "Calidad" },
    { value: "I", viewValue: "Listo para ingestar" },
    { value: "A", viewValue: "Arquitectura" },
    { value: "P", viewValue: "Producción" },
    { value: "R", viewValue: "Revisión de Tablones" },
    { value: "D", viewValue: "Descartados" },
  ];

  constructor(
    public rest: RestService,
    private router: Router,
    public snackBar: MatSnackBar
  ) {
    this.formControlAmbito.disable();
    this.formControlUUAA.disable();
    this.getAllProjects();
  }

  ngOnInit() { }

  getAllProjects() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getUuaas() {
    this.rest.getRequest('projects/uuaa/dataTable/' + this.selectedProject + '/').subscribe((data: any) => {
      this.uuaaOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  getAllOPBases() {
    this.rest.getRequest("operationalBase/logic=%20&desc=%20&name=%20&uuaa=%20/")
      .subscribe(
        (data) => {
          this.loadingIndicator = false;
          this.basesQuery = data;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
  }

  selectedProjectOption(value) {
    this.selectedProject = value;
    this.formControlAmbito.enable();
    this.formControlUUAA.enable();
    this.getUuaas();
  }

  selectAmbito(value) {
    this.selectedAmbito = value;
    this.uuaaQuery = this.selectedAmbito + this.selectedUUAA;
    this.uuaaFull =
      this.selectedAmbito.length > 0 && this.selectedUUAA.length > 0;
  }

  selectUuaa(value) {
    this.selectedUUAA = value;
    this.uuaaQuery = this.selectedAmbito + this.selectedUUAA;
    this.uuaaFull = this.selectedAmbito.length > 0 && this.selectedUUAA.length > 0;
  }

  getSearchBases() {
    this.loadingIndicator = true;
    var nameQueryTmp = this.nameQuery.length == 0 ? "%20" : this.nameQuery;
    var logicNameQueryTmp = this.logicNameQuery.length == 0 ? "%20" : this.logicNameQuery;
    var descQueryTmp = this.descQuery.length == 0 ? "%20" : this.descQuery;
    var uuaaQueryTmp = !this.uuaaFull ? "%20" : this.uuaaQuery;
    if (!this.uuaaFull) this.cleanUUAA();

    this.rest.getRequest("operationalBase/logic=" + logicNameQueryTmp + "&desc=" + descQueryTmp + "&name=" + nameQueryTmp + "&uuaa=" + uuaaQueryTmp + "/")
      .subscribe(
        (data) => {
          this.loadingIndicator = false;
          this.basesQuery = data;
          this.openSnackBar("Búsqueda finalizada", "Ok");
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
  }

  //Limpia los parametros de búsqueda
  clean() {
    this.loadingIndicator = true;
    this.nameQuery = "";
    this.logicNameQuery = "";
    this.descQuery = "";
    this.formControlProyecto.setValue("");
    this.cleanUUAA();
    this.getAllOPBases();
  }

  cleanUUAA() {
    this.selectedAmbito = "";
    this.selectedUUAA = "";
    this.formControlAmbito.setValue("");
    this.formControlUUAA.setValue("");
    this.uuaaFull = false;
    this.formControlAmbito.disable();
    this.formControlUUAA.disable();
  }

  viewOPBase(row) {
    this.router.navigate(['opbases/edit'], { queryParams: { idOpBase: row._id } });
  }

  toggleExpandRow(row, expanded) {
    this.table.rowDetail.toggleExpandRow(row);
    this.expandIcon = expanded ? "arrow_drop_down" : "arrow_drop_up";
  }

  onDetailToggle(event) { }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 1000;
    if (action == "Ok") {
      config.panelClass = ["sucessMessage"];
    } else {
      config.panelClass = ["errorMessage"];
    }
    this.snackBar.open(message, action, config);
  }
}
