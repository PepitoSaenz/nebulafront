import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';


@Component({
  selector: 'app-start-table',
  templateUrl: './start-table.component.html',
  styleUrls: ['./start-table.component.scss']
})

export class StartTableComponent implements OnInit {

  formControlDeployType = new FormControl('');
  formControlAlias = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlDesc = new FormControl('', [Validators.required, Validators.minLength(4)]);
  formControlSep = new FormControl('', [Validators.required, Validators.maxLength(3)]);
  formControlNumberField = new FormControl('', [Validators.required, Validators.minLength(1), Validators.min(1)]);

  @ViewChild('tacticalObject') selectorTacticalObject: any;

  showDivError = false;
  backlogs = [];
  selectedBacklogValue: any = null;
  tablesBacklog = [];
  activeHelp: string;

  fieldNumberField: number = 0;
  separator: string = "";
  alias: string = "";
  descAlias: string = "";

  userRol: string;

  raw_name = "";
  master_name = "";

  idTable: string = "";

  deployType_values = [
    "Global-Implementation",
    "Local",
    ""
  ];

  freeAlias = "";
  descName = "";
  fullName = "";
  deploymentType = "";
  masterCheck: boolean = false;

  constructor(public rest: RestService, private router: Router, public snackBar: MatSnackBar) {
    this.userRol = sessionStorage.getItem("rol");
    if (this.userRol != "I" && this.userRol != "G") 
      this.rest.sendHome(sessionStorage.getItem("rol"));
    this.getBacklogs();
    this.formControlDeployType.disable();
    this.formControlAlias.disable();
    this.formControlDesc.disable();
    this.formControlSep.disable();
    this.formControlNumberField.disable();
  }

  ngOnInit() {
  }

  createTable() {
    this.postDataDictionary();
  }

  postDataDictionary() {
    let tmpAlias = (this.deploymentType == "Global-Implementation" ? this.freeAlias : this.alias);

    var table = {
      separator: this.separator, numFields: this.fieldNumberField, user: sessionStorage.getItem("userId"),
      alias: tmpAlias, raw_name: this.raw_name, master_name: (this.masterCheck ? this.master_name : ""), idBacklog: this.selectedBacklogValue._id, deploymentType: this.deploymentType,
      originSystem: this.backlogInfo.originSystem, periodicity: this.backlogInfo.periodicity, history: this.backlogInfo.history, observationField: this.backlogInfo.observationField,
      typeFile: this.backlogInfo.typeFile, tacticalObject: this.backlogInfo.tacticalObject, perimeter: this.backlogInfo.perimeter
    };
    this.rest.postRequest('dataDictionaries/', JSON.stringify(table)).subscribe(
      (data: any) => {
        this.idTable = data.newTable;
        sessionStorage.setItem("idTable", this.idTable);
        this.router.navigate(['tables/edit'], { queryParams: { idTable: this.idTable } });
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  checkFillStatus() {
    this.raw_name = this.inputValidatorNamingTsu(this.raw_name);
    this.master_name = this.inputValidatorNamingTsu(this.master_name);
    if (this.validateFullName() && this.raw_name.length > 1 && this.master_name.length > 1 && this.fieldNumberField > 0) 
      return false;
    return true;
  }

  validateFullName(): boolean {
    var aliasTest = this.alias;
    let descTest = this.descAlias;

    if (this.deploymentType == "Global-Implementation") {
      aliasTest = this.freeAlias;
      descTest = this.fullName;
    }
    if(aliasTest.length > 1 && descTest.length > 1)
      if (this.raw_name.length > 32) {
        this.showDivError = true;
        return false;
      } else {
        this.showDivError = false;
        return true;
      }
    else
      return false;
  }

  inputForNames() {
    if (this.deploymentType == "Global-Implementation") {
      this.raw_name = ("t_" + this.backlogInfo.uuaaRaw + "_" + this.fullName).toLowerCase();
      this.master_name = ("t_" + this.backlogInfo.uuaaMaster + "_" + this.fullName).toLowerCase();
    } else {
      this.raw_name = ("t_" + this.backlogInfo.uuaaRaw
        + "_" + this.alias + "_" + this.descAlias).toLowerCase();
      this.master_name = ("t_" + this.backlogInfo.uuaaMaster
        + "_" + this.alias + "_" + this.descAlias).toLowerCase();
    }
    this.raw_name = this.inputValidatorNamingTsu(this.raw_name);
    this.validateFullName();
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  public inputValidatorNaming(event: any) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "");
    }
    this.inputForNames();
  }

  public inputValidatorNamingTsu(streing: any) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(streing)) {
      streing = streing.replace(/[^a-z0-9_]/g, "");
    }
    return streing;
  }

  inputValidatorNumberFields(event: any) {
    if (event.target.value.length > 4) {
      event.target.value = event.target.value.slice(0, 4);
    }
    if (event.target.value < 0)
      event.target.value = 0; 
  }

  getBacklogs() {
    this.backlogs = [];
    this.rest.getRequest('dataDictionaries/governances/' + sessionStorage.getItem("userId") + '/').subscribe(
      (data: any) => {
        this.backlogs = data;
        if (data.length == 0) {
          this.openSnackBar("No hay tablas disponibles en el backlog.", "Ok");
        }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  selectedBacklog(value) {
    this.selectedBacklogValue = value;
    this.backlogInfo = value;
    this.formControlDeployType.enable();
    this.formControlAlias.enable();
    this.formControlDesc.enable();
    this.formControlSep.enable();
    this.formControlNumberField.enable();
    //this.selectorTacticalObject.value=this.backlogInfo.tacticalObject;
  }

  selectedTacticalObject(event) {
    this.backlogInfo.tacticalObject = event;
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  //Body new tablon
  backlogInfo = {
    perimeter: "",
    baseName: "",
    expectedFinishDate: "",
    history: "",
    observationField: "",
    originSystem: "",
    periodicity: "",
    productOwner: "",
    responsable: "",
    tableRequest: "",
    tacticalObject: "",
    typeFile: "",
    uuaaMaster: "",
    uuaaRaw: "",
  };
}
