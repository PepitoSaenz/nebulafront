import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-view-table',
  templateUrl: './view-table.component.html',
  styleUrls: ['./view-table.component.scss']
})
export class ViewTableComponent implements OnInit {

  //Functional
  activeTab: string = "";
  rowsLimit: any = 5;
  rowsArray: any = [];
  suffixes: any = {};
  loadingIndicator: boolean = false;
  observationFlag: boolean = false;
  showTabRaw: boolean = false;
  showTabMaster: boolean = false;
  showHostTableFields: boolean = false;
  //Search
  searchNamingValue: string = "";
  searchLogicNamingValue: string = "";
  searchDescValue: string = "";
  searchLegacyNamingValue: string = "";
  searchLegacyDescValue: string = "";
  searchSuffixValue: string = "";

  //Data
  idTable: string = "0";
  activeObject: any = [];
  tableData: any;

  //For show
  aliasLongTabla: string = "";
  editableFlag: boolean = false;

  @ViewChild('table') table: any;
  @ViewChild('selectorExcel') selectorExcel: any;


  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router, public snackBar: MatSnackBar) {
    this.route.queryParams.subscribe(params => {
      this.rowsArray = this.columnsReview;
      this.activeTab = "FieldReview";
      this.idTable = params["idTable"];

      this.cleanNullsRequest();
      this.getInfoTable("FieldReview");
      this.getFields("FieldReview");
      this.whatIsGenerated();
      this.getSuffixes();
    });
  }

  ngOnInit() {
  }

  //Comprueba que la propiedad este visible (Para las columnas)
  visibleColumn(prop: string) {
    let searchArray = (this.activeTab === "FieldReview" ? this.columnsReview
      : (this.activeTab === "fieldsRaw" ? this.columnsRaw : this.columnsMaster));
    for (let cell of searchArray)
      for (let val of cell)
        if (val.prop == prop)
          return val.visible;
    return false;
  }

  //Cambia la fase actual dependiendo de la pestaña en la que se encuentre
  tabChanged(event: any) {
    this.cleanQuery();
    setTimeout(() => {
      this.activeTab = event.index === 0 ? "FieldReview" :
        event.index === 1 ? "fieldsRaw" : "fieldsMaster";
    }, 500);
    setTimeout(() => {
      this.updateTabData(
        (event.index === 0 ? "FieldReview" :
          (event.index === 1 ? "fieldsRaw" : "fieldsMaster"))
      );
    }, 300);
  }

  //Actualiza los datos de la fase en la pestaña que se este
  updateTabData(tab) {
    let fase = "";
    if (tab === "FieldReview") {
      fase = "FieldReview";
      this.rowsArray = this.columnsReview;
    } else if (tab === "fieldsRaw") {
      fase = "raw";
      this.rowsArray = this.columnsRaw;
    } else {
      fase = "master";
      this.rowsArray = this.columnsMaster;
    }
    this.getInfoTable(fase);
    this.getFields(tab);
  }

  //Peticion al back para la lista de sufijos de namings
  getSuffixes() {
    this.suffixes = [];
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixes = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Calcula el nombre del alias cuando la fase es propuesta (el objeto no tiene el nombre fisico solito)
  calcAliasLong() {
    var temp = this.activeObject.raw_name.split("_");
    temp.splice(0, 2);
    this.aliasLongTabla = temp.join("_");
  }


  cleanNullsRequest() {
    this.rest.getRequest("dataDictionaries/clean/" + this.idTable + "/")
      .subscribe((data: any) => {
        console.log("Nulls " + data)
      });
  }

  //Peticion al back para traer la información del objeto de la tabla
  getInfoTable(fase) {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/head/' + fase + '/').subscribe(
      (data: any) => {
        this.activeObject = data;
        if (fase !== 'raw' && fase !== 'master')
          this.calcAliasLong();
        if (data.originSystem.match(/HOST/g))
          this.showHostTableFields = true;
        if (this.activeObject.stateTable === "G" || this.activeObject.stateTable === "N")
          this.editableFlag = true;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back para traer todos los namings (campos) de la tabla en una fase especifica
  getFields(fase) {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + fase + '/')
      .subscribe(
        (data: any) => {
          this.tableData = data;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
  }

  //Verifica que fases estan generadas en la tabla para mostrar las pestañas
  whatIsGenerated() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + 'fieldsRaw' + '/')
      .subscribe(
        (data: any) => {
          this.showTabRaw = data.length > 0;
        }, (error) => { }
      );
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + 'fieldsMaster' + '/')
      .subscribe(
        (data: any) => {
          this.showTabMaster = data.length > 0;
        }, (error) => { }
      );
  }

  //Toggle para la vista de las columnas
  toggleAll(trueValue: boolean) {
    for (let cell of this.rowsArray)
      for (let val of cell)
        val.visible = trueValue;
  }

  //Busca namings en la tabla con el componente de busqueda
  getFieldsQuery() {
    this.loadingIndicator = true;
    if (this.searchNamingValue.length == 0 && this.searchDescValue.length == 0
      && this.searchLogicNamingValue.length == 0 && this.searchLegacyNamingValue.length == 0
      && this.searchLegacyDescValue.length == 0 && this.searchSuffixValue.length < 0) {
      this.loadingIndicator = false;
      this.cleanQuery();
      this.openSnackBar("Por favor, ingrese los datos de búsqueda.", "Error");
    } else {
      this.rest.getRequest('dataDictionaries/' + this.idTable + '/fases/FieldReview/naming=' + this.transformForRequest(this.searchNamingValue)
        + '&logic=' + this.transformForRequest(this.searchLogicNamingValue) + '&desc=' + this.transformForRequest(this.searchDescValue)
        + '&state=%20' + '&legacy=' + this.transformForRequest(this.searchLegacyNamingValue) + '&descLegacy=' + this.transformForRequest(this.searchLegacyDescValue)
        + '&suffix=' + this.transformForRequest(this.searchSuffixValue) + '/')
        .subscribe(
          (data: any) => {
            this.tableData = data;
            setTimeout(() => { this.loadingIndicator = false; }, 1000);
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
            setTimeout(() => { this.loadingIndicator = false; }, 1000);
          });
    }
  }

  //Verifica si el campo esta vacio o no para devolver "%20" como espacio para el parametro en una
  //peticion HTTP. Usado solo por 'getFieldsQuery()'
  transformForRequest(value: string) {
    return value.length <= 0 ? "%20" : value;
  }

  //Limpia los campos y la busqueda de namings
  cleanQuery() {
    this.searchNamingValue = "";
    this.searchDescValue = "";
    this.searchLogicNamingValue = "";
    this.searchLegacyNamingValue = "";
    this.searchLegacyDescValue = "";
    this.searchSuffixValue = "";
    this.getFields(this.activeTab);
  }

  openEdit() {
    this.router.navigate(["tables/edit"], { queryParams: { idTable: this.idTable } });
  }

  genExcels(option: number) {
    switch (option) {
      case 1: this.generateLegacyExcels();
        break;

      case 2: this.generateExcels();
        break;
    }
    this.selectorExcel.value = "";
  }

  generateObjectExcel() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/excels/Object/').subscribe(
      (data: any) => {
        this.exportAsExcelFileObject(data, this.activeObject.project_name + "-" + this.activeObject.alias + "-Object");
        this.openSnackBar("Generación exitosa de los Object. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  generateLegacyExcels() {
    this.generateObjectExcel();
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/excels/Legacy/').subscribe(
      (data: any) => {
        this.exportAsExcelFileObject(data, this.activeObject.project_name + "-" + this.activeObject.alias + "-FieldsLegacy");
        this.openSnackBar("Generación exitosa de los Fields Legacy. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  generateExcels() {
    this.generateObjectExcel();
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/excels/Fields/').subscribe(
      (data: any) => {
        this.exportAsExcelFileFields(data, this.activeObject.project_name + "-" + this.activeObject.alias + "-Fields");
        this.openSnackBar("Generación exitosa de los Fields. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-O': worksheet }, SheetNames: ['DDNG-O'] };
    XLSX.writeFile(workbook, ViewTableComponent.toExportFileName(excelFileName));
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-F': worksheet }, SheetNames: ['DDNG-F'] };
    XLSX.writeFile(workbook, ViewTableComponent.toExportFileName(excelFileName));
  }

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/;
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
  }

  //Pinta la celda del naming dependiendo de su tipo, global, propuesto, etc.
  getCellClass({ row, column, value }) {
    if (row.modification.length > 0) {
      if (row.modification[0].state == 'GL') {
        return {
          'is-global': true
        };
      } else if (row.modification[0].state == 'GC') {
        return {
          'is-common-global': true
        };
      } else if (row.modification[0].state == 'PS') {
        return {
          'is-proposal': true
        };
      } else if (row.modification[0].state == 'PC') {
        return {
          'is-common-proposal': true
        };
      }
    }
  }

  //Pinta la celda de campo legacy si esta repetido
  getCellClassLegacy({ row, column, value }) {
    if (row.legacy.repeat == 'Y') {
      return { 'legacy-repeated': true }
    }
  }

  //Devuelve el color de la fila si este es generado
  getRowClass(row: any) {
    return {
      'generatedRow': row.origin == ""
    };
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 1000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  private onPaginated(event) {
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  columnsReview = [
    [
      { prop: "startPosition", name: "Inicio", visible: true },
      { prop: "endPosition", name: "Final", visible: true },
      { prop: "length", name: "Longitud", visible: true },
      { prop: "originType", name: "Tipo Origen", visible: true }],
    [
      { prop: "outRec", name: "Outrec", visible: true }, //HOST
      { prop: "outVarchar", name: "Varchar Salida", visible: true },
      { prop: "outLength", name: "Salida", visible: true }],
    [
      { prop: "naming.naming", name: "Naming", visible: true },
      { prop: "logic", name: "Nombre Lógico", visible: true },
      { prop: "description", name: "Descripción", visible: true }],
    [
      { prop: "catalogue", name: "Catálogo", visible: true },
      { prop: "legacy.legacy", name: "Legacy", visible: true },
      { prop: "legacy.legacyDescription", name: "Descripción Legacy", visible: true }],
    [
      { prop: "example", name: "Ejemplo", visible: true },
      { prop: "key", name: "Key", visible: true },
      { prop: "mandatory", name: "Mandatory", visible: true }],
    [
      { prop: "symbol", name: "Signo", visible: true },
      { prop: "integers", name: "Enteros", visible: true },
      { prop: "decimals", name: "Decimales", visible: true }],
    [
      { prop: "destinationType", name: "Tipo Destino", visible: true },
      { prop: "governanceFormat", name: "Formato Gobierno", visible: true },
      { prop: "format", name: "Formato", visible: true },
      { prop: "default", name: "Valor Default", visible: true }],
    [
      //{prop: "actions", name: "Acciones", visible: true},
      { prop: "todo", name: "TODO", visible: true }]
  ];

  columnsRaw = [
    [
      { prop: "naming.code", name: "Codigo", visible: true },
      { prop: "naming.hierarchy", name: "Jerarquia", visible: true },
      { prop: "naming.governanceState", name: "Estado Gobierno", visible: true }],
    [
      { prop: "naming.naming", name: "Naming", visible: true },
      { prop: "logic", name: "Nombre Lógico", visible: true },
      { prop: "description", name: "Descripción", visible: true }],
    [
      { prop: "catalogue", name: "Catálogo", visible: true },
      { prop: "destinationType", name: "Tipo de dato", visible: true },
      { prop: "logicalFormat", name: "Formato Lógico", visible: true }],
    [
      { prop: "legacy.legacy", name: "Campo Origen", visible: true },
      { prop: "legacy.legacyDescription", name: "Descripción Origen", visible: true },
      { prop: "key", name: "Key", visible: true },
      { prop: "mandatory", name: "Mandatory", visible: true }],
    [
      { prop: "default", name: "Valor Default", visible: true },
      { prop: "tokenization", name: "Tokenización", visible: true },
      { prop: "tds", name: "TDS", visible: true }],
    //{prop: "check", name: "VoBo", visible: true}],
    [
      //{prop: "actions", name: "Acciones", visible: true},
      { prop: "todo", name: "TODO", visible: true }]
  ];

  columnsMaster = [
    [
      { prop: "naming.code", name: "Codigo", visible: true },
      { prop: "naming.hierarchy", name: "Jerarquia", visible: true },
      { prop: "naming.governanceState", name: "Estado Gobierno", visible: true }],
    [
      { prop: "naming.naming", name: "Naming", visible: true },
      { prop: "logic", name: "Nombre Lógico", visible: true },
      { prop: "description", name: "Descripción", visible: true }],
    [
      { prop: "catalogue", name: "Catálogo", visible: true },
      { prop: "destinationType", name: "Tipo de dato", visible: true },
      { prop: "logicalFormat", name: "Formato Lógico", visible: true }],
    [
      { prop: "legacy.legacy", name: "Campo Origen", visible: true },
      { prop: "key", name: "Key", visible: true },
      { prop: "mandatory", name: "Mandatory", visible: true }],
    [
      { prop: "format", name: "Formato", visible: true },
      { prop: "default", name: "Valor Default", visible: true },
      { prop: "tokenization", name: "Tokenización", visible: true },
      { prop: "tds", name: "TDS", visible: true }],
    [
      //{prop: "actions", name: "Acciones", visible: true},
      { prop: "todo", name: "TODO", visible: true }]
  ];
}