  import { Component, OnInit } from '@angular/core';
  import { Router } from '@angular/router';

@Component({
  selector: 'app-notfoundpage',
  templateUrl: './notfoundpage.component.html',
  styleUrls: []
})

export class NotfoundpageComponent implements OnInit {

  title="DATA HUB - PROYECTO GOBIERNO";
  message="404 - Page Not Found";

  constructor(private router: Router) {}

  ngOnInit() {}

  BackFunc(){
     this.router.navigate(['/login']);
  }
}
