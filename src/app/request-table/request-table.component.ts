import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Observable } from "rxjs";
import { debounceTime, map, startWith } from "rxjs/operators";
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-request-table',
  templateUrl: './request-table.component.html',
  styleUrls: ['./request-table.component.scss']
})
export class RequestTableComponent implements OnInit {

  backlog = null;
  oldBacklog = null;
  oldBacklogFlag = false;
  request: any;
  closedFlag = false;
  answerFlag = false;
  modifyFlag = false;
  enFlag = false;

  titleType = "";
  answerResult: string;
  originSystemOptions = [];
  periodicity = [];
  filetypes = [];
  uuaaOptions = [];
  messagesError = [];
  tmpHist = 0;
  showDivError = false;
  rol: string;
  viewAuditFlag = false;
  activeHelp = "";

  calcAmbito = "";
  currUUAA = "";
  ambitoOptions = []

  uuaaRawOptions = [];
  originSystems = [
    "H", "L", "S", "P", "O", "U", 
    "D", "A", "Q", "F", "R", "C", 
    "K", "M", "T", "N", "X", "E", 
    "M", "Y", "G", "B", "V", "I",
    "1", "2", "3", "4", "7", "8"
  ];
  appliValues = [
    "BA", "BG", "BT", "CA", "CN", "RD",
    "G2", "CX", "EF", "EY", "FP", "GF",
    "GI", "GP", "GT", "HA", "HR", "HX",
    "IC", "IM", "IO", "JG", "JI", "KN",
    "LS", "MC", "OG", "OZ", "PE", "PM",
    "QH", "RC", "RG", "RI", "RV", "SM",
    "SL", "SP", "S4", "TC", "TP", "UG",
    "V7", "W9", "XC", "XG", "QG", "MK",
    "KP", "FI", "CT", "UB", "PF", "PI",
    "DI", "TN", "US", "SR", "QR", "SF",
    "EX", "BR", "WP", "DG", "RR", "MF"
  ];

  @ViewChild('originSystem') selectorOriginSystem: any;
  @ViewChild('sPeriodicity') selectorPeriodicity: any;
  @ViewChild('fileType') selectorFileType: any;
  //@ViewChild('uuaaMasters') selectorUuaaMaster: any;
  @ViewChild('tacticalObject') selectorTacticalObject: any;

  filteredUUAAs: Observable<string[]>;

  formControlAmbito = new FormControl('', [Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.minLength(3)]);
  formControlUUAAMaster = new FormControl();

  requiredFormControlName = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlDesc = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlHist = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlRaw = new FormControl('', [Validators.minLength(4)]);
  requiredFormControlOrigin = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlMark = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlPerm = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlFile = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlComment = new FormControl('', [Validators.required, Validators.minLength(2)]);

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public rest: RestService,
    public snackBar: MatSnackBar, private dialogRef: MatDialogRef<RequestTableComponent>) {
    this.backlog = datas.backlog;
    this.tmpHist = this.backlog.history;
    this.request = datas;
    this.rol = sessionStorage.getItem("rol");
    this.getOrigSysOptions();
    this.getPeriodicityOptions();
    this.getTypeFilesOptions();
    this.getUuaas();
    this.loadAmbito();
    this.getAmbitos();
    this.loadUUAA();
    this.getRawUUAAOptions();

    this.initDialogState(this.request);
    if (this.backlog.stateTable === 'N') this.enFlag = true;
    if (this.request.hasOwnProperty('oldBacklog')) {
      this.oldBacklogFlag = true;
      this.oldBacklog = datas.oldBacklog;
    }
    if (this.backlog.created_time != "" &&
      this.backlog.modifications.user_name != "" &&
      this.backlog.modifications.date != "") {
      this.viewAuditFlag = true;
    }
    //TO DELETE
    if (!this.request.hasOwnProperty("comment")) this.request["comment"] = "";
    if (!this.request.hasOwnProperty("spo")) this.request["spo"] = "pepe@nada.com";
  }

  ngOnInit() {
    this.filteredUUAAs = this.formControlUUAAMaster.valueChanges.pipe(
      debounceTime(200),
      startWith(""),
      map((value) => this.filterUUAAs(value))
    );
  }

  //Valida que la primera letra de la UUAA de Raw empiece por C o K 
  firstLetterVal(): ValidatorFn {
    var nameRe: RegExp = /^[C|K|W|L][A-Z0-9]+$/;
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = nameRe.test(control.value);
      return forbidden ? null : { 'firstLetter': { value: control.value } };
    };
  }

  //Valida que la UUAA de Raw posea el código de un applicativo correcto
  applicationCodeVal(): ValidatorFn {
    var found: boolean = false;
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value[1] && control.value[2]) {
        let code = control.value[1] + control.value[2];
        found = this.appliValues.includes(code);
      }
      return found ? null : { 'appCode': { value: control.value } };
    };
  }

  //Valida que la UUAA de Raw posea el código de un applicativo correcto
  lastLetterVal(): ValidatorFn {
    var found: boolean = false;
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value[3])
        found = this.originSystems.includes(control.value[3]);
      return found ? null : { 'lastLett': { value: control.value } };
    };
  }

  //Initializa el campo de ambito local dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAmbito() {
    this.calcAmbito = this.backlog.uuaaMaster[0];
    if (this.calcAmbito === undefined)
      this.calcAmbito = "";
  }

  getAmbitos() {
    this.rest.getRequest('uuaa/ambitos/').subscribe((data: any) => {
      this.ambitoOptions = data;
    });
  }

  //Funcion que filtra las uuaas en el select a medida que se ingresen letras
  private filterUUAAs(value: any): string[] {
    if (value !== null) {
      if (value.hasOwnProperty("short_name")) value = value["short_name"];
      const filterValue = value.toLowerCase();
      let vau = this.uuaaOptions.filter((option) =>
        option["short_name"].toLowerCase().includes(filterValue)
      );
      return vau;
    }
  }

  //Valida que el naming de origen este dentro de las opciones permitidas.
  //Para evitar que ingresen namings no permitidos
  checkSelectedUUAA() {
    if (this.formControlUUAAMaster.value != null)
      if (this.formControlUUAAMaster.value.hasOwnProperty("short_name")) {
        if (this.currUUAA !== this.formControlUUAAMaster.value["short_name"])
          this.resetEditUUAA();
      } else this.resetEditUUAA();
  }

  //Resetea los valores del namign de origen cuando se agrega.
  resetEditUUAA() {
    this.formControlUUAAMaster.setValue({ short_name: this.currUUAA });
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayAmbito(ambito: any) {
    console.log(ambito)
    if (ambito) return ambito;
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayUUAA(uuaa: any) {
    if (uuaa) return uuaa["short_name"];
  }

  //Initializa el campo de la uuaa sin la letra del pais(ambito) dependiendo de la uuaa del tablon para su posible modificación y validación
  loadUUAA() {
    //if (this.backlog.uuaaMaster !== "") {
      //this.calcUUAA = { short_name: this.backlog.uuaaMaster.substring(1) } ;
      //this.formControlUUAA.setValue(this.calcUUAA );
    //}
    this.currUUAA = this.backlog.uuaaMaster.substring(1);
    this.formControlUUAAMaster.setValue({ short_name: this.currUUAA });
  }

  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa(event) {
    this.currUUAA = event.option.value.short_name;
    this.formControlUUAAMaster.setValue(event.option.value);
    this.inputUUAA();
  }

  //Input
  inputUUAA() {
    this.backlog.uuaaMaster = this.calcAmbito + this.currUUAA;
  }

  updateAmbito(new_ambito: string) {
    this.calcAmbito = new_ambito;
    this.updateUUAAMaster();
  }

  updateUUAAMaster() {
    this.backlog.uuaaMaster = this.calcAmbito + this.currUUAA;
  }

  disableAllFields() {
    this.requiredFormControlName.disable();
    this.requiredFormControlDesc.disable();
    this.requiredFormControlHist.disable();
    this.requiredFormControlRaw.disable();
    this.formControlAmbito.disable();
    this.formControlUUAA.disable();
    this.formControlUUAAMaster.disable();
    this.requiredFormControlOrigin.disable();
    this.requiredFormControlMark.disable();
    this.requiredFormControlPer.disable();
    this.requiredFormControlPerm.disable();
    this.requiredFormControlFile.disable();
  }

  initDialogState(datas: any) {
    if (datas.hasOwnProperty("requestStatus")) {
      if (datas.requestStatus === "A" || datas.requestStatus === "D") {
        this.answerFlag = true;
        this.disableAllFields();
        this.requiredFormControlComment.disable();
        if (datas.requestStatus === "A") {
          this.answerResult = "Aceptada";
          this.titleType = "Aceptada - Tabla: " + this.backlog.baseName;
        } else {
          this.answerResult = "Rechazada";
          this.titleType = "Rechazada - Tabla: " + this.backlog.baseName;
        }
      } else {
        if (this.rol === "PO") {
          this.closedFlag = false;
          this.modifyFlag = true;
        } else
          this.closedFlag = true;
        this.titleType = (datas.action === "Add" ? "Agregar Tabla: " :
          (datas.action === "Del" ? "Eliminar Tabla: " : "Modificar Tabla: ")) + this.backlog.baseName;
      }
    } else {
      this.titleType = (datas.action === "backlog" ? "Modificar: " + this.backlog.baseName : "Nueva Tabla");
    }
  }

  refreshOptions() {
    this.selectorOriginSystem.value = this.backlog.originSystem;
    this.selectorPeriodicity.value = this.backlog.periodicity;
    this.selectorFileType.value = this.backlog.typeFile;
    this.selectorTacticalObject.value = this.backlog.tacticalObject;
  }

  ValidateBacklog() {
    this.updateBacklog("validate").then((res: any) => {
      if (res) {
        this.cleanErrorDiv();
        this.sendClosedResponse(this.request);
      }
    });
  }

  modifySol() {
    this.updateBacklog("approve").then((res: any) => {
      if (res) {
        this.cleanErrorDiv();
        this.sendClosedResponse(this.request);
      }
    });
  }

  approveSol() {
    this.updateBacklog("approve").then((res: any) => {
      if (res) {
        if (this.request.comment.length === 0) this.request.comment = "Ok";
        let response = {
          solResult: "approve",
          dialogTempData: this.request
        }
        this.sendClosedResponse(response);
      }
    });
  }

  rejectSol() {
    if (this.requiredFormControlComment.hasError('required') && this.rol !== "PO") {
      this.openSnackBar("El comentario es obligatorio cuando se rechaza una solicitud", "Error");
    } else {
      let response = {
        solResult: "discard",
        dialogTempData: this.request
      }
      this.sendClosedResponse(response);
    }
  }

  checkBaseNameDuplicates() {
    var promise = new Promise((resolve, reject) => {
      if (this.backlog.baseName.includes('%'))
        resolve(0);
      else
        this.rest.getRequest("projects/backlogs/check/" + this.backlog.baseName + "/")
          .subscribe(
            (data: any) => {
              resolve(data.length);
            }, (error) => {
              this.openSnackBar("Error procesando petición al servidor.", "Error");
              reject(false);
            });
    });
    return promise;
  }

  cleanErrorDiv() {
    this.messagesError = [];
    this.showDivError = false;
  }

  addErrorDiv(message: string) {
    this.messagesError.push(message);
    this.showDivError = true;
  }

  updateBacklog(type: string) {
    var promise = new Promise((resolve) => {
      this.cleanErrorDiv();
      if (this.backlog.baseName.length < 1) {
        this.addErrorDiv("Por favor, diligencie el nombre base.")
        resolve(false);
      } else {
        this.checkBaseNameDuplicates().then((res: any) => {
          if (res > (type == "validate" ? 0 : 1)) {
            this.addErrorDiv("El nombre base ya esta relacionado a una tabla ya existente.");
            resolve(false);
          } else if (this.backlog.originSystem.length < 1 || this.backlog.periodicity.length < 1 ||
            this.backlog.typeFile.length < 1 || this.backlog.tacticalObject.length < 1 ||
            this.backlog.baseName.length < 1 || this.backlog.observationField.length < 1 ||
            this.backlog.perimeter.length < 1 || this.tmpHist < 1) {
            this.addErrorDiv("Por favor, verifique los campos requeridos.");
            resolve(false);
          } else if (this.backlog.uuaaRaw.length > 0) {
            if (!this.requiredFormControlRaw.valid || this.backlog.uuaaRaw.length !== 4) {
              this.addErrorDiv("Si diligencia la UUAA de RAW esta tiene que estar válida.");
              resolve(false);
            }
          } else if (this.calcAmbito !== "") {
            if (this.backlog.uuaaMaster.length !== 4) {
              this.addErrorDiv("El ambito y la UUAA de Master tienen que estar correctas si se quieren diligenciar.");
              resolve(false);
            }
          }
          this.backlog.baseName = this.backlog.baseName.replace(/[\n\r]/g, '');
          this.backlog.baseName = this.backlog.baseName.toUpperCase();
          this.backlog.uuaaRaw = this.backlog.uuaaRaw.toUpperCase();
          this.backlog.history = this.tmpHist
          this.request.backlog = this.backlog;
          resolve(true);
        });
      }
    });
    return promise;
  }

  inputValidatorHist(event: any) {
    if (event.target.value.length > 4) {
      event.target.value = event.target.value.slice(0, 4);
    }
  }

  sendClosedResponse(response: any) {
    this.dialogRef.close(response);
  }

  selectedOriginSystem(event) {
    this.backlog.originSystem = event;
  }

  selectedPeriodicity(event) {
    this.backlog.periodicity = event;
  }

  selectedFileType(event) {
    this.backlog.typeFile = event;
  }

  selectedUuaaMaster(event) {
    this.backlog.uuaaMaster = event;
  }

  selectedTacticalObject(event) {
    this.backlog.tacticalObject = event;
  }

  getRawUUAAOptions() {
    this.rest.getRequest('uuaa/raw/all/').subscribe(
      (data: any) => {
        this.uuaaRawOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getOrigSysOptions() {
    this.rest.getRequest('projects/backlogs/atributes/originSystem/').subscribe(
      (data: any) => {
        this.originSystemOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getPeriodicityOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicity = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getTypeFilesOptions() {
    this.rest.getRequest('projects/backlogs/atributes/typeFile/').subscribe(
      (data: any) => {
        this.filetypes = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getUuaas() {
    this.rest.getRequest("uuaa/master/allfull/").subscribe((data: any) => {
      this.uuaaOptions = data;
    });
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }


}
