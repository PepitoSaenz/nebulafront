import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Md5 } from 'ts-md5/dist/md5';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})

export class AuthComponent implements OnInit {

  //Variables de los valores de ingreso y funcionamiento
  username: string = "";
  password: string = "";
  encryptPassword: string = "";

  //Variables visuales
  hidePass: boolean = false;
  showDivError: boolean = false;
  errorMessage: string = "";

  //FormControls para los inputs
  user = new FormControl('');
  pass = new FormControl('');
  email = new FormControl('', Validators.compose([
    Validators.required,
    Validators.pattern('^[a-zA-Z0-9_.-]+@bbva.com$')
  ]))

  constructor(private router: Router, public rest: RestService) { }

  ngOnInit() { }

  //Metodo para redigirir a la vista de registro de usuario.
  signUpPanel(): void {
    this.router.navigate(['/register']);
  }

  //Redirige a un usuario ingresando como invitado a Nebula, asignandole un rol temporal sin asociacion a la base de datos.
  invitadoLogin(): void {
    sessionStorage.setItem("rol", "L");
    sessionStorage.setItem("userName", "Invitado");
    this.router.navigate(["/tables/search"]);
  }

  //Metodo para iniciar el proceso de log in en Nebula.
  // Toma los valores en los campos del email y password para su verificacion.
  login(): void {
    this.resetValueErrors();
    if ((this.user.value !== "" || this.user.value !== undefined)
      && (this.pass.value !== "" || this.pass.value !== undefined)) {
      this.getCredentials();
    } else {
      this.showValuesErrors("Por favor, ingrese sus credenciales");
    }
  }

  //Envia la contraseña de un usuario codificada para comparar y decidir si esta autorizado para logearse en Nebula,
  //  al ser exitosa, se envia a la vista inicial dependiendo del rol del usuario.
  getCredentials(): void {
    this.encryptPassword = "";
    this.encryptPassword = (Md5.hashStr(this.pass.value) as string);
    var userData = { email: this.user.value, password: this.encryptPassword };
    this.rest.postNoCheckRequest('users/login/', JSON.stringify(userData)).subscribe(
      (data: any) => {
        this.routeUserHome(data);
      }, (error) => {
        this.showValuesErrors("Datos Incorrectos");
      }
    );
  }

  //Metodo para reiniciar los valores en el caso de que hayan sido ya tocados o que se haya mostrado un error anteriormente.
  resetValueErrors(): void {
    this.showDivError = false;
    this.errorMessage = "";
    this.user.setErrors(null);
    this.pass.setErrors(null);
  }

  //Metodo para mostrar errores de los campos cuando algo sale mal.
  //(param) message: El mensaje de error para mostrar.
  showValuesErrors(message: string): void {
    this.showDivError = true;
    this.errorMessage = message;
    if (this.user.value !== "" || this.user.value !== undefined) this.user.setErrors({ 'incorrect': true });
    if (this.pass.value !== "" || this.pass.value !== undefined) this.pass.setErrors({ 'incorrect': true });
  }

  //Metodo para redigirir al usuario a la vista inicial despues de haberse autenticado y guarda la informacion en el sessionStorage local.
  //  En el caso de que por algun motivo no hay rol, muestra un mensaje de error.
  //(param) data: La informacion del usuario devuelta despues de haberse autenticado.
  routeUserHome(userData: any): void {
    sessionStorage.setItem("userId", userData["_id"]);
    sessionStorage.setItem("rol", userData["rol"]);
    sessionStorage.setItem("userName", userData["name"]);
    switch (userData.rol) {
      case "A":
        this.router.navigate(['/admin']);
        break;
      case "G": case "I": case "AR": case "F":
        this.router.navigate(['/home']);
        break;
      case "PO": case "SPO":
        this.router.navigate(['/home/productowner']);
        break;
      case "D":
        this.showValuesErrors("El usuario esta bloqueado, por favor contactar al equipo de Gobierno.");
      default:
        this.showValuesErrors("No se encontro un rol apropiado");
    }
  }
}
