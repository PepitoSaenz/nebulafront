import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Md5 } from 'ts-md5/dist/md5';
import { RestService } from '../../rest.service';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

  user = new FormControl('');
  email= new FormControl('', Validators.compose([
    Validators.required,
    Validators.pattern('^[a-z0-9_.+-]+(@bbva\\.com)$')
		//Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
	]));
  pass = new FormControl('', Validators.compose([ Validators.required, Validators.minLength(8)]));
  pass2 = new FormControl('', Validators.compose([
		Validators.required,
		this.comparePass()
  ]));

  hide: boolean = false;
  hide2: boolean = false;
  
  allControls: any = [this.user, this.email, this.pass, this.pass2];

  constructor(private router: Router, public rest: RestService) { 
  }

  ngOnInit() {
  }

  comparePass(): ValidatorFn {
    return (control): {[key: string]: any} | null => {
      return this.pass.value !== control.value ? {'comparePass': {value: control.value}} : null;
    };
  }

  validateAll(){
    for(let vali of this.allControls){
      vali.markAsTouched();
      vali.updateValueAndValidity();
    }
    return this.user.valid && this.email.valid  && this.pass.valid && this.pass2.valid;
  }

  register(){
    if (this.validateAll()) {
      var encryptPassword =  (Md5.hashStr(this.pass.value) as string);
      var userData = { name: this.user.value, email: this.email.value, password: encryptPassword };
      this.rest.postNoCheckRequest('users/', JSON.stringify(userData)).subscribe(
        (data: any) => {
          this.invitadoLogin();
        },
        (error) => {
          console.log("Algo salio mal");
        }
      );
    }
  }

  invitadoLogin() {
    sessionStorage.setItem("rol", "L");
    sessionStorage.setItem("userName", this.user.value);
    this.router.navigate(["/tables/search"]);
  }

}
