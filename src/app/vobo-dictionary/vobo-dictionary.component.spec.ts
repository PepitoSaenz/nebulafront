import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoboDictionaryComponent } from './vobo-dictionary.component';

describe('VoboDictionaryComponent', () => {
  let component: VoboDictionaryComponent;
  let fixture: ComponentFixture<VoboDictionaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoboDictionaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoboDictionaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
