import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import * as XLSX from 'xlsx';

import { RestService } from '../rest.service';
import { ErrorsComponent } from './../errors/errors.component';
import { StatisticsDialogComponent } from './../statistics-dialog/statistics-dialog.component';
import { NamingInfoComponent } from './../edit-tablon/naming-info/naming-info.component';



@Component({
  selector: 'app-vobo-dictionary',
  templateUrl: './vobo-dictionary.component.html',
  styleUrls: ['./vobo-dictionary.component.scss']
})

export class VoboDictionaryComponent implements OnInit {

  rowsLimit = 5;
  projectOptions: any = [];
  projectId: string;
  useCaseProject: string;
  selectedOptionUseCase: any;;
  lastToggleNamingNumber: any;
  consult: any;
  fieldsRaw: any = [];
  idTable: string;

  showTabMaster = false;
  masterObject: any;
  userId: string;
  userName: string;;

  showUseCasesProject = false;
  showUseCase = false;
  showTableValidation = false;
  showDataTableValidation = false;

  newComment: string = "";
  lastCommentTmp: string = "";

  originsArr = [];
  aliases: any = [];
  availableProjects: any = [];
  availableTables: any = [];
  originsDataTable: any = ["MG", "PG", "ML", "PL"];

  @ViewChild('TableValidation') tableValidations: any;
  @ViewChild('selectUseCase') selectUseCase: any;
  @ViewChild('voboTables') voboTables: any;

  constructor(public rest: RestService,
    public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.userId = sessionStorage.getItem("userId");
    this.userName = sessionStorage.getItem("userName");
    this.getProjectOptions();
    this.getProjects();
  }

  ngOnInit() {
  }

  //Toggle para el rowdetail de la tabla para revision de tabloens
  toggleExpand(group: any) {
    this.voboTables.groupHeader.toggleExpandGroup(group);
  }

  //Toggle para el rowDetails
  toggleExpandRow(row) {
    if (this.lastToggleNamingNumber == row.column) {
      this.lastToggleNamingNumber = -1;
      this.tableValidations.rowDetail.collapseAllRows();
    } else {
      this.tableValidations.rowDetail.collapseAllRows();
      this.lastToggleNamingNumber = row.column;
      this.tableValidations.rowDetail.toggleExpandRow(row);
    }
  }

  //Funcion visual para el toggle del row detail de los namings de origen
  activeDetailsCheck(column) {
    return this.lastToggleNamingNumber == column;
  }

  //Comprueba que si el naming actualmente es directo a su naming de origen. (Es el mismo sin procesamiento)
  checkDirectOriginNaming(row: any) {
    if (row.hasOwnProperty("direct"))
      return row.direct === 1;
    return false;
  }

  //Funcion que abre el popup para los detalles del naming de origen llamando la peticion que trae la info completa
  //y abriendo el popup con un vinculo a la tabla original
  namingDetailsFromOrigin(naming: any) {
    this.getNamingFromOrigin(naming).then(res => {
      this.namingDetails(res, naming.table_id);
    });
  }

  //Peticion que trae la info completa del naming de origen
  getNamingFromOrigin(naming: any) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + naming.table_id + '/head/' + 'FieldReview' + '/').toPromise()
        .then(res => {
          let fase = (naming.table_name.includes(res.uuaaRaw.toLowerCase()) ? "fieldsRaw" : "fieldsMaster");
          this.rest.getRequest('dataDictionaries/' + naming.table_id + '/fields/' + fase + '/').subscribe(
            (data: any) => {
              for (var obj of data)
                if (obj.naming.naming === naming.naming)
                  resolve(obj);
            }, (error) => {
              this.openSnackBar("Error procesando petición al servidor.", "Error");
              resolve(error);
            });
        })
    });
    return promise;
  }

  //Muestra el popup de los detalles del naming de origen antes de agregarlo
  namingDetails(namingRow: any, idTable: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "65%";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      naming: namingRow,
      idTable: idTable
    };
    this.dialog.open(NamingInfoComponent, dialogConfig);
  }

  //Trae todos las tablas para revision de VoBo que esten en el arreglo de proyectos disponibles.
  //Este arreglo debio haberse poblado antes y normalmente trae todos los proyectos en Nebula
  //con excepcion del proyecto 'Unassigned' el cual tiene todo lo descartado
  getProjects() {
    this.rest.getRequest('projects/user=' + this.userId + '/').subscribe(
      (data: any) => {
        this.availableProjects = data;
        this.getAvailableTables(this.availableProjects).then((res: any) => {
          this.availableTables = this.cleanTables(res);
          if (this.availableTables.length === 0) {
            this.openSnackBar("No hay tablas por revisar.", "Ok");
          }
        });
      }, (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); }
    );
  }

  //Limpia el arreglo de tablas para evitar repetidos y en vez suma el caso de uso si tiene mas de uno
  cleanTables(array: any) {
    var result = [];
    for (var outs of array) {
      for (var ins of outs) {
        var tmpArr = result.filter(obj => (obj._id === ins._id));
        if (tmpArr.length === 0) result.push(ins);
        else tmpArr[0].UseCaso.push(ins.UseCaso[0]);
      }
    }
    return result;
  }

  //Peticion de promesas para traer todas las tablas con estado RN para revision de VoBo a las que 
  //tenga acceso el usuario
  getAvailableTables(availableProjects: any) {
    var promise = new Promise((resolve, reject) => {
      let promises = [];
      for (let project of availableProjects)
        for (let useCase of project.useCases) {
          promises.push(this.getUseCaseRNTables(project.name, useCase));
          promises.push(this.getUseCaseRNDataTables(project.name, useCase));
        }
      Promise.all(promises)
        .then((result) => {
          resolve(result);
        })
    });
    return promise;
  }

  //Peticion promesa al back para traer los casos de uso y su nombre de un proyecto unicamente
  getUseCaseRNTables(projectName: string, idUseCase: string) {
    var promise = new Promise((resolve, reject) => {
      var resArray = [];
      this.rest.getRequest('projects/useCases/' + idUseCase + '/').subscribe(
        (data: any) => {
          this.rest.getRequest('projects/useCases/' + idUseCase + '/datadictionaries/').subscribe(
            (res: any) => {
              for (var tab of res) {
                tab.Tipo = "T";
                tab.Projecto = projectName;
                tab.UseCaso = [];
                tab.UseCaso.push(data.name);
                resArray.push(JSON.parse(JSON.stringify(tab)));
              }
              resolve(resArray);
            });
        });
    });
    return promise;
  }

  //Peticion promesa al back para traer las tablas que tengan estado RN para revision de VoBo del caso de uso enviado
  getUseCaseRNDataTables(projectName: string, idUseCase: string) {
    var promise = new Promise((resolve, reject) => {
      var resArray = [];
      this.rest.getRequest('projects/useCases/' + idUseCase + '/').subscribe(
        (data: any) => {
          this.rest.getRequest('projects/useCases/' + idUseCase + '/dataTables/VoBo/').subscribe(
            (res: any) => {
              for (var tab of res) {
                tab.Tipo = "DT";
                tab.Projecto = projectName;
                tab.UseCaso = [];
                tab.UseCaso.push(data.name);
                resArray.push(JSON.parse(JSON.stringify(tab)));
              }
              resolve(resArray);
            });
        });
    });
    return promise;
  }

  //Petición al back para traer todos los proyectos cargados en Nebula
  getProjectOptions() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      }, (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); }
    );
  }

  cleanLostCheck(fields: any){
    let new_check = {
      status: "NOK",
      user: "",
      comments: [],
      lastComment: ""
    };
    for (let naming of fields)
      if(!naming.hasOwnProperty("check"))
        naming["check"] = JSON.parse(JSON.stringify(new_check));
    return fields;
  }

  //Usa la tabla o tablon seleccionados para validar sus comentarios de VoBo y activar la interfaz
  validateTable(row) {
    this.goBack();
    this.masterObject = row;
    if (row.Tipo === 'T') {
      this.showTableValidation = true;
      if (row.fieldsRaw.length > 0) {
        row.fieldsRaw = this.cleanLostCheck(row.fieldsRaw);
        this.fieldsRaw = row.fieldsRaw;
      } else {
        this.fieldsRaw = [];
      }
    }
    else {
      this.showDataTableValidation = true;
      if (row.fields.length > 0) {
        this.fieldsRaw = row.fields;
      } else {
        this.fieldsRaw = [];
      }
      this.originsArr = this.masterObject.origin_tables;
      for(var tab of this.fieldsRaw)
        this.calcUniqueAlias(tab);
    }
    this.showUseCasesProject = false;
    this.showUseCase = false;
    this.showTabMaster = true;
    this.idTable = row._id;
    this.voboTables.groupHeader.collapseAllGroups();
  }

  //Actualiza el naming con el comentario y estado del VoBo
  postChanges(row: any, tablonFlag: boolean) {
    let route = tablonFlag ? "dataDictionaries" : "dataTables";
    if (row.check.lastComment.length > 0) {
      row.check.comments.unshift(String(row.check.lastComment + " - por " + this.userName));
      row.check.lastComment = "";
    }
    var data = {
      status: row.check.status,
      user: row.check.user,
      column: row.column,
      comments: row.check.comments
    };
    this.rest.postRequest(route + '/' + this.idTable + '/validations/', data).subscribe(
      (data) => { this.openSnackBar("Comentarios actualizados.", "Ok"); },
      (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); });
  }

  //Actualiza el naming de acuerdo a la valoracion del VoBo sobre este, si esta o no aprobado
  validate(row: any, accion: string, tableFlag: boolean) {
    if (accion == "OK") {
      row.check.status = "OK";
      row.check.user = this.userId;
      this.postChanges(row, tableFlag);
    } else if (accion == "NOK") {
      if (row.check.lastComment.length < 1) {
        this.openSnackBar("Es necesario un comentario cuando el campo no es aprobado.", "Error");
      } else {
        row.check.status = "NOK";
        row.check.user = this.userId;
        this.postChanges(row, tableFlag);
      }
    }
  }

  //Calcula los alias de las tablas de origen de un naming y los agrega a un arreglo global sin campos repetidos
  calcUniqueAlias(row: any) {
    var tempArr = [];
    for(var tab of row.originNamings)
      if(tab.hasOwnProperty("originAlias")) {
        tempArr.push(tab.originAlias);
      } else
        tempArr.push(this.calcAlias(tab.table_name, tab.originType));
    this.aliases[row.column] = Array.from(new Set(tempArr));
  }

  //Agrega el alias de una tabla de origen a su estructura
  calcAlias(name: string, type: string) {
    var temp = name.split("_");
    temp.splice(0, 2);
    if (type === "DT")
      if (this.originsDataTable.includes(temp[0].toUpperCase()))
        return name.substring(10);
      //else
        //return name.substring(7);
    return temp[0];
  }

  //Actualiza el nuevo comentario
  updateValueComment(row: any) {
    if (row.check.lastComment.length > 0) {
      row.check.comments.unshift(String(row.check.lastComment + " - por " + this.userName));
      row.check.lastComment = "";
    } else {
      this.openSnackBar("La observación no es válida.", "Error");
    }
  }

  //Finaliza la validacion del VoBo y la envia a gobierno
  validationAlready() {
    if (confirm("La validación será enviada a Gobierno-Datahub.")) {
      let route = this.masterObject.Tipo === 'T' ? "dataDictionaries" : "dataTables";
      this.rest.putRequest(route + '/' + this.idTable + '/state/',
        JSON.stringify({ from: "business", to: "governance", user_id: this.userId })).subscribe(
          (result: any) => {
            this.openSnackBar("Se ha finalizado la aprobación, la tabla será enviada para revisión gobierno.", "Ok");
            this.goBack();
          },
          (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          });
    }
  }

  //Retrocede en la validacion de VoBo para "reiniciar"
  goBack() {
    this.showTableValidation = false;
    this.showDataTableValidation = false;
    this.getProjectOptions();
    this.getProjects();
  }

  //Actualiza el valor del ultimo comentario del naming
  updateValueLastComment(event, row) {
    row.check.lastComment = event.target.value;
  }

  //Abre el dialog de la historia de los comentarios
  historyComment(row) {
    const dialogConfiguration = new MatDialogConfig();
    dialogConfiguration.minHeight = "275px";
    dialogConfiguration.minWidth = "850px";
    dialogConfiguration.disableClose = false;
    dialogConfiguration.autoFocus = true;
    dialogConfiguration.data = {
      type: "comments",
      comments: row.check.comments
    };
    this.dialog.open(ErrorsComponent, dialogConfiguration);
  }

  cleanNewValuesExcelsObject(data: any) {
    for (let row of data)
      if (!row["SECURITY LEVEL"])
        row["SECURITY LEVEL"] = "";

    return data;
  }

  cleanNewValuesExcelsFields(data: any) {
    for (let row of data)
      if (row["STORAGE ZONE"])
        if (row["STORAGE ZONE"] === "RAWDATA")
          if (!row["TOKENIZED AT DATA SOURCE"])
            row["TOKENIZED AT DATA SOURCE"] = "NO"

    return data;
  }

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-O': worksheet }, SheetNames: ['DDNG-O'] };
    XLSX.writeFile(workbook, VoboDictionaryComponent.toExportFileName(excelFileName));
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-F': worksheet }, SheetNames: ['DDNG-F'] };
    XLSX.writeFile(workbook, VoboDictionaryComponent.toExportFileName(excelFileName));
  }

  generateExcels() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/excels/Object/').subscribe(
      (data: any) => {
        data = this.cleanNewValuesExcelsObject(data);
        this.exportAsExcelFileObject(data, this.masterObject.project_name + "-" + this.masterObject.alias + "-Object");
        this.openSnackBar("Generación exitosa de los Object. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/excels/Fields/').subscribe(
      (data: any) => {
        data = this.cleanNewValuesExcelsFields(data);
        this.exportAsExcelFileFields(data, this.masterObject.project_name + "-" + this.masterObject.alias + "-Fields");
        this.openSnackBar("Generación exitosa de los Fields. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Devuelve la clase CSS para colorear la celda del naming de una tabla
  getCellClass({ row, column, value }) {
    if (row.modification.length > 0) {
      if (row.modification[0].state == 'GL') {
        return {
          'is-global': true
        };
      } else if (row.modification[0].state == 'GC') {
        return {
          'is-common-global': true
        };
      } else if (row.modification[0].state == 'PS') {
        return {
          'is-proposal': true
        };
      } else if (row.modification[0].state == 'PC') {
        return {
          'is-common-proposal': true
        };
      }
    }
  }

  //Devuelve la clase CSS de la fila dependiendo de si la fila esta o no aprobada por el VoBo
  getStatusRow({ row, column, value }) {
    if (row.check.status == 'NOK') {
      return { 'is-nok': true };
    } else if (row.check.status == 'OK') {
      return { 'is-ok': true };
    }
  }

  //Devuelve la clase CSS de la fila dependiendo de si el naming legacy esta repetido
  getCellClassLegacy({ row, column, value }) {
    if (row.legacy.repeat == 'Y') {
      return { 'legacy-repeated': true };
    }
  }

  //Devuelve la clase CSS para colorear la celda del naming
  getCellClassDT({ row }) {
    if(row.originNamings.length === 0) {
      if(row.default === "empty")
        return { 'generatedRow': true }
      if(row.modification.length > 0 && row.modification[0].stateValidation){
        if(row.modification[0].stateValidation === 'YES')
          return { 'is-valid-proposal': true }
        else
          return { 'is-wrong': true};
      } else
        return { 'is-wrong': true};
    }
    if(row.hasOwnProperty("direct"))
      if (row.direct === 1)
        return { 'is-directnaming': true };
    if (row.naming.isGlobal === 'Y')
      return { 'is-global': true };
    return { 'is-proposal': true };
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 1000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.duration = 5000;
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/;
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.tableValidations.limit = this.rowsLimit;
    this.tableValidations.recalculate();
  }

  private onPaginated(event) {
    this.tableValidations.limit = this.rowsLimit;
    this.tableValidations.recalculate();
  }
}
