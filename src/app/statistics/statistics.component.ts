import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../rest.service';
import * as CanvasJS from './canvasjs.min';

import * as Chart from '../../../node_modules/chart.js/dist/Chart';
import * as pallete from './palette';



import { MatSnackBar,MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  chart :any;

  //User
  userOption: any = [];
  userSelect: any;

  //Team
  teamOption: any = [];
  teamSelect: any;

  //table
  tableOptions: any = [];
  tableSelect: any;
  titleTabs="Estado general de las tablas - ";

  startDate: Date;
  endDate: Date;
  countByDateUser: Number = 0;
  tmpDate: Date;

  projectOptions:any = [];
  projectSelected = "";

  showStuff = false;
  stuffData: any;

  //new

  selectedMainOp = "";
  selectedSecOp = "";
  
  selectedRawOp = "";
  selectedMasterOp = "";

  mainStatOptions = ["UUAA", "Proyectos", "Tablas", "Tablones"];
  dddtStatOptions = ["Estado de procesamiento", "Cantidad"];

  projStatOptions = ["Estado de tablas/tablones", "Cantidad Tablas", "Cantidad Tablones"];
  uuaasRawOptions = [];
  uuaasMasterOptions = [];

  secondStatOptions = [];

  generalStatInfo = {};

  tablesData = {};
  tablonesData = {};
  tablesProyectsData = [];
  tablonesProyectsData = {};

  projectsStatesDD = {values: []};
  projectsStatesDT = {values: []};

  statusOrder = { "En propuesta namings": 0, "Gobierno": 1, "Revisión Negocio": 2, "Para ingestar": 3, "Produccion": 4 };

  stateColors = ['#f7893b','#2dcccd', '#5bbeff', '#1973b8', '#d8be75'];


  ctx;
  myChart;
  myPieChart: any;
  myBigPieChart: any;

  constructor(public rest: RestService,private changeDetectorRef: ChangeDetectorRef, public snackBar: MatSnackBar) {
    this.chart = [];
    this.getGeneralCountStatistics();
    this.getUsers();
    this.getTeams();
    this.getTablesStats();
    this.getTablonesStats();
    this.getTablesProyects();
    this.getTablonesProyects();
    this.getProjectOptions_Two();
  }

  ngOnInit() {}

  getTablesStats(){
    this.rest.getRequest('statistics/main/').subscribe(
      (data:any)=>{
        this.tablesData = this.cleanStatData(data, true);
        this.ctx = document.getElementById('chartTables');

        new Chart(this.ctx, {
          type: 'doughnut',
          data: {
            datasets: [{
                label: "Estadisticas",
                data: this.tablesData["values"],
                backgroundColor: this.stateColors,
                borderWidth: 3
            }],
            labels: this.tablesData["labels"],
          },
          options: { responsive: true, legend:{ display:false } }
        });
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  getTablonesStats(){
    this.rest.getRequest('statistics/main/dt').subscribe(
      (data:any)=>{
        //this.drawNewChart(data, "Estadisticas Estado Tablas");
        let res = this.cleanStatData(data, true);
        this.tablonesData = res;
        this.ctx = document.getElementById('chartTablones');

        new Chart(this.ctx, {
          type: 'doughnut',
          data: {
            datasets: [{
                label: "Estadisticas",
                data: res["values"],
                //backgroundColor: res["colors"],
                backgroundColor: this.stateColors,
                //borderColor: this.stateColors,
                borderWidth: 3
            }],
            labels: res["labels"],
          },
          options: { responsive: true, legend:{ display:false } }
        });

      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  getTablesProyects() {
    this.rest.getRequest('statistics/allproys/' + 'dd' + '/').subscribe(
      (data:any)=>{
        let res = this.cleanStatData(data, false);
        this.tablesProyectsData = this.splitStatData(res);
        this.ctx = document.getElementById('chartTablesProyects');

        new Chart(this.ctx, {
          type: 'horizontalBar',
          data: {
            datasets: [{
                data: res["values"],
                backgroundColor: res["colors"],
                borderWidth: 100,
                minBarLength: 3,
                barPercentage: 0.94,
                categoryPercentage: 1
            }],
            labels: res["labels"],
          },
          options: {
            responsive: true,
            legend: {
              display: false
            },
            scales: {
              yAxes: [{
                  ticks: { 
                    display: false
                  }, gridLines: {display: false}
                }],
              xAxes: [{
                ticks: {
                    max: Math.max(...res["values"]) + 10,
                    stepSize: 50,
                }
              }]
            }
          }
        });
        //this.drawNewChart(data, "Estadisticas Estado por "+ (typo === "dd" ? "Tablas en " : "Tablones en ") + this.secondStatOptions["name"]);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  getTablonesProyects() {
    this.rest.getRequest('statistics/allproys/' + 'dt' + '/').subscribe(
      (data:any)=>{
        let res = this.cleanStatData(data, false);
        this.tablonesProyectsData = this.splitStatData(res);
        this.ctx = document.getElementById('chartTablonesProyects');

        new Chart(this.ctx, {
          type: 'horizontalBar',
          data: {
            datasets: [{
                data: res["values"],
                backgroundColor: res["colors"],
                borderWidth: 100,
                minBarLength: 3,
                barPercentage: 0.97,
                categoryPercentage: 1
            }],
            labels: res["labels"],
          },
          options: {
            responsive: true,
            legend: {
              display: false
            },
            scales: {
              yAxes: [{
                  ticks: { 
                    display: false, 
                    barPercentage: 1,
                    categoryPercentage: 1 
                  },
                  gridLines: {display: false}
                }],
              xAxes: [{
                ticks: {
                    max: Math.max(...res["values"]) + 5,
                    stepSize: 25,
                }
              }]
            }
          }
        });
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }
  
  //Trae estadisticas basicas del back sobre las funcionalidades de Nebula
  getGeneralCountStatistics() {
    this.rest.getRequest('statistics/general/').subscribe(
      (data: {}) => {
        this.generalStatInfo = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  getUsers() {
    this.rest.getRequest('users/').subscribe(
      (data: {}) => {
        this.userOption = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  getTeams() {
    this.rest.getRequest('users/teams/').subscribe(
      (data: {}) => {
        this.teamOption = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }
  
  getProjectOptions(){
    this.rest.getRequest('users/'+sessionStorage.getItem("userId")+'/projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      }, (error)=>{
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  getProjectOptions_Two() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
        this.selectedProjectStatus(this.projectOptions[Math.floor(Math.random() * this.projectOptions.length)]);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getTables(){
    this.rest.getRequest('').subscribe(
      (data: any) => {
        this.tableOptions=data;
      }, (error)=> {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  statisticsByDatesUser(){
    this.rest.getRequest('statistics/users/code='+this.userSelect+'&startDate='+this.startDate+'&endDate='+this.endDate).subscribe(
      (data: {})=>{
        this.countFieldsByDay(data,"Campos por usuario","chartContainerUsers");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  statisticsByDatesTeam(){
    this.rest.getRequest('statistics/teams/code='+this.teamSelect+'&startDate='+this.startDate+'&endDate='+this.endDate).subscribe(
      (data: {})=>{
        this.countFieldsByDay(data, "Campos por equipo","chartContainerTeams");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      });
    this.reProcessNumber();
  }

  statusTable(){
    this.rest.getRequest('statistics/tables/'+this.tableSelect).subscribe(
      (data:{})=>{
        this.drawStateTable([
          {y: data["blocker"],name:"Bloqueadas"},
          {y: data["architecture"], name: "Arquitectura"},
          {y: data["governance"], name:"Gobierno"},
          {y: data["unproposal"], name:"En propuesta"},
          {y: data["return"], name:"Devuelta"}
        ],"Estado tabla","chartContainer");
      }, (error)=>{
        this.openSnackBar("Error procesando petición al servidor.","Error");
      });
    this.reProcessPer();
  }

  reProcessPer(){
    this.rest.getRequest('statistics/reProcessPer/'+this.tableSelect).subscribe(
      (data:{})=>{
        this.drawStateTable([
          {y: data["NoOk"],name:"Re procesadas"},
          {y: data["OK"],name:"Ok"},
          {y: data["SR"],name:"Sin revision"}
        ],"Número Reprocesos","chartContainer2");
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.","Error");
    });
  }

  reProcessNumber(){
    this.rest.getRequest('statistics/tables/'+this.tableSelect).subscribe(
      (data:{})=>{
        this.countFieldsByDay([
          {y: data["Buenas Practicas"],label:"Buenas practicas"},
          {y: data["Clasificacion"],label:"Clasificacion"},
          {y: data["Consistencia de Informacion"],label:"Buenas practicas"},
          {y: data["Existente"],label:"Existentes"},
          {y: data["Falta de Informacion"],label:"Falta informacion"},
          {y: data["Homologacion"],label:"Homologacion"},
          {y: data["Inconsistencia Datos"],label:"Inconsistencia Datos"},
          {y: data["Total"],label:"Campos reprocesados"},
        ],"Número de reprocesos","chartContainerTeams2");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      } 
    );
  }

  statisticStatusTables(){
    this.titleTabs ="Estado General "+new Date().toLocaleString();
    this.rest.getRequest('statistics/status/where=project&code='+this.projectSelected).subscribe(
      (data:any)=>{
        this.drawStateTable(data,"Estado proyecto", "fieldsContainerGeneral");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
    /** 
      this.rest.getRequest('statistics/fieldStatus/').subscribe(
        (data:{})=>{
            this.countFieldsByDay([
            {y: data["blocker"], label:"Bloqueadas"},
            {y: data["architecture"], label: "Arquitectura"},
            {y: data["governance"], label:"Gobierno"},
            {y: data["unproposal"], label:"En propuesta"},
            {y: data["return"], label:"Devueltos"}
            ],"Estado campos","fieldsContainerGeneral2");
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.","Error");
        });
    **/
  }

  tabChanged (event){
    if(event.tab.textLabel=='General'){
      this.titleTabs ="Estado General "+new Date().toLocaleString();
      this.getProjectOptions();
    }

  }

  countFieldsByDay(datos,title,divshowgraph){
    this.changeDetectorRef.detectChanges();
    let seeData = new CanvasJS.Chart(divshowgraph,{
      animationEnable: true,
      exportEnable: true,
      title: {
          text: title
      },
      data: [{
          type: "column",
          dataPoints: datos
      }]
    });
    seeData.render();
  }

  drawStateTable(dataToPaint,title,divshowgraphs){
    let seeData = new CanvasJS.Chart(divshowgraphs,{
      theme: "light2",
		  animationEnabled: true,
		  exportEnabled: true,
		  title:{
			  text: title
      },
      data:[{
        type: "pie",
        showInLegend: false,
        toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
        dataPoints: dataToPaint
      }]
    });
    seeData.render();
  }

  selectedUser(value){
    this.userSelect = value;
  }

  selectedTeam(value){
    this.teamSelect = value;
  }

  selectedTable(value){
    this.tableSelect = value;
  }

  selectedProject(event){
    this.projectSelected=event;
    this.statisticStatusTables();
  }

  getUUAAsOptions() {
    this.rest.getRequest('dataDictionaries/uuaas/rawOptions/').subscribe(
      (data: any) => {
        this.uuaasRawOptions = data;
      }, (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); });
    this.rest.getRequest('dataDictionaries/uuaas/masterOptions/').subscribe(
      (data: any) => {
        this.uuaasMasterOptions = data;
      }, (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); });
  }


  selectedMain(value: any){
    switch (value) {
      case "UUAA":
        this.getUUAAsOptions();
        break;
      case "Tablas": case "Tablones": 
        this.secondStatOptions = this.dddtStatOptions;
        break;
      case "Proyectos": 
        this.getProjectOptions_Two();
        break;
    }
    this.selectedMainOp = value;
  }

  showStatistics(){
    switch (this.selectedMainOp) {
      case "Tablas": 
        this.rest.getRequest('statistics/main/').subscribe(
          (data:any)=>{
            this.drawNewChart(data, "Estadisticas Estado Tablas");
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.","Error");
          }
        );
        break;
      case "Tablones": 
        this.rest.getRequest('statistics/main/dt').subscribe(
          (data:any)=>{
            this.drawNewChart(data, "Estadisticas Estado Tablones");
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.","Error");
          }
        );
        break;
    }
  }

  showStatisticsUUAA(loc: string, typo: string){
    this.rest.getRequest('statistics/uuaa/uuaa='+ (loc === "raw" ? this.selectedRawOp : this.selectedMasterOp) + '&type=' + typo + '&loc=' + loc + '/').subscribe(
      (data:any)=>{
        this.drawNewBigChart(data, "Estadisticas Namings " + (loc === "raw" ? "RAW por" : "MASTER por") + " UUAA " + this.secondStatOptions);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
    });
  }

  showStatisticsProyects(typo: string){
    this.rest.getRequest('statistics/status/' + typo + '/where=project&code=' + this.secondStatOptions["_id"]).subscribe(
      (data:any)=>{
        this.drawNewChart(data, "Estadisticas Estado por "+ (typo === "dd" ? "Tablas en " : "Tablones en ") + this.secondStatOptions["name"]);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  showStatisticsSimple(type: string){
    this.rest.getRequest('statistics/allproys/' + type + '/').subscribe(
      (data:any)=>{
        this.drawNewChart(data, "Estadisticas Proyectos - Numero " + (type == "dd" ? "Tablas" : "Tablones") );
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.","Error");
      }
    );
  }

  selectedProjectStatus(selectedVal: any){
    let finalArr = [];
    this.secondStatOptions = selectedVal;

    this.rest.getRequest('statistics/status/' + "dd" + '/where=project&code=' + selectedVal["_id"]).subscribe(
      (data:any)=>{
        this.projectsStatesDD = this.cleanStatData(data, true);
        finalArr.push({
          label: "Tablas",
          backgroundColor: "#5bbeff",
          data: this.sortStatData(data)
        });
        this.rest.getRequest('statistics/status/' + "dt" + '/where=project&code=' + selectedVal["_id"]).subscribe(
          (data:any)=>{
            this.projectsStatesDT = this.cleanStatData(data, true);
            finalArr.push({
              label: "Tablones",
              backgroundColor: "#2dcccd",
              data: this.sortStatData(data)
            });
            this.drawProjectStatusChart(finalArr);
          }
        );
      }
    );
  }
  

  drawNewChart(data: any, label: string){
    let res = this.cleanStatData(data, false);
    this.ctx = document.getElementById('myChart');

    if (this.myBigPieChart) this.myBigPieChart.destroy();
    if (this.myPieChart) this.myPieChart.destroy();

    this.myPieChart = new Chart(this.ctx, {
      type: 'doughnut',
      data: {
        datasets: [{
            label: "Estadisticas",
            data: res["values"],
            backgroundColor: res["colors"],
            /*
            backgroundColor: [
              '#f54842',
              '#f2f542',
              '#66f542',
              '#427bf5',
              '#dde2ed'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            */
            borderWidth: 3
        }],
        labels: res["labels"],
      },
      options: { responsive: true, legend:{display:false}}
    });
    this.stuffData = res;
    this.showStuff = true;
  }

  drawNewBigChart(data: any, label: string){
    let res = this.cleanStatData(data, false);
    this.ctx = document.getElementById('myBigChart');

    if (this.myBigPieChart) this.myBigPieChart.destroy();
    if (this.myPieChart) this.myPieChart.destroy();

    this.myPieChart = new Chart(this.ctx, {
      type: 'horizontalBar',
      data: {
        datasets: [{
            label: "Cantidad",
            data: res["values"],
            backgroundColor: res["colors"],
            borderWidth: 1
        }],
        labels: res["labels"],
      },
      options: { responsive: true, title:{ display: true, text: label }}
    });
    this.stuffData = res;
    this.showStuff = false;
  }

  drawProjectStatusChart(dataset: any){
    this.ctx = document.getElementById('chartProjectStatus');
    if (this.myPieChart) this.myPieChart.destroy();

    this.myPieChart = new Chart(this.ctx, {
      type: 'bar',
      data: {
        datasets: dataset,
        labels: ["En propuesta namings", "Gobierno", "Revisión Negocio", "Para ingestar", "Produccion"]
      },
      options: {
        maintainAspectRatio: false,
        legend: { display: false },
        barValueSpacing: 20,
        scales: {
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        }
      }
    });
  }

  dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);

    return "rgb(" + r + "," + g + "," + b + ")";
  };

  cleanStatData(data: any, sort: boolean){
    let values = [];
    let labels = [];
    let colors = [];

    for (let dat of data)
      if (dat["y"] > 0)
        if(sort){
          values[ this.statusOrder[dat["name"]] ] = dat["y"];
          labels[ this.statusOrder[dat["name"]] ] = dat["name"];
          colors[ this.statusOrder[dat["name"]] ] = this.dynamicColors();
        } else {
          values.push(dat["y"]);
          labels.push(dat["name"]);
          colors.push(this.dynamicColors());
        }
    return {values: values, labels: labels, colors: colors};
  }



  sortStatData(array: any){
    let tmpYe = [0,0,0,0,0];
    for(let value of array) 
      tmpYe[ this.statusOrder[value["name"]] ] = value["y"];

    return tmpYe;
  }

  splitStatData(data: any){
    let amount = Math.floor(data["values"].length / 2);
    let resArr = [];
    let done = false;
    let insidePosition = 0;
    let tmpPos = 0;

    for (insidePosition; insidePosition < 2; insidePosition++) {
      let tmpArr = [];
      for (let ind = 0; ind <= amount && !done; ind++, tmpPos++) {
        if (data["values"][tmpPos]) {
          tmpArr[ind] = {
            value: data["values"][tmpPos],          
            label: data["labels"][tmpPos],
            color: data["colors"][tmpPos]
          };
        } else 
          done = true;
      }
      done = false;
      tmpPos = amount + 1;
      resArr[insidePosition] = tmpArr;
    }
    return resArr;
  }
  

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if(action=="Ok"){
        config.panelClass = ['sucessMessage'];
    }else{
        config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
