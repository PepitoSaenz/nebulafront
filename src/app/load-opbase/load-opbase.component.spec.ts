import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadOpbaseComponent } from './load-opbase.component';

describe('LoadOpbaseComponent', () => {
  let component: LoadOpbaseComponent;
  let fixture: ComponentFixture<LoadOpbaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadOpbaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadOpbaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
