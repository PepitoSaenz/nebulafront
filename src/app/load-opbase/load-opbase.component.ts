import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import * as XLSX from 'xlsx';
import {CdkDragDrop, moveItemInArray, CdkDrag} from '@angular/cdk/drag-drop';
import { RestService } from '../rest.service';

import { LoadSettingsComponent } from './load-settings/load-settings.component'


@Component({
  selector: 'app-load-opbase',
  templateUrl: './load-opbase.component.html',
  styleUrls: ['./load-opbase.component.scss']
})
export class LoadOpbaseComponent implements OnInit {

  widthP: any;
  fieldsSettings = [];
  objectSettings = [];

  defaultFields = [];

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, private dialogRef: MatDialogRef<LoadOpbaseComponent>,
    public rest: RestService, public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.getSettings();
   }

  ngOnInit() {
  }

  cdkDragStarted(event:any){
    this.widthP = event.source.element.nativeElement.offsetWidth
 }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.fieldsSettings, event.previousIndex, event.currentIndex);
    this.updateAllFields();
  }

  getSettings() {
    this.rest.getRequest("operationalBase/settings/").subscribe(
      (data: any) => {
        this.fieldsSettings = data["arrayFields"];
        this.defaultFields = data["defaultFields"];
        //this.objectSettings = this.addNewBox(data["objectFields"]);
      });
  }

  addFieldProp() {
    if (this.fieldsSettings.length <= 13) {
      this.fieldsSettings.push({
        name: "--nuevo--",
        property: "--nuevo--",
        desc: "Nueva descripcion"
      })
      this.updateAllFields();
      this.openSnackBar("Se ha agregado una nueva propiedad al final", "Ok");
    }
  }

  restartDefaults() {
    if (confirm("¿Esta seguro de reiniciar las columnas de carga a los valores por defecto?")) {
      let newFields = [];

      for (let fild of this.defaultFields) {
        fild["name"] = fild["property"];
        newFields.push(fild);
      }
      this.fieldsSettings = newFields;
      this.updateAllFields();
      this.openSnackBar("Se han reiniciado las columnas a sus valores por defecto", "Ok");
    }
  }

  updateAllFields() {
    this.rest.putRequest("operationalBase/settings/", this.fieldsSettings).subscribe(
      (data: any) => {
        console.log(data);
      });
  }
  
  openSettings(propData: any) {
    const dialogConfig = new MatDialogConfig();
        dialogConfig.minHeight = "300px";
        dialogConfig.minWidth = "850px";
        dialogConfig.disableClose = false;
        dialogConfig.data = {
          propData: propData,
          propPos: this.fieldsSettings.indexOf(propData)
        };
        const dialogRef = this.dialog.open(LoadSettingsComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((result) => {
          if (result)
            if (result.status == "delete") {
              let deloto =this.fieldsSettings.splice(result.pos, 1);
              this.updateAllFields();
              this.openSnackBar("Se ha eliminado la propiedad " + deloto["property"], "Ok");
            }
        });
  }

  downloadExcel() {
    this.exportAsExcelFileObject(this.buildJSONExcel(this.fieldsSettings), "Plantilla Carga Bases Ops");
    this.openSnackBar("Descarga correcta de la plantilla", "Ok");
  }

  buildJSONExcel(array: any[]) {
    let tmpRow = {};

    for (let colu of array) {
      if (!colu.property.includes("--")) 
        tmpRow[colu.name ? colu.name : colu.property] = "";
    }
    return [tmpRow];
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Fields': worksheet }, SheetNames: ['Fields'] };
    XLSX.writeFile(workbook, `${excelFileName}_export_${new Date().getTime()}.xlsx`);
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 9000;
    if (action == "Ok") {
      config.panelClass = ["sucessMessage"];
    } else if (action == "Warning") {
      config.panelClass = ["warnningMessage"];
    } else {
      config.panelClass = ["errorMessage"];
    }
    this.snackBar.open(message, action, config);
  }

  /**
   * Predicate function that only allows even numbers to be
   * sorted into even indices and odd numbers at odd indices.
   */
  sortPredicate(index: number, item: CdkDrag<number>) {
    return index-1 !== this.fieldsSettings.length;
  }


  defaultValues = [{"property": "length", "description": "Longitud del naming. Solo recibe números"}, 
    {"property": "naming", "description": "Naming, nombre del campo. Si el naming pertenece al RCN los datos de este seran cargados de la base de datos."},
    {"property": "logic", "description": "Nombre lógico del naming. Si el campo del naming pertenece al RCN, el nombre lógico sera cargado de la base de datos."},
    {"property": "description", "description": "Descripción del naming. Si el campo del naming pertenece al RCN, la descripción sera cargada de la base de datos."},
    {"property": "catalogue", "description": "Catalogo del naming. No tiene validación por defecto"},
    {"property": "key", "description": "Bandera para marcar el campo como llave. Recibe 1 o 0 para verdadero(campo llave) o falso(campo NO llave) respectivamente. Si el campo se coloca como llave, el campo mandatorio se marca como verdadero automaticamente."},
    {"property": "mandatory", "description": "Bandera para marcar el campo como mandatorio. Recibe 1 o 0 para verdadero(campo mandatorio) o falso(campo NO mandatorio) respectivamente. Si el campo de llave es verdadero, el campo mandatorio se marca como verdadero automaticamente."},
    {"property": "dataType", "description": "Tipo de dato de salida. Los valores que recibe son constantes los cuales son: STRING, INT32, INT64, DECIMAL, DATE, TIMESTAMP. Es posible agregar tipos de datos completamente abiertos sin validación pero el formato lógico debera ser cargado obligatoriamente."},
    {"property": "logicalFormat", "description": "Formato lógico. Si el naming es parte del RCN este campo es cargado de la base de datos. Si el campo de tipo de salida no esta entre las opciones este campo debera ser diligenciado obligatoriamente."},
    {"property": "format", "description": "Formato del naming. No tiene validación pero si el campo de dato de salida es TIMESTAMP se cargara un formato predeterminado."},
    {"property": "origin_desc", "description": "Origen del naming. No tiene validación por lo que puede ser cualquier texto, como namings o descripciones."}
  ];

  
  movies = [
    { "name": "naming",
      "property": "naming.naming",
      "description": "el coso de namings" 
    },
    { "name": "lengthdasdasdasdasdasdasd",
      "property": "length",
      "description": "el coso de length" 
    },
    { "name": "dataType",
      "property": "dataType",
      "description": "el coso de dataType" 
    },
    { "name": "logic",
      "property": "logic",
      "description": "el coso de logic" 
    },
    { "name": "description",
      "property": "description",
      "description": "el coso de descriptiondescription" 
    },
    { "name": "format",
      "property": "format",
      "description": "formatsadasdasdas" 
    },
    { "name": "naming",
      "property": "naming.naming",
      "description": "el coso de namings" 
    },
    { "name": "coso oso so oso oasdo asd as do doasd asdas dasasd",
      "property": "length",
      "description": "el coso de length" 
    },
    { "name": "dataType",
      "property": "dataType",
      "description": "el coso de dataType" 
    },
    { "name": "logic",
      "property": "logic",
      "description": "el coso de logic" 
    },
    { "name": "description",
      "property": "description",
      "description": "el coso de descriptiondescription" 
    },
    { "name": "format",
      "property": "format",
      "description": "formatsadasdasdas" 
    }
  ];

}
