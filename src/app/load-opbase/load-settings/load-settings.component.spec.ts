import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadSettingsComponent } from './load-settings.component';

describe('LoadSettingsComponent', () => {
  let component: LoadSettingsComponent;
  let fixture: ComponentFixture<LoadSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
