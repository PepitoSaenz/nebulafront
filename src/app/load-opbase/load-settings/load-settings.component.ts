import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { RestService } from '../../rest.service';



@Component({
  selector: 'app-load-settings',
  templateUrl: './load-settings.component.html',
  styleUrls: ['./load-settings.component.scss']
})
export class LoadSettingsComponent implements OnInit {
  
  formControlProp = new FormControl('', [Validators.required, Validators.minLength(3)]);

  pos: any;
  data: any;
  allSettings: any;
  fieldsOptions: any;

  validOptions = ["", "Naming correcto", "Valor no vacio", "Tipo dato de salida", "Formato de Gobierno"];



  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, private dialogRef: MatDialogRef<LoadSettingsComponent>, 
    public rest: RestService) {
    this.pos = datas["propPos"];
    this.data = datas["propData"];
    this.getSettings();
  }

  ngOnInit() {
  }

  addEmptyValue(array: any) {
    array.push({
      property: "--nuevo--"
    })
  }

  cleanName(data: any) {
    if (!data["name"])
      data["name"] = data["property"];
    return data;
  }

  getSettings() {
    this.rest.getRequest("operationalBase/settings/").subscribe(
      (data: any) => {
        this.allSettings = data;
        this.fieldsOptions = data["defaultFields"];
        //this.objectSettings = this.addNewBox(data["objectFields"]);
      });
  }

  checkRepeatedValues(newValue: any, pos: number) {
    for (let sett in this.allSettings["arrayFields"]) {
      if (this.allSettings["arrayFields"].indexOf(sett) != this.pos) {
        if (sett["property"] === this.data["property"]) {
          return true;
        }
      }
    }
    return false;
  }


  selectedProp(value) {
    //this.data["property"] = value;    
    this.data["description"] = this.fieldsOptions.find(fild => fild.property == value)["description"];
  }

  pushSettings() {
    this.rest.postRequest("operationalBase/settings/", {pos: this.pos, data: this.data}).subscribe(
      (data: any) => {
        console.log("done update sett");
      });
  }


  deleteDialog() {
    if (confirm("Esta seguro que desea eliminar esta propiedad de la carga?")) 
      this.dialogRef.close({status: "delete", pos: this.pos});
  }

  closeDialog(loaded: boolean) {
    this.dialogRef.close({status: "sali"});
  }
}
