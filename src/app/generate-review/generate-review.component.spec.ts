import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateReviewComponent } from './generate-review.component';

describe('GenerateReviewComponent', () => {
  let component: GenerateReviewComponent;
  let fixture: ComponentFixture<GenerateReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
