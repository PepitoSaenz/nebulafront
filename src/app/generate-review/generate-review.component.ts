import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { MatTabChangeEvent, MatDialog, MatDialogConfig } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { RestService } from '../rest.service';
import { NamingValidationService } from '../naming-validation.service';

import { ErrorsComponent } from './../errors/errors.component';
import * as XLSX from 'ts-xlsx';
import { CommentComponentComponent } from './../comment-component/comment-component.component';


@Component({
  selector: 'app-generate-review',
  templateUrl: './generate-review.component.html',
  styleUrls: ['./generate-review.component.scss']
})
export class GenerateReviewComponent implements OnInit {

  columns = [
    { prop: "column", name: "No.", visible: true },
    { prop: "startPosition", name: "Inicio", visible: true },
    { prop: "endPosition", name: "Final", visible: true },
    { prop: "length", name: "Longitud", visible: true },
    { prop: "originType", name: "Tipo origen", visible: true },
    { prop: "outRec", name: "Outrec", visible: true }, //HOST
    { prop: "outVarchar", name: "Varchar salida", visible: true }, //HOST
    { prop: "outLength", name: "Salida", visible: true }, //HOST
    { prop: "naming.naming", name: "Naming", visible: true },
    { prop: "logic", name: "Nombre Lógico", visible: true },
    { prop: "description", name: "Descripción", visible: true },
    { prop: "catalogue", name: "Catálogo", visible: true },
    { prop: "legacy", name: "Legacy", visible: true },
    { prop: "legacyDescription", name: "Descripción Legacy", visible: true },
    { prop: "example", name: "Ejemplo", visible: true },
    { prop: "key", name: "Key", visible: true },
    { prop: "mandatory", name: "Mandatory", visible: true },
    { prop: "symbol", name: "Signo", visible: true }, //17
    { prop: "integers", name: "Enteros", visible: true },
    { prop: "decimals", name: "Decimales", visible: true },
    { prop: "destinationType", name: "Tipo destino", visible: true }, //20
    { prop: "governanceFormat", name: "Formato Gobierno", visible: true }, //21
    { prop: "format", name: "Formato", visible: true }, //22
    { prop: "actions", name: "Acciones", visible: true }, //23
    { prop: "code", name: "Codigo", visible: true }, //24
    { prop: "logicalFormat", name: "Formato Logico", visible: true }, //25
    { prop: "tokenization", name: "Tokenización", visible: true }, //26
    { prop: "VoBo", name: "VoBo", visible: true },
    { prop: "tds", name: "TDS", visible: true },
    { prop: "destinationType", name: "Tipo de dato", visible: true }, //29
    { prop: "hierarchy", name: "Jerarquia", visible: true }, //30
    { prop: "governanceState", name: "Estado gobierno", visible: true }, //31
    { prop: "default", name: "Default", visible: true } //32
  ];

  rowsLimit = 5;
  @ViewChild('table') table: any;

  userId = "0";
  idTable: string = "";

  showHostTableFields: boolean = false;
  stateGovernance: boolean = false;

  tableData: any = {
    "modifications": {"user_name":"", "date":""},
    "created_time":""
  };

  fieldsTable: any = [];
  editing: any = [];
  blockers: any = [];

  showTabRaw: boolean;
  showTabMaster: boolean;

  reasons: any = [];
  suffixes: any = [];
  frecuencyOptions: any = [];
  objectTypesOptions = ["File", "Table"];
  loadingTypesOptions = ["incremental", "complete"];

  errors: any = [];
  badNamings = [];

  arraySuffixTmp: any = "";

  rawObjectOk: boolean;
  masterObjectOk: boolean;

  activeTab: string;
  tabs: any = [];
  selected = new FormControl(0);
  uuaaShort: string;
  aliasLongTabla: string = "";
  activeObject: any = {};
  loadingIndicator: boolean = false;
  allColumnsChecked: boolean = true;

  tableRawDataTemp: any = {};
  tableMasterDataTemp: any = {};
  fieldsRawTemp: any = [];
  fieldsMasterTemp: any = [];

  namingGlobalSearch: string = "";
  suffixSearch: string = "";
  logicGlobalSearch: string = "";
  legacySearch: string = "";
  namingStatusSearch: string = "";
  descLegacySearch: string = "";
  descGlobalSearch: string = "";
  selectedStateFilter: string = "";

  fileList: File = null;
  value = "";
  uuaaOptions = [];

  observationFlag = true;
  viewAuditFlag = false;


  constructor(public vali: NamingValidationService, public rest: RestService, private route: ActivatedRoute, private router: Router, private changeDetectorRef: ChangeDetectorRef, public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.activeTab = "FieldReview";
    this.userId = sessionStorage.getItem("userId");
    this.route.queryParams.subscribe(params => {
      this.idTable = params["idTable"];
      this.getInfoTable(this.activeTab);
      this.getFields(this.activeTab);
      this.tabs[0] = "Propuesta";
      this.isGenerated('fieldsRaw');
      this.isGenerated('fieldsMaster');
    });
    if (this.idTable.length == 0) {
      this.router.navigate(['/tables']);
    }
    this.getSuffixes();
    this.getUuaas();
    this.getFrecuency();
    this.getReasons();
  }

  ngOnInit() {
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  private onPaginated(event) {
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  //Peticion al back que devuelve la información de la tabla que no tengan que ver con campos ni namings
  getInfoTable(fase: string) {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/head/' + fase + '/').subscribe(
      (data: any) => {
        if (fase === 'raw' || fase === 'master') {
          this.activeObject = data;
          this.activeObject.baseName = this.activeObject.baseName.toUpperCase();
          var temp = this.activeObject[fase + '_name'].split("_");
          this.uuaaShort = temp[0] + "_" + temp[1] + "_";
          temp.splice(0, 2);
          this.aliasLongTabla = temp.join("_");
          if(data.created_time != "" && data.modifications.user_name != "" && data.modifications.date != "")
            this.viewAuditFlag = true;
          //this.observationFlag = this.activeObject[(fase === 'raw' ? 'raw' : 'master') + "_comment"].length >= 1;
        } else {
          this.tableData = data;
          if(data.created_time != "" && data.modifications.user_name != "" && data.modifications.date != "")
            this.viewAuditFlag = true;
          if (this.tableData.stateTable !== "G" && this.tableData.stateTable !== "N") {
            this.rest.sendHome(sessionStorage.getItem("rol"));
            this.openSnackBar("Esa tabla no esta disponible para editar", "Error");
          }
          var temp = this.tableData.raw_name.split("_");
          this.uuaaShort = temp[0] + "_" + temp[1] + "_";
          temp.splice(0, 2);
          this.aliasLongTabla = temp.join("_");
          if (data.stateTable != "N") {
            this.stateGovernance = true;
          }
        }
        if (data.originSystem.match(/HOST/g)) {
          this.showHostTableFields = true;
        }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Peticion al back que devuelve todos los campos de la tabla
  getFields(fase: string) {
    var promise = new Promise((resolve, reject) => {
      this.loadingIndicator = true;
      this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + fase + '/')
        .subscribe(
          (data: any) => {
            this.fieldsTable = data;
            this.getBlockers(this.fieldsTable);
            resolve(true);
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
            reject(false);
          }
        );
      this.loadingIndicator = false;
    });
    return promise;
  }

  getBlockers(arrTable: any) {
    this.blockers = [];
    for (let i = 0; i < arrTable.length; i++) {
      this.blockers[i] = arrTable[i].naming.governanceState === "C" ? true : false;
    }
  }

  updateFields(fase) {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + fase + '/').subscribe(
      (data: any) => {
        this.fieldsTable = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  notRawMasterGen() {
    return this.showTabRaw && this.showTabMaster;
  }

  isGenerated(fase: string) {
    let flag = false;
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + fase + '/')
      .subscribe(
        (data: any) => {
          if (data.length > 0) {
            if (fase === 'fieldsRaw') {
              this.fieldsRawTemp = data;
              this.showTabRaw = true
              this.tabs[1] = "Raw";
            } else if (fase === 'fieldsMaster') {
              this.fieldsMasterTemp = data;
              this.showTabMaster = true
              this.tabs[2] = "Master";
            }
          }
        }, (error) => {
          (fase === 'fieldsRaw' ? this.showTabRaw = false : this.showTabMaster = false);
        }
      );
    return flag;
  }

  //Genera la version RAW de la tabla
  generateFieldsRaw() {
    if (this.allValidations(this.activeTab, false)) {
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/head/raw/', JSON.stringify({user_id: this.userId})).subscribe(
        (data: any) => {
          this.rest.putRequest('dataDictionaries/' + this.idTable + '/fases/',
            JSON.stringify({ from: 'FieldReview', to: 'raw', user_id: this.userId})).subscribe(
              (data: any) => {
                this.getInfoTable('raw');
                this.openSnackBar("Se ha generado la fase raw.", "Ok");
                this.isGenerated('fieldsRaw');
              }, (error) => {
                this.openSnackBar("Error procesando petición al servidor. No se pudo generar RAW", "Error");
              }
            );
        }, (error) => {
          this.openSnackBar("Error  procesando petición al servidor.", "Error");
        }
      );
    } else {
      this.openSnackBar("No es posible generar la fase con namings invalidos", "Error");
    }
  }

  //Genera la version MASTER de la tabla
  generateFieldsMaster() {
    if (this.allValidations(this.activeTab, false)) {
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/head/master/', JSON.stringify({user_id: this.userId})).subscribe(
        (data: any) => {
          this.rest.putRequest('dataDictionaries/' + this.idTable + '/fases/',
            JSON.stringify({ from: 'fieldsRaw', to: 'master', user_id: this.userId})).subscribe(
              (data: any) => {
                this.getInfoTable('master');
                this.openSnackBar("Se ha generado la fase master.", "Ok");
                this.isGenerated('fieldsMaster');
              }, (error) => {
                this.openSnackBar("Error procesando petición al servidor. No se pudo generar MASTER", "Error");
              }
            );
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    }
  }

  //Valida la tabla completa, Propuesta, Raw, Master y la envia a gobierno para seguir con el flujo
  sendTableGovernance() {
    this.validateRawMasterPromise().then((res: any) => {
      this.validateFinalGovernance(res);
    });
  }

  //Valida la tabla en las fases de raw y master (si existe) unicamente
  validateRawMasterPromise() {
    var promise = new Promise((resolve, reject) => {
      let rawR = "";
      this.validateRawGovernance().then((res: string) => {
        rawR = res;
        if (this.showTabMaster)
          this.validateMasterGovernance().then((res: string) => {
            resolve(rawR.concat(res));
          });
        else
          resolve(rawR);
      });
    });
    return promise;
  }

  //Valida la fase de Raw
  validateRawGovernance() {
    let errorsRaw = [];
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + this.idTable + '/head/' + 'raw' + '/').subscribe(
        (data: any) => {
          if (!this.objectValidation(data, 'raw', false))
            errorsRaw.push("***El objeto de Raw tiene campos sin validar***" + "\n");
          this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + 'fieldsRaw' + '/').subscribe(
            (data: any) => {
              this.fieldsRawTemp = data;
              for (var row of this.fieldsRawTemp)
                if (this.rowValidation(row, 'fieldsRaw', this.fieldsRawTemp)) this.errors = [];
                else {
                  for (var errors of this.errors)
                    errorsRaw.push(errors);
                  this.errors = [];
                }
              if (errorsRaw.length > 0)
                errorsRaw.unshift("* Reporte fase: RAW" + "\n");
              resolve(errorsRaw);
            }
          );
        });
    });
    return promise;
  }

  //Valida la fase de Master
  validateMasterGovernance() {
    let errorsMaster = [];
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + this.idTable + '/head/' + 'master' + '/').subscribe(
        (data: any) => {
          if (!this.objectValidation(data, 'master', false))
            errorsMaster.push("***El objeto de Master tiene campos sin validar***" + "\n");
          this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + 'fieldsMaster' + '/').subscribe(
            (data: any) => {
              this.fieldsMasterTemp = data;
              for (var row of this.fieldsMasterTemp){
                if (this.rowValidation(row, 'fieldsMaster', this.fieldsMasterTemp)) 
                  this.errors = [];
                else {
                  for (var errors of this.errors)
                    errorsMaster.push(errors);
                  this.errors = [];
                }
              }
              if (errorsMaster.length > 0)
                errorsMaster.unshift("* Reporte fase: MASTER *" + "\n");
              resolve(errorsMaster);
            }
          );
        });
    });
    return promise;
  }

  //Finaliza la validacion para gobierno y la envia para cambiar su estado o imprime los errores
  //en la validación
  validateFinalGovernance(errors: any) {
    if (errors.length == 0) {
      //this.updateAllValidations(this.fieldsRawTemp, 'fieldsRaw');
      //this.updateAllValidations(this.fieldsMasterTemp, 'fieldsMaster');
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/state/',
        JSON.stringify({ from: "ingesta", to: "governance", user_id: this.userId})).subscribe(
          (data: any) => {
            this.openSnackBar("Se ha enviado la tabla para revisión en gobierno. ", "Ok");
            this.router.navigate(['/home']);
          }, (error) => {
            this.openSnackBar("No se pudo enviar la tabla para revisión en gobierno.", "Error");
          });
    } else {
      const dialogErr = new MatDialogConfig();
      dialogErr.height = "65%";
      dialogErr.width = "70%";
      dialogErr.disableClose = true;
      dialogErr.autoFocus = true;
      dialogErr.data = errors;
      this.dialog.open(ErrorsComponent, dialogErr);
    }
  }

  getReasons() {
    this.reasons = [];
    this.rest.getRequest('dataDictionaries/comments/reasons/').subscribe(
      (data: {}) => {
        this.reasons = data;
        this.reasons.unshift({ error: "NOK" });
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back que devuelve el arreglo con los posibles sufijos
  getSuffixes() {
    this.suffixes = [];
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixes = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back que devuelve las uuaas
  getUuaas() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/uuuaa/').subscribe((data: any) => {
      this.uuaaOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  //Peticion al back que devuelve los tipos de frecuencias/periodicidad
  getFrecuency() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe((data: any) => {
      this.frecuencyOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  //Cambia la fase actual dependiendo de la pestaña en la que se encuentre
  onTabChange(event: MatTabChangeEvent) {
    //this.allValidations(this.activeTab, false);
    switch (event.tab.textLabel) {
      case "Propuesta":
        this.activeTab = "FieldReview";
        this.getInfoTable(this.activeTab);
        this.getFields(this.activeTab);
        break;
      case "Raw":
        this.activeTab = "fieldsRaw";
        this.getInfoTable("raw");
        this.getFields(this.activeTab);
        break;
      case "Master":
        this.activeTab = "fieldsMaster";
        this.getInfoTable("master");
        this.getFields(this.activeTab);
        break;
    }
  }

  //TODO
  selectedCommentState(value) {
    this.namingStatusSearch = value;
  }

  //Busqueda de namings dentro de la tabla
  getFieldsQuery() {
    this.loadingIndicator = true;
    var namingGlobalSearchTmp = this.namingGlobalSearch;
    var logicGlobalSearchTmp = this.logicGlobalSearch;
    var descGlobalSearchTmp = this.descGlobalSearch;
    var legacySearchTmp = this.legacySearch;
    var descLegacySearchTmp = this.descLegacySearch;
    var suffixSearchTmp = this.suffixSearch;
    var namingStatusSearchTmp = this.namingStatusSearch;
    if (legacySearchTmp.length == 0 && descLegacySearchTmp.length < 2 && namingGlobalSearchTmp.length == 0
      && logicGlobalSearchTmp.length == 0 && descGlobalSearchTmp.length < 2 && suffixSearchTmp.length < 0
      && namingStatusSearchTmp.length < 0) {
      this.loadingIndicator = false;
      this.cleanQuery();
      this.openSnackBar("Por favor, ingrese los datos de búsqueda.", "Error");
    } else {
      if (namingGlobalSearchTmp.length == 0) namingGlobalSearchTmp = "%20";
      if (logicGlobalSearchTmp.length == 0) logicGlobalSearchTmp = "%20";
      if (descGlobalSearchTmp.length == 0) descGlobalSearchTmp = "%20";
      if (legacySearchTmp.length == 0) legacySearchTmp = "%20";
      if (descLegacySearchTmp.length == 0) descLegacySearchTmp = "%20";
      if (suffixSearchTmp.length == 0) suffixSearchTmp = "%20";
      if (namingStatusSearchTmp.length == 0) namingStatusSearchTmp = "%20";
      this.rest.getRequest('dataDictionaries/' + this.idTable + '/fases/' + this.activeTab + '/naming=' + namingGlobalSearchTmp
        + '&logic=' + logicGlobalSearchTmp + '&desc=' + descGlobalSearchTmp + '&state=' + namingStatusSearchTmp + '&legacy='
        + legacySearchTmp + '&descLegacy=' + descLegacySearchTmp + '&suffix=' + suffixSearchTmp + '/')
        .subscribe(
          (data: any) => {
            this.fieldsTable = data;
            setTimeout(() => { this.loadingIndicator = false; }, 1000);
          }, (error) => {
            this.loadingIndicator = false;
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          });
    }
    this.table.offset = 0;
  }

  //Limpia los campos de busqueda de namings
  cleanQuery() {
    this.namingGlobalSearch = "";
    this.suffixSearch = "";
    this.logicGlobalSearch = "";
    this.legacySearch = "";
    this.namingStatusSearch = "";
    this.descLegacySearch = "";
    this.descGlobalSearch = "";
    this.table.offset = 0;
    this.getFields(this.activeTab);
  }

  //Verifica si una columna esta visible - POSIBLE DEPRECATED
  isChecked(col: string) {
    return this.columns.find(c => {
      if (c.name === col) return c.visible;
    });
  }

  //Toggle para la vista de las columnas
  toggle(col: string) {
    this.columns.find(c => {
      if (c.name === col) return c.visible = !c.visible;
    });
  }

  //Toggle para todas las columnas
  toggleAllP() {
    if (!this.allColumnsChecked) {
      for (let i = 0; i < this.columns.length; i++)
        this.columns[i].visible = true;
      this.allColumnsChecked = true;
    } else {
      for (let i = 0; i < this.columns.length; i++)
        this.columns[i].visible = !this.columns[i].visible;
      this.allColumnsChecked = !this.allColumnsChecked;
    }
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClass({ row, column, value }) {
    if (row.modification.length > 0) {
      if (row.modification[0].stateValidation)
        if (row.modification[0].stateValidation === 'NO')
          return { 'is-wrong': true };
      if (row.modification[0].state == 'GL') {
        return { 'is-global': true };
      } else if (row.modification[0].state == 'GC') {
        return { 'is-common-global': true };
      } else if (row.modification[0].state == 'PS') {
        return { 'is-proposal': true };
      } else if (row.modification[0].state == 'PC') {
        return { 'is-common-proposal': true };
      }
    }
  }

  //Devuelve la clase css para un campo legacy
  getCellClassLegacy({ row, column, value }) {
    if (row.legacy.repeat == 'Y') return { 'legacy-repeated': true }
  }

  //Devuelve el color de la fila si este es generado
  getRowClass(row: any) {
    return { 'generatedRow': row.origin == "" };
  }

  //Devuelve la clase css dependiendo del estado abierto o cerrado del naming
  getCellClassState({ row, column, value }) {
    return row.naming.governanceState === "C" ? { 'naming-closed': true } : { 'naming-open': true };
  }



  //Valida que la informacion de la tabla (objeto) tenga campos validos antes de enviar la peticion al back para actualizarlo
  objectValidation(object: any, fase: string, errFlag: boolean): boolean {
    var flag = false;
    var tmp_errors = [];
    let uuaaShortTmp: string;
    let aliasLongTablaTmp: string;

    object[fase + '_name'] = this.uuaaShort + this.aliasLongTabla;
    var temp = object[fase + '_name'].split("_");
    uuaaShortTmp = temp[0] + "_" + temp[1] + "_";
    temp.splice(0, 2);
    aliasLongTablaTmp = temp.join("_");
    object[fase + '_name'] = uuaaShortTmp + aliasLongTablaTmp.replace(/\s/g, '');
    var tmp_structure_logic = object[fase + '_name'].split("_");

    if (tmp_structure_logic.length >= 4) {
      object[fase + '_name'] = object[fase + '_name'].toLowerCase();
      var tmp_logic = object[fase + '_name'].replace(/[^a-z0-9_]/g, "");
      if (object[fase + '_name'] != tmp_logic)
        tmp_errors.push("El nombre físico del objeto continene caracteres especiales.");
    } else {
      tmp_errors.push("El nombre físico del objeto no cumple con el estándar t_uuaa_alias_descripcion");
    }
    object.baseName = object.baseName.replace(/^\s+|\s+$/g, "");
    object.baseName = object.baseName.toUpperCase()
    var tmp_base_name = object.baseName.replace(/[^A-Z0-9_-]/g, " ");
    if (object.baseName.length < 60) {
      if (object.baseName != tmp_base_name) {
        tmp_errors.push("El nombre lógico del objeto continene caracteres especiales.");
      }
    } else {
      tmp_errors.push("El nombre lógico no debe sobrepasar los 60 caracteres.");
    }
    if (object.baseName.length < 350 && object.baseName.length > 100) {
      tmp_errors.push("La descripción del objeto debe estar comprendida en una longitud de mínimo 100 caracteres y máximo 350 caracteres.");
    }
    var uuaaPropertyName = fase.charAt(0).toUpperCase() + fase.slice(1);
    object['uuaa' + uuaaPropertyName] = object['uuaa' + uuaaPropertyName].toUpperCase();
    if (object['uuaa' + uuaaPropertyName].length != 4) {
      tmp_errors.push("El código de sistema/uuaa es un campo obligatorio.");
    }
    if (object.periodicity.length < 2) {
      tmp_errors.push("La frecuencia del objeto es un campo obligatorio.");
    }
    if (object.perimeter.length < 2) {
      tmp_errors.push("El perímetro del objeto es un campo obligatorio.");
    }
    if (object.information_level.length < 2) {
      tmp_errors.push("El nivel información del objeto es un campo obligatorio.");
    }
    if (object.objectType.length < 2) {
      tmp_errors.push("El tipo del objeto es un campo obligatorio.");
    }
    if (object.loading_type.length < 2) {
      tmp_errors.push("El tipo de carga del objeto es un campo obligatorio.");
    }
    //Confirmar los errores conseguidos y mostrarlos
    if (tmp_errors.length > 0) {
      tmp_errors.unshift(object[fase + '_name'] + " - " + (fase === 'raw' ? "RAW" : "MASTER"));
      if (errFlag) {
        const dialogErr = new MatDialogConfig();
        dialogErr.disableClose = true;
        dialogErr.autoFocus = true;
        dialogErr.data = tmp_errors;
        this.dialog.open(ErrorsComponent, dialogErr);
      }
    } else {
      flag = true;
    }
    return flag;
  }

  //Actualiza los campos de longitud
  updateLengthField(value: any, row: any, fase: string) {
    if (value > 0) {
      row.length = value;
      row.outLength = Number(row.length) * (row.originType == "DECIMALEMP" ? 2 : 1);
      row.outFormat = "ALPHANUMERIC(" + row.length + ")";
      if (row.column == 0) {
        row.startPosition = 1;
        row.endPosition = Number(row.outLength);
      }
      for (let i = 1; i < this.fieldsTable.length; i++) {
        this.fieldsTable[i].startPosition = Number(this.fieldsTable[i - 1].endPosition) + 1;
        this.fieldsTable[i].endPosition = Number(this.fieldsTable[i].startPosition) + Number(this.fieldsTable[i].outLength - 1);
      }
      if (fase === 'FieldReview')
        this.updateAllAfterChangeLengthFieldReview();
    } else {
      row.length = 0;
      row.outLength = 0;
      row.outFormat = "ALPHANUMERIC(" + row.length + ")";
    }
  }

  //Actualiza el archivo actual (excel) por subir
  onFileChange(event: any) {
    if (event.target.files.length >= 1)
      this.fileList = event.target.files[0];
    else
      this.fileList = null;
  }

  //Carga la tabla legacy desde un excel
  excelLegacy() {
    var arrayBuffer: any;
    if (this.fileList != null) {
      let fileReader = new FileReader();
      let jsontmp;
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) {
          arr[i] = String.fromCharCode(data[i]);
        }
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, { type: "binary" });
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        jsontmp = XLSX.utils.sheet_to_json(worksheet, { raw: true })
        for (var j = 0; j != jsontmp.length; ++j) {
          if (jsontmp[j].format == null) {
            jsontmp[j].format = "empty";
          }
          if (jsontmp[j].decimales == null) {
            jsontmp[j].decimales = 0;
          }
          if (jsontmp[j].enteros == null) {
            jsontmp[j].enteros = 0;
          }
          if (jsontmp[j].key == null || jsontmp[j].key == "N") {
            jsontmp[j].key = 0;
          } else if (jsontmp[j].key == "S") {
            jsontmp[j].key = 1;
          }
          if (jsontmp[j].mandatory == null || jsontmp[j].mandatory == "N") {
            jsontmp[j].mandatory = 0;
          } else if (jsontmp[j].mandatory == "S") {
            jsontmp[j].mandatory = 1;
          }
        }
        for (var k = 0; k != jsontmp.length; ++k) {
          if (jsontmp[k].__rowNum__ != k + 1) {
            jsontmp.splice(k, 0, { length: 0, legacy: "empty", description: "empty", type: "CHAR", format: "empty", decimales: 0, enteros: 0, key: 0, mandatory: 0 });
          }
        }
        this.rest.putRequest('dataDictionaries/' + this.idTable + '/legacys/', JSON.stringify({user_id: this.userId,fields: jsontmp})).subscribe(
          (data: any) => {
            this.getFields('FieldReview');
            if (jsontmp.length != this.fieldsTable.length) {
              this.openSnackBar("Campos actualizados con diferencias encontradas. Longitud archivo:" + jsontmp.length + " " + "Longitud campos propuesta:" + this.fieldsTable.length, "Warning");
            } else {
              this.openSnackBar("Campos actualizados.", "Ok");
            }
          }, (error) => {
            this.openSnackBar(error.error.reason, "Error");
          }
        );
      }
      fileReader.readAsArrayBuffer(this.fileList);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
    }
  }

  //Actualiza todos los campos OUTREC si son de host
  updateAllAfterChangeLengthFieldReview() {
    for (let i = 0; i < this.fieldsTable.length; i++) {
      this.fieldsTable[i].outRec = "";
      this.fieldsTable[i].outVarchar = "";
      this.fieldsTable[i].outLength = "";
      if (this.fieldsTable[i].originType == "DECIMALEMP") {
        this.fieldsTable[i].outFormat = "ALPHANUMERIC(" + this.fieldsTable[i].length + ")";
        if (i === 0) {
          this.fieldsTable[i].outRec = "OUTREC FIELDS=(";
        }
        this.fieldsTable[i].outRec = this.fieldsTable[i].outRec + this.fieldsTable[i].startPosition + "," + this.fieldsTable[i].length + ",PD,M25";
        this.fieldsTable[i].outLength = Number(this.fieldsTable[i].length) * 2;
        this.fieldsTable[i].outVarchar = "ALPHANUMERIC(" + this.fieldsTable[i].outLength + ")";
        this.fieldsTable[i].outFormat = this.fieldsTable[i].outVarchar;
        if (i == this.fieldsTable.length - 1) {
          this.fieldsTable[i].outRec = this.fieldsTable[i].outRec + ')';
        } else {
          this.fieldsTable[i].outRec = this.fieldsTable[i].outRec + ",";
        }
      } else {
        this.fieldsTable[i].outFormat = "ALPHANUMERIC(" + this.fieldsTable[i].length + ")";
        if (i === 0) {
          this.fieldsTable[i].outRec = "OUTREC FIELDS=(";
        }
        this.fieldsTable[i].outRec = this.fieldsTable[i].outRec + this.fieldsTable[i].startPosition + "," + this.fieldsTable[i].length + "," + "TRAN=ALTSEQ";
        this.fieldsTable[i].outLength = Number(this.fieldsTable[i].length);
        this.fieldsTable[i].outVarchar = "ALPHANUMERIC(" + this.fieldsTable[i].length + ")";
        this.fieldsTable[i].outFormat = this.fieldsTable[i].outVarchar;
        if (i == this.fieldsTable.length - 1) {
          this.fieldsTable[i].outRec = this.fieldsTable[i].outRec + ')';
        } else {
          this.fieldsTable[i].outRec = this.fieldsTable[i].outRec + ",";
        }
      }
    }
    this.fieldsTable = [...this.fieldsTable];
  }

  //Actualiza los datos de información de la tabla, especificamente en raw o master
  updateRawMasterObject(object: any) {
    var res = true;
    let fase = (this.activeTab === "fieldsRaw" ? "raw" : "master");
    var partition = object["partitions_" + fase];
    if (partition.length > 0)
      res = this.validatePartitionNamings(partition, this.fieldsTable);
    if (res) {
      delete object["_id"];
      delete object["user"];
      delete object["sprint"];
      delete object["stateTable"];
      delete object["project_name"];
      this.activeObject.current_depth = Number(this.activeObject.current_depth);
      this.activeObject.estimated_volume_records = Number(this.activeObject.estimated_volume_records);
      this.activeObject.required_depth = Number(this.activeObject.required_depth);
      object["fase"] = (this.activeTab === "fieldsRaw" ? "Raw" : "Master");
      object["user_id"] = this.userId;
      if (this.objectValidation(object, fase, true)) {
        this.rest.putRequest('dataDictionaries/' + this.idTable + '/head/', JSON.stringify(object)).subscribe(
          (data: any) => {
            this.getInfoTable(fase);
            this.openSnackBar("Campos actualizados.", "Ok");
          },
          (error) => {
            this.openSnackBar(error.error.reason, "Error");
          }
        );
      }
    } else {
      this.openSnackBar("Los namings de particiones están erróneos, por favor revisar", "Error");
    }
  }



  //Valida que las particiones existan como namings en la tabla/fase
  validatePartitionNamings(partition: string, namingsTable: any) {
    var finished = false;
    var namesArr = partition.split(";");
    var valNaming: boolean;
    if (namesArr.length < 1) finished = true;
    for (var i = 0; i < namesArr.length && !finished; i++) {
      valNaming = false;
      namingsTable.find(c => {
        if (c.naming.naming === namesArr[i]) valNaming = true;
      });
      if (valNaming === false) finished = true;
    }
    return !finished;
  }


  //Actualiza un campo de la tabla con una propiedad unicamente (EJ. data.naming)
  updateValueGeneral(value: any, row: any, property: string) {
    if (property === 'length') {
      this.updateLengthField(value, row, this.activeTab);
      for (var tab of this.fieldsTable) //Actualiza el formato de gobierno de todos los namings
        this.updateValueGovernanceFormat(tab);
      this.validateFieldWithOutStatistics(row, this.activeTab);
    } else if (property === 'logic') {
      if (value.trim().length == 0) {
        value = "empty";
        row[property] = value;
        this.validateFieldWithOutStatistics(row, this.activeTab);
      } else if (!this.isRepeatedLogic(row.column, this.clearLogic(value), this.fieldsTable)) {
        row[property] = this.clearLogic(value);
        this.validateFieldWithOutStatistics(row, this.activeTab);
        return;
      } else
        this.openSnackBar("Nombre lógico repetido.", "Error");
      return;
    } else if (property === 'mandatory') {
      if (row.key == 1) {
        row[property] = 1;
        this.openSnackBar("No se puede cambiar el mandatory cuando el campo es llave", "Error");
      } else 
        row[property] = Number(value);
      this.validateFieldWithOutStatistics(row, this.activeTab);
      return;
    } else if (property === 'key') {
      if (row[property] == 0 && value == 1)
        row.mandatory = 1;
      row[property] = Number(value);
      this.validateFieldWithOutStatistics(row, this.activeTab);
      return;
    } else if (property === 'integers') {
      if (value > 0) {
        row[property] = value;
        row.governanceFormat = "DECIMAL(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
        row.format = "(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
      }
      this.validateFieldWithOutStatistics(row, this.activeTab);
      return;
    } else if (property === 'decimals') {
      if (value >= 0) {
        row[property] = value;
        row.governanceFormat = "DECIMAL(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
        row.format = "(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
      }
      this.validateFieldWithOutStatistics(row, this.activeTab);
      return;
    } else if (property === 'originType') {
      if (value.trim().length == 0) value = "empty";
      row[property] = value;
      this.updateLengthField(row.length, row, this.activeTab);
      this.updateAllAfterChangeLengthFieldReview();
      this.validateFieldWithOutStatistics(row, this.activeTab);
    } else if (property === 'destinationType') {
      if (value.includes("DECIMAL")) {
        row[property] = value;
        row.governanceFormat = "DECIMAL(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
        if (this.calculatedField(row))
          row.destinationType = row.governanceFormat;
      } else
        row[property] = value;
      this.updateValueGovernanceFormat(row);
      this.validateFieldWithOutStatistics(row, this.activeTab);
      return;
    } else if (property === 'GEN_logicalFormat') {
      row.logicalFormat = "ALPHANUMERIC(" + value.trim() + ")";
      this.validateFieldWithOutStatistics(row, this.activeTab);
    } else if (property === 'GEN_ENT_logicalFormat') {
      row.integers = Number(value.trim());
      row.logicalFormat = "DECIMAL(" + row.integers + "," + row.decimals + ")";
      row.destinationType = row.logicalFormat;
      this.validateFieldWithOutStatistics(row, this.activeTab);
    } else if (property === 'GEN_DEC_logicalFormat') {
      row.decimals = Number(value.trim());
      row.logicalFormat = "DECIMAL(" + row.integers + "," + row.decimals + ")";
      row.destinationType = row.logicalFormat;
      this.validateFieldWithOutStatistics(row, this.activeTab);
    } else if (property === 'GEN_outFormat') {
      if (value.includes("DECIMAL")) {
        row.format = "(0,0)"
        row.outFormat = "DECIMAL" + row.format;
      } else
        row.outFormat = value;
      this.validateFieldWithOutStatistics(row, this.activeTab);
    } else if (property === 'GEN_ENT_outFormat') {
      let dec = row.format.split(",")[1];
      dec = dec.split(")")[0];
      row.format = "(" + value + "," + dec + ")";
      row.outFormat = "DECIMAL" + row.format;
    } else if (property === 'GEN_DEC_outFormat') {
      let ent = row.format.split(",")[0];
      ent = ent.split("(")[1];
      row.format = "(" + ent + "," + value + ")";
      row.outFormat = "DECIMAL" + row.format;
    } else {
      if (value.trim().length == 0) value = "empty";
      row[property] = value;
      this.validateFieldWithOutStatistics(row, this.activeTab);
    }
  }

  //Metodo que devuelve el tipo de formato de gobierno futuro de un campo generado
  getFutureFormat(row: any): string {
    switch (row.outFormat) {
      case "STRING":
        return row.logicalFormat;
      case "INT":
        let lon = row.logicalFormat.split('(')[1];
        return (parseInt(lon) > 4 ? 'NUMERIC LARGE' : 'NUMERIC SHORT');
      case "INT64":
        return "NUMERIC BIG";
      case "DATE":
        return "DATE";
      case "TIMESTAMP_MILLIS": case "TIMESTAMP":
        return "TIMESTAMP";
    }
    return "I am alone";
  }

  //Metodo que modifica un campo de la tabla en donde tenga dos propiedades dentro del JSON (EJ. data.naming.naming)
  updateValueTwice(event: any, row: any, propertyOne: string, propertyTwo: string) {
    if (propertyTwo === "naming") {
      if (event.trim().length > 0) {
        if (!this.isNamingRepeated(row.column, event.trim(), this.fieldsTable)) {
          row[propertyOne][propertyTwo] = event.toLowerCase().trim();
          this.checkNamingGlobal(row, event.trim().toLowerCase(), this.activeTab);
          this.validateFieldWithOutStatistics(row, this.activeTab);
        } else {
          this.openSnackBar("Naming repetido.", "Error");
        }
      } else {
        event = "empty";
        row[propertyOne][propertyTwo] = "empty"; //event.target.value
        this.validateFieldWithOutStatistics(row, this.activeTab);
      }
      if (this.calculatedField(row) && this.activeTab === 'fieldsMaster') {
        row.destinationType = "empty";
        row.logicalFormat = "empty";
      }
    } else {
      if (event.trim().length == 0) event = "empty";
      row[propertyOne][propertyTwo] = event;
      this.validateFieldWithOutStatistics(row, this.activeTab);
    }
  }

  //Metodo que envia una peticion POST para actualizar un campo de la tabla
  validateFieldWithOutStatistics(row, fase) {
    this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=%20/',
      JSON.stringify({ notSave: "notSave", row: row }))
      .subscribe(
        (data: any) => {
          //this.openSnackBar("Se han guardado los cambios.","Ok");
        }, (error) => {
          //this.openSnackBar("Error procesando petición al servidor.","Error");
        });
  }

  //Valida naming, logico y descripcion para evitar duplicacion de datos por medios externosk
  validateRepeatedValues(index: number, newValue: string) {
    let errorProps = "";
    let tmpName = "";
    let tmpLogic = "";
    let tmpDesc = "";

    tmpName = newValue;
    tmpLogic = this.fieldsTable[index].logic;
    tmpDesc = this.fieldsTable[index].description;
    if (this.isNamingRepeated(index, tmpName, this.fieldsTable))
      errorProps += "Naming"
    /*
    if(this.isRepeatedLogic(index, tmpLogic, this.fieldsTable)){
      if(errorProps.length > 0) errorProps += ", "
      errorProps += "Nombre Lógico"
      this.fieldsTable[index].logic = "empty";
    }
    if(this.isRepeatedDesc(index, tmpDesc, this.fieldsTable)){
      if(errorProps.length > 0) errorProps += ", "
      errorProps += "Descripcion"
      this.fieldsTable[index].description = "empty";
    }
    */
    if (errorProps.length > 0) {
      this.openSnackBar(errorProps + (errorProps.length === 1 ? " esta repetido" : " están repetidos"), "Error");
      return false;
    }
    return true;
  }

  //Comprueba que el naming no este repetido en la tabla y fase actual
  isNamingRepeated(index, naming, fieldsTableTmp) {
    let flag = false;
    if (naming.trim() !== "empty")
      for (let i = 0; i < fieldsTableTmp.length && !flag; i++) {
        if (fieldsTableTmp[i].naming.naming === naming && fieldsTableTmp[i].column != index) {
          flag = true;
          break;
        }
      }
    return flag;
  }

  //Verifica si el naming esta repetido
  isRepeatedLogic(index, logicNaming, fieldsTableTmp) {
    let flag = false;
    if (logicNaming.trim() !== "empty")
      for (let i = 0; i < fieldsTableTmp.length && !flag; i++) {
        if (fieldsTableTmp[i].logic === logicNaming && fieldsTableTmp[i].column != index) {
          flag = true;
          break;
        }
      }
    return flag;
  }

  //Verifica si la descripcion esta repetido
  isRepeatedDesc(index: number, description: string, fieldsTableTmp: any) {
    let flag = false;
    if (description.trim() !== "empty")
      for (let i = 0; i < fieldsTableTmp.length && !flag; i++) {
        if (fieldsTableTmp[i].description === description && fieldsTableTmp[i].column != index) {
          flag = true;
          break;
        }
      }
    return flag;
  }

  //Busca si el naming esta en el repositorio global
  checkNamingGlobal(row: any, naming: string, fase: string) {
    this.rest.getRequest('namings/' + naming).subscribe(
      (data: any) => {
        var p = /~/gi;
        if (data != null) {
          row.naming.isGlobal = data.naming.isGlobal;
          row.logic = data.logic;
          row.description = data.originalDesc.replace(p, "\n");
          row.naming.code = data.code;
          row.naming.codeLogic = data.codeLogic;
          row.naming.Words = data.naming.Words;
          row.naming.suffix = data.naming.suffix;
          row.naming.hierarchy = data.level;
        } else {
          row.naming.isGlobal = 'N';
          row.naming.code = "empty";
          row.naming.codeLogic = "empty";
          row.naming.hierarchy = "";
          row.naming.Words = [];
        }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Limpia el nombre logico (?)
  clearLogic(logic: string) {
    let tmp = logic.toUpperCase();
    let logicRem = "";
    let i = 0;
    while (i < logic.length)
      if (tmp.charCodeAt(i) == 209) {
        if (i + 1 < logic.length && tmp.charCodeAt(i + 1) == 73) {
          logicRem += "NI";
          i += 2;
        } else {
          logicRem += "NI";
          i++;
        }
      } else {
        logicRem += tmp.charAt(i);
        i++
      }
    return logicRem;
  }

  //Agrega los namings de particiones al campo de particiones de la tabla
  formatPartitionRoute(type: number) {
    let newpart = (type === 1 ? "partition_data_day_id" : type === 2
      ? "partition_data_month_id" : "partition_data_year_id");

    if (this.activeTab === 'fieldsRaw') {
      this.activeObject.partitions_raw = this.activeObject.partitions_raw.length === 0 ?
        newpart : this.activeObject.partitions_raw + ";" + newpart;
    } else if (this.activeTab === 'fieldsMaster') {
      this.activeObject.partitions_master = this.activeObject.partitions_master.length === 0 ?
        newpart : this.activeObject.partitions_master + ";" + newpart;
    }
  }

  //Actualiza el formato de gobierno de la fila
  updateValueGovernanceFormat(row: any) {
    if (!this.blockers[row.column]) {
      row.outFormat = "ALPHANUMERIC(" + row.length + ")";
      if (row.destinationType.match(/STRING/g)) {
        if (this.showHostTableFields) {
          row.governanceFormat = "ALPHANUMERIC(" + row.outLength + ")";
          row.format = "empty";
        } else {
          row.governanceFormat = "ALPHANUMERIC(" + row.length + ")";
          row.format = "empty";
        }
      } else if (row.destinationType.match(/DATE/g)) {
        row.governanceFormat = "DATE";
        row.format = "yyyy-MM-dd";
      } else if (row.destinationType.match(/INT64/g)) {
        row.governanceFormat = "NUMERIC BIG";
        row.format = "empty";
      } else if (row.destinationType.match(/INT/g)) {
        row.governanceFormat = "NUMERIC SHORT";
        row.format = "empty";
        if (this.showHostTableFields) {
          if (row.outLength > 4) row.governanceFormat = "NUMERIC LARGE";
        } else if (row.length > 4) row.governanceFormat = "NUMERIC LARGE";
      } else if (row.destinationType.match(/TIMESTAMP/g)) {
        row.governanceFormat = "TIMESTAMP";
        row.format = "yyyy-MM-dd HH:mm:ss.SSSSSS";
      } else if (row.destinationType.match(/DECIMAL/g)) {
        if (row.decimals < 1 && row.integers < 1)
          this.openSnackBar("Por favor, diligencie los valores decimales y enteros.", "Error");
        row.governanceFormat = "DECIMAL(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
        row.format = "(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
      }
    }
  }

  //Abre el popup de los comentarios
  commentsWindows(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      fase: this.activeTab,
      user: sessionStorage.getItem("userId"),
      userRol: sessionStorage.getItem("rol"),
      row: row,
      idTable: this.idTable
    };
    this.dialog.open(CommentComponentComponent, dialogConfig);
  }

  commentsWindowsVoBo(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      vobo: true,
      fase: this.activeTab,
      user: sessionStorage.getItem("userName"),
      userRol: sessionStorage.getItem("rol"),
      row: row.check,
      naming: row.naming,
      column: row.column,
      idTable: this.idTable
    };
    this.dialog.open(CommentComponentComponent, dialogConfig);
  }

  //DEPRECATED muestra el dialogo del historial de comentarios
  historyComment(row: any) {
    const dialogConfiguration = new MatDialogConfig();
    dialogConfiguration.disableClose = true;
    dialogConfiguration.autoFocus = true;
    dialogConfiguration.data = {
      type: "comments",
      comments: row.check.comments
    };
    this.dialog.open(ErrorsComponent, dialogConfiguration);
  }

  //Retorna la clase CSS para el cuadro de VoBo
  getStatusRow({ row, column, value }) {
    if (row.check !== undefined)
      if (row.check.status == 'NOK') {
        return { 'is-nok': true };
      } else if (row.check.status == 'OK') {
        return { 'is-ok': true };
      }
  }

  //Valida la fila
  rowValidation(row: any, fase: string, fieldsTableTmp: any) {
    let isValid = false;
    row.naming.naming = row.naming.naming.trim();
    let tmpSuffix = row.naming.naming.split('_');
    let index = tmpSuffix.indexOf('');
    let suffix = tmpSuffix[tmpSuffix.length - 1];
    this.errors.push("*" + (row.column + 1) + " - " + row.naming.naming + ": \n");
    if (index === -1) {
      if (row.naming.naming !== "empty") {
        this.checkNamingGlobal(row, row.naming.naming, fase);
        isValid = this.validateNaming(row, suffix) && (fase === 'FieldReview' ? this.validateLogicNamingReview(row, fase, fieldsTableTmp)
        : this.validateLogicNaming(row, fase, fieldsTableTmp));
        /*
        if (row.naming.isGlobal === 'N') {
          isValid = this.validateNaming(row, suffix) && (fase === 'FieldReview' ? this.validateLogicNamingReview(row, fase, fieldsTableTmp)
            : this.validateLogicNaming(row, fase, fieldsTableTmp));
        } else {
          isValid = (fase === 'FieldReview' ? this.validateLogicNamingReview(row, fase, fieldsTableTmp)
            : this.validateLogicNaming(row, fase, fieldsTableTmp));
        }
        */
        if (fase !== 'fieldsRaw') {
          if (row.destinationType.includes("(")) {
            if (!this.dataTypeList(row, fase).includes(row.destinationType.split("(")[0])) {
              isValid = false;
              this.errors.push("El tipo de dato de destino no concuerda con el sufijo del naming.\n");
            }
          } else if (!this.dataTypeList(row, fase).includes(row.destinationType)) {
            isValid = false;
            this.errors.push("El tipo de dato de destino no concuerda con el sufijo del naming.\n");
          }
        }
      } else {
        this.errors.push("El naming propuesto para el campo, se encuentra vacío. \n");
      }
    } else {
      this.errors.push("Por favor revisar el naming. El símbolo '_' está repetido de seguido. \n");
    }
    return isValid;
  }

  //Valida si el naming empieza con 'gl_', 'gf_' ó 'g_' indicando que es de un modelo global
  //el cual no pertenece a una tabla
  validateGlobalModel(naming: string) {
    let values = ["gf", "g"];
    let tmp = naming.split("_")[0]
    return values.includes(tmp);
  }

  //Valida un naming solamente
  validateNaming(row: any, suffix: string): boolean {
    let isValid = false;
    let flagAll = true;
    let suffixIndex = -1;
    if (row.naming.naming == "empty") this.errors.push("Por favor, diligenciar el campo naming" + "\n");
    if (!this.inputValidatorNaming(row.naming.naming)) this.errors.push("El naming no puede empezar con números" + "\n");
    if (this.validateGlobalModel(row.naming.naming)) {
      this.errors.push("El naming no puede empezar con 'gl' ó 'g' en una tabla, son para modelos globales" + "\n");
      flagAll = false;
    }
    if (row.logic == "empty") this.errors.push("Por favor, diligenciar el campo nombre lógico" + "\n");
    if (row.description == "empty") this.errors.push("Por favor, diligenciar el campo descripción" + "\n");
    if (row.naming.isGlobal === 'N' && row.naming.naming.length > 30)
      this.errors.push("El naming no es global y tiene mas de 30 caracteres" + "\n");
    else if (row.naming.naming != "empty" && row.description != "empty" && row.logic != "empty") {
      for (let i = 0; i < this.suffixes.length; i++)
        if (this.suffixes[i].suffix === suffix) {
          suffixIndex = i;
          break;
        }
      if (row.naming.suffix === -1) this.addCheckSuffix(row, suffix);
      if (suffixIndex !== -1) {
        let flagS = false;
        let flagD = false;
        for (let j = 0; j < this.suffixes[suffixIndex].Logic.length && !flagS; j++) {
          if (row.logic.startsWith(this.suffixes[suffixIndex].Logic[j])) {
            flagS = true;
            break;
          }
        }
        for (let j = 0; j < this.suffixes[suffixIndex].DESCRIPTION.length && !flagD; j++) {
          if (row.description.toUpperCase().startsWith(this.suffixes[suffixIndex].DESCRIPTION[j].toUpperCase())) {
            flagD = true;
            break;
          }
        }
        if (!flagS) this.errors.push("El nombre lógico no concuerda con el sufijo del naming." + "\n");
        if (!flagD) this.errors.push("La descripción no concuerda con el sufijo del naming." + "\n");
        isValid = (flagS && flagD) && flagAll; //Aqui se puede volver true 
      } else {
        this.errors.push("El súfijo no es válido." + "\n");
      }
      if (row.description.toUpperCase() == row.logic) {
        this.errors.push("El nombre lógico y la descripción son iguales." + "\n");
      }
    }
    return isValid;
  }

  //Devuelve la lista de tipos de datos validos para el sufijo
  dataTypeList(row: any, fase: string) {
    let suffix = this.checkNamingSuffix(row);
    let array = [];
    switch (suffix) {
      case "type": case "id": case "desc": case "name":
        array = ["STRING"];
        break;
      case "per": case "amount":
        array = ["DECIMAL"];
        break;
      case "number":
        array = ["INT", "INT64", "DECIMAL"];
        break;
      case "date":
        array = ["DATE"];
        if (fase === "FieldReview") {
          array.push("TIMESTAMP");
          if (row.naming.isGlobal === "Y") array.push("STRING");
        } else
          array.push("TIMESTAMP");
        break;
      case "":
        array = [];
        break;
    }
    this.arraySuffixTmp = array;
    return array;
  }

  cleanDestinationTypeValues(row) {
    this.resetDestinationType(row);
    let suffix = this.checkNamingSuffix(row);
    if (suffix !== "amount" && suffix !== "number") {
      row.decimals = 0; row.integers = 0;
    }
  }

  //Verifica si el cambio del sufijo del naming implica otro tipo de destino y lo reinicia (x = "") 
  resetDestinationType(row: any) {
    this.dataTypeList(row, this.activeTab);
    if (!this.arraySuffixTmp.includes(row.destinationType)) {
      row.destinationType = "empty";
      row.governanceFormat = "empty";
      row.format = "empty";
    }
  }

  //Verifica el sufijo del naming actual
  checkNamingSuffix(row: any): string {
    let tmpSuffix = row.naming.naming.trim().split('_');
    let suffix = tmpSuffix[tmpSuffix.length - 1];
    let suffixIndex = -1;
    for (let i = 0; i < this.suffixes.length; i++)
      if (this.suffixes[i].suffix === suffix) {
        suffixIndex = i;
        break;
      }
    if (suffixIndex !== -1)
      return suffix;
    return "";
  }

  //Modifica la estructura del naming para agregar el sufix adecuado
  addCheckSuffix(row: any, suffix: string) {
    switch (suffix) {
      case "id": row.naming.suffix = 1; break;
      case "type": row.naming.suffix = 2; break;
      case "amount": row.naming.suffix = 3; break;
      case "date": row.naming.suffix = 4; break;
      case "name": row.naming.suffix = 5; break;
      case "desc": row.naming.suffix = 6; break;
      case "per": row.naming.suffix = 7; break;
      case "number": row.naming.suffix = 8; break;
    }
  }

  //Valida el nombre y descripcion legacy
  validateLogicNaming(row: any, fase: string, fieldsTableTmp: any): boolean {
    let isValid = false;
    if (this.isNamingRepeated(row.column, row.naming.naming, fieldsTableTmp)) {
      this.errors.push("Naming repetido." + "\n");
    } else {
      if (row.destinationType == "empty") this.errors.push("Por favor, diligenciar el campo tipo de destino." + "\n");
      if (row.legacy.legacy == "empty")
        this.errors.push("Por favor, diligenciar el campo del nombre origen del naming." + "\n");
      ((row.destinationType != "empty" && row.legacy.legacy != "empty") ? isValid = true : //Aqui se puede volver true
        this.errors.push("El campo del nombre origen del naming y el tipo destino deben diligenciarse." + "\n"));
    }
    return isValid;
  }

  //Valida el nombre y descripcion legacy de la fase de propuesta (tiene mas validaciones)
  validateLogicNamingReview(row: any, fase: string, fieldsTableTmp: any): boolean {
    let isValid = false;
    if (this.isNamingRepeated(row.column, row.naming.naming, fieldsTableTmp)) {
      this.errors.push("Naming repetido." + "\n");
    } else {
      if (this.validateGlobalModel(row.naming.naming)) this.errors.push("El naming no puede empezar con 'gf', 'gl' ó 'g' en una tabla, son para modelos globales" + "\n");
      if (row.length <= 0) this.errors.push("Por favor, diligenciar el campo longitud." + "\n");
      if (row.originType == "empty")
        this.errors.push("Por favor, diligenciar el campo tipo de origen." + "\n");
      if (row.destinationType == "empty")
        this.errors.push("Por favor, diligenciar el campo tipo de destino." + "\n");
      if (row.legacy.legacy == "empty")
        this.errors.push("Por favor, diligenciar el campo legacy." + "\n");
      if (row.legacy.legacyDescription == "empty")
        this.errors.push("Por favor, diligenciar el campo descripción legacy." + "\n");
      if (row.length > 0 && row.originType != "empty" && row.destinationType != "empty" && row.legacy.legacy != "empty"
        && row.legacy.legacyDescription != "empty") {
        if (row.destinationType.search("DECIMAL") >= 0) {
          if (row.decimals < 1 && row.integers < 1) {
            this.errors.push("Por favor, diligencie los valores decimales y enteros." + "\n");
          } else {
            isValid = true;
          }
        } else {
          isValid = true;
        }
      }
    }
    return isValid;
  }

  //Elimina la fila
  deleteFieldReview(row, fase) {
    let index = this.fieldsTable.length === row.column + 1 ? row.column - 1 : row.column;
    this.rest.putRequest('dataDictionaries/' + this.idTable + '/fields/',
      JSON.stringify({ action: "substract", column: row.column, field: fase, user_id : this.userId})).subscribe(
        (data: any) => {
          this.getFields(fase).then((data: any) => {
            this.updateValueGeneral(this.fieldsTable[index].length,
              this.fieldsTable[index], 'length');
          });
        }, (error) => {
          this.openSnackBar("No se ha podido eliminar el campo.", "Error");
        }
      );
  }

  //Agrega una fila una posicion arriba
  addAboveFieldReview(row, fase) {
    this.rest.putRequest('dataDictionaries/' + this.idTable + '/fields/',
      JSON.stringify({ action: "insert", column: row.column, field: fase , user_id : this.userId}))
      .subscribe(
        (data: any) => {
          this.getFields(fase).then((res: any) => {
            this.updateValueGeneral("1", this.fieldsTable[row.column], 'length');
          });
        }, (error) => {
          this.openSnackBar("No se ha podido agregar el campo.", "Error");
        }
      );
  }

  //Agrega una fila una posicion abajo
  addBelowFieldReview(row, fase) {
    this.rest.putRequest('dataDictionaries/' + this.idTable + '/fields/',
      JSON.stringify({ action: "insert", column: row.column + 1, field: fase, user_id : this.userId}))
      .subscribe(
        (data: any) => {
          this.getFields(fase).then((res: any) => {
            this.updateValueGeneral("1", this.fieldsTable[row.column + 1], 'length');
          });
        }, (error) => {
          this.openSnackBar("No se ha podido agregar el campo.", "Error");
        }
      );
  }

  addCleanModification(row: any, validation: boolean) {
    var newModi = {
      //name: sessionStorage.getItem("name"),
      startDate: new Date(),
      state: "",
      user: this.userId,
      stateValidation: (validation ? "YES" : "NO")
    }
    if (row.modification.length >= 5)
      row.modification.splice(4, row.modification.length - 1);
    row.modification.unshift(newModi);
  }

  //Valida la fila
  stateValidation(row: any, fase: string) {
    this.loadingIndicator = true;
    let resu = this.vali.basicNamingValidation(row, {title: true, dataType: "destinationType"}, this.tableData);
    this.errors = resu.errors;
    let validation = resu.valid;
    this.addCleanModification(row, validation);
    this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=' + this.userId + '/', JSON.stringify({ row: row })).subscribe(
      (data: any) => {
        validation ? this.errors.push("Campo actualizado." + "\n") : this.errors.push("Campo con errores" + "\n", "Error")
        this.updateFields(this.activeTab);
      }, (error) => {
        if (error.error.reason.length > 0) {
          this.openSnackBar(error.error.reason, "Error");
        } else {
          this.openSnackBar("Error procesando petición al servidor", "Error");
        }
      }
    );
    this.openErrorsComponent({ pretty: true, errors: this.errors });
  }

  //Peticion al back que actualiza toda la fila de un naming
  updateNamingRow(row: any, fase: string) {
    this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=%20/',
      JSON.stringify({ notSave: "notSave", row: row })).subscribe(
        (data: any) => {
          //this.openSnackBar("Se han guardado los cambios.","Ok");
        }, (error) => {
          console.error(JSON.stringify(error));
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
  }

  //Abre el dialog de errores de namings y objects
  openErrorsComponent(data: any) {
    const dialogErr = new MatDialogConfig();
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.minWidth = "50%";
    dialogErr.data = data;
    this.dialog.open(ErrorsComponent, dialogErr);
    this.loadingIndicator = false;
  }


  //Actualiza el estado de la fila al backend
  putUpdateStatesRow(row: any, action: string, fase: string) {
    return this.rest.putRequest('dataDictionaries/' + this.idTable + '/fields/',
      JSON.stringify({ action: action, column: row.column, field: fase, user_id : this.userId}))
      .subscribe(
        (data: any) => {
          return true;
        }, (error) => {this.idTable + '/fase=' + fase + '&user='
          return false;
        }
      );
  }

  //Valida todo
  allValidations(fase: string, showMessages: boolean) {
    let validation;
    var arr = [];
    var flago = true;
    this.errors = [];
    for (let i = 0; i < this.fieldsTable.length; i++) {
      let resu = this.vali.basicNamingValidation(this.fieldsTable[i], {title: true, dataType: "destinationType"}, this.tableData);
      this.errors = this.errors.concat(resu.errors);
      let validation = resu.valid;
      this.addCleanModification(this.fieldsTable[i], validation);
      flago = validation ? flago : false;
    }
    if (showMessages) 
      this.openErrorsComponent({ pretty: true, errors: this.errors });
    else 
      this.errors = [];

    return flago;
  }

  //TO BE DEPRECATED
  //Actualiza las validaciones de los campos de la tabla en una fase especifica
  updateAllValidations(fields: any, fase: string) {
    if (fields.length > 0) {
      this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=' + this.userId + '/',
        JSON.stringify({ all: "", row: fields }))
        .subscribe(
          (data: any) => {
            this.updateFields(fase);
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          }
        );
    }
  }

  //Verifica si la fila/naming es calculado
  calculatedField(row: any): boolean {
    if (this.activeTab != "FieldReview")
      return row.origin == "";
    return false;
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  public inputValidatorNaming(value: any) {
    const pattern = /^[a-z][a-z0-9_]*$/;
    if (!pattern.test(value))
      value = value.replace(/^[a-z][^a-z0-9_]/g, "");
    return pattern.test(value);
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para logicos
  public inputValidatorLogic(event: any) {
    const pattern = /^[a-zA-Z0-9 ]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9 ]/g, "");
    }
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
  }

  

  //Abre el enlace externo para el template de las plantillas de carga
  openExternalTemplateLink() {
    window.open("https://docs.google.com/spreadsheets/d/17DLxrve0_J2Xhph2SquZhLO5B4QRl0exM4zyZHAjFkk/edit#gid=813161028", '_blank');
  }


  //Por depreciar
/////////////////////////////////////////////////////////////

  //Bloquea la fila actual para edicion
  stateBlocker(row: any, fase: string) {
    this.blockers[row.column] = !this.blockers[row.column];
    if (this.blockers[row.column]) {
      this.blockRow(row, fase);
    } else {
      this.unblockRow(row, fase);
    }
  }

  //Bloquea la fila
  blockRow(row: any, fase: string) {
    if (this.putUpdateStatesRow(row, 'blocker', fase)) {
      this.openSnackBar("El campo se ha bloqueado exitosamente.", "Ok");
    } else {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    }
  }

  //Desbloquea la fila
  unblockRow(row: any, fase: string) {
    if (this.putUpdateStatesRow(row, 'unBlocker', fase)) {
      this.openSnackBar("El campo se ha desbloqueado exitosamente.", "Ok");
    } else {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    }
  }

}
