import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NamingInfoComponent } from '../edit-tablon/naming-info/naming-info.component';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-view-tablon',
  templateUrl: './view-tablon.component.html',
  styleUrls: ['./view-tablon.component.scss']
})
export class ViewTablonComponent implements OnInit {

  idTablon: string;
  rowsLimit: number = 5;

  activeHelp: string = "";
  lastToggleNamingNumber: number;
  showAddOriginNaming: boolean;
  originsDataTable: any = ["MG", "PG", "ML", "PL"];
  suffixesOptions: any;

  tablonInfo: any = {
    "modifications": {"user_name":"", "date":""},
    "created_time":""
  };
  tablonFields: any = [];
  originTables: any = [];
  currAmbito: string;
  currUUAA: string;
  currScope: string;
  descAlias: string;

  namingQuery = "";
  typeQuery = "";
  logicQuery = "";
  descQuery = "";
  stateQuery = "";

  aliases: any = [];
  selectedOriginOptions: any = [];
  stateTableClean: string;
  viewAuditFlag = false;
  editableFlag: boolean = false;

  ambitoOptions = []

  formControlBaseName = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlAmbito = new FormControl('', [Validators.required, Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlScope = new FormControl('', Validators.required);
  formControlAlias = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlDesc = new FormControl('', [Validators.required, Validators.minLength(4)]);
  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);

  @ViewChild('myTable') table: any;

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router
    , public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.route.queryParams.subscribe(params => {
      this.idTablon = params["idTablon"];
      this.getSuffixes();
      this.getAmbitos();
      this.getInfoTablon();
      this.getFieldsTablon();
      
    });
  }

  ngOnInit() {
  }

  //Peticion al back que devuelve el arreglo con los posibles sufijos
  getSuffixes() {
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixesOptions = data;
      });
  }

  getAmbitos() {
    this.rest.getRequest('uuaa/ambitos/').subscribe((data: any) => {
      this.ambitoOptions = data;
    });
  }

  //Peticion al back que devuelve la información de la tabla que no tengan que ver con campos ni namings
  getInfoTablon() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/head/').subscribe(
      (data: any) => {
        this.tablonInfo = data;
        this.originTables = data.origin_tables;
        if(this.tablonInfo.created_time != "" && 
        this.tablonInfo.modifications.user_name != "" && 
        this.tablonInfo.modifications.date != ""){
        this.viewAuditFlag = true;
      }
        this.cleanStatesTable();
        this.loadOriginTablesOptions();
        //this.getProjectData();
        this.loadAmbito();
        this.loadUUAA();
        this.loadAliasDesc();
        if (this.tablonInfo.stateTable === "G" || this.tablonInfo.stateTable === "N")
          this.editableFlag = true; 
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  openEdit(){
    this.router.navigate(["tablon/edit"], { queryParams: { idTablon: this.idTablon } });
  }

  //Initializa el campo de ambito local dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAmbito() {
    for (let ambito of this.ambitoOptions) {
      if (this.tablonInfo.uuaaMaster[0] == ambito["letter"]) {
        this.currAmbito = "" + ambito["letter"] + " - " + ambito["country"];
      }
    }
  }

  //Initializa el campo de la uuaa sin la letra del pais(ambito) dependiendo de la uuaa del tablon para su posible modificación y validación
  loadUUAA() {
    this.formControlUUAA.setValue(this.tablonInfo.uuaaMaster.substring(1));
    this.currUUAA = this.tablonInfo.uuaaMaster.substring(1);
  }

  //Initializa el campo de la descripcion del alias dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAliasDesc() {
    this.currScope = (this.tablonInfo.alias[0] + this.tablonInfo.alias[1]).toUpperCase();
    if (this.currAmbito === "Global - K") {
      this.descAlias = this.tablonInfo.master_name.substring(7);
    } else
      this.descAlias = this.tablonInfo.master_name.substring(10);
  }

  //Carga las tablas de origen en el arreglo de options para mostrar en la tabla de origen
  loadOriginTablesOptions() {
    let tableIds = [];
    let dataTablesIds = [];
    for (var tab of this.tablonInfo.origin_tables)
      tab.originType === "DT" ? dataTablesIds.push(tab._id) : tableIds.push(tab._id);

    this.prepareOriginTablesOptions(tableIds, "T").then((res: any) => {
      this.selectedOriginOptions = (this.selectedOriginOptions.length < 1 ?
        res : this.selectedOriginOptions.concat(res));
      this.prepareOriginTablesOptions(dataTablesIds, "DT").then((res: any) => {
        this.selectedOriginOptions = (this.selectedOriginOptions.length < 1 ?
          res : this.selectedOriginOptions.concat(res));
        //this.validateDuplicateOriginTables();
      });
    });
  }

  //Prepara las promesas para cargar las tablas de origen al arreglo de options
  prepareOriginTablesOptions(tableIds, typeOrigin) {
    var promise = new Promise((resolve, reject) => {
      let promises = [];
      if (typeOrigin === 'DT')
        for (let id of tableIds)
          promises.push(this.requestPromiseDT(id))
      else
        for (let id of tableIds)
          promises.push(this.requestPromise(id))
      Promise.all(promises)
        .then((result) => {
          resolve(result);
        })
    });
    return promise;
  }

  //Peticion que llama la peticion para preparar la tabla de origen dependiendo de su fase
  requestPromise(tabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + tabId + '/head/raw/').toPromise()
        .then((res: any) => {
          if (res !== null)
            this.checkOriginFase(res, tabId).then(row => {
              resolve(row);
            })
          else resolve({});
        });
    });
    return promise;
  }

  //Funcion que prepara la tabla de origen dependiendo de la fase que este seleccionada para mostrar
  //en la vista de tablas de origen
  checkOriginFase(row: any, id: string) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + id + '/head/' + 'FieldReview/').toPromise()
        .then(res => {
          let index = this.tablonInfo.origin_tables.map(function (e) { return e._id; }).indexOf(id);
          if (index >= 0) {
            let name = this.tablonInfo.origin_tables[index].originName;
            let fase = (name.includes(res.uuaaRaw.toLowerCase()) ? "RAW" : "MASTER");
            row.faseSeleccionada = fase;
            this.tablonInfo.origin_tables[index].originSystem = (fase === "RAW" ? "HDFS-Avro" : "HDFS-Parquet");
            this.tablonInfo.origin_tables[index].originType = "T";
          }
          row._id = id;
          resolve(row);
        })
    });
    return promise;
  }

  fieldsQuery() {
    //console.log("> " + this.namingQuery + this.logicQuery + this.descQuery + this.stateQuery + this.typeQuery);
    if(this.namingQuery == "" && this.logicQuery == "" && this.descQuery == ""
      && this.stateQuery == "" && this.namingQuery == "" && this.typeQuery == "") {
        this.openSnackBar("Digite valores de búsqueda", "Error");
    } else {
      this.rest.getRequest('dataTables/' + this.idTablon 
        + '/suffix='+ (this.typeQuery == "" ? "%20" : this.typeQuery)
        + '&naming=' + (this.namingQuery == "" ? "%20" : this.namingQuery) 
        + '&logic=' + (this.logicQuery == "" ? "%20" : this.logicQuery)  
        + '&desc=' + (this.descQuery == "" ? "%20" : this.descQuery)  
        + '&state=' + (this.stateQuery == "" ? "%20" : this.stateQuery) + '/')
        .subscribe(
          (data: any) => {
            this.tablonFields = data;
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
            console.log(error);
          });
    }
  }

  cleanQuery(){
    this.namingQuery = "";
    this.logicQuery = "";
    this.descQuery = "";
    this.stateQuery = ""; 
    this.namingQuery = ""; 
    this.typeQuery = "";
    this.getFieldsTablon();
  }

  //Peticion que llama la peticion para preparar la tabla de origen dependiendo de su fase
  requestPromiseDT(datatabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataTables/' + datatabId + '/head/').toPromise()
        .then((res: any) => {
          if (res !== null) resolve(res);
          else resolve({});
        });
    });
    return promise;
  }
  
  //Peticion al back para que traiga todas las filas (namings) del tablón
  getFieldsTablon() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/fields/').subscribe(
      (data: any) => {
        this.tablonFields = data;
        for (var tab of this.tablonFields)
          this.calcUniqueAlias(tab);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Imprime el estado del tablon natural dependiendo del codigo
  cleanStatesTable() {
    switch (this.tablonInfo.stateTable) {
      case "G": this.stateTableClean = "Gobierno"; break;
      case "N": this.stateTableClean = "Propuesta"; break;
      case "RN": this.stateTableClean = "Revisión Visto Bueno"; break;
      case "P": case "A": this.stateTableClean = "Producción"; break;
      case "I": this.stateTableClean = "Listo para Ingestar"; break;
      case "D": this.stateTableClean = "Rechazada"; break;
    }
  }

  //Calcula los alias de las tablas de origen de un naming y los agrega a un arreglo global sin campos repetidos
  calcUniqueAlias(row: any) {
    var tempArr = [];
    for (var tab of row.originNamings)
      if (tab.hasOwnProperty("originAlias")) {
        tempArr.push(tab.originAlias);
        //tempArr.push(JSON.parse(JSON.stringify({ alias: tab.originAlias, type: tab.originType })));
      } else
        tempArr.push(this.calcAlias(tab.table_name, tab.originType));
    this.aliases[row.column] = Array.from(new Set(tempArr));
  }

  //Agrega el alias de una tabla de origen a su estructura
  calcAlias(name: string, type: string) {
    var temp = name.split("_");
    temp.splice(0, 2);
    if (type === "DT")
      if (temp[0] && this.originsDataTable.includes(temp[0].toUpperCase()))
        return name.substring(10);
      //else
        //return name.substring(7);
    return temp[0];
  }

  //Funcion que verifica a que fase pertenence la tabla de origen si es que pertenence a una
  disabledFaseCheck(row: any, fase: string) {
    if (row.hasOwnProperty('faseSeleccionada'))
      if (row.faseSeleccionada !== fase)
        return true;
      else
        return false;
    else
      return true;
  }

    //Funcion que abre el popup para los detalles del naming de origen llamando la peticion que trae la info completa
  //y abriendo el popup con un vinculo a la tabla original
  namingDetailsFromOrigin(naming: any) {
    this.getNamingFromOrigin(naming).then(res => {
      this.namingDetails(res["naming"], res["origin"]);
    });
  }

  //Peticion que trae la info completa del naming de origen
  getNamingFromOrigin(naming: any) {
    if(naming.originType && naming["originType"] === "DT"){
      var promise = new Promise((resolve, reject) => {
        this.rest.getRequest('dataTables/' + naming.table_id + '/fields/').subscribe(
          (data: any) => {
            for (var obj of data)
              if (obj.naming.naming === naming.naming){
                let resu = { naming: obj, origin:{ _id: naming.table_id, originType: "DT"} }
                resolve(resu);
              }
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
            resolve(error);
          });
      });
      return promise;
    } else{
      var promise = new Promise((resolve, reject) => {
        this.rest.getRequest('dataDictionaries/' + naming.table_id + '/head/' + 'FieldReview' + '/').toPromise()
          .then(res => {
            let fase = (naming.table_name.includes(res.uuaaRaw.toLowerCase()) ? "fieldsRaw" : "fieldsMaster");
            this.rest.getRequest('dataDictionaries/' + naming.table_id + '/fields/' + fase + '/').subscribe(
              (data: any) => {
                for (var obj of data)
                  if (obj.naming.naming === naming.naming){
                    let resu = { naming: obj, origin:{ _id: naming.table_id, originType: "T"} }
                    resolve(resu);
                  }
              }, (error) => {
                this.openSnackBar("Error procesando petición al servidor.", "Error");
                resolve(error);
              });
          }, (error) => {
            this.openSnackBar("Error con origen de tablón, se sugiere volver a cargar este naming.", "Error");
            resolve(error);
          });
      });
      return promise;
    }
  }

  //Comprueba que si el naming actualmente es directo a su naming de origen. (Es el mismo sin procesamiento)
  checkDirectOriginNaming(row: any) {
    if (row.hasOwnProperty("direct"))
      return row.direct === 1;
    return false;
  }

  //Muestra el popup de los detalles del naming de origen antes de agregarlo
  namingDetails(namingRow: any, originT: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "65%";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      naming: namingRow,
      idTable: originT._id,
      originType: originT.originType
    };
    this.dialog.open(NamingInfoComponent, dialogConfig);
  }
    
  //Funcion visual para el toggle del row detail de los namings de origen
  activeDetailsCheck(column) {
    return this.lastToggleNamingNumber == column;
  }

  //Toggle para el rowDetails
  toggleExpandRow(row) {
    if(this.lastToggleNamingNumber == row.column) {
      this.lastToggleNamingNumber = -1;
      this.table.rowDetail.collapseAllRows();
    } else {
      this.table.rowDetail.collapseAllRows();
      this.lastToggleNamingNumber = row.column;
      this.table.rowDetail.toggleExpandRow(row);
    }
    this.showAddOriginNaming = false;
  }

  //Devuelve la clase CSS para colorear la celda del naming
  getCellClass({ row }) {
    if(row.originNamings.length === 0) {
      if(row.default === "empty")
        return { 'generatedRow': true }
      if(row.modification.length > 0 && row.modification[0].stateValidation){
        if(row.modification[0].stateValidation === 'YES')
          return { 'is-valid-proposal': true }
        else
          return { 'is-wrong': true};
      } else
        return { 'is-wrong': true};
    }
    if(row.hasOwnProperty("direct"))
      if (row.direct === 1)
        return { 'is-directnaming': true };
    if (row.naming.isGlobal === 'Y')
      return { 'is-global': true };
    return { 'is-proposal': true };
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassRaw({ row, column, value }) {
    if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "RAW")
      return { 'selected': true };
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassMaster({ row, column, value }) {
    if (!row.hasOwnProperty("uuaaRaw"))
      return { 'selectedDT': true };
    else if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "MASTER")
      return { 'selected': true };
  }

  getExcels() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/excels/Object/').subscribe(
      (data: any) => {
        this.exportAsExcelFileObject(data, "Tablon-Objects " + this.tablonInfo.baseName);
        this.openSnackBar("Generación exitosa de los Object. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
    this.rest.getRequest('dataTables/' + this.idTablon + '/excels/Fields/').subscribe(
      (data: any) => {
        this.exportAsExcelFileFields(data, "Tablon-Fields " + this.tablonInfo.baseName);
        this.openSnackBar("Generación exitosa de los Fields. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/;   
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-O': worksheet }, SheetNames: ['DDNG-O'] };
    XLSX.writeFile(workbook, ViewTablonComponent.toExportFileName(excelFileName));
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-F': worksheet }, SheetNames: ['DDNG-F'] };
    XLSX.writeFile(workbook, ViewTablonComponent.toExportFileName(excelFileName));
  }

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }
  
  private onPaginated(event) {
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
