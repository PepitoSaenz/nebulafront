import { TestBed } from '@angular/core/testing';

import { NamingValidationService } from './naming-validation.service';

describe('NamingValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NamingValidationService = TestBed.get(NamingValidationService);
    expect(service).toBeTruthy();
  });
});
