import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { MatMenuTrigger } from "@angular/material";
import { RestService } from "../rest.service";

@Component({
  selector: "app-toolbar",
  templateUrl: "./toolbar.component.html",
  styleUrls: ["./toolbar.component.scss"]
})

export class ToolbarComponent implements OnInit {
  linkManual = "https://docs.google.com/document/d/1SLtL7DCBA9KQa2u-CJUtmamtkCKxrqE1rQcsxu5hr6I/edit?usp=sharing";
  linkDataHub = "https://sites.google.com/bbva.com/datahub/data-developer";

  userName = "";
  rol = "";
  rolLevel = 0;
  menuToggled = false;
  

  @ViewChild(MatMenuTrigger) profileMenu: MatMenuTrigger;

  constructor(public rest: RestService, private router: Router) {
    this.userName = sessionStorage.getItem("userName");
    this.rol = sessionStorage.getItem("rol");
    this.setupRol();
  }

  ngOnInit() {}

  //Toggle para el menu principal. Cierra el submenu del perfil de usuario siempre.
  toggleNav(): void {
    this.profileMenu.closeMenu();
    if (this.menuToggled) {
      this.menuToggled = !this.menuToggled;
      document.getElementById("main-side-nav").style.width = "0";
    } else {
      this.menuToggled = !this.menuToggled;
      document.getElementById("main-side-nav").style.width = "250px";
    }
  }

  //Configura el nivel del rol para otros filtros.
  setupRol() {
    switch (sessionStorage.getItem("rol")) {

      case "I": case "E": case "G": case "F":
        this.rolLevel = 1;
        break;
      case "PO": 
        this.rolLevel = 2;
        break;
      case "SPO":
        this.rolLevel = 3;
        break;
      case "A":
        this.rolLevel = 4;
        break;
    }
  }
  
  //Toggle para el submenu del perfil de usuario. Cierra el menu principal siempre.
  toggleProfile() {
    this.menuToggled = false;
    document.getElementById("main-side-nav").style.width = "0";
  }

  //Rutas para acceder a las otras vistas de Nebula
  LogOut() {
    sessionStorage.clear();
    this.router.navigate(["/login"]);
  }

  profileView() {
    this.router.navigate(["/profile"]);
  }

  useCaseEditNav() {
    this.router.navigate(['/home/productowner']);
  }

  queryTables() {
    this.router.navigate(['/tables/search']);
  }

  voBoNegocio() {
    this.router.navigate(['/tables/vobo']);
  }

  validateACL() {
    this.router.navigate(['/acl']);
  }

  creationTableNav() {
    this.router.navigate(["/tables"]);
  }

  pendingTablesNav() {
    this.router.navigate(["/home"]);
  }

  statisticsNav() {
    this.router.navigate(["/statistics"]);
  }

  searchNamingNav() {
    this.router.navigate(["/naming"]);
  }

  abbreviationsNav() {
    this.router.navigate(["/abbreviations"]);
  }
  namingLegacyNav() {
    this.router.navigate(["/legacy"]);
  }

  adminHomeNav() {
    this.router.navigate(['/admin']);
  }

  cpanelHomeNav() {
    this.router.navigate(['/cpanel']);
  }

  validateUseCasesNav() {
    this.router.navigate(['/home/sproductowner']);
  }

  initTablon() {
    this.router.navigate(['/tablones']);
  }

  initOpBase() {
    this.router.navigate(['/opbases']);
  }

  searchOpBase() {
    this.router.navigate(['/opbases/search']);
  }

  viewUUAAs() {
    this.router.navigate(['/view/uuaas']);
  }

}
