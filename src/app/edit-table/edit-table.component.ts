import { Component, OnInit, ViewChild } from '@angular/core'
import {
  AbstractControl,
  FormControl,
  ValidatorFn,
  Validators
} from '@angular/forms'
import {
  MatDialog,
  MatDialogConfig,
  MatSnackBar,
  MatSnackBarConfig
} from '@angular/material'
import { ActivatedRoute, Router } from '@angular/router'

import { Observable } from 'rxjs'
import { debounceTime, map, skip, startWith } from 'rxjs/operators'
import * as XLSX from 'xlsx'

import { NamingValidationService } from '../naming-validation.service'
import { RestService } from '../rest.service'
import { CommentComponentComponent } from './../comment-component/comment-component.component'
import { ErrorsComponent } from './../errors/errors.component'
import { LoadInfoComponent } from './load-info/load-info.component'

@Component({
  selector: 'app-edit-table',
  templateUrl: './edit-table.component.html',
  styleUrls: ['./edit-table.component.scss']
})
export class EditTableComponent implements OnInit {
  originSystems = [
    'H',
    'L',
    'S',
    'P',
    'O',
    'U',
    'D',
    'A',
    'Q',
    'F',
    'R',
    'C',
    'K',
    'M',
    'T',
    'N',
    'X',
    'E',
    'M',
    'Y',
    'G',
    'B',
    'V',
    'I',
    '1',
    '2',
    '3',
    '4',
    '7',
    '8'
  ]
  appliValues = [
    'BA',
    'BG',
    'BT',
    'CA',
    'CN',
    'RD',
    'G2',
    'CX',
    'EF',
    'EY',
    'FP',
    'GF',
    'GI',
    'GP',
    'GT',
    'HA',
    'HR',
    'HX',
    'IC',
    'IM',
    'IO',
    'JG',
    'JI',
    'KN',
    'LS',
    'MC',
    'OG',
    'OZ',
    'PE',
    'PM',
    'QH',
    'RC',
    'RG',
    'RI',
    'RV',
    'SM',
    'SL',
    'SP',
    'S4',
    'TC',
    'TP',
    'UG',
    'V7',
    'W9',
    'XC',
    'XG',
    'QG',
    'MK',
    'KP',
    'FI',
    'CT',
    'UB',
    'PF',
    'PI',
    'DI',
    'TN',
    'US',
    'SR',
    'QR',
    'SF',
    'EX',
    'BR',
    'WP',
    'DG',
    'RR',
    'MF'
  ]

  deployType_values = ['', 'Global-Implementation', 'Local']
  entryType_values = ['', 'AVRO', 'PARQUET']
  userId: string
  userRol: string

  //Functional
  activeHelp: string = ''
  activeTab: string = ''
  rowsLimit: any = 5
  rowsArray: any = []

  errors: any = []
  arraySuffixTmp: any = []
  arrayLogFormatTmp: any = []
  inputEntTmp: number
  inputDecTmp: number
  inputLenRawTmp: number
  editing: any = []
  reasons: any = []
  suffixes: any = []
  ambitoOptions = []
  uuaaOptions = []
  frecuencyOptions: any = []
  originSystemOptions = []
  objectTypesOptions = ['File', 'Table']
  loadingTypesOptions = ['incremental', 'complete']
  stateTableOptions = {
    N: 'Propuesta',
    G: 'Gobierno',
    I: 'Lista para Ingestar',
    RN: 'Revisión Visto Bueno',
    P: 'Producción',
    A: 'Arquitectura'
  }
  reasonsOptions: any = []

  loadingIndicator: boolean = false
  observationFlag: boolean = false
  showTabRaw: boolean = false
  showTabMaster: boolean = false
  showHostTableFields: boolean = false

  fileList: File = null
  fileName: string = ''

  tmpFile: any

  //Search
  searchNamingValue: string = ''
  searchLogicNamingValue: string = ''
  searchDescValue: string = ''
  searchLegacyNamingValue: string = ''
  searchLegacyDescValue: string = ''
  searchSuffixValue: string = ''
  searchCommentState: string = ''

  //Data
  idTable: string = '0'
  activeObject: any = []
  tableData: any
  blockers: any = []
  undoNamings = []
  cleanStateTable = ''
  currAmbito = ''
  currUUAA = ''

  //For show
  uuaaShort: string = ''
  aliasLongTabla: string = ''
  tmpSeparator: string = ''
  viewAuditFlag = false

  requiredFormControlRaw = new FormControl('', [
    Validators.required,
    Validators.minLength(2)
    //this.firstLetterVal(),
    //this.applicationCodeVal(),
  ])
  formControlAmbito = new FormControl('', [
    Validators.required,
    Validators.minLength(1)
  ])
  formControlUUAA = new FormControl('', [
    Validators.required,
    Validators.minLength(3)
  ])
  formControlAlias = new FormControl('', [Validators.required])

  once = true

  rawUAOptions: any[] = []
  uuaaOptions2: any[] = []
  filteredUUAAs: Observable<string[]>
  uuaaControl = new FormControl()

  @ViewChild('table') table: any
  @ViewChild('loadInput') loadInput: any
  @ViewChild('selectorExcel') selectorExcel: any
  @ViewChild('selectorLoad') selectorLoad: any

  constructor(
    public rest: RestService,
    public vali: NamingValidationService,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.idTable.length == 0) this.router.navigate(['/tables'])

      this.userId = sessionStorage.getItem('userId')
      this.userRol = sessionStorage.getItem('rol')

      this.rowsArray = this.columnsReview
      this.activeTab = 'FieldReview'
      this.idTable = params['idTable']

      this.getAmbitos()
      this.getUuaas()
      this.getMasterUUAAs()

      this.getFrecuencyOptions()
      this.cleanNullsRequest()
      this.getInfoTable('FieldReview')
      this.getFields('FieldReview')
      console.log("coso")

      this.whatIsGenerated()
      this.getSuffixes()
      this.getOrigSysOptions()
      this.getReasons()
    })
  }

  //Formatea el valor del campo de UUAA RAW en mayusculas
  forceUppercaseConditionally(event: any) {
    this.requiredFormControlRaw.setValue(event.target.value.toUpperCase())
  }

  checkLogicalFormat() {
    for (let data of this.tableData) {
      if (data.governanceFormat.toUpperCase() === 'STRING') {
        data.governanceFormat = this.getGovernanceFormat(data)[0]
        this.updateValuesRequest(data, this.activeTab)
      }
    }
  }

  //Guarda la fila antes de ser eliminada para deshacer si se desea
  undoSave(rowToSave: any) {
    this.undoNamings.push(rowToSave)
  }

  undoApply() {
    let tmpRow = this.undoNamings.pop()
    this.rest
      .putRequest(
        'dataDictionaries/' + this.idTable + '/fields/',
        JSON.stringify({
          action: 'insert',
          column: tmpRow.column,
          field: this.activeTab,
          user_id: this.userId
        })
      )
      .subscribe(
        (data: any) => {
          this.getFields(this.activeTab).then((res: any) => {
            this.tableData[tmpRow.column] = tmpRow
            this.updateValue('1', this.tableData[tmpRow.column], 'length')
            this.openSnackBar(
              'Deshacer eliminar naming ' + tmpRow.naming.naming,
              'Ok'
            )
          })
        },
        error => {
          this.openSnackBar('No se ha podido agregar el campo.', 'Error')
        }
      )
  }

  ngOnInit() {
    this.filteredUUAAs = this.uuaaControl.valueChanges.pipe(
      debounceTime(200),
      startWith(''),
      map(value => this.filterUUAAs(value))
    )
  }

  //Funcion que filtra las uuaas en el select a medida que se ingresen letras
  private filterUUAAs(value: any): string[] {
    if (value !== null) {
      if (value.hasOwnProperty('short_name')) value = value['short_name']
      const filterValue = value.toLowerCase()
      let vau = this.uuaaOptions2.filter(option =>
        option['short_name'].toLowerCase().includes(filterValue)
      )
      return vau
    }
  }

  //Valida que el naming de origen este dentro de las opciones permitidas.
  //Para evitar que ingresen namings no permitidos
  checkSelectedUUAA() {
    if (this.uuaaControl.value != null)
      if (this.uuaaControl.value.hasOwnProperty('short_name')) {
        if (this.currUUAA !== this.uuaaControl.value['short_name'])
          this.resetEditUUAA()
      } else this.resetEditUUAA()
  }

  //Resetea los valores del namign de origen cuando se agrega.
  resetEditUUAA() {
    this.uuaaControl.setValue({ short_name: this.currUUAA })
  }

  //Valida que la primera letra de la UUAA de Raw empiece por "C"
  firstLetterVal(): ValidatorFn {
    var nameRe: RegExp = /^[C|K|W][A-Z0-9]+$/
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = nameRe.test(control.value)
      return forbidden ? null : { firstLetter: { value: control.value } }
    }
  }

  //Valida que la UUAA de Raw posea el código de un applicativo correcto
  applicationCodeVal(): ValidatorFn {
    var found: boolean = false
    var tmp: string = ''
    var nameRe: RegExp = /^[A-Z][A-Z]+$/
    return (control: AbstractControl): { [key: string]: any } | null => {
      for (var app of this.appliValues) {
        tmp = '^[A-Z][' + app[0] + '][' + app[1] + '][A-Z]+$'
        nameRe = new RegExp(tmp, 'g')
        found = nameRe.test(control.value)
        if (found) break
      }
      return found ? null : { appCode: { value: control.value } }
    }
  }

  //Comprueba que la propiedad este visible (Para las columnas)
  visibleColumn(prop: string) {
    let searchArray =
      this.activeTab === 'FieldReview'
        ? this.columnsReview
        : this.activeTab === 'fieldsRaw'
          ? this.columnsRaw
          : this.columnsMaster
    for (let cell of searchArray)
      for (let val of cell) if (val.prop == prop) return val.visible
    return false
  }

  //Cambia la fase actual dependiendo de la pestaña en la que se encuentre
  tabChanged(event: any) {
    this.cleanQuery(false)

    setTimeout(() => {
      this.activeTab =
        event.tab.textLabel === 'Propuesta'
          ? 'FieldReview'
          : event.tab.textLabel === 'Raw'
            ? 'fieldsRaw'
            : 'fieldsMaster'
    }, 500)
    setTimeout(() => {
      this.updateTabData(
        event.tab.textLabel === 'Propuesta'
          ? 'FieldReview'
          : event.tab.textLabel === 'Raw'
            ? 'fieldsRaw'
            : 'fieldsMaster'
      )
    }, 300)
  }

  //Actualiza los datos de la fase en la pestaña que se este
  updateTabData(tab) {
    let fase = ''
    if (tab === 'FieldReview') {
      fase = 'FieldReview'
      this.rowsArray = this.columnsReview
    } else if (tab === 'fieldsRaw') {
      fase = 'raw'
      this.rowsArray = this.columnsRaw
    } else {
      fase = 'master'
      this.rowsArray = this.columnsMaster
    }
    this.getInfoTable(fase)
    this.getFields(tab)
  }

  updateUUAAInfo() {
    this.rest
      .postRequest('uuaa/update_object/', { type: 'DD', id: this.idTable })
      .subscribe((data: any) => {
        console.log(data)
      })
  }

  getAmbitos() {
    this.rest.getRequest('uuaa/ambitos/').subscribe((data: any) => {
      this.ambitoOptions = data
    })
  }

  getRawUUAAs() {
    this.rest.getRequest('uuaa/raw/all/').subscribe((data: any) => {
      this.rawUAOptions = data
    })
  }

  getMasterUUAAs() {
    this.rest.getRequest('uuaa/master/all/').subscribe((data: any) => {
      this.uuaaOptions2 = data
    })
  }

  getUuaas() {
    this.rest.getRequest('uuaa/master/all/').subscribe((data: any) => {
      this.uuaaOptions = data
    })
  }

  getFrecuencyOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.frecuencyOptions = data
      },
      error => {
        this.openSnackBar('Error procesando petición al servidor.', 'Error')
      }
    )
  }

  //Peticion al back para la lista de sufijos de namings
  getSuffixes() {
    this.suffixes = []
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixes = data
      },
      error => {
        this.openSnackBar('Error procesando petición al servidor.', 'Error')
      }
    )
  }

  getOrigSysOptions() {
    this.rest.getRequest('projects/backlogs/atributes/originSystem/').subscribe(
      (data: any) => {
        this.originSystemOptions = data
      },
      error => {
        this.openSnackBar('Error procesando petición al servidor.', 'Error')
      }
    )
  }

  getReasons() {
    this.reasonsOptions = []
    this.rest.getRequest('dataDictionaries/comments/reasons/').subscribe(
      (data: {}) => {
        this.reasonsOptions = data
        this.reasonsOptions.unshift({ error: 'NOK' })
      },
      error => {
        this.openSnackBar('Error procesando petición al servidor.', 'Error')
      }
    )
  }

  //Calcula el nombre del alias cuando la fase es propuesta (el objeto no tiene el nombre fisico solito)
  calcAliasLong(fase: string) {
    var temp =
      fase === 'master'
        ? this.activeObject.master_name.split('_')
        : this.activeObject.raw_name.split('_')
    this.uuaaShort = temp[0] + '_' + temp[1] + '_'
    temp.splice(0, 2)
    this.aliasLongTabla = temp.join('_')
    this.currAmbito = this.activeObject.uuaaMaster.charAt(0)
    this.currUUAA = this.activeObject.uuaaMaster.substring(1)
    this.uuaaControl.setValue({ short_name: this.currUUAA })

    if (this.activeObject.master_name.trim() == '') {
      this.activeObject.master_name = ''
    }
  }

  //Verifica que la tabla tenga un estado valido para su edicion, el cual puede ser Gobierno (G) o Propuesta (N)
  checkTableStatus() {
    if (this.activeObject.stateTable)
      if (
        this.activeObject.stateTable !== 'G' &&
        this.activeObject.stateTable !== 'N'
      ) {
        this.sendViewTable(this.idTable)
        this.openSnackBar('Esa tabla no esta disponible para editar', 'Error')
      }
    if (this.activeObject.separator.trim() == '')
      this.activeObject.separator = ' '
    if (typeof this.activeObject.current_depth != 'number')
      this.activeObject.current_depth = parseInt(
        this.activeObject.current_depth
      )
    if (isNaN(this.activeObject.current_depth))
      this.activeObject.current_depth = 0

    if (typeof this.activeObject.required_depth != 'number')
      this.activeObject.required_depth = parseInt(
        this.activeObject.required_depth
      )
    if (isNaN(this.activeObject.required_depth))
      this.activeObject.required_depth = 0

    if (typeof this.activeObject.estimated_volume_records != 'number')
      this.activeObject.estimated_volume_records = parseInt(
        this.activeObject.estimated_volume_records
      )
    if (isNaN(this.activeObject.estimated_volume_records))
      this.activeObject.estimated_volume_records = 0
  }

  sendViewTable(idTable: string) {
    this.router.navigate(['tables/view'], {
      queryParams: { idTable: idTable }
    })
  }

  selectedPeriodicity(value: any) {
    if (this.activeTab === 'fieldsRaw') this.activeObject.periodicity = value
    else this.activeObject.periodicity_master = value
  }

  //Peticion al back para traer la información del objeto de la tabla
  postUpdateOldTable() {
    this.rest
      .postRequest('dataDictionaries/' + this.idTable + '/updateOld/', {
        update: true
      })
      .subscribe(
        (data: any) => {
          //console.log(data);
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }

  cleanNullsRequest() {
    this.rest
      .getRequest('dataDictionaries/clean/' + this.idTable + '/')
      .subscribe((data: any) => {
        //console.log("Nulls " + data)
      })
  }

  //Peticion al back para traer la información del objeto de la tabla
  getInfoTable(fase) {
    this.rest
      .getRequest('dataDictionaries/' + this.idTable + '/head/' + fase + '/')
      .subscribe(
        (data: any) => {
          this.activeObject = data
          this.checkTableStatus()
          this.calcAliasLong(fase)
          this.cleanStateTable = this.stateTableOptions[
            this.activeObject.stateTable
          ]
          if (
            data.created_time != '' &&
            data.modifications.user_name != '' &&
            data.modifications.date != ''
          )
            this.viewAuditFlag = true
          if (data.originSystem.match(/HOST/g)) this.showHostTableFields = true
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }

  //Peticion al back para traer todos los namings (campos) de la tabla en una fase especifica
  getFields(fase) {
    var promise = new Promise((resolve, reject) => {
      this.loadingIndicator = true
      this.rest
        .getRequest(
          'dataDictionaries/' + this.idTable + '/fields/' + fase + '/'
        )
        .subscribe(
          (data: any) => {
            this.tableData = data
            this.getBlockers(this.tableData)

            //if (fase === 'fieldsRaw' || fase === 'fieldsMaster')
              //this.cleanDataRaw()
              //resolve(true)
            //else {
              //this.checkLogicalFormat();
              //if (this.tableData.length > 0 && this.tableData[0].startPosition === 0)
               // this.updateAllLenghts()
            //}
            resolve(true)
          },
          error => {
            this.openSnackBar('Error procesando petición al servidor.', 'Error')
            reject(false)
          }
        )
      this.loadingIndicator = false
    })
    return promise
  }

  //Convierte los blockers dependiendo de su estado de gobierno
  getBlockers(arrTable: any) {
    this.blockers = []
    for (let i = 0; i < arrTable.length; i++) {
      this.blockers[i] =
        arrTable[i].naming.governanceState === 'C' ? true : false
    }
  }

  //Limpia algunos valores para mostrarlos correctamente en la vista de propuesta
  cleanDataRaw() {
    for (let row of this.tableData) {
      if (!row['tokenization']) {
        row['tokenization'] = 'empty'
        this.updateValuesRequest(row, this.activeTab)
      }
    }
  }

  //Limpia algunos valores para mostrarlos correctamente en la vista de propuesta
  cleanData() {
    this.updateAllLenghts()
  }

  //Verifica que fases estan generadas en la tabla para mostrar las pestañas
  whatIsGenerated() {
    this.rest
      .getRequest(
        'dataDictionaries/' + this.idTable + '/fields/' + 'fieldsRaw' + '/'
      )
      .subscribe(
        (data: any) => {
          this.showTabRaw = data.length > 0
        },
        error => { }
      )
    this.rest
      .getRequest(
        'dataDictionaries/' + this.idTable + '/fields/' + 'fieldsMaster' + '/'
      )
      .subscribe(
        (data: any) => {
          this.showTabMaster = data.length > 0
        },
        error => { }
      )
  }

  //Verifica si la fila/naming es calculado
  calculatedField(row: any): boolean {
    if (this.activeTab != 'FieldReview') return row.origin == ''
    return false
  }

  //Metodo que devuelve el tipo de formato de gobierno futuro de un campo generado
  getFutureFormat(row: any): string {
    let lon;
    switch (row.outFormat) {
      case 'STRING':
        return row.logicalFormat
      case 'INT': case 'INT32':
        if (row.hasOwnProperty("length")) lon = row["length"].length
        else lon = row.logicalFormat.split('(')[1]
        return parseInt(lon) >= 5 ? 'NUMERIC LARGE' : 'NUMERIC SHORT'
      case 'INT64':
        return 'NUMERIC BIG'
      case 'DATE':
        return 'DATE'
      case 'TIMESTAMP_MILLIS':
      case 'TIMESTAMP':
        return 'TIMESTAMP'
    }
    return 'I am alone'
  }

  cleanOldObjProperties(object: any) {
    delete object['_id']
    delete object['user']
    delete object['sprint']
    delete object['stateTable']
    delete object['project_name']
  }

  //Actualiza los datos de información de la tabla, especificamente en raw o master
  updateRawMasterObject(object: any) {
    this.objectValidation(object, true).then((res: any) => {
      if (res) {
        this.cleanOldObjProperties(object)
        object['user_id'] = this.userId
        object['fase'] = 'raw' //Doesnt matter

        this.rest
          .putRequest(
            'dataDictionaries/' + this.idTable + '/head/',
            JSON.stringify(object)
          )
          .subscribe(
            (data: any) => {
              this.getInfoTable('FieldReview')
              this.openSnackBar('Campos actualizados', 'Ok')
            },
            error => {
              this.openSnackBar(error.error.reason, 'Error')
            }
          )
      }
    })
  }

  //Regex para validar la sintaxis del Model Version Fase Raw
  validateModelVersion(object: any, model_prop_name: string) {
    var regexo: RegExp = /^[v][\d]*[.][\d]*[.][\d]*/
    try {
      return regexo.test(object[model_prop_name])
    } catch {
      return false
    }
  }

  //Regex para validar la sintaxis del Object Version Fase Raw
  validateObjectVersion(object: any, object_prop_name: string) {
    var regexo: RegExp = /^[\d]*[.][\d]*[.][\d]*/
    try {
      return regexo.test(object[object_prop_name])
    } catch {
      return false
    }
  }

  //Actualiza el ambito y los campos que dependan de el
  selectedAmbito(event) {
    this.currAmbito = event
    this.inputUUAA()
  }

  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa(event) {
    this.currUUAA = event.option.value.short_name
    this.uuaaControl.setValue(event.option.value)
    this.inputUUAA()
  }

  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa2(event) {
    this.currUUAA = event
    this.inputUUAA()
  }

  //Input
  inputUUAA() {
    this.activeObject.uuaaMaster = this.currAmbito + this.currUUAA
  }

  //Metodo que trae el arreglo de namings de una fase de la tabla dada la ruta de entrada.
  //route: La ruta a la que traer los namings que corresponde a la URL. Se aceptan fieldsRaw y fieldsMaster y FieldsReview.
  getPromiseFields(route: string) {
    var promise = new Promise(resolve => {
      this.rest
        .getRequest(
          'dataDictionaries/' + this.idTable + '/fields/' + route + '/'
        )
        .subscribe((data: any) => {
          resolve(data)
        })
    })
    return promise
  }

  //Metodo que valida los namings de particion enviados como parametros en sus respectivas rutas.
  //partition: La variable que contiene las particiones, separadas por punto y coma (;)
  //route: La ruta a la que traer los namings que corresponde a la URL. Se aceptan fieldsRaw y fieldsMaster.
  validatePartitionNamingsP(partition: string, route: string) {
    var promise = new Promise(resolve => {
      if (partition.trim().length > 0) {
        var namesArr = partition.split(';')
        this.getPromiseFields(route).then((res: any) => {
          let finished = false
          for (var i = 0; i < namesArr.length && !finished; i++) {
            let found = false
            res.find((c: any) => {
              if (c.naming.naming === namesArr[i]) found = true
            })
            if (found === false) finished = true
          }
          resolve(!finished)
        })
      } else resolve(true)
    })
    return promise
  }

  //Valida las particiones /////////////////////////////////////////////////////////////////////////////
  validatePartitions(object: any) {
    var promise = new Promise(resolve => {
      var tmp_errors = []
      this.validatePartitionNamingsP(
        object['partitions_raw'],
        'fieldsRaw'
      ).then((res: any) => {
        if (!res)
          tmp_errors.push('Los namings de particiones de RAW están erróneos.')
        if (this.showTabMaster)
          this.validatePartitionNamingsP(
            object['partitions_master'],
            'fieldsMaster'
          ).then((res: any) => {
            if (!res)
              tmp_errors.push(
                'Los namings de particiones de MASTER están erróneos.'
              )
            resolve(tmp_errors)
          })
        else resolve(tmp_errors)
      })
    })
    return promise
  }

  //Valida que la informacion de la tabla (objeto) tenga campos validos antes de enviar la peticion al back para actualizarlo.
  //object: El objeto a validar.
  //errFlag: Bandera para indiciar si es necesario mostrar o no el pop-up de los errores. Si se deja en false el metodo solo devuelve un
  //booleano identificando si la validacion fue correcta o falsa.
  objectValidation(object: any, errFlag: boolean) {
    var promise = new Promise(resolve => {
      var validationStatus = false
      var tmp_errors = []

      object.raw_name =
        't_' + object.uuaaRaw.toLowerCase() + '_' + this.aliasLongTabla
      object.baseName = object.baseName.replace(/^\s+|\s+$/g, '')
      object.baseName = object.baseName.toUpperCase()

      this.validatePartitions(object).then((res: any) => {
        tmp_errors = tmp_errors.concat(res)
        tmp_errors = tmp_errors.concat(this.validateObjectFields(object))
        if (tmp_errors.length > 0) {
          tmp_errors.unshift('')
          if (errFlag) this.openErrorsComponent(tmp_errors)
        } else validationStatus = true

        resolve(validationStatus)
      })
    })
    return promise
  }

  //Valida los demas campos del objeto que no son de prioridad alta o requieren acciones adicionales.
  //object: El objeto a validar.
  validateObjectFields(object: any) {
    var tmp_errors = []

    if (this.currAmbito == 'C')
      if (object['raw_name'].split('_').length >= 4)
        object['raw_name'] = object['raw_name'].toLowerCase()
      else
        tmp_errors.push(
          "El nombre físico del objeto no cumple con el estándar 't_uuaa_alias_descripcion'."
        )
    else if (object['raw_name'].split('_').length >= 3)
      object['raw_name'] = object['raw_name'].toLowerCase()
    else
      tmp_errors.push(
        "El nombre físico del objeto no cumple con el estándar 't_uuaa_descripcion'."
      )
    if (this.requiredFormControlRaw.invalid)
      tmp_errors.push('El código de la UUAA de RAW es inválido.')
    if (object.baseName.length > 60)
      tmp_errors.push('El nombre lógico no debe sobrepasar los 60 caracteres.')
    if (object.baseName.length < 350 && object.baseName.length > 100)
      tmp_errors.push(
        'La descripción del objeto debe estar comprendida en una longitud de mínimo 100 caracteres y máximo 350 caracteres.'
      )
    if (object.perimeter.length < 2)
      tmp_errors.push('El perímetro del objeto es un campo obligatorio.')
    if (object.information_level.length < 2)
      tmp_errors.push(
        'El nivel información del objeto es un campo obligatorio.'
      )
    if (object.objectType.length < 2)
      tmp_errors.push('El tipo del objeto es un campo obligatorio.')
    if (object.deploymentType === 'Global-Implementation') {
      if (this.showTabRaw && !this.validateModelVersion(object, 'modelVersion'))
        tmp_errors.push(
          'El campo Model Version Fase Raw tiene formato inválido.'
        )
      if (
        this.showTabRaw &&
        !this.validateObjectVersion(object, 'objectVersion')
      )
        tmp_errors.push(
          'El campo Object Version Fase Raw tiene formato inválido.'
        )
      if (
        this.showTabMaster &&
        !this.validateModelVersion(object, 'modelVersionMaster')
      )
        tmp_errors.push(
          'El campo Model Version Fase Master tiene formato inválido.'
        )
      if (
        this.showTabMaster &&
        !this.validateObjectVersion(object, 'objectVersionMaster')
      )
        tmp_errors.push(
          'El campo Object Version Fase Master tiene formato inválido.'
        )
    } else {
      if (object.modelVersion.trim() != '') object.modelVersion = ''
      if (object.objectVersion.trim() != '') object.objectVersion = ''
      try {
        if (object.modelVersionMaster.trim() != '')
          object.modelVersionMaster = ''
        if (object.objectVersionMaster.trim() != '')
          object.objectVersionMaster = ''
      } catch (Exception) {
        object.modelVersionMaster = ''
        object.objectVersionMaster = ''
      }
    }
    return tmp_errors
  }

  //Actualiza el estado del naming, Abierto o Cerrado. Solo Gob.
  updateValueGovernanceState(value: any, row: any, fase: string) {
    row.naming.governanceState = value
    this.blockers[row.column] =
      row.naming.governanceState === 'C' ? true : false
    this.updateValuesRequest(row, fase)
  }

  //Peticion al back que actualiza toda la fila de un naming
  updateValuesRequest(row: any, fase: string) {
    this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=' + this.userId + '/',
      JSON.stringify({ notSave: 'notSave', row: row })).subscribe(
        (data: any) => {
          this.getFields(this.activeTab)
        })
  }

  //Actualiza una propiedad de un naming y envia la peticion al back
  updateValue(value: any, row: any, property: string) {
    if (value.trim().length > 0) {
      if (property.includes('GEN')) {
        this.updateValueGeneratedField(value, row, property)
      } else {
        switch (property) {
          case 'length':
            row.length = Number(value)
            this.updateLengthField(value, row, this.activeTab)
            for (var tab of this.tableData) this.quickGovFormatUpdate(tab)
            break
          case 'naming':
            if (value.trim().length < 1) value = 'empty'
            if (this.validateNamingInput(row, value))
              row.naming.naming = value.trim()
            break
          case 'logic':
            if (this.validateLogicNaming(row, value))
              row.logic = this.clearLogic(value)
            break
          case 'integers':
            if (value >= 0) {
              row.integers = value
              if (row.destinationType.includes('DECIMAL'))
                row.governanceFormat = 'DECIMAL(' +
                  (Number(row.integers) + Number(row.decimals)) + ',' + row.decimals + ')'
            }
            break
          case 'decimals':
            if (value >= 0) {
              row.decimals = value
              if (row.destinationType.includes('DECIMAL'))
                row.governanceFormat =
                  'DECIMAL(' +
                  (Number(row.integers) + Number(row.decimals)) +
                  ',' +
                  row.decimals +
                  ')'
            }
            break
          case 'originType':
            if (value.trim().length == 0) value = 'empty'
            row[property] = value
            this.updateLengthField(row.length, row, this.activeTab)
            this.updateAllAfterChangeLengthFieldReview()
            break
          case 'destinationType':
            row.destinationType = value
            this.quickGovFormatUpdate(row)
            break
          case 'dataType':
            row.dataType = value
            if (row.dataType.match(/DATE/g)) row.length = 10
            if (row.dataType.match(/TIMESTAMP/g)) row.length = 26
            this.quickGovFormatUpdate(row)
            break
          case 'legacy':
            if (value.trim().length == 0) value = 'empty'
            row.legacy.legacy = value
            break
          case 'legacyDescription':
            if (value.trim().length == 0) value = 'empty'
            row.legacy.legacyDescription = value
            break
          case 'format':
            if (value.trim() === '') row.format = 'empty'
            else row.format = value
            break
          case 'key':
            if (row.key == 0 && value == 1) row.mandatory = 1
            row.key = Number(value)
            break
          case 'mandatory':
            if (row.key == 1) {
              row.mandatory = 1
              this.openSnackBar(
                'No se puede cambiar el mandatory cuando el campo es llave',
                'Error'
              )
            } else row.mandatory = Number(value)
            break
          case 'symbol':
            row.symbol = Number(value)
            break
          case 'format_int':
            row.logicalFormat = 'DECIMAL' + row.format
            break
          case 'format_dec':
            row.logicalFormat = 'DECIMAL' + row.format
            break
          case 'tds':
            row.tds = Number(value)
            break
          case 'default':
            if (value.trim().length == 0) value = 'empty'
            row.default = value
            break
          default:
            row[property] = value
        }
      }
      this.updateValuesRequest(row, this.activeTab)
    } else this.openSnackBar('El nuevo valor esta vacío', 'Error')
  }

  updateValueGeneratedField(value: any, row: any, property: string) {
    switch (property) {
      case 'GEN_logicalFormat':
        row.logicalFormat = 'ALPHANUMERIC(' + value.trim() + ')'
        if (row.outGov)
          if (row.outGov.includes('ALPHANUMERIC'))
            row.outGov = row.logicalFormat
          else if (row.outGov.includes('DECIMAL(p)'))
            row.outGov = 'DECIMAL(' + value.trim() + ')'
        break
      case 'GEN_ENT_logicalFormat':
        row.integers = Number(value.trim())
        row.logicalFormat = 'DECIMAL(' + row.integers + ',' + row.decimals + ')'
        row.destinationType = row.logicalFormat
        break
      case 'GEN_DEC_logicalFormat':
        row.decimals = Number(value.trim())
        row.logicalFormat = 'DECIMAL(' + row.integers + ',' + row.decimals + ')'
        row.destinationType = row.logicalFormat
        break
      case 'GEN_outFormat':
        row.outFormat = value
        this.quickGovFormatUpdate(row)
        break
      case 'GEN_LEN_outFormat':
        row.outGov = 'DECIMAL(' + value + ')'
        break
      case 'GEN_ENT_outFormat':
        let outFormTMPent =
          this.activeTab == 'fieldsRaw' ? 'outGov' : 'logicalFormat'
        let dec = row.format.split(',')[1]
        dec = dec.split(')')[0]
        row.format = '(' + value + ',' + dec + ')'
        row[outFormTMPent] = 'DECIMAL' + row.format
        break
      case 'GEN_DEC_outFormat':
        let outFormTMPdec =
          this.activeTab == 'fieldsRaw' ? 'outGov' : 'logicalFormat'
        let ent = row.format.split(',')[0]
        ent = ent.split('(')[1]
        row.format = '(' + ent + ',' + value + ')'
        row[outFormTMPdec] = 'DECIMAL' + row.format
        break
      case 'GEN_LEN_outFormatMast':
        if (row.destinationType.includes('STRING'))
          row.logicalFormat = 'ALPHANUMERIC(' + value + ')'
        else if (row.destinationType.includes('DECIMAL'))
          row.logicalFormat = 'DECIMAL(' + value + ')'
        row.outFormat = 'ALPHANUMERIC(' + value + ')'
        this.arrayLogFormatTmp[0] = row.outFormat
        break
      default:
        row[property] = value
    }
  }

  //Actualiza los campos de longitud
  updateLengthField(value: any, row: any, fase: string) {
    if (value > 0) {
      row.outLength =
        Number(row.length) * (row.originType == 'DECIMALEMP' ? 2 : 1)
      row.outFormat = 'STRING'
      if (row.column == 0) {
        row.startPosition = 1
        row.endPosition = Number(row.outLength)
      }
      for (let i = row.column; i < this.tableData.length; i++) {
        if (i > 0)
          this.tableData[i].startPosition =
            Number(this.tableData[i - 1].endPosition) + 1
        this.tableData[i].endPosition =
          Number(this.tableData[i].startPosition) +
          Number(this.tableData[i].outLength - 1)
      }
      if (fase === 'FieldReview') this.updateAllAfterChangeLengthFieldReview()
    } else {
      row.length = 0
      row.outLength = 0
      row.outFormat = 'STRING'
    }
  }

  //Actualiza todos los campos OUTREC si son de host
  updateAllAfterChangeLengthFieldReview() {
    for (let i = 0; i < this.tableData.length; i++) {
      this.tableData[i].outRec = ''
      this.tableData[i].outVarchar = ''
      this.tableData[i].outLength = ''
      if (this.tableData[i].originType == 'DECIMALEMP') {
        this.tableData[i].outFormat =
          'ALPHANUMERIC(' + this.tableData[i].length + ')'
        if (i === 0) this.tableData[i].outRec = 'OUTREC FIELDS=('
        this.tableData[i].outRec =
          this.tableData[i].outRec +
          this.tableData[i].startPosition +
          ',' +
          this.tableData[i].length +
          ',PD,M25'
        this.tableData[i].outLength = Number(this.tableData[i].length) * 2
        this.tableData[i].outVarchar =
          'ALPHANUMERIC(' + this.tableData[i].outLength + ')'
        this.tableData[i].outFormat = this.tableData[i].outVarchar
        if (i == this.tableData.length - 1)
          this.tableData[i].outRec = this.tableData[i].outRec + ')'
        else this.tableData[i].outRec = this.tableData[i].outRec + ','
      } else {
        this.tableData[i].outFormat =
          'ALPHANUMERIC(' + this.tableData[i].length + ')'
        if (i === 0) this.tableData[i].outRec = 'OUTREC FIELDS=('
        this.tableData[i].outRec =
          this.tableData[i].outRec +
          this.tableData[i].startPosition +
          ',' +
          this.tableData[i].length +
          ',TRAN=ALTSEQ'
        this.tableData[i].outLength = Number(this.tableData[i].length)
        this.tableData[i].outVarchar =
          'ALPHANUMERIC(' + this.tableData[i].length + ')'
        this.tableData[i].outFormat = this.tableData[i].outVarchar
        if (i == this.tableData.length - 1)
          this.tableData[i].outRec = this.tableData[i].outRec + ')'
        else this.tableData[i].outRec = this.tableData[i].outRec + ','
      }
    }
    this.tableData = [...this.tableData]
  }

  //Actualiza el formato de gobierno de la fila (TO BE DEPRECATED)
  updateValueGovernanceFormat(row: any) {
    if (!this.blockers[row.column]) {
      row.outFormat = 'ALPHANUMERIC(' + row.length + ')'
      if (row.destinationType.match(/STRING/g)) {
        if (this.showHostTableFields) {
          row.governanceFormat = 'ALPHANUMERIC(' + row.outLength + ')'
          row.format = 'empty'
        } else {
          row.governanceFormat = 'ALPHANUMERIC(' + row.length + ')'
          row.format = 'empty'
        }
      } else if (row.destinationType.match(/DATE/g)) {
        row.governanceFormat = 'DATE'
        row.format = 'yyyy-MM-dd'
      } else if (row.destinationType.match(/INT64/g)) {
        row.governanceFormat = 'NUMERIC BIG'
        row.format = 'empty'
      } else if (row.destinationType.match(/INT/g)) {
        row.governanceFormat = 'NUMERIC SHORT'
        row.format = 'empty'
        if (this.showHostTableFields) {
          if (row.outLength > 4) row.governanceFormat = 'NUMERIC LARGE'
        } else if (row.length > 4) row.governanceFormat = 'NUMERIC LARGE'
      } else if (row.destinationType.match(/TIMESTAMP/g)) {
        row.governanceFormat = 'TIMESTAMP'
        row.format = 'yyyy-MM-dd HH:mm:ss.SSSSSS'
      } else if (row.destinationType.match(/DECIMAL/g)) {
        if (row.decimals < 1 && row.integers < 1)
          this.openSnackBar(
            'Por favor, diligencie los valores decimales y enteros.',
            'Error'
          )
        row.governanceFormat =
          'DECIMAL(' +
          (Number(row.integers) + Number(row.decimals)) +
          ',' +
          row.decimals +
          ')'
        row.format =
          '(' +
          (Number(row.integers) + Number(row.decimals)) +
          ',' +
          row.decimals +
          ')'
      }
    }
  }

  //Actualiza el campo de formato de gobierno y formato rapidamente. Reemplazo efectivo del metodo 'updateValueGovernanceFormat'
  quickGovFormatUpdate(row: any) {
    let destinationTypeTMP = 'destinationType'

    if (this.activeTab === 'FieldReview')
      row.governanceFormat = this.getGovernanceFormat(row)[0]
    else if (this.activeTab === 'fieldsRaw') {
      row.outGov = this.getGovernanceFormat(row)[0]
      destinationTypeTMP = 'outFormat'
    } else if (this.activeTab === 'fieldsMaster')
      row.logicalFormat = this.getGovernanceFormat(row)[0]

    if (row[destinationTypeTMP].includes('DECIMAL'))
      if (row[destinationTypeTMP] == 'DECIMAL(p)')
        row.format = '(' + Number(row.integers) + ',' + '0' + ')'
      else if (row[destinationTypeTMP] == 'DECIMAL(p,s)')
        row.format =
          '(' +
          (Number(row.integers) + Number(row.decimals)) +
          ',' +
          row.decimals +
          ')'
      else row.format = '(0,0)'
    if (row[destinationTypeTMP].includes('TIME'))
      row.format = 'yyyy-MM-dd HH:mm:ss.SSSSSS'
  }

  getTempInputs(row: any) {
    let destType
    let logiFormat
    if (this.activeTab === 'fieldsRaw') {
      destType = 'outFormat'
      logiFormat = 'outGov'
      this.inputLenRawTmp = row['logicalFormat'].split('(')[1].split(')')[0]
    } else if (this.activeTab === 'fieldsMaster') {
      destType = 'destinationType'
      logiFormat = 'logicalFormat'
    }

    if (
      (row[destType].includes('STRING') &&
        row[logiFormat].includes('ALPHANUMERIC')) ||
      row[destType].includes('DECIMAL(p')
    )
      this.inputEntTmp = row[logiFormat].split('(')[1].split(')')[0]
    if (row[destType] == 'DECIMAL(p,s)') {
      this.inputEntTmp = row[logiFormat]
        .split('(')[1]
        .split(')')[0]
        .split(',')[0]
      this.inputDecTmp = row[logiFormat]
        .split('(')[1]
        .split(')')[0]
        .split(',')[1]
    }
  }

  //Devuelve un arreglo de tipos de formato logico validos para cada tipo de dato de destino.
  //No asigna nada para la fila, solo para el arreglo global 'arrayLogFormatTmp' que posee las opciones de la ultima fila dobleclickeada
  getGovernanceFormat(row: any) {
    let arrRes = []
    let destType = 'destinationType'
    let outLength = row.outLength
    let ints = row.integers
    let decs = row.decimals

    if (this.activeTab === 'fieldsRaw') {
      destType = 'outFormat'
      outLength = row.logicalFormat.split('(')[1].split(')')[0]
    } else if (this.activeTab === 'fieldsMaster')
      if (row.outFormat.includes('ALPHANUMERIC'))
        outLength = row.outFormat.split('(')[1].split(')')[0]
      else outLength = 0

    switch (row[destType]) {
      case 'STRING':
        arrRes = ['ALPHANUMERIC(' + outLength + ')', 'CLOB', 'XML', 'TIME']
        break
      case 'INT':
      case 'INT32':
        arrRes = ['NUMERIC SHORT', 'NUMERIC LARGE']
        break
      case 'INT64':
        arrRes = ['NUMERIC BIG']
        break
      case 'DECIMAL(p)':
        if (this.activeTab != 'FieldReview') {
          ints = row.format.split(',')[0].split('(')[1]
          decs = row.format.split(',')[1].split(')')[0]
        }
        if (ints < 1 && decs < 1)
          this.openSnackBar(
            'Por favor, diligencie el valor de los enteros.',
            'Error'
          )
        arrRes = ['DECIMAL(' + Number(ints) + ')']
        break
      case 'DECIMAL':
      case 'DECIMAL(p,s)':
        if (this.activeTab != 'FieldReview') {
          ints = row.format.split(',')[0].split('(')[1]
          decs = row.format.split(',')[1].split(')')[0]
        }
        if (ints < 1 && decs < 1)
          this.openSnackBar(
            'Por favor, diligencie los valores decimales y enteros.',
            'Error'
          )
        arrRes = ['DECIMAL(' + (Number(ints) + Number(decs)) + ',' + decs + ')']
        break
      case 'FLOAT':
        arrRes = ['FLOAT']
        break
      case 'DOUBLE':
        arrRes = ['DOUBLE']
        break
      case 'DATE':
        arrRes = ['DATE']
        break
      case 'TIME':
        arrRes = ['TIME', 'TIMESTAMP']
        break
      case 'TIMESTAMP':
      case 'TIMESTAMP_MILLIS':
        arrRes = ['TIMESTAMP', 'TIME']
        break
      default:
        arrRes = ['ALPHANUMERIC(' + outLength + ')']
    }
    this.arrayLogFormatTmp = arrRes
    return arrRes
  }

  //Solo cuando se llena el campo, no se verifica la integridad del naming
  validateNamingInput(row: any, value: any) {
    if (value.trim().length == 0) {
      value = 'empty'
      return true
    } else {
      if (!this.isNamingRepeated(value)) {
        this.tableData[row.column].naming.naming = value.trim().toLowerCase()
        this.checkNamingGlobal(row.column, value.trim().toLowerCase())
        return true
      } else {
        this.openSnackBar('Naming repetido.', 'Error')
        return false
      }
    }
  }

  //Valida todos los namings de una fase en especifico en donde el nombre de la variable tipo de dato (dataType) depende
  //de la fase en la que se encuentre el naming.
  allValidations(fase: string, showMessages: boolean) {
    var flago = true
    this.errors = []

    for (let i = 0; i < this.tableData.length; i++) {
      let body =
        this.activeTab === 'fieldsRaw'
          ? { title: true }
          : { title: true, dataType: "destinationType", fase: this.activeTab };
      let resu = this.vali.basicNamingValidation(this.tableData[i], body, this.tableData);
      this.errors = this.errors.concat(resu.errors);
      let validation = resu.valid && flago;
      this.addCleanModification(this.tableData[i], validation);
      flago = validation ? flago : false;
    }
    if (showMessages)
      this.openErrorsComponent({
        pretty: true,
        subTitle:
          this.activeTab === 'FieldReview'
            ? 'Propuesta'
            : this.activeTab === 'fieldsRaw'
              ? 'Raw'
              : 'Master',
        errors: this.errors
      })
    else this.errors = []

    return flago
  }

  //Verifica que el naming enviado no exista en el arreglo de namings
  isNamingRepeated(naming: string) {
    for (var tab of this.tableData)
      if (tab.naming.naming === naming) return true
    return false
  }

  //Verifica que dentro del arreglo de namings no existan repetidos
  checkNamingRepeated(tableFields: any) {
    const namings = [];
    for (var tab of tableFields) {
      namings.push(tab.naming.naming);
    }
    const set = new Set(namings);
    const duplicates = namings.filter(item => {
      if (set.has(item)) {
        set.delete(item);
      } else {
        return item;
      }
    });
    return duplicates;
  }

  //Limpia el tipo de dato de salida del naming
  cleanDataTypeChange(row: any) {
    row.dataType = "empty";
    row.format = "empty";
    row.logicalFormat = "empty";
  }

  //Verifica que el naming sea global, si lo es carga toda la info, si no, lo marca como no global.
  checkNamingGlobal(index: number, naming: string) {
    var promise = new Promise((resolve) => {
      this.rest.getRequest('namings/' + naming).subscribe(
        (data: any) => {
          var p = /~/gi
          if (data != null) {
            this.tableData[index].naming.isGlobal = data.naming.isGlobal
            this.tableData[index].logic = data.logic
            this.tableData[index].description = data.originalDesc.replace(p, '\n')
            this.tableData[index].naming.hierarchy = data.level
            this.tableData[index].naming.code = data.code
            this.tableData[index].naming.codeLogic = data.codeLogic
            this.tableData[index].naming.Words = data.naming.Words
            this.tableData[index].naming.suffix = data.naming.suffix
            this.cleanLogicalFormat(index, data);
          } else {
            this.tableData[index].naming.isGlobal = 'N'
            this.tableData[index].naming.code = 'empty'
            this.tableData[index].naming.codeLogic = 'empty'
            this.tableData[index].naming.Words = []
          }
          this.updateValuesRequest(this.tableData[index], this.activeTab)
        })
    })
    return promise
  }

  //Limpia el formato logico y tipo de dato de un naming si es del global y si esta por defecto o sin longitud,
  //ejemplo: ALPHANUMERIC(0)
  cleanLogicalFormat(index: number, global_naming: any) {
    let formatValue = ""

    if (this.activeTab == "FieldReview")
      formatValue = "governanceFormat";
    if (this.activeTab == "fieldsRaw")
      formatValue = "outGov";
    if (this.activeTab == "fieldsMaster")
      formatValue = "destinationType";

    this.tableData[index][formatValue] = global_naming.logical_format
    this.tableData[index].outFormat = global_naming.logical_format

    /*
    try {
      if (this.tableData[index][formatValue].includes("ALPHANUMERIC") && global_naming.logical_format.includes("ALPHANUMERIC"))
        this.tableData[index][formatValue] = global_naming.logical_format
      else if (global_naming.hasOwnProperty("colombia_format_avro"))
        this.tableData[index][formatValue] = global_naming.colombia_format_avro.split(';')[1]
    } catch (e) {
      this.tableData[index][formatValue] = global_naming.logical_format
    }
    */
  }

  //Valida el nuevo nombre logico a modificar. No este repetido o si esta vacio
  validateLogicNaming(row: any, value: any) {
    if (value.trim().length == 0) {
      value = 'empty'
      return true
    } else {
      if (!this.isRepeatedLogic(this.clearLogic(value.trim()))) {
        //if(row.naming.isGlobal === 'Y') row.naming.isGlobal = 'N';
        return true
      } else this.openSnackBar('Nombre lógico repetido.', 'Error')
      return false
    }
  }

  //Verifica si el naming esta repetido
  isRepeatedLogic(logicNaming: string) {
    for (let field of this.tableData)
      if (field.logic === logicNaming) return true
    return false
  }

  //Limpia el nombre logico (?)
  clearLogic(logic: string) {
    let logicRem = logic
    if (logic !== 'empty') {
      let tmp = logic.toUpperCase()
      logicRem = ''
      let i = 0
      while (i < logic.length) {
        if (tmp.charCodeAt(i) == 209) {
          if (i + 1 < logic.length && tmp.charCodeAt(i + 1) == 73) {
            logicRem += 'NI'
            i += 2
          } else {
            logicRem += 'NI'
            i++
          }
        } else {
          logicRem += tmp.charAt(i)
          i++
        }
      }
    }
    return logicRem
  }

  //Devuelve la lista de tipos de datos validos para el sufijo
  dataTypeList(row: any, fase: string) {
    let suffix = this.checkNamingSuffix(row)
    let array = []
    switch (suffix) {
      case 'type':
      case 'id':
      case 'desc':
      case 'name':
        array = ['STRING']
        break
      case 'per':
        array = ['DECIMAL(p,s)', 'FLOAT', 'DOUBLE']
        break
      case 'amount':
        array = ['STRING', 'DECIMAL(p)', 'DECIMAL(p,s)', 'FLOAT', 'DOUBLE']
        break
      case 'number':
        array = [
          'INT32',
          'INT64',
          'DECIMAL(p)',
          'DECIMAL(p,s)',
          'FLOAT',
          'DOUBLE'
        ]
        break
      case 'date':
        array = ['DATE', 'TIME']
        if (fase === 'FieldReview') {
          array.push('TIMESTAMP')
          if (row.naming.isGlobal === 'Y') array.push('STRING')
        } else array.push('TIMESTAMP')
        break
      case 'ars':
        array = ['ARRAY', 'STRING']
        break
      case '':
        array = ['STRING', 'INT32', 'INT64', 'DECIMAL', 'DATE', 'TIMESTAMP']
        break
    }
    this.arraySuffixTmp = array
    return array
  }

  //Verifica el sufijo del naming actual
  checkNamingSuffix(row: any): string {
    let tmpSuffix = row.naming.naming.trim().split('_')
    let suffix = tmpSuffix[tmpSuffix.length - 1]
    let suffixIndex = -1
    for (let i = 0; i < this.suffixes.length; i++)
      if (this.suffixes[i].suffix === suffix) {
        suffixIndex = i
        break
      }
    if (suffixIndex !== -1) return suffix
    return ''
  }

  cleanDestinationTypeValues(row) {
    this.resetDestinationType(row)
    let suffix = this.checkNamingSuffix(row)
    if (suffix !== 'amount' && suffix !== 'number') {
      row.decimals = 0
      row.integers = 0
    }
  }

  //Verifica si el cambio del sufijo del naming implica otro tipo de destino y lo reinicia (x = "")
  resetDestinationType(row: any) {
    this.dataTypeList(row, this.activeTab)
    if (!this.arraySuffixTmp.includes(row.destinationType)) {
      row.destinationType = 'empty'
      row.governanceFormat = 'empty'
      row.format = 'empty'
    }
  }

  //Genera la version RAW de la tabla
  generateFieldsRaw() {
    if (
      confirm(
        '¿Desea generar la fase RAW?. Si la fase ya esta generada se sobreescribiran los datos.'
      )
    ) {
      if (this.allValidations(this.activeTab, false)) {
        this.rest
          .putRequest(
            'dataDictionaries/' + this.idTable + '/fases/',
            JSON.stringify({
              from: 'FieldReview',
              to: 'raw',
              user_id: this.userId
            })
          )
          .subscribe(
            (data: any) => {
              this.openSnackBar('Se ha generado la fase RAW.', 'Ok')
              this.getInfoTable('FieldReview')
              if (this.activeTab == 'fieldsRaw') this.getFields('fieldsRaw')
              this.showTabRaw = true
            },
            error => {
              this.openSnackBar(
                'Error procesando petición al servidor. No se pudo generar RAW',
                'Error'
              )
            }
          )
      } else
        this.openSnackBar(
          'No es posible generar la fase con namings invalidos',
          'Error'
        )
    }
  }

  //Genera la version MASTER de la tabla
  generateFieldsMaster() {
    if (this.currAmbito == 'C' && !this.showTabRaw)
      this.openSnackBar('No es posible generar la fase MASTER sin generar la fase RAW primero para tablas que no pertenecen al Global.', 'Error')
    else if (
      confirm('¿Desea generar la fase MASTER?. Si la fase ya esta generada se sobreescribiran los datos.')
    ){
      if (this.allValidations(this.activeTab, false)) {
        this.rest.putRequest('dataDictionaries/' + this.idTable + '/fases/',
            JSON.stringify({
              from: 'fieldsRaw',
              to: 'master',
              user_id: this.userId
            })
          ).subscribe(
            (data: any) => {
              this.openSnackBar('Se ha generado la fase MASTER.', 'Ok')
              this.getInfoTable('FieldReview')
              if (this.activeTab == 'fieldsMaster')
                this.getFields('fieldsMaster')
              this.showTabMaster = true
            },
            error => {
              this.openSnackBar('Error procesando petición al servidor. No se pudo generar MASTER', 'Error')
            }
          )
      } else
        this.openSnackBar('No es posible generar la fase con namings invalidos', 'Error')
    }
  }

  //Abre el popup de los comentarios
  commentsWindows(row: any) {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.minHeight = '275px'
    dialogConfig.minWidth = '1050px'
    dialogConfig.disableClose = false
    dialogConfig.data = {
      fase: this.activeTab,
      user: sessionStorage.getItem('userId'),
      userRol: sessionStorage.getItem('rol'),
      row: row,
      idTable: this.idTable
    }
    this.dialog.open(CommentComponentComponent, dialogConfig)
  }

  commentsWindowsVoBo(row: any) {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.minHeight = '275px'
    dialogConfig.minWidth = '1050px'
    dialogConfig.disableClose = false
    dialogConfig.data = {
      vobo: true,
      fase: this.activeTab,
      user: sessionStorage.getItem('userName'),
      userRol: sessionStorage.getItem('rol'),
      row: row.check,
      naming: row.naming,
      column: row.column,
      idTable: this.idTable
    }
    this.dialog.open(CommentComponentComponent, dialogConfig)
  }

  //Valida la tabla en las fases de raw y master (si existe) unicamente
  validateRawMasterPromise() {
    var promise = new Promise((resolve, reject) => {
      let rawR = ''
      this.validateRawGovernance().then((res: any) => {
        rawR = res
        if (this.showTabMaster)
          this.validateMasterGovernance().then((res: any) => {
            resolve(rawR.concat(res))
          })
        else resolve(rawR)
      })
    })
    return promise
  }

  //Valida la fase de Raw
  validateRawGovernance() {
    let errorsRaw = []
    var promise = new Promise((resolve, reject) => {
      this.rest
        .getRequest('dataDictionaries/' + this.idTable + '/head/' + 'raw' + '/')
        .subscribe((data: any) => {
          if (!this.objectValidation(data, false))
            errorsRaw.push(
              '***El objeto de Raw tiene campos sin validar***' + '\n'
            )
          this.rest
            .getRequest(
              'dataDictionaries/' +
              this.idTable +
              '/fields/' +
              'fieldsRaw' +
              '/'
            )
            .subscribe((data: any) => {
              /*
              this.fieldsRawTemp = data;
              for (var row of this.fieldsRawTemp)
                if (this.rowValidation(row, 'fieldsRaw', this.fieldsRawTemp)) this.errors = [];
                else {
                  for (var errors of this.errors)
                    errorsRaw.push(errors);
                  this.errors = [];
                }
              if (errorsRaw.length > 0)
                errorsRaw.unshift("* Reporte fase: RAW" + "\n");
              resolve(errorsRaw);
              */
            })
        })
    })
    return promise
  }

  //Valida la fase de Master
  validateMasterGovernance() {
    let errorsMaster = []
    var promise = new Promise((resolve, reject) => {
      this.rest
        .getRequest(
          'dataDictionaries/' + this.idTable + '/head/' + 'master' + '/'
        )
        .subscribe((data: any) => {
          if (!this.objectValidation(data, false))
            errorsMaster.push(
              '***El objeto de Master tiene campos sin validar***' + '\n'
            )
          this.rest
            .getRequest(
              'dataDictionaries/' +
              this.idTable +
              '/fields/' +
              'fieldsMaster' +
              '/'
            )
            .subscribe((data: any) => {
              /*
              this.fieldsMasterTemp = data;
              for (var row of this.fieldsMasterTemp){
                if (this.rowValidation(row, 'fieldsMaster', this.fieldsMasterTemp)) 
                  this.errors = [];
                else {
                  for (var errors of this.errors)
                    errorsMaster.push(errors);
                  this.errors = [];
                }
              }
              if (errorsMaster.length > 0)
                errorsMaster.unshift("* Reporte fase: MASTER *" + "\n");
              resolve(errorsMaster);
              */
            })
        })
    })
    return promise
  }

  //Finaliza la validacion para gobierno y la envia para cambiar su estado o imprime los errores
  //en la validación
  validateFinalSendStatus(destiny: string, errors: any) {
    let fromTmp: string
    let title, messageOk: string

    switch (destiny) {
      case 'governance':
        fromTmp = 'ingesta'
        title =
          'Se devolverá la tabla para revisión en Gobierno.  ¿Desea continuar?'
        messageOk = 'Se ha enviado la tabla para revisión en Gobierno.'
        break
      case 'ingesta':
        fromTmp = 'governance'
        title =
          'Se enviará la tabla para su revisión en Ingesta.  ¿Desea continuar?'
        messageOk = 'Se ha enviado la tabla para revisión en Ingesta.'
        break

      case 'business':
        fromTmp = 'governance'
        title =
          'Se enviará la tabla para su revisión por el VoBo de Negocio.  ¿Desea continuar?'
        messageOk = 'Se ha enviado la tabla hacia VoBo Negocio.'
        break

      case 'architecture':
        fromTmp = 'ingesta'
        title =
          'Se enviará la tabla para su revisión en Arquitectura.  ¿Desea continuar?'
        messageOk = 'Se ha enviado la tabla para su revisión en Arquitectura. '
        break

      case 'closeTable':
        fromTmp = 'architecture'
        destiny = 'ingesta'
        title =
          'Se cerrara la propuesta del diccionario de la tabla. Solo es posible volver a editarla cambiando el estado desde el Panel de Control.  ¿Desea continuar?'
        messageOk =
          'Se ha cerrado la propuesta del diccionario, ahora esta lista para su ingesta.'
        break

      case 'functional':
        fromTmp = 'ingesta'
        title =
          'Se enviara la tabla para su revisión en el Gobierno Funcional.  ¿Desea continuar?'
        messageOk = 'Se ha enviado la tabla para Gobierno Funcional.'
        break
    }
    if (!errors || errors.length == 0) {
      if (confirm(title)) {
        this.rest
          .putRequest(
            'dataDictionaries/' + this.idTable + '/state/',
            JSON.stringify({ from: fromTmp, to: destiny, user_id: this.userId })
          )
          .subscribe(
            (data: any) => {
              this.openSnackBar(messageOk, 'Ok')
              this.router.navigate(['/home'])
            },
            error => {
              this.openSnackBar(
                'No se pudo realizar el cambio de estado de la tabla.',
                'Error'
              )
            }
          )
      }
    } else this.openErrorsComponent(errors)
  }

  //Valida la tabla completa, Propuesta, Raw, Master y la envia a gobierno para seguir con el flujo
  sendTableGovernance() {
    this.validateRawMasterPromise().then((res: any) => {
      this.validateFinalSendStatus('governance', res)
    })
  }

  //Valida la fila
  stateValidation(row: any, fase: string) {
    this.loadingIndicator = true
    let resu = this.vali.basicNamingValidation(row, this.activeTab === 'fieldsRaw' ? { title: true }
      : { title: true,dataType: "destinationType", fase: this.activeTab }, this.tableData);
    this.errors = resu.errors;
    let validation = resu.valid;
    this.addCleanModification(row, validation);
    this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=' + this.userId + '/', JSON.stringify({ row: row }))
      .subscribe(
        (data: any) => {
          validation
            ? this.errors.push('Campo actualizado.' + '\n')
            : this.errors.push('Campo con errores' + '\n', 'Error')
          this.checkNamingGlobal(row.column, row.naming.naming)
          this.updateFields(this.activeTab)
        },
        error => {
          if (error.error.reason.length > 0) {
            this.openSnackBar(error.error.reason, 'Error')
          } else {
            this.openSnackBar('Error procesando petición al servidor', 'Error')
          }
        }
      )
    this.openErrorsComponent({
      pretty: true,
      subTitle:
        this.activeTab === 'FieldReview'
          ? 'Propuesta'
          : this.activeTab === 'fieldsRaw'
            ? 'Raw'
            : 'Master',
      errors: this.errors
    })
  }

  //Modifica el elemento de modification de la fila
  addCleanModification(row: any, validation: boolean) {
    var newModi = {
      startDate: new Date(),
      state: '',
      user: this.userId,
      stateValidation: validation ? 'YES' : 'NO'
    }
    if (row.modification.length >= 5)
      row.modification.splice(4, row.modification.length - 1)
    row.modification.unshift(newModi)

    return row;
  }

  //Actualiza todos los campos de una fase despues de haber sido validados
  updateFields(fase: string) {
    this.rest
      .getRequest('dataDictionaries/' + this.idTable + '/fields/' + fase + '/')
      .subscribe(
        (data: any) => {
          this.tableData = data
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }

  //Abre el dialog de errores de namings y objects
  openErrorsComponent(data: any) {
    const dialogErr = new MatDialogConfig()
    dialogErr.disableClose = true
    dialogErr.autoFocus = true
    dialogErr.minWidth = '50%'
    dialogErr.data = data
    this.dialog.open(ErrorsComponent, dialogErr)
    this.loadingIndicator = false
  }

  //Agrega una fila una posicion arriba
  addAboveFieldReview(row: any, fase: string) {
    this.rest
      .putRequest(
        'dataDictionaries/' + this.idTable + '/fields/',
        JSON.stringify({
          action: 'insert',
          column: row.column,
          field: fase,
          user_id: this.userId
        })
      )
      .subscribe(
        (data: any) => {
          this.getFields(fase).then((res: any) => {
            if (this.activeTab === 'FieldReview')
              this.updateValue('1', this.tableData[row.column], 'length')
          })
        },
        error => {
          this.openSnackBar('No se ha podido agregar el campo.', 'Error')
        }
      )
  }

  //Agrega una fila una posicion abajo
  addBelowFieldReview(row: any, fase: string) {
    this.rest
      .putRequest(
        'dataDictionaries/' + this.idTable + '/fields/',
        JSON.stringify({
          action: 'insert',
          column: row.column + 1,
          field: fase,
          user_id: this.userId
        })
      )
      .subscribe(
        (data: any) => {
          this.getFields(fase).then((res: any) => {
            if (this.activeTab === 'FieldReview')
              this.updateValue('1', this.tableData[row.column + 1], 'length')
          })
        },
        error => {
          this.openSnackBar('No se ha podido agregar el campo.', 'Error')
        }
      )
  }

  //Mueve un naming de posicion, arriba o abajo de su ubicacion una posicion
  moveNaming(row: any, fase: string, type: string) {
    this.rest
      .putRequest(
        'dataDictionaries/' + this.idTable + '/fields/',
        JSON.stringify({
          action: 'move',
          move_dir: type,
          column: row.column,
          field: fase,
          user_id: this.userId
        })
      )
      .subscribe(
        (data: any) => {
          this.getFields(fase).then((res: any) => {
            if (this.activeTab === 'FieldReview')
              this.updateValue('1', this.tableData[row.column], 'length')
            this.openSnackBar(
              'Se ha cambiado la posicion hacia ' +
              (type == 'up' ? 'arriba' : 'abajo') +
              '.',
              'Ok'
            )
          })
        },
        error => {
          this.openSnackBar('No se ha podido mover el campo.', 'Error')
        }
      )
  }

  //Elimina la fila
  deleteFieldReview(row, fase) {
    if (
      confirm(
        'Esta seguro de eliminar el naming? Es posible deshacer el borrado con ciertas condiciones en el menu de Edicion de Namings.'
      )
    ) {
      this.undoSave(row)

      //let index = this.tableData.length === row.column + 1 ? row.column : row.column - 1;

      this.rest
        .putRequest(
          'dataDictionaries/' + this.idTable + '/fields/',
          JSON.stringify({
            action: 'substract',
            column: row.column,
            field: fase,
            user_id: this.userId
          })
        )
        .subscribe(
          (data: any) => {
            if (this.tableData.length <= 1)
              this.addAboveFieldReview({ column: 0 }, this.activeTab)
            else {
              this.getFields(fase).then((data: any) => {
                if (
                  this.activeTab === 'FieldReview' &&
                  row.column != this.tableData.length - 1
                )
                  this.updateValue(
                    '' + this.tableData[row.column].length,
                    this.tableData[row.column],
                    'length'
                  )

                this.openSnackBar(
                  'Naming ' + row.naming.naming + ' eliminado',
                  'Ok'
                )
              })
            }
          },
          error => {
            this.openSnackBar('No se ha podido eliminar el campo.', 'Error')
          }
        )
    }
  }

  //Abre el enlace externo para el template de las plantillas de carga
  openExternalTemplateLink() {
    window.open(
      'https://docs.google.com/spreadsheets/d/17DLxrve0_J2Xhph2SquZhLO5B4QRl0exM4zyZHAjFkk/edit#gid=813161028',
      '_blank'
    )
  }

  //Abre el enlace externo para el template de las plantillas de carga
  openExternalTemplateLinkTWO() {
    window.open(
      'https://docs.google.com/spreadsheets/d/1lhT8ao2mhuzOTSj5CFce7W9cC2Ek7gjAJn_-Fz2cdXY/edit#gid=0',
      '_blank'
    )
  }

  //Actualiza el archivo actual (excel) por subir
  onFileChange(event: any) {
    if (event && event.target.files.length >= 1) {
      this.fileList = event.target.files[0]
      this.fileName = this.fileList.name
    } else {
      this.fileList = null
      this.fileName = ''
    }
  }

  excelLegacyVersionTWO() {
    var arrayBuffer: any
    if (this.fileList != null) {
      let fileReader: FileReader = new FileReader()
      fileReader.onloadend = e => {
        arrayBuffer = fileReader.result
        var data = new Uint8Array(arrayBuffer)
        var arr = new Array()
        for (var i = 0; i != data.length; ++i) {
          arr[i] = String.fromCharCode(data[i])
        }
        var bstr = arr.join('')
        var workbook = XLSX.read(bstr, { type: 'binary' })
        var first_sheet_name = workbook.SheetNames[0]
        var worksheet = workbook.Sheets[first_sheet_name]
        const dialogConfig = new MatDialogConfig()
        dialogConfig.minHeight = '180px'
        dialogConfig.minWidth = '600px'
        dialogConfig.disableClose = false
        dialogConfig.data = {
          idTable: this.idTable,
          fase: this.activeTab,
          length: this.tableData.length,
          worksheet: worksheet,
          userId: this.userId,
          originSystem: this.activeObject.originSystem
        }
        const dialogRef = this.dialog.open(LoadInfoComponent, dialogConfig)
        dialogRef.afterClosed().subscribe(result => {
          if (result != null && result['status']) {
            this.getInfoTable('FieldReview')
            this.getFields('FieldReview')
          }
        })
      }
      fileReader.readAsArrayBuffer(this.fileList)
    } else {
      this.openSnackBar('Por favor, suba un archivo.', 'Error')
    }
    this.onFileChange(null)
  }

  //Carga la tabla legacy desde un excel
  excelLegacy() {
    var arrayBuffer: any
    if (this.fileList != null) {
      let fileReader = new FileReader()
      let jsontmp
      fileReader.onload = e => {
        arrayBuffer = fileReader.result
        var data = new Uint8Array(arrayBuffer)
        var arr = new Array()
        for (var i = 0; i != data.length; ++i) {
          arr[i] = String.fromCharCode(data[i])
        }
        var bstr = arr.join('')
        var workbook = XLSX.read(bstr, { type: 'binary' })
        var first_sheet_name = workbook.SheetNames[0]
        var worksheet = workbook.Sheets[first_sheet_name]
        jsontmp = XLSX.utils.sheet_to_json(worksheet, { raw: true })
        jsontmp = JSON.parse(
          JSON.stringify(jsontmp).replace(/"([^"]+)":/g, function ($0, $1) {
            return '"' + $1.toLowerCase() + '":'
          })
        )
        for (var j = 0; j != jsontmp.length; ++j) {
          jsontmp[j].type = jsontmp[j].type.toUpperCase()
          if (jsontmp[j].catalogue == null) {
            jsontmp[j].catalogue = 'N/A'
          }
          if (
            !jsontmp[j].format ||
            jsontmp[j].format == null ||
            jsontmp[j].format === undefined
          ) {
            jsontmp[j].format = 'empty'
          }
          if (
            !jsontmp[j].decimales ||
            jsontmp[j].decimales == null ||
            jsontmp[j].decimales === undefined
          ) {
            jsontmp[j].decimales = 0
          }
          if (
            !jsontmp[j].enteros ||
            jsontmp[j].enteros == null ||
            jsontmp[j].enteros === undefined
          ) {
            jsontmp[j].enteros = 0
          }
          if (jsontmp[j].key == 'S' || jsontmp[j].key == 's') {
            jsontmp[j].key = 1
          } else jsontmp[j].key = 0

          if (
            jsontmp[j].mandatory == 'S' ||
            jsontmp[j].mandatory == 's' ||
            jsontmp[j].key === 1
          ) {
            jsontmp[j].mandatory = 1
          } else jsontmp[j].mandatory = 0

          if (jsontmp[j].signo == 'S' || jsontmp[j].signo == 's') {
            jsontmp[j].signo = 1
          } else jsontmp[j].signo = 0
        }

        this.rest
          .putRequest(
            'dataDictionaries/' + this.idTable + '/legacys/',
            JSON.stringify({ user_id: this.userId, fields: jsontmp })
          )
          .subscribe(
            (data: any) => {
              if (data != null && data.status === '206')
                this.openSnackBar(
                  data.reason + ' ::: Campos actualizados',
                  'Warning'
                )
              else if (jsontmp.length != this.tableData.length) {
                this.openSnackBar(
                  'Campos actualizados con diferencias encontradas. Longitud archivo:' +
                  jsontmp.length +
                  ' /// ' +
                  'Longitud campos propuesta:' +
                  this.tableData.length,
                  'Warning'
                )
              } else {
                this.openSnackBar('Campos actualizados.', 'Ok')
              }
              this.getFields('FieldReview')
              this.fileList = null
            },
            error => {
              this.openSnackBar(
                'Error en campo ::: ' + error.error.reason,
                'Error'
              )
              this.fileList = null
            }
          )
      }
      fileReader.readAsArrayBuffer(this.fileList)
    } else {
      this.openSnackBar('Por favor, suba un archivo.', 'Error')
    }
    this.onFileChange(null)
  }

  updateAllLenghts() {
    for (let row of this.tableData) {
      this.updateLengthField(row.length ? row.length : 0, row, this.activeTab)
      this.updateAllAfterChangeLengthFieldReview()
      //this.updateValueGovernanceFormat(row);
      this.quickGovFormatUpdate(row)
      this.updateValuesRequest(row, this.activeTab)
    }
  }

  //Toggle para la vista de las columnas
  toggleAll(trueValue: boolean) {
    for (let cell of this.rowsArray)
      for (let val of cell) val.visible = trueValue
  }

  //Busca namings en la tabla con el componente de busqueda
  getFieldsQuery() {
    this.loadingIndicator = true
    if (
      this.searchNamingValue.length == 0 &&
      this.searchDescValue.length == 0 &&
      this.searchLogicNamingValue.length == 0 &&
      this.searchLegacyNamingValue.length == 0 &&
      this.searchLegacyDescValue.length == 0 &&
      this.searchSuffixValue.length < 0 &&
      this.searchCommentState.length < 0
    ) {
      this.loadingIndicator = false
      this.cleanQuery(true)
      this.openSnackBar('Por favor, ingrese los datos de búsqueda.', 'Error')
    } else {
      this.rest
        .getRequest(
          'dataDictionaries/' +
          this.idTable +
          '/fases/' +
          this.activeTab +
          '/naming=' +
          this.transformForRequest(this.searchNamingValue) +
          '&logic=' +
          this.transformForRequest(this.searchLogicNamingValue) +
          '&desc=' +
          this.transformForRequest(this.searchDescValue) +
          '&state=%20' +
          '&legacy=' +
          this.transformForRequest(this.searchLegacyNamingValue) +
          '&descLegacy=' +
          this.transformForRequest(this.searchLegacyDescValue) +
          '&suffix=' +
          this.transformForRequest(this.searchSuffixValue) +
          '&comment=' +
          this.transformForRequest(this.searchCommentState) +
          '/'
        )
        .subscribe(
          (data: any) => {
            this.tableData = data
            setTimeout(() => {
              this.loadingIndicator = false
            }, 1000)
          },
          error => {
            this.openSnackBar('Error procesando petición al servidor.', 'Error')
            setTimeout(() => {
              this.loadingIndicator = false
            }, 1000)
          }
        )
    }
  }

  //Verifica si el campo esta vacio o no para devolver "%20" como espacio para el parametro en una
  //peticion HTTP. Usado solo por 'getFieldsQuery()'
  transformForRequest(value: string) {
    return value.length <= 0 ? '%20' : value
  }

  //Limpia los campos y la busqueda de namings
  cleanQuery(getFields: boolean) {
    this.searchNamingValue = ''
    this.searchDescValue = ''
    this.searchLogicNamingValue = ''
    this.searchLegacyNamingValue = ''
    this.searchLegacyDescValue = ''
    this.searchSuffixValue = ''
    this.searchCommentState = ''
    if (getFields) this.getFields(this.activeTab)
  }

  cleanNewValuesExcelsFields(data: any) {
    for (let row of data)
      if (row['STORAGE ZONE'])
        if (row['STORAGE ZONE'] === 'RAWDATA')
          if (!row['TOKENIZED AT DATA SOURCE'])
            row['TOKENIZED AT DATA SOURCE'] = 'NO'

    return data
  }

  cleanNewValuesExcelsObject(data: any) {
    for (let row of data) if (!row['SECURITY LEVEL']) row['SECURITY LEVEL'] = ''
    return data
  }

  genExcels(option: number) {
    switch (option) {
      case 1:
        this.generateLegacyExcels()
        break
      case 2:
        this.generateExcels()
        break
      case 3:
        this.generateExcelsVoBo()
        break
    }
    this.selectorExcel.value = ''
  }

  loadExcels(option: number) {
    switch (option) {
      case 1:
        this.excelLegacy()
        break
      case 2:
        this.excelLegacyVersionTWO()
        break
    }
    this.selectorLoad.value = ''
  }

  generateObjectExcel() {
    this.rest
      .getRequest('dataDictionaries/' + this.idTable + '/excels/Object/')
      .subscribe(
        (data: any) => {
          this.exportAsExcelFileObject(
            data,
            this.activeObject.project_name +
            '-' +
            this.activeObject.alias +
            '-Object'
          )
          this.openSnackBar('Generación exitosa de los Object. ', 'Ok')
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }

  generateLegacyExcels() {
    this.generateObjectExcel()
    this.rest
      .getRequest('dataDictionaries/' + this.idTable + '/excels/Legacy/')
      .subscribe(
        (data: any) => {
          this.exportAsExcelFileFields(
            data,
            this.activeObject.project_name +
            '-' +
            this.activeObject.alias +
            '-FieldsLegacy'
          )
          this.openSnackBar('Generación exitosa de los Fields Legacy. ', 'Ok')
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }

  generateExcels() {
    this.generateObjectExcel()
    this.rest
      .getRequest('dataDictionaries/' + this.idTable + '/excels/Fields/')
      .subscribe(
        (data: any) => {
          this.exportAsExcelFileFields(
            data,
            this.activeObject.project_name +
            '-' +
            this.activeObject.alias +
            '-Fields'
          )
          this.openSnackBar('Generación exitosa de los Fields. ', 'Ok')
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }

  generateExcelsVoBo() {
    this.rest
      .getRequest('dataDictionaries/' + this.idTable + '/excels/VoBo/')
      .subscribe(
        (data: any) => {
          this.exportAsExcelFileFields(
            data,
            this.activeObject.project_name +
            '-' +
            this.activeObject.alias +
            '-VoBo'
          )
          this.openSnackBar('Generación exitosa de los Fields con VoBo. ', 'Ok')
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }

  deleteMasterFase() {
    if (
      confirm(
        'Se eliminará la fase MASTER de la tabla, esta accion no se puede revertir. ¿Desea Continuar?'
      )
    )
      this.rest
        .postRequest(
          'dataDictionaries/' + this.idTable + '/updateMaster/',
          JSON.stringify({ master_name: 'true' })
        )
        .subscribe(
          (data: any) => {
            this.getInfoTable('FieldReview')
            this.showTabMaster = false
            this.openSnackBar('Fase Master Eliminada.', 'Ok')
          },
          error => {
            this.openSnackBar('Error procesando petición al servidor.', 'Error')
          }
        )
  }

  deleteFaseRaw() {
    if (
      confirm(
        'Se eliminará la fase Raw de la tabla, esta accion no se puede revertir. ¿Desea Continuar?'
      )
    )
      this.rest
        .postRequest(
          'dataDictionaries/' + this.idTable + '/deleteRaw/',
          JSON.stringify({ raw_name: 'true' })
        )
        .subscribe(
          (data: any) => {
            this.getInfoTable('FieldReview')
            this.showTabRaw = false
            this.openSnackBar('Fase Raw Eliminada.', 'Ok')
          },
          error => {
            this.openSnackBar('Error procesando petición al servidor.', 'Error')
          }
        )
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json)
    const workbook: XLSX.WorkBook = {
      Sheets: { 'DDNG-O': worksheet },
      SheetNames: ['DDNG-O']
    }
    XLSX.writeFile(workbook, EditTableComponent.toExportFileName(excelFileName))
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json)
    const workbook: XLSX.WorkBook = {
      Sheets: { 'DDNG-F': worksheet },
      SheetNames: ['DDNG-F']
    }
    XLSX.writeFile(workbook, EditTableComponent.toExportFileName(excelFileName))
  }

  //Metodo para generar el nombre de un archivo de excel
  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  public inputValidatorNaming(event: any) {
    const pattern = /^[a-z][a-z0-9_]*$/
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(/[^a-z0-9_]/g, '')
    return pattern.test(event.target.value)
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para logicos
  public inputValidatorLogic(event: any) {
    const pattern = /^[a-zA-Z0-9 ]*$/
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9 ]/g, '')
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(/[^0-9]/g, '')
  }

  //Pinta la celda del naming dependiendo de su tipo, global, propuesto, etc.
  getCellClass({ row, column, value }) {
    if (row.modification.length > 0) {
      if (row.modification[0].state == 'GL') {
        return {
          'is-global': true
        }
      } else if (row.modification[0].state == 'GC') {
        return {
          'is-common-global': true
        }
      } else if (row.modification[0].state == 'PS') {
        return {
          'is-proposal': true
        }
      } else if (row.modification[0].state == 'PC') {
        return {
          'is-common-proposal': true
        }
      }
    }
  }

  //Pinta la celda de campo legacy si esta repetido
  getCellClassLegacy({ row, column, value }) {
    if (row.legacy.repeat == 'Y') {
      return { 'legacy-repeated': true }
    }
  }

  //Devuelve la clase css dependiendo del estado abierto o cerrado del naming
  getCellClassState({ row }) {
    return row.naming.governanceState === 'C'
      ? { 'naming-closed': true }
      : { 'naming-open': true }
  }

  //Retorna la clase CSS para el cuadro de VoBo
  getStatusRow({ row, column, value }) {
    if (row.check !== undefined)
      if (row.check.status == 'NOK') {
        return { 'is-nok': true }
      } else if (row.check.status == 'OK') {
        return { 'is-ok': true }
      }
  }

  //Devuelve el color de la fila si este es generado
  getRowClass(row: any) {
    return {
      generatedRow: row.origin == ''
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig()
    config.duration = 9000
    if (action == 'Ok') {
      config.panelClass = ['sucessMessage']
    } else if (action == 'Warning') {
      config.panelClass = ['warnningMessage']
    } else {
      config.panelClass = ['errorMessage']
    }
    this.snackBar.open(message, action, config)
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayUUAA(uuaa: any) {
    if (uuaa) return uuaa['short_name']
  }

  //Metodos para el comportamiento del footer personalizado de las tablas.
  //Cambia el limite maximo de filas a mostrar por pagina en la tabla de namings
  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10)
    this.table.limit = this.rowsLimit
    this.table.recalculate()
  }

  //Evento disparado para recalcular las cantidad de filas a mostrar por pagina en la tabla de namings
  private onPaginated(event) {
    this.table.limit = this.rowsLimit
    this.table.recalculate()
  }

  columnsReview = [
    [
      {
        prop: 'naming.governanceState',
        name: 'Estado Gobierno',
        visible: true
      },
      { prop: 'startPosition', name: 'Inicio', visible: true },
      { prop: 'endPosition', name: 'Final', visible: true },
      { prop: 'length', name: 'Longitud', visible: true }
    ],
    [
      { prop: 'originType', name: 'Tipo Origen', visible: true },
      { prop: 'outRec', name: 'Outrec', visible: true }, //HOST
      { prop: 'outVarchar', name: 'Varchar Salida', visible: true },
      { prop: 'outLength', name: 'Salida', visible: true }
    ],
    [
      { prop: 'naming.naming', name: 'Naming', visible: true },
      { prop: 'logic', name: 'Nombre Lógico', visible: true },
      { prop: 'description', name: 'Descripción', visible: true }
    ],
    [
      { prop: 'catalogue', name: 'Catálogo', visible: true },
      { prop: 'legacy.legacy', name: 'Legacy', visible: true },
      {
        prop: 'legacy.legacyDescription',
        name: 'Descripción Legacy',
        visible: true
      }
    ],
    [
      { prop: 'example', name: 'Ejemplo', visible: true },
      { prop: 'key', name: 'Key', visible: true },
      { prop: 'mandatory', name: 'Mandatory', visible: true }
    ],
    [
      { prop: 'symbol', name: 'Signo', visible: true },
      { prop: 'integers', name: 'Enteros', visible: true },
      { prop: 'decimals', name: 'Decimales', visible: true }
    ],
    [
      { prop: 'destinationType', name: 'Tipo Destino', visible: true },
      { prop: 'governanceFormat', name: 'Formato Gobierno', visible: true },
      { prop: 'format', name: 'Formato', visible: true },
      { prop: 'default', name: 'Valor Default', visible: true }
    ],
    [
      //{prop: "actions", name: "Acciones", visible: true},
      { prop: 'todo', name: 'TODO', visible: true }
    ]
  ]

  columnsRaw = [
    [
      { prop: 'naming.code', name: 'Codigo', visible: true },
      { prop: 'naming.level', name: 'Jerarquia', visible: true },
      {
        prop: 'naming.governanceState',
        name: 'Estado Gobierno',
        visible: true
      }
    ],
    [
      { prop: 'naming.naming', name: 'Naming', visible: true },
      { prop: 'logic', name: 'Nombre Lógico', visible: true },
      { prop: 'description', name: 'Descripción', visible: true }
    ],
    [
      { prop: 'catalogue', name: 'Catálogo', visible: true },
      { prop: 'destinationType', name: 'Tipo de dato', visible: true },
      { prop: 'logicalFormat', name: 'Formato Lógico', visible: true }
    ],
    [
      { prop: 'legacy.legacy', name: 'Campo Origen', visible: true },
      {
        prop: 'legacy.legacyDescription',
        name: 'Descripción Origen',
        visible: true
      },
      { prop: 'key', name: 'Key', visible: true },
      { prop: 'mandatory', name: 'Mandatory', visible: true },
      { prop: 'format', name: 'Formato', visible: true }
    ],
    [
      { prop: 'default', name: 'Valor Default', visible: true },
      { prop: 'tokenization', name: 'Tokenización', visible: true },
      { prop: 'tds', name: 'TDS', visible: true },
      { prop: 'check', name: 'VoBo', visible: true }
    ],
    [
      //{prop: "actions", name: "Acciones", visible: true},
      { prop: 'todo', name: 'TODO', visible: true }
    ]
  ]

  columnsMaster = [
    [
      { prop: 'naming.code', name: 'Codigo', visible: true },
      { prop: 'naming.level', name: 'Jerarquia', visible: true },
      {
        prop: 'naming.governanceState',
        name: 'Estado Gobierno',
        visible: true
      }
    ],
    [
      { prop: 'naming.naming', name: 'Naming', visible: true },
      { prop: 'logic', name: 'Nombre Lógico', visible: true },
      { prop: 'description', name: 'Descripción', visible: true }
    ],
    [
      { prop: 'catalogue', name: 'Catálogo', visible: true },
      { prop: 'destinationType', name: 'Tipo de dato', visible: true },
      { prop: 'logicalFormat', name: 'Formato Lógico', visible: true }
    ],
    [
      { prop: 'legacy.legacy', name: 'Campo Origen', visible: true },
      {
        prop: 'legacy.legacyDescription',
        name: 'Descripción Origen',
        visible: true
      },
      { prop: 'key', name: 'Key', visible: true },
      { prop: 'mandatory', name: 'Mandatory', visible: true }
    ],
    [
      { prop: 'format', name: 'Formato', visible: true },
      { prop: 'default', name: 'Valor Default', visible: true },
      { prop: 'tokenization', name: 'Tokenización', visible: true },
      { prop: 'tds', name: 'TDS', visible: true },
      { prop: 'check', name: 'VoBo', visible: true }
    ],
    [
      //{prop: "actions", name: "Acciones", visible: true},
      { prop: 'todo', name: 'TODO', visible: true }
    ]
  ]
}
