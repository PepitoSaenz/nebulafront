import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { RestService } from '../../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

import { NamingValidationService } from '../../naming-validation.service';
import { ErrorsComponent } from './../../errors/errors.component'


@Component({
  selector: 'app-load-info',
  templateUrl: './load-info.component.html',
  styleUrls: ['./load-info.component.scss']
})
export class LoadInfoComponent implements OnInit {

  loadData: any;
  objectData = {
    "uuaaRaw": " ",
    "uuaaMaster": " ",
    "observationField": " ",
    "perimeter":  " ",
    "information_level":  " ",
    "current_depth":  0,
    "required_depth": 0,
    "partitions": " ",
    "separator": " ",
    "typeFile": " "
  };
  cleanedObjectData: any;
  allFields: any;
  activeTab: string = "";

  loadT = "";
  uuaaOptions = [];

  goodArr: any = [];
  badArr: any = [];
  readyToLoad: boolean = true;
  errors: any = [];

  namingsValidationResult: boolean = false;

  userId: string = "";

  ambitOptions = ["K","N","B","W","C"];
  dataTypeOptions = ["CHAR", "DECIMAL", "DECIMALP", "DATE", "TIMESTAMP", "INTEGER", "TIME"];

  @ViewChild('badTable') badTable: any;
  @ViewChild('okTable') okTable: any;


  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, private dialogRef: MatDialogRef<LoadInfoComponent>, public vali: NamingValidationService,
  public rest: RestService, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.userId = sessionStorage.getItem('userId');
    this.loadData = datas;
    console.log(this.loadData)

    this.getUuaasPromise().then((res: any) => {
      this.processData();
    });
  }

  ngOnInit() {
    
  }

  closeDialog(loaded: boolean) {
    this.dialogRef.close({status: loaded});
  }

  toggleGroup(group: any) {
    this.badTable.groupHeader.toggleExpandGroup(group);
  }

  getUuaasPromise() {
    var promise = new Promise((resolve) => {
      this.rest.getRequest('uuaa/master/all/').subscribe((data: any) => {
        let tmpOptions = []
        for (let uuaa of data) {
          //uuaa = uuaa.substring(1);
          tmpOptions.push(uuaa["short_name"]);
        }
        this.uuaaOptions = tmpOptions;
        resolve(tmpOptions);
      });
    });
    return promise;
  }

  processObjectData(worksheet: any) {
    this.objectData = {
      "uuaaRaw": worksheet["C10"] ?  worksheet["C10"]["v"].trim().toUpperCase() : " ",
      "uuaaMaster": worksheet["F10"] ?  worksheet["F10"]["v"].trim().toUpperCase() : " ",
      "observationField": worksheet["C9"] ?  worksheet["C9"]["v"].trim() : " ",
      "perimeter": worksheet["J7"] ?  worksheet["J7"]["v"].trim() : " ",
      "information_level": worksheet["J8"] ?  worksheet["J8"]["v"].trim() : " ",
      "current_depth": worksheet["J9"] ?  parseInt(worksheet["J9"]["v"]) : 0,
      "required_depth": worksheet["J10"] ?  parseInt(worksheet["J10"]["v"]) : 0,
      "partitions": worksheet["J11"] ?  worksheet["J11"]["v"].trim() : " ",
      "separator": worksheet["N8"] ?  worksheet["N8"]["v"].trim() : " ",
      "typeFile": worksheet["N7"] ?  worksheet["N7"]["v"].trim() : " "
    }
    this.cleanObjectData()
  }

  validateObjectData() {
    let errors = [];

    if (this.cleanedObjectData["uuaaRaw"])
      if (this.cleanedObjectData["uuaaRaw"].length != 4)  
        errors.push("La longitud de la UUAA debe ser exactamente 4 caracteres.");
    if (this.cleanedObjectData["uuaaMaster"])
      errors = errors.concat(this.validateUUAAMaster(this.cleanedObjectData["uuaaMaster"]));

    return errors;
  }
  
  validateUUAAMaster(uaMaster: string) {
    let tmpError = [];
  
    if (uaMaster.length != 4)  
      tmpError.push("La longitud de la UUAA debe ser exactamente 4 caracteres.");
    if (!this.ambitOptions.includes(uaMaster.substr(0, 1)))  
      tmpError.push("El ambito de la UUAA esta incorrecto. (K, N, B, W, C)");
    if (!this.uuaaOptions.includes(uaMaster.substr(1, 4)))  
      tmpError.push("El código de la UUAA esta incorrecto.");

    return tmpError;
  }

  cleanObjectData() {
    this.cleanedObjectData = this.objectData;
    let props = Object.keys(this.cleanedObjectData);
    for (let keo in props) 
      if (String(this.cleanedObjectData[props[keo]]).trim().length == 0)
        delete this.cleanedObjectData[props[keo]];

    if (this.objectData["partitions"] == undefined){
      this.objectData["partitions"] = "";
      this.cleanedObjectData["partitions_raw"] = " ";
      this.cleanedObjectData["partitions_master"] = " ";
    }

    if (this.cleanedObjectData["partitions"]) {
      this.cleanedObjectData["partitions_raw"] = this.cleanedObjectData["partitions"].replace(/[\r\n]/g, ";");
      this.cleanedObjectData["partitions_master"] = this.cleanedObjectData["partitions"].replace(/[\r\n]/g, ";");
      delete this.cleanedObjectData["partitions"];
    }

    if (this.objectData["separator"] == undefined)
      this.objectData["separator"] = " ";
    
    if (this.objectData["typeFile"] == undefined)
      this.objectData["typeFile"] = " ";
  }


  //Procesa la data de un excel cargado para dejarlo con la estructura de una tabla y sus namings.
  processData() {
    let worksheet = this.loadData.worksheet
    let leng = false;
    let loadType = "";
    this.allFields = [];
    this.processObjectData(worksheet);

    console.log(worksheet)
    console.log(worksheet["C7"])

    if (worksheet["C7"])
      loadType = (worksheet["C7"]["v"] === "DICCIONARIO" ? "dic" : "pre");
    this.loadT = loadType;

    if (loadType == "pre") {
      for (let i = 17; !leng; i++){
        let tmpName = {};
        if(worksheet["B" + i]) {
          tmpName = {
            "pos": worksheet["B" + i]["v"],
            "legacy": worksheet["C" + i] ? worksheet["C" + i]["v"].trim() : "",
            "description": worksheet["I" + i] ? worksheet["I" + i]["v"].trim() : "",
            "catalogue": worksheet["J" + i] ? worksheet["J" + i]["v"].trim() : "",
            "originType": worksheet["K" + i] ? worksheet["K" + i]["v"].trim() : "",
            "type": worksheet["K" + i] ? worksheet["K" + i]["v"].trim() : "",
            "length": worksheet["M" + i] ? worksheet["M" + i]["v"] : "",
            "enteros": worksheet["N" + i] ? worksheet["N" + i]["v"] : "0",
            "decimales": worksheet["O" + i] ? worksheet["O" + i]["v"] : "0",
            "signo": worksheet["P" + i] ? (worksheet["P" + i]["v"] === "S" ? 1 : 0) : 0,
            "format": worksheet["Q" + i] ? worksheet["Q" + i]["v"] : "",
            "key": worksheet["R" + i] ? (worksheet["R" + i]["v"] === "S" ? 1 : 0) : 0,
            "mandatory": worksheet["S" + i] ? (worksheet["S" + i]["v"] === "S" ? 1 : 0) : 0,
          }
          this.allFields.push(tmpName);
        } else leng = true;
      }
      this.validateLoadData(loadType);
    } else if (loadType == "dic") {
      for (let i = 17; !leng; i++){
        let tmpName = {};

        if(worksheet["B" + i]) {
          tmpName = {
            "pos": worksheet["B" + i]["v"],
            "column": worksheet["B" + i]["v"],
            "naming": worksheet["D" + i] ? worksheet["D" + i]["v"].trim() : "",
            "logic": worksheet["E" + i] ? worksheet["E" + i]["v"].trim() : "",
            "description": worksheet["F" + i] ? worksheet["F" + i]["v"].trim() : "",
            "alias": worksheet["H" + i] ? worksheet["H" + i]["v"].trim() : "",
            "legacy": worksheet["C" + i] ? worksheet["C" + i]["v"].trim() : "",
            "legacyDescription": worksheet["I" + i] ? worksheet["I" + i]["v"].trim() : "",
            "catalogue": worksheet["J" + i] ? worksheet["J" + i]["v"] : "",
            "originType": worksheet["K" + i] ? worksheet["K" + i]["v"].trim() : "",
            "type": worksheet["K" + i] ? worksheet["K" + i]["v"].trim() : "",
            "destinationType": worksheet["L" + i] ? worksheet["L" + i]["v"].trim() : "",
            "length": worksheet["M" + i] ? worksheet["M" + i]["v"] : "0",
            "integers": worksheet["N" + i] ? worksheet["N" + i]["v"] : "0",
            "decimals": worksheet["O" + i] ? worksheet["O" + i]["v"] : "0",
            "enteros": worksheet["N" + i] ? worksheet["N" + i]["v"] : "0",
            "decimales": worksheet["O" + i] ? worksheet["O" + i]["v"] : "0",
            "symbol": worksheet["P" + i] ? (worksheet["P" + i]["v"] === "S" ? "1" : "0") : "0",
            "format": worksheet["Q" + i] ? worksheet["Q" + i]["v"] : "",
            "key": worksheet["R" + i] ? (worksheet["R" + i]["v"] === "S" ? "1" : "0") : "0",
            "mandatory": worksheet["S" + i] ? (worksheet["S" + i]["v"] === "S" ? "1" : "0") : "0",
          }
          this.allFields.push(tmpName);
        } else leng = true;
      }
      this.validateLoadData(loadType);
      this.namingsValidationResult = this.allNamingValidations(this.allFields, true)
    } else {
      this.openSnackBar("Formato desconocido - CARGA PLANTILLA", "Error");
      this.closeDialog(false);
    }
  }

  //Agrega un campo a la modificacion dependiendo con el resultado de la validacion del naming
  addCleanModification(row: any, validation: boolean) {
    row["modification"] = [{startDate: new Date(),
      state: "",
      user: this.userId,
      stateValidation: (validation ? "YES" : "NO")
    }];
  }

  //Muestra el popup de la validacion de namings para volver a ver el resumen de la validación
  showPreviousValidation() {
    this.openErrorsComponent({
      pretty: true,
      subTitle: "Carga Diccionario",
      errors: this.errors
    })
  }

  //Valida todos los namings de una fase en especifico en donde el nombre de la variable tipo de dato (dataType) depende
  //de la fase en la que se encuentre el naming.
  allNamingValidations (namingsArray: any[], showMessages: boolean) {
    var flago = true
    this.errors = []
    const dupFields = this.vali.checkNamingRepeated(namingsArray);

    for (let i = 0; i < namingsArray.length; i++) {
      namingsArray[i]["column"] = i;
      let body =
        this.activeTab === 'fieldsRaw'
          ? { title: true }
          : { title: true, dataType: 'destinationType', fase: this.activeTab, length: true }
      let resu = this.vali.basicNamingValidation(namingsArray[i], body, [])
      if (dupFields.indexOf(namingsArray[i].naming.naming) !== -1) {
        resu.valid = false
        resu.errors.push("Naming repetido, ya existe un campo con el mismo naming");
      }
      let validation = resu.valid && flago

      this.errors = this.errors.concat(resu.errors)
      this.addCleanModification(namingsArray[i], validation)
      flago = validation ? true : false
    }
    if (!flago && showMessages)
      this.openErrorsComponent({
        pretty: true,
        subTitle: "Carga Diccionario",
        errors: this.errors
      })

    return flago
  }

  validateLoadData(loadType: string){
    //this.errors = [];
    let allPos = [];
    let tmpError = [];
    let row: any;

    let objectErrors = this.validateObjectData();
    if(objectErrors.length > 0){
      this.readyToLoad = false;
      this.badArr.push({
        rowPos: -1,
        rowLegacyN: "OBJETO",
        errors: objectErrors
      });
    }

    for (let ind = 0; ind < this.allFields.length; ind++) {
      tmpError = [];
      row = this.allFields[ind];
      if (!allPos.includes(row["pos"])) allPos.push();
      else tmpError.push("La posicion del naming esta repetida: " + row["pos"]);

      if (row["length"].length === 0 || row["length"] == "0")
        tmpError.push("La longitud esta vacia o tiene que ser mayor a 0");

      if (this.dataTypeOptions.includes(row["originType"])){
        switch (row["originType"]) {
          case "DECIMAL": case "DECIMALP":
            if (row["enteros"].length === 0 || row["enteros"] === 0)
              tmpError.push("El campo de enteros es obligatorio para los tipos DECIMAL");
            break;
          case "DATE":
            if (row["format"] !== "yyyy-MM-dd")
              tmpError.push("El formato para los tipos DATE es obligatoriamente: 'yyyy-MM-dd'");
            break;
          case "TIMESTAMP":
            if (row["format"] !== "yyyy-MM-dd HH:mm:ss.SSSSSS")
              tmpError.push("El formato para los tipos TIMESTAMP es obligatoriamente: 'yyyy-MM-dd HH:mm:ss.SSSSSS'");
            break;
          case "INTEGER":
            if (row["format"] !== "")
              tmpError.push("El formato para los tipos INTEGER tiene que ir obligatoriamente vacio");
            break;
        }
      } else {
        tmpError.push("Tipo de dato de origen invalido");
      }

      if (loadType == "dic" && tmpError.length === 0) { 
        this.vali.namingValidationForLoad(row, this.allFields, (this.loadData.originSystem === "HOST" ? true : false));
      }

      if(tmpError.length > 0){
        this.readyToLoad = false;
        this.badArr.push({
          rowPos: ind,
          rowLegacyN: row["legacy"],
          errors: tmpError
        });
      }
    }
  }

  confirmLoad(){
    let jsontmp = this.allFields;
    if (this.loadT == "pre") { 
      this.rest.putRequest('dataDictionaries/' + this.loadData.idTable + '/legacys/', JSON.stringify({user_id: this.loadData.userId, fields: jsontmp, object: this.cleanedObjectData})).subscribe(
        (data: any) => {
          if (data != null && data.status === "206")
            this.openSnackBar(data.reason + " ::: Campos actualizados" , "Warning");
          else {
            if (jsontmp.length != this.loadData.length) {
              this.openSnackBar("Campos actualizados con diferencias encontradas. Longitud archivo:" + jsontmp.length + " /// " + "Longitud campos propuesta:" + this.loadData.length, "Warning");
            } else {
              this.openSnackBar("Campos actualizados.", "Ok");
            }
          }
          this.closeDialog(true);
        }, (error) => {
          this.openSnackBar("Error en campo ::: " + error.error.reason, "Error");
          this.closeDialog(false);
        }
      );
    }    
    if (this.loadT == "dic") { 
      this.rest.putRequest('dataDictionaries/' + this.loadData.idTable + '/uploadAll/', JSON.stringify({user_id: this.loadData.userId, fields: jsontmp, object: this.cleanedObjectData})).subscribe(
        (data: any) => {
          if (data != null && data.status === "206")
            this.openSnackBar(data.reason + " ::: Campos actualizados" , "Warning");
          else {
            if (jsontmp.length != this.loadData.length) {
              this.openSnackBar("Campos actualizados con diferencias encontradas. Longitud archivo:" + jsontmp.length + " /// " + "Longitud campos propuesta:" + this.loadData.length, "Warning");
            } else {
              this.openSnackBar("Campos actualizados.", "Ok");
            }
          }
          this.closeDialog(true);
        }, (error) => {
          this.openSnackBar("Error en campo ::: " + error.error.reason, "Error");
          this.closeDialog(false);
        }
      );
    }
  }

  //Abre el dialog de errores de namings y objects
  openErrorsComponent (data: any) {
    const dialogErr = new MatDialogConfig()
    dialogErr.disableClose = true
    dialogErr.autoFocus = true
    dialogErr.minWidth = '50%'
    dialogErr.data = data
    this.dialog.open(ErrorsComponent, dialogErr)
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 9000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
