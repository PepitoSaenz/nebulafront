import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatDialog, MatDialogConfig } from '@angular/material';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as XLSX from 'ts-xlsx';

import { RestService } from '../rest.service';
import { CommentComponentComponent } from './../comment-component/comment-component.component';


@Component({
  selector: 'app-functional-gov',
  templateUrl: './functional-gov.component.html',
  styleUrls: ['./functional-gov.component.scss']
})
export class FunctionalGovComponent implements OnInit {

  formControlBaseName = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlAmbito = new FormControl('', [Validators.required, Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlScope = new FormControl('', Validators.required);
  formControlAlias = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlDesc = new FormControl('', [Validators.required, Validators.minLength(4)]);
  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);

  formControlPerimetro = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlLoadingT = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlTargetFileT = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlInfLevel = new FormControl('', [Validators.required, Validators.minLength(3)]);

  idTable: string;

  userId;

  tableData: any = {};
  tableFields: any = [];

  editing: any = [];
  uuaaOptions: any = [];
  periodicityOptions = [];
  frecuencyOptions: any = [];
  objectTypesOptions = ["File", "Table"];

  showHost: boolean = false;
  rowsLimit: number = 5;
  activeHelp: string = "";
  viewAuditFlag = false;

  fileList: File = null;


  @ViewChild('table') table: any;


  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.route.queryParams.subscribe(params => {
      this.userId = sessionStorage.getItem("userId");
      this.idTable = params["idTable"];

      this.getInfoTable();
      this.getFieldsTable();

      this.getPeriodicityOptions();
      this.getFrecuency();
    });
  }

  ngOnInit() {
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  private onPaginated(event) {
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  /*
  getUuaas() {
    this.rest.getRequest('projects/uuaa/dataTable/' + this.idProject + '/').subscribe((data: any) => {
      this.uuaaOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }
  */

  getPeriodicityOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicityOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back que devuelve los tipos de frecuencias/periodicidad
  getFrecuency() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe((data: any) => {
      this.frecuencyOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  //Devuelve la clase css para un campo legacy
  getCellClassLegacy({ row, column, value }) {
    if (row.legacy.repeat == 'Y') return { 'legacy-repeated': true }
    else return { 'legacy': true }
  }

  //Peticion al back para traer la información del objeto de la tabla
  getInfoTable() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/head/' + "functional" + '/').subscribe(
      (data: any) => {
        this.tableData = data;
        if (data.created_time != "" && data.modifications.user_name != "" && data.modifications.date != "") this.viewAuditFlag = true;
        if (data.originSystem.match(/HOST/g)) this.showHost = true;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back para traer la información del objeto de la tabla
  getFieldsTable() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/functional/fields/').subscribe(
      (data: any) => {
        this.tableFields = data;
        for (let field of this.tableFields)
          this.addCheckFunctional(field);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  updateObject() {
    this.tableData.current_depth = Number(this.tableData.current_depth);
    this.tableData.estimated_volume_records = Number(this.tableData.estimated_volume_records);
    this.tableData.required_depth = Number(this.tableData.required_depth);
    this.tableData.user_id = this.userId;
    this.rest.putRequest('dataDictionaries/' + this.idTable + '/functional/head/', JSON.stringify(this.tableData)).subscribe(
      (data: any) => {
        this.openSnackBar("Campos actualizados.", "Ok");
      },
      (error) => {
        this.openSnackBar(error.error.reason, "Error");
      }
    );
  }

  //Actualiza una propiedad de un naming y envia la peticion al back
  updateValue(value: any, row: any, property: string) {
    switch (property) {
      case "length":
        row.length = value;
        this.updateLengthField(value, row);
        for (var tab of this.tableFields) this.updateValueOut(tab);
        break;
      case "legacy":
        row.legacy.legacy = value;
        break;
      case "legacyDescription":
        row.legacy.legacyDescription = value;
        break;
      case "integers":
        if (value > 0) {
          row.integers = Number(value);
          row.format = "(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
        }
        break;
      case "decimals":
        if (value >= 0) {
          row.decimals = Number(value);
          row.format = "(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
        }
        break;
      case "symbol":
        if (row.symbol == 1 && value == 0)
          row.length -= 1; 
        else if (row.symbol == 0 && value == 1)
          row.length += 1; 
        
        row.symbol = Number(value);
        this.updateLengthField(row.length, row);
        for (var tab of this.tableFields) this.updateValueOut(tab);
        break;
      case "key":
        if (row.key == 0 && value == 1) row.mandatory = 1;
        row.key = Number(value);
        break;
      case "mandatory":
        if (row.key == 1){
          row.mandatory == 1;
          this.openSnackBar("No se puede cambiar el mandatory cuando el campo es llave", "Error");
        } 
        else row.mandatory = Number(value);
        break;
      case "checkFunctional":
        row.checkFunctional = Number(value);
        break;
      default:
        row[property] = value;
    }
    this.updateValuesRequest(row);
  }

  //Verifica si la fila no tiene el campo de checkFunctional para esta etapa y lo agrega
  //(Solo tablas viejas como para no usar una query y modificar toda la DB)
  addCheckFunctional(row: any){
    if (!row["checkFunctional"]) {
      row["checkFunctional"] = 0;
      this.updateValuesRequest(row);
    }
  }

  //Actualiza el formato de gobierno de la fila
  updateValueOut(row: any) {
    row.outFormat = "ALPHANUMERIC(" + row.length + ")";
    row.outVarchar = "ALPHANUMERIC(" + row.outLength + ")";
    row.format = "empty";
  }

  //Actualiza los campos de longitud
  updateLengthField(value: any, row: any) {
    if (value > 0) {
      row.outLength = Number(row.length) * (row.originType == "DECIMALEMP" ? 2 : 1);
      row.outFormat = "ALPHANUMERIC(" + row.length + ")";
      if (row.column == 0) {
        row.startPosition = 1;
        row.endPosition = Number(row.outLength);
      }
      for (let i = 1; i < this.tableFields.length; i++) {
        this.tableFields[i].startPosition = Number(this.tableFields[i - 1].endPosition) + 1;
        this.tableFields[i].endPosition = Number(this.tableFields[i].startPosition) + Number(this.tableFields[i].outLength - 1);
      }
      this.updateAllAfterChangeLengthFieldReview();
    } else {
      row.length = 0;
      row.outLength = 0;
      row.outFormat = "ALPHANUMERIC(" + row.length + ")";
    }
  }

  //Actualiza todos los campos OUTREC si son de host
  updateAllAfterChangeLengthFieldReview() {
    for (let i = 0; i < this.tableFields.length; i++) {
      this.tableFields[i].outRec = "";
      this.tableFields[i].outVarchar = "";
      this.tableFields[i].outLength = "";
      if (this.tableFields[i].originType == "DECIMALEMP") {
        this.tableFields[i].outFormat = "ALPHANUMERIC(" + this.tableFields[i].length + ")";
        if (i === 0) {
          this.tableFields[i].outRec = "OUTREC FIELDS=(";
        }
        this.tableFields[i].outRec = this.tableFields[i].outRec + this.tableFields[i].startPosition + "," + this.tableFields[i].length + ",PD,M25";
        this.tableFields[i].outLength = Number(this.tableFields[i].length) * 2;
        this.tableFields[i].outVarchar = "ALPHANUMERIC(" + this.tableFields[i].outLength + ")";
        this.tableFields[i].outFormat = this.tableFields[i].outVarchar;
        if (i == this.tableFields.length - 1) {
          this.tableFields[i].outRec = this.tableFields[i].outRec + ')';
        } else {
          this.tableFields[i].outRec = this.tableFields[i].outRec + ",";
        }
      } else {
        this.tableFields[i].outFormat = "ALPHANUMERIC(" + this.tableFields[i].length + ")";
        if (i === 0) {
          this.tableFields[i].outRec = "OUTREC FIELDS=(";
        }
        this.tableFields[i].outRec = this.tableFields[i].outRec + this.tableFields[i].startPosition + "," + this.tableFields[i].length + "," + "TRAN=ALTSEQ";
        this.tableFields[i].outLength = Number(this.tableFields[i].length);
        this.tableFields[i].outVarchar = "ALPHANUMERIC(" + this.tableFields[i].length + ")";
        this.tableFields[i].outFormat = this.tableFields[i].outVarchar;
        if (i == this.tableFields.length - 1) {
          this.tableFields[i].outRec = this.tableFields[i].outRec + ')';
        } else {
          this.tableFields[i].outRec = this.tableFields[i].outRec + ",";
        }
      }
      this.updateValuesRequest(this.tableFields[i]);
    }
    this.tableFields = [...this.tableFields];
  }

  //Peticion al back que actualiza toda la fila de un naming
  updateValuesRequest(row: any) {
    let rowArray = []; rowArray.push(row);
    this.rest.postRequest('dataDictionaries/' + this.idTable + '/functional/fields/', { user_id: this.userId, user: this.userId, row: rowArray }).subscribe(
      (data: any) => {
        //this.openSnackBar("Modificado", "Ok");
      }, (error) => {
        console.error(JSON.stringify(error));
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Envia la tabla propuesta de namings 
  sendReview() {
    if (confirm("¿Desea enviar la tabla a propuesta de namings?")) {
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/state/',
        JSON.stringify({ from: "functional", to: "ingesta", user_id: this.userId })).subscribe(
          (data: any) => {
            this.openSnackBar("Se ha enviado la tabla para propuesta de namings.", "Ok");
            this.router.navigate(['/home']);
          }, (error) => {
            this.openSnackBar("No se pudo enviar la tabla para revisión en gobierno.", "Error");
          });
    }
  }

  //Abre el popup de los comentarios
  commentsWindows(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      functional: true,
      user: sessionStorage.getItem("userId"),
      userRol: sessionStorage.getItem("rol"),
      row: row,
      idTable: this.idTable
    };
    this.dialog.open(CommentComponentComponent, dialogConfig);
  }

  //Actualiza el archivo actual (excel) por subir
  onFileChange(event: any) {
    if (event.target.files.length >= 1)
      this.fileList = event.target.files[0];
    else
      this.fileList = null;
  }

  //Carga la tabla legacy desde un excel
  excelLegacy() {
    var arrayBuffer: any;
    if (this.fileList != null) {
      let fileReader = new FileReader();
      let jsontmp;
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) {
          arr[i] = String.fromCharCode(data[i]);
        }
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, { type: "binary" });
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        jsontmp = XLSX.utils.sheet_to_json(worksheet, { raw: true })
        for (var j = 0; j != jsontmp.length; ++j) {
          if (jsontmp[j].format == null) {
            jsontmp[j].format = "empty";
          }
          if (jsontmp[j].decimales == null) {
            jsontmp[j].decimales = 0;
          }
          if (jsontmp[j].enteros == null) {
            jsontmp[j].enteros = 0;
          }
          if (jsontmp[j].key == null || jsontmp[j].key == "N") {
            jsontmp[j].key = 0;
          } else if (jsontmp[j].key == "S") {
            jsontmp[j].key = 1;
          }
          if (jsontmp[j].mandatory == null || jsontmp[j].mandatory == "N") {
            jsontmp[j].mandatory = 0;
          } else if (jsontmp[j].mandatory == "S") {
            jsontmp[j].mandatory = 1;
          }
          if (jsontmp[j].signo == null || jsontmp[j].signo == "N") {
            jsontmp[j].signo = 0;
          } else if (jsontmp[j].signo == "S") {
            jsontmp[j].signo = 1;
          }
        }
        for (var k = 0; k != jsontmp.length; ++k) {
          if (jsontmp[k].__rowNum__ != k + 1) {
            jsontmp.splice(k, 0, { length: 0, legacy: "empty", description: "empty", type: "CHAR", format: "empty", decimales: 0, enteros: 0, key: 0, mandatory: 0, signo: 0 });
          }
        }
        this.rest.putRequest('dataDictionaries/' + this.idTable + '/legacys/', JSON.stringify({user_id: this.userId,fields: jsontmp})).subscribe(
          (data: any) => {
            this.getFieldsTable();
            this.openSnackBar("Campos actualizados.", "Ok");
            /*
            if (jsontmp.length != this.tableData.length) {
              this.openSnackBar("Campos actualizados con diferencias encontradas. Longitud archivo:" + jsontmp.length + " " + "Longitud campos propuesta:" + this.tableData.length, "Warning");
            } else {
              this.openSnackBar("Campos actualizados.", "Ok");
            }*/
          }, (error) => {
            this.openSnackBar("Error en campo ::: " + error.error.reason, "Error");
            this.fileList = null;
          }
        );
      }
      fileReader.readAsArrayBuffer(this.fileList);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
    }
  }

  //Abre el enlace externo para el template de las plantillas de carga
  openExternalTemplateLink() {
    window.open("https://docs.google.com/spreadsheets/d/17DLxrve0_J2Xhph2SquZhLO5B4QRl0exM4zyZHAjFkk/edit#gid=813161028", '_blank');
  }

  //Devuelve la clase CSS de la fila dependiendo de si la fila esta o no aprobada por el VoBo
  getCheckRow({ row, column, value }) {
    if (row.checkFunctional == 0) {
      return { 'is-nok': true };
    } else if (row.checkFunctional == 1) {
      return { 'is-ok': true };
    }
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
  }

}
