import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionalGovComponent } from './functional-gov.component';

describe('FunctionalGovComponent', () => {
  let component: FunctionalGovComponent;
  let fixture: ComponentFixture<FunctionalGovComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionalGovComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionalGovComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
