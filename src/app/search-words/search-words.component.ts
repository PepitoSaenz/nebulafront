
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-search-words',
  templateUrl: './search-words.component.html',
  styleUrls: ['./search-words.component.scss']
})

export class SearchWordsComponent implements OnInit {

  activeHelp: string = "";

  words = [];
  wordQuery = "";
  spaWord = "";
  abbreviationWord = "";
  synonymous = "";
  selectedTypeOption = "or";
  loadingIndicator: boolean = false;
  tableOffset = 0;

  @ViewChild('myTable') table: any;
  @ViewChild('selectType') selectorType: any;

  constructor(public rest: RestService, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.selectorType.value = "or";
  }

  abbreviationsQuery() {
    this.loadingIndicator = true;
    var wordQueryTmp = this.wordQuery;
    var spaWordTmp = this.spaWord;
    var abbreviationWordTmp = this.abbreviationWord;
    var synonymousTmp = this.synonymous;
    var selectedTypeOptionTmp = this.selectedTypeOption;
    if (this.wordQuery.length == 0) {
      wordQueryTmp = "%20";
    }
    if (this.spaWord.length == 0) {
      spaWordTmp = "%20";
    }
    if (this.abbreviationWord.length == 0) {
      abbreviationWordTmp = "%20";
    }
    if (this.synonymous.length == 0) {
      synonymousTmp = "%20";
    }
    if (this.selectedTypeOption.length == 0) {
      selectedTypeOptionTmp = "%20";
    }
    this.rest.getRequest('namings/words/word=' + wordQueryTmp + '&abbreviation=' + abbreviationWordTmp + '&spanish=' + spaWordTmp + '&synonymous=' + synonymousTmp + '&type=' + selectedTypeOptionTmp + '/').subscribe(
      (data: any) => {
        this.words = data;
        console.log("this.words " + JSON.stringify(this.words));
        setTimeout(() => { this.loadingIndicator = false; }, 1000);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
    this.tableOffset = 0;
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  clean() {
    this.wordQuery = "";
    this.spaWord = "";
    this.abbreviationWord = "";
    this.synonymous = "";
    this.selectedTypeOption = "or";
    this.selectorType.value = "or";
    this.tableOffset = 0;
    this.words = [];
  }

  selectedType(value) {
    this.selectedTypeOption = value;
  }

  onChange(event) {
    this.tableOffset = event.offset;
  }

}
