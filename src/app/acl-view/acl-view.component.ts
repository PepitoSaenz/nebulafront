import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTabChangeEvent, MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import * as XLSX from 'xlsx';
import { RestService } from '../rest.service';
import { CommentsDetailsComponent } from './comments-details/comments-details.component';


@Component({
  selector: 'app-acl-view',
  templateUrl: './acl-view.component.html',
  styleUrls: ['./acl-view.component.scss']
})
export class AclViewComponent implements OnInit {

  @ViewChild('uuaaRWork') uuaaRWork: any;
  @ViewChild('uuaaRLive') uuaaRLive: any;

  @ViewChild('groupRWork') groupRWork: any;
  @ViewChild('groupRLive') groupRLive: any;

  @ViewChild('aclTableWorkOk') aclTableWorkOk: any;
  @ViewChild('aclTableWorkNOk') aclTableWorkNOk: any;
  @ViewChild('aclTableLiveOk') aclTableLiveOk: any;
  @ViewChild('aclTableLiveNOk') aclTableLiveNOk: any;
  @ViewChild('accsTableWork') accsTableWork: any;
  @ViewChild('accsTableLive') accsTableLive: any;


  //Variables para almacenar informacion del estado de las acls
  index = 0;
  fileLoaded: boolean = false;
  aclLoaded: boolean = false;
  accsLoaded: boolean = false;
  usersLoaded: boolean = false;
  fileList: File = null;
  selectedProject: any = undefined;
  selectedUuaa: any = undefined;

  //Variables de las relaciones entre de las uuaas
  uuaaRelationsWork: any = [];
  uuaaRelationsWorkTmp: any = [];
  uuaaRelationsLive: any = [];
  uuaaRelationsLiveTmp: any = [];
  searchUUAAWork: string = "";
  searchUUAALive: string = "";

  //Variables de las relaciones entre los grupos
  groupRelationsWork: any = [];
  groupRelationsWorkTmp: any = [];
  groupRelationsLive: any = [];
  groupRelationsLiveTmp: any = [];
  searchGroupWork: string = "";
  searchGroupLive: string = "";

  //Variable para almacenar las consultas
  tablesQuery = [];
  envOptions = [];

  //Variables de indicadores
  loadingIndicator: boolean = false;
  foldersResponse: any = undefined;

  //Variables para almacenar la informacionde las ACLs
  aclsWork: any = [];
  aclsLive: any = [];
  accsWork: any = [];
  accsLive: any = [];

  //Variables para almacenar informacion de los proyectos y las uuas
  projectOptions: any = [];
  uuaaOptions: any = [];

  constructor(public rest: RestService, public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.getAllProjects();
    this.searchUUAARequestWork();
    this.searchUUAARequestLive();
    this.searchGroupRequestWork();
    this.searchGroupRequestLive();
  }

  ngOnInit() {
  }

  //Consulta la informacion de los proyectos existentes en Nebula
  getAllProjects() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      },
      (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Consulta la informacion de las uuas asociadas a un proyecto
  getUuaas() {
    this.rest.getRequest('projects/functionalMap/' + this.selectedProject._id + '/').subscribe(
      (data: any) => {
        this.uuaaOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Consulta la informacion de las uuas asociadas a un proyecto
  selectProject(value: any) {
    this.selectedProject = value;
    this.getUuaas();
  }

  //Muestra el popup de los detalles del naming de origen antes de agregarlo
  commentDetails(row: any, fase: string) {
    var subFase = fase[0].toUpperCase() + fase.toLowerCase().slice(1);
    if (this.phaseCheck(row, fase, "in" + subFase)) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.maxHeight = "60%";
      dialogConfig.maxWidth = "95%";
      dialogConfig.minHeight = "275px";
      dialogConfig.minWidth = "650px";
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        route: row.route,
        fase: fase,
        comments: row[fase].comments
      };
      this.dialog.open(CommentsDetailsComponent, dialogConfig);
    } else
      this.openSnackBar("La ruta no esta en " + subFase, "Error");
  }

  //Muestra el popup de los detalles del naming de origen antes de agregarlo
  commentDetailsACL(row: any, fase: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "95%";
    dialogConfig.minWidth = "650px";
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = JSON.parse(JSON.stringify(row));
    dialogConfig.data.comments.unshift("*** " + row.route + " ***");
    dialogConfig.data["simple"] = true;
    this.dialog.open(CommentsDetailsComponent, dialogConfig);
  }

  //Muestra el popup de los detalles del naming de origen antes de agregarlo
  commentDetailsAccs(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "95%";
    dialogConfig.minWidth = "650px";
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = JSON.parse(JSON.stringify(row));
    dialogConfig.data.comments.unshift("*** " + row.group + " ***");
    dialogConfig.data["simple"] = true;
    this.dialog.open(CommentsDetailsComponent, dialogConfig);
  }

  //Cambia la fase actual dependiendo de la pestaña en la que se encuentre
  onTabChange(event: MatTabChangeEvent) {
    switch (event.tab.textLabel) {
      case "Backlog":
        this.index = 0;
        break;
      case "Rutas":
        this.index = 1;
        break;
    }
  }

  //Actualiza el archivo actual (excel) por subir
  onFileChange(event: any) {
    this.fileList = event.target.files[0];
  }

  //Carga el excel
  loadLegacy() {
    this.cleanFile();
    var result = [];
    var tmpSheet: any;
    var arrayBuffer: any;
    var isFine = true;
    if (this.fileList != null) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        try {
          var workbook = XLSX.read(data, { type: 'array' });
        } catch (error) {
          isFine = false;
          this.fileLoaded = false;
          if (error.message.includes("password-protected"))
            this.openSnackBar("El excel esta protegido por contraseña", "Error");
          else this.openSnackBar("Error: " + error.message, "Error");
        }
        if (isFine) {
          for (var tabo of workbook.SheetNames) {
            tmpSheet = workbook.Sheets[tabo];
            switch (tabo) {
              case "Folders":
                result[0] = (this.buildACLRoutes1(this.cleanCSV(XLSX.utils.sheet_to_csv(tmpSheet))));
                break;
              case "ACLs":
                result[1] = (this.buildACLRoutes2(this.cleanCSV(XLSX.utils.sheet_to_csv(tmpSheet))));
                break;
              case "Accounts":
                result[2] = (this.buildACLRoutes2(this.cleanCSV(XLSX.utils.sheet_to_csv(tmpSheet))));
                break;
              case "Users":
                result[3] = (this.buildACLRoutes3(this.cleanCSV(XLSX.utils.sheet_to_csv(tmpSheet))));
                break;
            }
          }
          this.sendRoutes(result[0]);
          this.sendRoutesACL(result[1]);
          this.sendRoutesAccounts(result[2], result[3]);
        }
      }
      fileReader.readAsArrayBuffer(this.fileList);
      this.fileLoaded = isFine;
      this.openSnackBar("Archivo cargado correctamente", "Ok");
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
    }
  }

  // Limpia las variables donde se almacenan la informacion de los archivos a cargar
  cleanFile() {
    this.foldersResponse = [];
    this.aclsWork = [];
    this.aclsLive = [];
    this.accsWork = [];
    this.accsLive = [];
    this.fileLoaded = false;
  }

  //Limpia el CSV y lo devuelve como una matriz
  cleanCSV(tempo: any) {
    var coso = [];
    var ecs = 0;
    var why = 0;
    for (let tab of tempo.split(",")) {
      if (coso[ecs] === undefined) coso[ecs] = [];
      if (tab === '\n') { //Salta una linea
        ecs += 1;
        why = 0;
        coso[ecs] = [];
      } else {
        coso[ecs][why] = tab;
        if (tab.includes('\n')) { //Si dentro del string hay un salto de linea
          var tmp = coso[ecs][why];
          coso[ecs][why] = "";
          ecs += 1;
          coso[ecs] = [];
          why = 0;
          coso[ecs][why] = tmp.slice(1);
        }
      }
      why += 1;
    }
    return coso;
  }

  //Construye las rutas para la primera hoja del excel de ACLs (Folders)
  buildACLRoutes1(matrix: any) {
    var flag = false;
    var levels = Array(6).fill("");
    var tmpRoute = "";
    var tmpArray = [];
    var tmpIndex = 0;
    var masterFlag = false;
    for (var index = 0; index < matrix.length; index++) {
      if (matrix[index].includes("WORK ENVIRONMENT") || matrix[index].includes("LIVE ENVIRONMENT")) {
        flag = true;
        matrix[index].includes("WORK ENVIRONMENT") ? tmpArray.push("WORK") : tmpArray.push("LIVE");
        index += 3;
        tmpIndex += 1;
      }
      if (flag) {
        for (var sec = 4; sec < 10; sec++) {
          if (matrix[index].every(function checkAdult(stro) { return stro === ""; })) levels = levels.fill("");
          if (matrix[index][sec] !== undefined) {
            if (matrix[index][sec].includes("master")) masterFlag = true;
            if (masterFlag && sec === 6 && matrix[index][sec] !== "") masterFlag = false;
          }
          if (matrix[index][sec] === "") matrix[index][sec] = (sec != 9 ? levels[sec] : "");
          else levels[sec] = matrix[index][sec];
          if (matrix[index][sec] !== undefined && matrix[index][sec].includes("master") && masterFlag)
            levels[6] = "#";
          if (matrix[index][sec] !== "" && matrix[index][sec] !== undefined && !matrix[index][sec].includes("current"))
            tmpRoute = tmpRoute + "/" + matrix[index][sec];
        }
      }
      if (tmpRoute !== "") {
        tmpArray[tmpIndex] = tmpRoute;
        tmpIndex += 1;
        tmpRoute = "";
      }
    }
    if (tmpArray[tmpArray.length - 1].includes("undefined")) tmpArray.splice(tmpArray.length - 1);
    return tmpArray;
  }

  //Construye las rutas para la segunda y tercera hoja del excel de ACLs
  buildACLRoutes2(matrix: any) {
    var flag = false;
    var tmpRoute = "";
    var tmpArray = [];
    var tmpIndex = 0;
    for (var ind = 0; ind < matrix.length; ind++) {
      if (matrix[ind].includes("WORK ENVIRONMENT") || matrix[ind].includes("LIVE ENVIRONMENT")) {
        flag = true;
        matrix[ind].includes("WORK ENVIRONMENT") ? tmpArray.push("WORK") : tmpArray.push("LIVE");
        ind += 4;
        tmpIndex += 1;
      }
      if (flag) {
        tmpRoute = matrix[ind][2] + "||" + matrix[ind][3] + "||" + matrix[ind][4];
        if (tmpRoute.split("|")[0] !== "") {
          tmpArray[tmpIndex] = tmpRoute;
          tmpIndex += 1;
        }
        tmpRoute = "";
      }
    }
    if (tmpArray[tmpArray.length - 1].includes("undefined")) tmpArray.splice(tmpArray.length - 1);
    return tmpArray;
  }

  //Construye las rutas para la cuarta hoja del excel de ACLs
  buildACLRoutes3(matrix: any) {
    var flag = false;
    var tmpRoute = "";
    var tmpArray = [];
    var tmpIndex = 0;
    for (var ind = 0; ind < matrix.length; ind++) {
      if (matrix[ind].includes("PROJECT’S USERS")) {
        flag = true;
        ind += 3;
        //tmpArray.push("USERS");
        //tmpIndex += 1;
      }
      if (flag && matrix[ind][0] !== undefined) {
        tmpRoute = matrix[ind][0].split('\n')[1] + "||" + matrix[ind][2];
        if (tmpRoute.split("||")[0] !== "") {
          tmpArray[tmpIndex] = tmpRoute;
          tmpIndex += 1;
        }
        tmpRoute = "";
      }
    }
    if (tmpArray[tmpArray.length - 1].includes("undefined")) tmpArray.splice(tmpArray.length - 1);
    return tmpArray;
  }

  // Limpia las rutas de las ACLs y devuelve los arreglos correspondientes
  cleanACLRoutesArray(bigArr: any) {
    var currGroup: string;
    var tmpTab: any = undefined;
    var result = [];
    var resOK = [];
    var resNOK = [];
    for (var tab of bigArr) {
      currGroup = tab.group;
      for (var teb of tab.routes) {
        tmpTab = teb;
        tmpTab.groupN = currGroup;
        teb.isOk == 1 ? resOK.push(JSON.parse(JSON.stringify(tmpTab)))
          : resNOK.push(JSON.parse(JSON.stringify(tmpTab)))
      }
    }
    result[1] = resOK;
    result[0] = resNOK;
    return result;
  }

  // Filtra la informacion de las consultas realizadas para las uuaas work
  updateFilterUUAAWork(event: any) {
    this.uuaaRelationsWork = this.uuaaRelationsWorkTmp;
    this.searchUUAAWork = event.toLowerCase();
    const val = this.searchUUAAWork;
    const temp = this.uuaaRelationsWork.filter(function (res) {
      return res.groupN.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.uuaaRelationsWork = temp;
    this.uuaaRWork.offset = 0;
  }

  // Filtra la informacion de las consultas realizadas para las uuaas live
  updateFilterUUAALive(event: any) {
    this.uuaaRelationsLive = this.uuaaRelationsLiveTmp;
    this.searchUUAALive = event.toLowerCase();
    const val = this.searchUUAALive;
    const temp = this.uuaaRelationsLive.filter(function (res) {
      return res.groupN.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.uuaaRelationsLive = temp;
    this.uuaaRLive.offset = 0;
  }

  // Filtra la informacion de las consultas realizadas para los grupos work
  updateFilterGroupWork(event: any) {
    this.groupRelationsWork = this.groupRelationsWorkTmp;
    this.searchGroupWork = event.toLowerCase();
    const val = this.searchGroupWork;
    const temp = this.groupRelationsWork.filter(function (res) {
      return res.groupN.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.groupRelationsWork = temp;
    this.groupRWork.offset = 0;
  }

  // Filtra la informacion de las consultas realizadas para los grupos live
  updateFilterGroupLive(event: any) {
    this.groupRelationsLive = this.groupRelationsLiveTmp;
    this.searchGroupLive = event.toLowerCase();
    const val = this.searchGroupLive;
    const temp = this.groupRelationsLive.filter(function (res) {
      return res.groupN.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.groupRelationsLive = temp;
    this.groupRLive.offset = 0;
  }

  // Manda la peticion para buscar las relaciones existentes entre las uuaas work
  searchUUAARequestWork() {
    this.rest.getRequest("acl/work/association/").subscribe(
      (data: any) => {
        this.uuaaRelationsWork = this.cleanUUAARequest(data);
        this.uuaaRelationsWorkTmp = this.uuaaRelationsWork;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  // Manda la peticion para buscar las relaciones existentes entre las uuaas live
  searchUUAARequestLive() {
    this.rest.getRequest("acl/live/association/").subscribe(
      (data: any) => {
        this.uuaaRelationsLive = this.cleanUUAARequest(data);
        this.uuaaRelationsLiveTmp = this.uuaaRelationsLive;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  // Manda la peticion para buscar las relaciones existentes entre los grupos work
  searchGroupRequestWork() {
    this.rest.getRequest("acl/work/groups/").subscribe(
      (data: any) => {
        this.groupRelationsWork = this.cleanGroupRequest(data);
        this.groupRelationsWorkTmp = this.groupRelationsWork;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  // Manda la peticion para buscar las relaciones existentes entre los grupos live
  searchGroupRequestLive() {
    this.rest.getRequest("acl/live/groups/").subscribe(
      (data: any) => {
        this.groupRelationsLive = this.cleanGroupRequest(data);
        this.groupRelationsLiveTmp = this.groupRelationsLive;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  // Funcionens que limpian los filtros de busquedas para las uuaas y los grupos de work y live
  cleanFilterUUAAWork() {
    this.searchUUAAWork = "";
    this.searchUUAARequestWork();
    this.uuaaRWork.offset = 0;
  }

  cleanFilterUUAALive() {
    this.searchUUAALive = "";
    this.searchUUAARequestLive();
    this.uuaaRLive.offset = 0;
  }

  cleanFilterGroupWork() {
    this.searchGroupWork = "";
    this.searchGroupRequestWork();
    this.groupRWork.offset = 0;
  }

  cleanFilterGroupLive() {
    this.searchGroupLive = "";
    this.searchGroupRequestLive();
    this.groupRWork.offset = 0;
  }

  cleanUUAARequest(data: any) {
    var currGroup: string;
    var tmpTab: any = undefined;
    var result = [];
    for (var tab of data) {
      currGroup = tab.uuaa;
      for (var teb of tab.relations) {
        tmpTab = {};
        tmpTab.uuaa = teb;
        tmpTab.groupN = currGroup;
        result.push(JSON.parse(JSON.stringify(tmpTab)));
      }
    }
    return result;
  }

  cleanGroupRequest(data: any) {
    var currGroup: string;
    var tmpTab: any = undefined;
    var result = [];
    for (var tab of data) {
      currGroup = tab.group;
      for (var teb of tab.users) {
        tmpTab = teb;
        tmpTab.groupN = currGroup;
        result.push(JSON.parse(JSON.stringify(tmpTab)));
      }
    }
    return result;
  }

  sendRoutes(result: any) {
    if (result !== undefined) {
      var res = this.cleanFirst(result);
      var worku = res[0] !== undefined ? res[0] : [];
      var livu = res[1] !== undefined ? res[1] : [];
      this.rest.postRequest('acl/folders/validation/', { work: worku, live: livu }).subscribe(
        (data: any) => {
          this.foldersResponse = data;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
    }
  }

  sendRoutesACL(result: any) {
    if (result !== undefined) {
      var res = this.cleanFirst(result);
      var worku = res[0] !== undefined ? res[0] : [];
      var livu = res[1] !== undefined ? res[1] : [];
      this.rest.postRequest('acl/work/acls/validation/', { work: worku }).subscribe(
        (data: any) => {
          this.aclsWork = this.cleanACLRoutesArray(data);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
      this.rest.postRequest('acl/live/acls/validation/', { live: livu }).subscribe(
        (data: any) => {
          this.aclsLive = this.cleanACLRoutesArray(data);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
    }
  }

  sendRoutesAccounts(accs: any, usrs: any) {
    if (accs !== undefined) {
      var res = this.cleanFirst(accs);
      if (usrs === undefined) usrs = [];
      var worku = res[0] !== undefined ? res[0] : [];
      var livu = res[1] !== undefined ? res[1] : [];
      this.rest.postRequest('acl/work/accounts/validation/', { accounts: worku, users: usrs }).subscribe(
        (data: any) => {
          this.accsWork = data;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
      this.rest.postRequest('acl/live/accounts/validation/', { accounts: livu, users: usrs }).subscribe(
        (data: any) => {
          this.accsLive = data;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
    }
  }

  cleanFirst(array: any) {
    var ind = -1;
    var result = [];
    for (var tab of array) {
      if (tab.includes("FOUND Folder") || tab.includes("WORK") || tab.includes("LIVE")) {
        ind = tab.includes("WORK") ? 0 : 1;
        result[ind] = [];
      } else
        if (ind >= 0) result[ind].push(tab);
    }
    return result;
  }

  //Funcion que verifica a que fase pertenence la tabla de origen si es que pertenence a una
  phaseCheck(row: any, fase: string, sub: string) {
    if (row.hasOwnProperty(fase)) return row[fase]
    ["in" + fase[0].toUpperCase() + fase.toLowerCase().slice(1)] === 1;
  }

  // Funciones para los botones expoansivos
  toggleExpandUUAAR(group: any, ok: boolean) {
    if (ok) this.uuaaRWork.groupHeader.toggleExpandGroup(group);
    else this.uuaaRLive.groupHeader.toggleExpandGroup(group);
  }

  toggleExpandGroupR(group: any, ok: boolean) {
    if (ok) this.groupRWork.groupHeader.toggleExpandGroup(group);
    else this.groupRLive.groupHeader.toggleExpandGroup(group);
  }

  toggleExpandGroupWork(group: any, ok: boolean) {
    if (ok) this.aclTableWorkOk.groupHeader.toggleExpandGroup(group);
    else this.aclTableWorkNOk.groupHeader.toggleExpandGroup(group);
  }

  toggleExpandGroupLive(group: any, ok: boolean) {
    if (ok) this.aclTableLiveOk.groupHeader.toggleExpandGroup(group);
    else this.aclTableLiveNOk.groupHeader.toggleExpandGroup(group);
  }

  toggleExpandGroupAccs(group: any, work: boolean) {
    if (work) this.accsTableWork.groupHeader.toggleExpandGroup(group);
    else this.accsTableLive.groupHeader.toggleExpandGroup(group);
  }


  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
