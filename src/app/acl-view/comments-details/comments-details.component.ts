import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-comments-details',
  templateUrl: './comments-details.component.html',
  styleUrls: ['./comments-details.component.scss']
})
export class CommentsDetailsComponent implements OnInit {

  title = "";
  isEmpty: boolean = false;
  bigData: any = undefined;
  cleanArray: any = [];
  loaded = false;

  simpleFlag = false;

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any) {
    if (!datas.hasOwnProperty("simple"))
      this.cleanArray = this.cleanValues(datas.comments);
    else {
      this.simpleFlag = true;
      this.title = "Detalles Comentarios Ruta ACL";
      this.bigData = datas.comments;
      this.isEmpty = this.bigData.length <= 1;
    }
  }

  ngOnInit() {
  }

  cleanValues(datas: any) {
    this.title = "Detalles Ruta ACL";
    var clean: string;
    var tmp = { group: "", route: "", perms: "" };
    var tmpArr = [];
    for (var row of datas) {
      clean = row.split(" ");
      tmp.group = clean[1];
      tmp.route = clean[3];
      tmp.perms = clean[5];
      tmpArr.push(JSON.parse(JSON.stringify(tmp)));
    }
    return tmpArr;
  }

}
