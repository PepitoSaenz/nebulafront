import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AclViewComponent } from './acl-view.component';

describe('AclViewComponent', () => {
  let component: AclViewComponent;
  let fixture: ComponentFixture<AclViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AclViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AclViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});