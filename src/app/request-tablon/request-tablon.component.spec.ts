import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestTablonComponent } from './request-tablon.component';

describe('RequestTablonComponent', () => {
  let component: RequestTablonComponent;
  let fixture: ComponentFixture<RequestTablonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestTablonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestTablonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
