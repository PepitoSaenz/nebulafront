
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-request-tablon',
  templateUrl: './request-tablon.component.html',
  styleUrls: ['./request-tablon.component.scss']
})
export class RequestTablonComponent implements OnInit {

  uuaaOptions = [];
  periodicityOptions = [];
  idProject = "";
  request:any;
  tablon:any;
  tittle = "";

  viewFlag: boolean = false;
  answerFlag: boolean = false;
  answerResult: string = "";
  activeHelp: string = "";

  requiredFormControlPerm = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlMaster = new FormControl('', [Validators.required, Validators.minLength(2)]);

  requiredFormControlComment = new FormControl('', [Validators.required, Validators.minLength(2)]);

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public rest: RestService,
      public snackBar: MatSnackBar, private dialogRef: MatDialogRef<RequestTablonComponent>) {    
    console.log(datas);
    if(datas.hasOwnProperty("viewOnly")) this.viewFlag = true;
    this.tablon = JSON.parse(JSON.stringify(datas.backlog));
    this.request = datas;
    this.idProject = this.request.idProject;
    this.initDialogState(datas);
    this.cleanData();
    this.getUuaas();
    this.getPeriodicityOptions();
  }

  ngOnInit() {

  }

  //Lee el estado de la solicitud para permitir que se Acepte/Rechaze o solo se consulte
  initDialogState(datas: any) {
    if (datas.hasOwnProperty("requestStatus")) {
      if (datas.requestStatus === "A" || datas.requestStatus === "D") {
        this.answerFlag = true;
        //this.disableAllFields();
        this.requiredFormControlComment.disable();
        if (datas.requestStatus === "A") {
          this.answerResult = "Aprobada";
          this.tittle = "Solicitud Aprobada - Tabla: " + datas.backlog.alias;
        } else {
          this.answerResult = "Descartada";
          this.tittle = "Solicitud Descartada - Tabla: " + datas.backlog.alias;
        }
      }
      this.tittle = 'Solicitud '+ (datas.action === "Add" ? "Agregar Tablón: " : "Eliminar Tablón: ") + datas.backlog.alias;
    } else
      this.tittle = "I AM BROKEN"
  }

  getUuaas(){
    this.rest.getRequest("uuaa/master/allfull/").subscribe((data: any) => {
      this.uuaaOptions = data;
    });
  }

  getPeriodicityOptions(){
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicityOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Limpia datos que tengan que ser arreglados para mostrar al usuario
  cleanData() {
    this.tablon.stateTable = this.tablon.stateTable === 'N' ? 'Propuesta' 
      : this.tablon.stateTable === 'G' ? 'Gobierno' : 'Arquitectura';
    this.tablon.tacticalObject = this.tablon.tacticalObject === 'YES' ? 'SI' : 'NO';
    this.tablon.perimeter = (this.tablon.perimeter === "" ? "-" : this.tablon.perimeter);
    this.request.comment = (this.request.comment === undefined ? "" : this.request.comment);
  }

  approveSol() {
    if(this.request.comment.length === 0) this.request.comment = "<< OK >>";
    let response = {
      solResult: "approve",
      dialogTempData: this.request
    }
    this.sendClosedResponse(response);
  }

  rejectSol() {
    if(this.requiredFormControlComment.hasError('required')) {
      this.openSnackBar("El comentario es obligatorio cuando se rechaza una solicitud", "Error");
    } else {
      let response = {
        solResult: "reject",
        dialogTempData: this.request
      }
      this.sendClosedResponse(response);
    }
  }
  
  discardSol(){
    this.request.comment = "<< DESCARTADA >>";
    let response = {
      solResult: "discard",
      dialogTempData: this.request
    }
    this.sendClosedResponse(response);
  }

  showDataTable() {
    window.open(this.rest.getHomeWebAddress() + "tablon/view?idTablon=" 
      + this.request.idBacklog, '_blank');
  }

  sendClosedResponse(response: any) {
    this.dialogRef.close(response);
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
