import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegacySearchComponent } from './legacy-search.component';

describe('LegacySearchComponent', () => {
  let component: LegacySearchComponent;
  let fixture: ComponentFixture<LegacySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegacySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegacySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
