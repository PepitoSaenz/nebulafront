import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar,MatSnackBarConfig } from '@angular/material';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-legacy-search',
  templateUrl: './legacy-search.component.html',
  styleUrls: ['./legacy-search.component.scss']
})
export class LegacySearchComponent implements OnInit {

  legacyNaming="";
  legacyDescription="";
  result:any=[];
  loadingIndicator=false;
  tableOffset = 0;
  
  @ViewChild('myTable') table: any;

  constructor(public snackBar: MatSnackBar, public rest:RestService) { }
  
  ngOnInit() {}
  
  clean(){
    this.legacyNaming="";
    this.legacyDescription="";
    this.result=[];
    this.tableOffset = 0;
  }

  queryLegacyTables() {
    this.loadingIndicator = true;
    var query=this.legacyNaming;
    if(query.length>0){
      this.rest.getRequest('dataDictionaries/legacys/'+query+'/').subscribe(
        (data: any) => {
            this.result = data;
            setTimeout(() => { this.loadingIndicator = false; }, 1000);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.","Error");
        }
      );
    }
    this.tableOffset = 0;
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
  }

  onChange(event) {
    this.tableOffset = event.offset;
  }

  deleteField(row) {
    let index=this.result.indexOf(row);
    let temp = [...this.result];
    temp.splice(index, 1);
    this.result = temp;
  }

  openOriginPage(row){
    window.open(this.rest.getHomeWebAddress() + "tables/edit?idTable=" + row._id, '_blank');
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if(action=="Ok"){
        config.panelClass = ['sucessMessage'];
    }else{
        config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
