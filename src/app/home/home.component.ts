import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { RestService } from "../rest.service";
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {

  fields = [];
  loadingFlag = false;
  showPendingTablesDiv = true;
  rol = "";
  stateTables = []

  restObject: any;

  //Data
  currentTables: any = [];
  currentTablesBk: any = [];

  ourTables = [];
  teamOption: any = [];
  teamSelect = "";
  userOption: any = [];
  userSelect = "";
  //Reference
  tableTittle = "";
  state = "";

  tmpSearchData = [];
  searchFilterIngestaName: any;
  searchFilterIngestaAlias: any;
  searchFilterGobiernoName: any;
  searchFilterGobiernoAlias: any;

  searchFilterLogicName: string = "";
  searchFilterAlias: string = "";

  tablonesGobierno = [];
  tmpSearchDataTablones = [];
  searchFilterTablonesGobiernoName: any;
  searchFilterTablonesGobiernoAlias: any;
  
  tablonesPendUser = [];
  tmpTablonesPendUsers = [];
  searchTablonesPenUsersName: any;
  searchTablonesPenUsersAlias: any;

  teamOptionTablones: any = [];
  teamSelectTablones = "";
  userOptionTablones: any = [];
  userSelectTablones = "";
  stateTablones = []
  stateTablon = "";
  ourTablones = [];

  searchBaseName = "";
  searchAlias = "";
  searchRawName = "";
  searchMasterName = "";
  currFilters = [];

  constructor(public rest: RestService, private router: Router, public snackBar: MatSnackBar) {
    if (this.rol == null)
      this.router.navigate(["/login"]);
    this.rol = sessionStorage.getItem("rol")

    this.stateTables = [
      //{value: 'I', viewValue: 'Ingestado'},
      //{value: 'IN', viewValue: 'Listo para ingestar'},
      //{value: 'M', viewValue: 'Mallas'},
      //{value: 'P', viewValue: 'Producción'},
      //{value: 'Q', viewValue: 'Calidad'},
      { value: 'G', viewValue: 'Gobierno' },
      { value: 'N', viewValue: 'En propuesta' },
    ];
    this.stateTablones = this.stateTables;
  }

  ngOnInit() {
    //Default
    this.state = "G";
    this.getSearch('DD');
    window.dispatchEvent(new Event('resize'));
  }

  //Metodo propio activado cuando se cambia de pestaña entre tablas y tablones
  tabChanged(event: any) {
    let tmpState = this.state;
    this.cleanSearchFilters();
    this.state = tmpState;

    if (event.index === 0)
      this.getSearch('DD');
    else if (event.index === 1)
      this.getSearch('DT');
  }

  //Limpia todos los filtros de busqueda actuales y restaura la busqueda actual.
  cleanSearchFilters() {
    this.searchBaseName = "";
    this.searchAlias = "";
    this.searchRawName = "";
    this.searchMasterName = "";
    this.currFilters = [];
    this.currentTables = this.currentTablesBk;
  }

  //Metodo de busqueda que llama las busquedas de tablas o tablones por defecto respectivas.
  selectedSearch(type: string, value: string){
    this.state = value;
    this.getSearch(type);
  }

  //Metodo que filtra las tablas o tablones con los valores enviados como parametro.
  //searchType: La propiedad de busqueda, como alias, nombre.
  //searchValue: El valor de la propiedad a buscar.
  filterCurrentTables(searchType: string, searchValue: string) {
    let found = this.currFilters.map(function(x) {return x.type;}).indexOf(searchType);
    if (found == -1) {
      this.currFilters.push({
        type: searchType,
        value: searchValue
      });
    } else {
      if (searchValue.trim().length == 0)
        this.currFilters.splice(found, 1)
      else
        this.currFilters[found]["value"] = searchValue;
    }

    this.currentTables = this.currentTablesBk;
    for (let filter of this.currFilters)
      this.filterByProp(filter["type"], filter["value"]);
  }

  //Metodo para filtrar el arreglo actual de tablas o tablones de busqueda.
  private filterByProp(type: string, value: string) {
    const val = value.toLowerCase();
    const temp = this.currentTables.filter(function (d) {
      return d[type].toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.currentTables = temp;
  }

  //Trae todasl as tablas dado solo el estado (Puede traer muchas tablas)
  getSearch(searchType: string){
    let ddFlag = searchType == 'DD' ? true : false

    if (this.loadingFlag) {
      this.currentTables = []
      this.restObject.unsubscribe();
    } else
      this.loadingFlag = true;

    if (this.state.length > 0) {
      this.restObject = this.rest.getRequest(''+ (ddFlag ? 'dataDictionaries' : 'dataTables') + '/state=' + this.state + '/').subscribe(
        (data: any) => {
          this.currentTables = data;
          this.currentTablesBk = data;
          this.openSnackBar("Búsqueda " + (ddFlag ? 'Tablas' : 'Tablones') + " completa.", "Ok");
          this.loadingFlag = false;
        });
    } else {
        this.openSnackBar("Por favor, seleccione un estado.", "Error");
        this.loadingFlag = false;
    }

  }

  //Metodo que envia a la vista de edicion de la tabla o tablon.
  goToEdit(row: any, option: string){
    this.router.navigate([( option == 'DT' ? "tablon" : "tables") + "/edit"], { queryParams: ( option == 'DT' ? { idTablon: row._id } : { idTable: row._id })});
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 5000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
