import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MatSnackBar,MatSnackBarConfig } from '@angular/material';

import { RestService } from '../rest.service';

@Component({
  selector: 'app-search-table',
  templateUrl: './search-table.component.html',
  styleUrls: ['./search-table.component.scss']
})
export class SearchTableComponent implements OnInit {

  firstTime: boolean = true;

  //Variables de entorno
  idCase: string = "";
  searchType: string = ""; //Tablas o tablones
  useCaseInfo: any = {"tables": []};

  //Opciones de busqueda
  originSystemOptions: any[] = [];
  uuaaMasterOptions: any[] = [];

  //Valores de busqueda
  baseNameQuery: string = "";
  aliasQuery: string = "";
  originSystemQuery: string = "";
  observationFieldQuery: string = "";
  uuaaMasterQuery: string = "";
  uuaaRawQuery: string = "";

  //Arreglos de valores de busqueda
  tablesQuery: any[] = [];
  selectedTables: any[] = [];

  //Valores de apariencia y/o funcionamiento
  loadingFlag: boolean = false;
  showDescRow: boolean = false;


  @ViewChild('table') table;
  @ViewChild('orginSystemOpt') selectorOrigin;
  @ViewChild('uuaaSelectorOption') selectorUuaaMaster;

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public rest:RestService, public snackBar: MatSnackBar, private dialogRef: MatDialogRef<SearchTableComponent>) {
    this.idCase = this.datas.use_case["_id"];
    this.searchType = this.datas.searchType;
    this.useCaseInfo = this.datas.use_case;

    this.getOrigSysOptions();
    this.getMasterUuaas();
    this.queryData();
  }

  ngOnInit(){
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, (400));
  }

  //Metodo sobreescrito para el ngx-datatable que se invoca cada vez que se selecciona un elemento en la tabla
  //  para agregar al arreglo final al momento de aceptar.
  //(param) selected: El elemento seleccionado en la tabla
  onSelect({ selected }){
    this.selectedTables = [];
    this.selectedTables.push(...selected);
  }

  getOrigSysOptions(){
    this.rest.getRequest('projects/backlogs/atributes/originSystem/').subscribe(
      (data: any) => {
        this.originSystemOptions=data;
      });
  }

  getMasterUuaas(){
    this.rest.getRequest("dataDictionaries/uuaas/masterOptions/").subscribe((data: any) => {
      this.uuaaMasterOptions = data;
    });
  }

  //Metodo que realiza la peticion al back para la busqueda de la data con los parametros enviados en la url.
  //(param) url: La url con los parametros de busqueda.
  queryData() {
    var baseNameTmp = this.baseNameQuery.length > 0 ? this.baseNameQuery : "%20";
    var aliasTmp = this.aliasQuery.length > 0 ? this.aliasQuery : "%20";
    var originSystemTmp = this.originSystemQuery.length > 0 ? this.originSystemQuery : "%20";
    var observationFieldTmp = this.observationFieldQuery.length > 0 ? this.observationFieldQuery : "%20";
    var uuaaRawTmp = this.uuaaRawQuery.length > 0 ? this.uuaaRawQuery : "%20";
    var uuaaMasterTmp = this.uuaaMasterQuery.length > 0 ? this.uuaaMasterQuery : "%20";

    let url = `projects/backlogs/query/id_case=${this.idCase}&search_type=${this.searchType}&base_name=${baseNameTmp}`
    + `&alias=${aliasTmp}&origin_system=${originSystemTmp}&observation_field=${observationFieldTmp}&uuaa_raw=${uuaaRawTmp}&uuaa_master=${uuaaMasterTmp}/`;
    this.loadingFlag = true;
    this.rest.getRequest(url).subscribe(
      (data: any) => {
        this.tablesQuery = data;
        this.loadingFlag = false;
        this.cleanBacklog("tables")
      });
  }


  cleanBacklog(array_name: string){
    for (let value of this.useCaseInfo[array_name]) {
      this.tablesQuery = this.tablesQuery.filter(option =>
        !option['_id'].toLowerCase().includes(value)
      );
    }
  }

  validateTables(){
    var resultIds=[];
    if (this.selectedTables.length>0){
      for(var j = 0; j<this.selectedTables.length; j++){
        /*
        var flag = this.useCaseInfo.tables.find(c => {
          if(c == this.selectedTables[j]._id) return true;
        });
        if (!flag){
          resultIds.push(this.selectedTables[j]._id);
        }
        */
        resultIds.push(this.selectedTables[j]["_id"]);
      }
      var res = {
        tableIds: resultIds,
        tables: this.selectedTables
      }
      this.dialogRef.close(res);
    }else{
      this.dialogRef.close(null);
    }
  }

  cleanQuery(){
    this.baseNameQuery = "";
    this.aliasQuery = "";
    this.originSystemQuery = "";
    this.observationFieldQuery = "";
    this.uuaaMasterQuery = "";
    this.uuaaRawQuery = "";

    this.selectorOrigin.value="";
    this.selectorUuaaMaster.value="";
    this.queryData();
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if(action=="Ok"){
        config.panelClass = ['sucessMessage'];
    }else{
        config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
