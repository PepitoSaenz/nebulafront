import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from "xlsx";

import { RestService } from '../rest.service';
import { LoadTableComponent } from './load-table/load-table.component';
import { UserRequestComponent } from './user-request/user-request.component';
import { ProyectRequestComponent } from './proyect-request/proyect-request.component';
import { RepoDatatableComponent } from './../repo-datatable/repo-datatable.component';

//Hash MD5 de la constraseña "password" para reinicio
const default_pass = "5f4dcc3b5aa765d61d8327deb882cf99";

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.scss']
})
export class AdminViewComponent implements OnInit {
  //Constantes
  userId: string; //Tecnicamente deberia siempre ser el id del admin

  //Variables legacy para depreciar
  projectOptions: any = [];
  useCaseOptions: any = [];
  projectSelected: any = undefined;

  //Constantes para consulta
  teamsOptions: any = [];

  //Variables compartidas de carga
  newData = [];

  //Variables para la carga de Tablas
  fileList: File = null;
  fullArray = [];
  headTabla = [];
  fieldsTablas = [];

  //Variables de carga para Tablones
  fileListTablon: File = null;
  fullArrayTablon = [];
  headTablones = [];
  fieldsTablones = [];

  //Variables de carga para Bases Operacionales
  fileListBases: File = null;
  fullArrayBases = [];
  headBases = [];
  fieldsBases = [];

  //Variables para creacion de Equipos
  teamName: string = "";
  countriesOptions: any = [];
  selteamCountry: any;

  //Variables de busqueda para usuarios
  pendingUsers: any = [];
  allRegisteredUsers = [];
  currRegisteredUsers = [];
  searchFilters = [];
  searchUName = "";
  searchUEmail = "";
  searchURol: any;

  //Variables de busqueda para proyectos
  allProyects: any = [];
  currProyects: any = [];
  searchFiltersProy: any = [];
  searchPName = "";
  searchPDesc = "";

  //Spinner message
  spinnerM: string = "";

  //Loading flags para mini spinner carga tablas/tablones
  loading_table_datum: boolean = false;
  loading_table_sophia: boolean = false;
  loading_table_old: boolean = false;

  masterTables: any = [];
  emptyMasterTables: boolean = true;

  //Arreglo con la data de los roles para crear usuarios.
  rolOptions: any[] = [
    { value: '', viewValue: '' },
    { value: 'I', viewValue: 'Ingesta' },
    { value: 'G', viewValue: 'Gobierno' },
    { value: 'RN', viewValue: 'VoBo Negocio' },
    { value: 'PO', viewValue: 'Gestor de Proyectos' },
    { value: 'SPO', viewValue: 'Super Gestor' }
  ];

  @ViewChild('myTable') table: any; //Tabla para usuarios y proyectos
  @ViewChild('selector') selector: any; //selector legacy de varios elementos

  constructor(public rest: RestService, private router: Router, public dialog: MatDialog, public snackBar: MatSnackBar,
    private spinner: NgxSpinnerService) {
    this.userId = sessionStorage.getItem("userId");
    if (sessionStorage.getItem("rol") !== "A") {
      this.openSnackBar("ACCESO NO AUTORIZADO", "Error");
      this.router.navigate(["/tables/search"]);
    }
  }

  ngOnInit() {
    this.getAllRegisteredUsers();
    this.getProjectOptions();
    this.getTeamsOptions();
    this.getContriesOptions();
  }

  onTabChanged(event: any) {
    this.cleanSearchFilters();
  }

  showSpinner(message: string) {
    this.spinner.show();
    this.spinnerM = message;
  }

  //Trae los usuarios pendientes por asignar rol. Aquellos registrados desde el home de Nebula.
  getPendingUsers() {
    this.rest.getRequest('users/' + this.userId + '/pending/').subscribe(
      (data: any) => {
        this.pendingUsers = data;
      }, (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); });
  }

  //Trae todos los usuarios en nebula.

  getAllRegisteredUsers() {
    this.rest.postRequest("users/listAll/", { "admin": true }).subscribe(
      (data: any) => {
        this.allRegisteredUsers = data;
        this.currRegisteredUsers = this.allRegisteredUsers;
      });
    this.getPendingUsers();
  }

  //Trae todos los proyectos en Nebula.
  getProjectOptions() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
        this.currProyects = data;
        this.allProyects = data;
      });
  }

  //Trae los equipos disponibles en Nebula TO BE DEPRECATED
  getTeamsOptions() {
    this.rest.getRequest("users/teams/").subscribe(
      (data: any) => {
        this.teamsOptions = data;
      });
  }

  //Trae las opciones de paises para la creacion de Equipos. TO BE DEPRECATED 
  getContriesOptions() {
    this.rest.getRequest('users/teams/countries/').subscribe(
      (data: []) => {
        this.countriesOptions = data;
      });
  }

  //Returns the name of a team given its ID
  prettyTeamsName(team_id: string) {
    let result = this.teamsOptions.find(filter => filter['_id'] === team_id);
    return result != undefined ? result["name"] : "undefined";
  }

  //Muestra el nombre del rol natural dada su letra de identificacion.
  //(param) uglyRol: La letra que identifica el rol del usuario.
  //(return) El nombre del rol natural. ej: G > Gobierno
  prettyRol(uglyRol: string) {
    switch (uglyRol) {
      case "I": return "Ingesta";
      case "G": return "Gobierno";
      case "RN": return "Visto Bueno Negocio";
      case "PO": return "Gestor de Proyectos";
      case "SPO": return "SUPER Gestor de Proyectos";
      case "A": return "Administrador";
      //TO BE DEPRECATED
      case "E": return "Extracción";
      case "L": return "En Registro / Invitado Temporal";
    }
  }

  //Metodo que filtra un array dada una propiedad y el valor a filtrar
  private filterArray(array: any[], property: string, value: string) {
    return array.filter(function (d) {
      return d[property].toLowerCase().indexOf(value) !== -1 || !value;
    });
  }

  //Filtra la tabla de datos de un tipo de fuente
  filterData(data: string, type: string, value: any) {
    let allData: any[];
    let currData: any[];
    switch (data) {
      case "users": allData = this.allRegisteredUsers; currData = this.currRegisteredUsers; break;
      case "proyects": allData = this.allProyects; currData = this.currProyects; break;
    }
    currData = allData
    value = value.toLowerCase();
    let prop = { "type": type, "value": value };
    let prevProp = this.searchFilters.find(filter => filter['type'] === type);

    if (value.trim().length > 0)
      if (prevProp == undefined)
        this.searchFilters.push(prop)
      else
        this.searchFilters[this.searchFilters.indexOf(prevProp)] = prop;
    else if (value.trim().length == 0 && prevProp != undefined)
      this.searchFilters.splice(this.searchFilters.indexOf(prevProp))

    for (let filter of this.searchFilters)
      currData = this.filterArray(currData, filter["type"], filter["value"])

    if (type == "rol")
      if (value.trim().length == 0)
        this.searchURol = null;

    switch (data) {
      case "users": this.currRegisteredUsers = currData; break;
      case "proyects": this.currProyects = currData; break;
    }
    this.table.offset = 0;
  }

  //Reinicia los filtros de busqueda para los usuarios
  cleanSearchFilters() {
    this.currRegisteredUsers = this.allRegisteredUsers;
    this.currProyects = this.allProyects;
    this.searchFilters = []
    this.searchUName = ""
    this.searchUEmail = ""
    this.searchPName = ""
    this.searchPDesc = ""
    this.table.offset = 0;
  }

  //Config para el PopUp de la info de usuario.
  private userDialogConfig(data: any) {
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = false;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.width = "60%";
    dialogConfigRequest.minWidth = "800px";
    dialogConfigRequest.minHeight = "350px";
    dialogConfigRequest.data = data;
    return dialogConfigRequest;
  }

  //Metodo que llama el dialog de usuarios para crear uno enviandole el body de un usuario vacio.
  createUser() {
    let new_user = {
      "new_user": true,
      "name": "",
      "rol": "",
      "email": "",
      "teams": [{ _id: "5c2f821f698b6abf9fd5ffbf", name: "Gobierno" }], //Equipo de Gobierno por defecto
      "password": "5f4dcc3b5aa765d61d8327deb882cf99"
    }
    const dialogRef = this.dialog.open(UserRequestComponent, this.userDialogConfig(new_user));
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRegisteredUsers();
    });
  }

  //Solicita el dialog del usuario dado un "row" o body de datos de un user.
  usersRequests(row: any) {
    const dialogRef = this.dialog.open(UserRequestComponent, this.userDialogConfig(row));
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRegisteredUsers();
    });
  }

  //Config para el dialog de la info de un proyecto.
  private proyectDialogConfig(data: any) {
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = false;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.width = "60%";
    dialogConfigRequest.minWidth = "800px";
    dialogConfigRequest.minHeight = "350px";
    dialogConfigRequest.data = data;
    return dialogConfigRequest;
  }

  //Solicita el dialog del proyecto dado un "row" o body de datos de un proyecto.
  proyectsRequests(row: any) {
    const dialogRef = this.dialog.open(ProyectRequestComponent, this.proyectDialogConfig(row));
    dialogRef.afterClosed().subscribe(result => {
      this.getProjectOptions();
    });
  }

  //Metodo que llama el dialog de proyectos para crear uno enviandole el body de un proyecto vacio.
  createProject() {
    let new_project = {
      "new_project": true,
      "name": "",
      "shortDesc": "",
      "useCases": [],
      "VoBo": [],
      "teams": ["5c2f821f698b6abf9fd5ffbf"], //Equipo de Gobierno por defecto
      "owners": [],
      "countries": ["Colombia"],
      "technicalResponsible": "datahub.co.group@bbva.com",
    };
    const dialogRef = this.dialog.open(ProyectRequestComponent, this.proyectDialogConfig(new_project));
    dialogRef.afterClosed().subscribe(result => {
      this.getProjectOptions();
    });
  }

  //Trae los casos de uso de un proyecto dado.
  getProjectUseCases(idProject: string) {
    this.rest.getRequest('projects/' + idProject + '/useCases/').subscribe(
      (data: any) => {
        this.useCaseOptions = data;
      });
  }

  //Construye el body de una tabla leyendo todos los campos que este contenga en el excel y construyendo formato para back.
  //con el formato nuevo/actual de Sophia
  buildFullTablaDatum(result: any) {
    this.newData = result;
    var flagObject = result[0] ? true : false;
    var flagFields = result[1] ? true : false;
    var tmpRow = {
      phys_name: flagObject ? result[0][0]["Physical name object"] : result[1][0]["Physical name object"],
      baseName: flagObject ? result[0][0]["Logical name object"] : "--",
      observationField: flagObject ? result[0][0]["Description object"] : "--",
      alias: flagObject ? result[0][0]["Tags"] : result[1][0]["Tags"],
      column: 0,
      fields: flagFields ? result[1].length : "--",
      periodicity: flagObject ? result[0][0]["Frequency"] : "--",
      uuaa: flagObject ? result[0][0]["System code"] : "--",
      storageZone: flagObject ? result[0][0]["Storage Zone"] : "--",
      type: "DD"
    };

    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "820px";
    dialogConfigRequest.minHeight = "345px";
    dialogConfigRequest.data = tmpRow;
    const dialogRef = this.dialog.open(LoadTableComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          var carga = {
            project_id: result.projec_id,
            use_case_id: result.use_case_id,
            user: result.user,
            alias: tmpRow["alias"],
            new: false,
            load_type: flagObject && flagFields ? "full" : (flagFields ? "fields" : "object"),
            newData: this.newData,
            datum: true
          }
          this.loading_table_datum = true;
          this.rest.postRequest('projects/backlogs/dataDictionaryNew/import/', carga).subscribe(
            (res: any) => {
              this.openSnackBar("La TABLA fue cargada exitosamente " + res, "Ok");
              this.loading_table_datum = false;
            }, (error) => {
              this.openSnackBar(error.error.reason, "Error");
            });
        }
      });
  }

  //Carga una tabla con la plantilla que Datum exporta
  loadTablasDatum() {
    var result = [];
    var arrayBuffer: any;

    this.fullArray = [];
    this.headTabla = [];
    this.fieldsTablas = [];

    if (this.fileList != null) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        try {
          var workbook = XLSX.read(new Uint8Array(arrayBuffer), { type: 'array' });
        } catch (error) {
          this.openSnackBar("Error: " + error.message, "Error");
          return null;
        }
        var tempResult = [];
        var csvKeys = [];
        tempResult = XLSX.utils.sheet_to_json(workbook.Sheets["Sheet1"], { defval: "" });
        csvKeys = Object.keys(tempResult[0]);
        if (csvKeys.includes("Field ID")){
          result[1] = tempResult; // Determina si el archivo es de tipo FIELD
        } else {
          result[0] = tempResult;// Determina que el archivo es de tipo OBJECT
        }
        this.buildFullTablaDatum(result);
      }
      fileReader.readAsArrayBuffer(this.fileList);
      this.openSnackBar("Archivo TABLA cargado correctamente", "Ok");
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
    }
    this.onFileChange(null);
  }

  //Construye el body de una tabla leyendo todos los campos que este contenga en el excel y construyendo formato para back.
  //con el formato nuevo/actual de Sophia
  buildFullTablaNew(result: any) {
    this.newData = result;

    var flagObject = result[0] ? true : false;
    var flagFields = result[1] ? true : false;
    var tmpRow = {
      phys_name: flagObject ? result[0][0]["PHYSICAL NAME OBJECT"] : result[1][0]["PHYSICAL NAME OBJECT"],
      baseName: flagObject ? result[0][0]["LOGICAL NAME OBJECT"] : "--",
      observationField: flagObject ? result[0][0]["DESCRIPTION OBJECT"] : "--",
      alias: flagObject ? result[0][0]["TAGS"] : result[1][0]["TAGS"],
      column: 0,
      fields: flagFields ? result[1].length : "--",
      periodicity: flagObject ? result[0][0]["FREQUENCY"] : "--",
      uuaa: flagObject ? result[0][0]["SYSTEM CODE/UUAA"] : "--",
      storageZone: flagObject ? result[0][0]["STORAGE ZONE"] : "--",
      type: "DD"
    };

    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "820px";
    dialogConfigRequest.minHeight = "345px";
    dialogConfigRequest.data = tmpRow;
    const dialogRef = this.dialog.open(LoadTableComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          var carga = {
            project_id: result.projec_id,
            use_case_id: result.use_case_id,
            user: result.user,
            alias: tmpRow["alias"],
            new: false,
            load_type: flagObject && flagFields ? "full" : (flagFields ? "fields" : "object"),
            newData: this.newData,
            datum: false
          }
          this.loading_table_sophia = true;
          this.rest.postRequest('projects/backlogs/dataDictionaryNew/import/', carga).subscribe(
            (res: any) => {
              this.openSnackBar("La TABLA SOPHIA fue cargada exitosamente " + res, "Ok");
              this.loading_table_sophia = false;
            }, (error) => {
              this.openSnackBar(error.error.reason, "Error");
            });
        }
      });
  }

  //Carga una tabla con la plantilla que Sophia exporta
  loadTablasNew() {
    var result = [];
    var arrayBuffer: any;

    this.fullArray = [];
    this.headTabla = [];
    this.fieldsTablas = [];

    if (this.fileList != null) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        try {
          var workbook = XLSX.read(new Uint8Array(arrayBuffer), { type: 'array' });
        } catch (error) {
          if (error.message.includes("password-protected"))
            this.openSnackBar("El excel esta protegido por contraseña", "Error");
          else this.openSnackBar("Error: " + error.message, "Error");
          return null;
        }
        for (var tabo of workbook.SheetNames) {
          if (tabo == "DDNG-O")
            result[0] = XLSX.utils.sheet_to_json(workbook.Sheets[tabo], { defval: "" });
          if (tabo == "DDNG-F")
            result[1] = XLSX.utils.sheet_to_json(workbook.Sheets[tabo], { defval: "" });
        }
        this.buildFullTablaNew(result);
      }
      fileReader.readAsArrayBuffer(this.fileList);
      this.openSnackBar("Archivo TABLA cargado correctamente", "Ok");
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
    }
    this.onFileChange(null);
  }

  //Devuelve el alias de un nombre de tabla o tablon unicamente
  calcAlias(name: string) {
    var temp = name.split("_");
    temp.splice(0, 2);
    return temp[0];
  }

  //Limpia un json de carga y devuelve un arreglo con los elementos de ese json, uno detras de otro
  cleanRowJSON(jeison: any) {
    let ind = 0;
    let finished = false;
    var resArr = [];
    while (!finished) {
      if (jeison !== undefined && jeison.hasOwnProperty(ind)) {
        resArr.push(jeison[ind]);
      } else
        finished = true;
      ind += 1;
    }
    return resArr;
  }

  //Construye los fields de una tabla a cargar 
  buildFields(alias: string, data: any) {
    var finished = false;
    var ind = 0;
    var resArr = [];
    var endArr = [];
    for (let obj of data) {
      if (this.calcAlias(obj["1"]) === alias) {
        ind = 0;
        finished = false;
        resArr = [];
        while (!finished) {
          if (obj.hasOwnProperty(ind)) {
            resArr.push(obj[ind]);
          } else
            finished = true;
          ind += 1;
        }
        resArr.splice(32); //Para que llegue hasta el campo de TDS ONLY
        endArr.push(resArr);
      }
    }
    return endArr;
  }

  //Construye el objeto completo para enviar al back con la peticion de carga de una tabla
  //leyendo todos los campos que este contenga en el excel
  buildFull(result: any) {
    this.fullArray = [];
    var mapo = new Map();
    for (let ind = 0; ind < result[0].length; ind++) {
      let obj = result[0][ind];
      if (!mapo.has(this.calcAlias(obj[1]))) {
        let tmpRaw = this.cleanRowJSON(obj);
        let tmpMas = (result[1] ? this.cleanRowJSON(result[1][ind]) : []);
        let tmpFieldsRaw = this.buildFields(this.calcAlias(obj[1]), result[2]);
        let tmpFieldsMaster = (result[3] ? this.buildFields(this.calcAlias(obj[1]), result[3]) : []);
        var finalObj = {
          alias: this.calcAlias(obj[1]),
          object: {
            raw: tmpRaw,
            master: tmpMas
          },
          fields: {
            raw: tmpFieldsRaw,
            master: tmpFieldsMaster
          }
        };
        this.fullArray.push(JSON.parse(JSON.stringify(finalObj)));
      }
    }
  }

  //Limpia una hoja de un archivo excel/csv para su lectura.
  cleanCSV(sheet: any) {
    sheet.splice(0, 3);
    return sheet;
  }

  //Carga una tabla con format de excel de sophia viejo, con los encabezados con colores
  loadLegacy() {
    this.showSpinner("Cargando Tablas...");
    this.loading_table_old = true;

    var result = [];
    var tmpSheet: any;
    var arrayBuffer: any;
    var isFine = true;

    if (this.fileList != null) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        try {
          var workbook = XLSX.read(data, { type: 'array' });
        } catch (error) {
          isFine = false;
          if (error.message.includes("password-protected"))
            this.openSnackBar("El excel esta protegido por contraseña", "Error");
          else this.openSnackBar("Error: " + error.message, "Error");
        }
        if (isFine) {
          for (var tabo of workbook.SheetNames) {
            tmpSheet = workbook.Sheets[tabo];
            switch (tabo) {
              case "DC-DD-Object-RAW":
                result[0] = this.cleanCSV(XLSX.utils.sheet_to_json(tmpSheet, { defval: "" }));
                break;
              case "DC-DD-Object-MASTER":
                result[1] = this.cleanCSV(XLSX.utils.sheet_to_json(tmpSheet, { defval: "" }));
                break;
              case "DC-DD-Field-RAW":
                result[2] = this.cleanCSV(XLSX.utils.sheet_to_json(tmpSheet, { defval: "" }));
                break;
              case "DC-DD-Field-MASTER":
                result[3] = this.cleanCSV(XLSX.utils.sheet_to_json(tmpSheet, { defval: "" }));
                break;
            }
          }
          this.buildFull(result);
        }
      }
      fileReader.readAsArrayBuffer(this.fileList);
      this.openSnackBar("Archivo cargado correctamente", "Ok");
      setTimeout(() => { this.spinner.hide(); this.loading_table_old = false; }, 2000);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
      this.spinner.hide();
      this.loading_table_old = false;
    }
  }

  //Envia la peticion al back para cargar una sola tabla, ya con el backlog y el usuario responsable
  //seleccionados
  sendLoadTable(data: any, index: any) {
    this.rest.postRequest('projects/backlogs/dataDictionary/import/', data).subscribe(
      (res: any) => {
        this.fullArray.splice(index, 1);
        this.fullArray = [...this.fullArray];
        this.openSnackBar("Tabla " + data.alias + " cargada exitosamente", "Ok");
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  //Abre el dialog para cargar una tabla previamente detectada en el excel de carga
  loadTable(row: any) {
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = row;
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "820px";
    dialogConfigRequest.minHeight = "345px";
    const dialogRef = this.dialog.open(LoadTableComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          let index = this.fullArray.indexOf(row);
          this.sendLoadTable(result.dialogTempData, index);
        }
      });
  }

  //leyendo todos los campos que este contenga en el excel y construyendo formato para back.
  buildFullTablonNew(result: any) {
    this.newData = result;
    var flagObject = result[0] ? true : false;
    var flagFields = result[1] ? true : false;
    var tmpRow = {
      master_name: flagObject ? result[0][0]["PHYSICAL NAME OBJECT"] : result[1][0]["PHYSICAL NAME OBJECT"],
      baseName: flagObject ? result[0][0]["LOGICAL NAME OBJECT"] : "--",
      observationField: flagObject ? result[0][0]["DESCRIPTION OBJECT"] : "--",
      alias: flagObject ? result[0][0]["TAGS"] : result[1][0]["TAGS"],
      column: 0,
      fields: flagFields ? result[1].length : "--",
      periodicity: flagObject ? result[0][0]["FREQUENCY"] : "--",
      uuaaMaster: flagObject ? result[0][0]["SYSTEM CODE/UUAA"] : "--",
      type: "T"
    }

    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "820px";
    dialogConfigRequest.minHeight = "345px";
    dialogConfigRequest.data = tmpRow;
    const dialogRef = this.dialog.open(LoadTableComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          var carga = {
            project_id: result.projec_id,
            use_case_id: result.use_case_id,
            user: result.user,
            data_table: {
              object: this.headTablones[result[0]],
              fields: this.fieldsTablones[result[1]]
            },
            alias: tmpRow["alias"],
            new: true,
            load_type: flagObject && flagFields ? "full" : (flagFields ? "fields" : "object"),
            newData: this.newData
          }
          this.rest.postRequest('projects/backlogs/dataTable/import/', carga).subscribe(
            (res: any) => {
              this.openSnackBar("El tablón fue cargado exitosamente " + res, "Ok");
            }, (error) => {
              this.openSnackBar(error.error.reason, "Error");
            });
        }
      });
    this.onFileChangeTablon(null);
  }

  //Carga del excel para subir tablones con formato de Sophia y para un tablon a la vez.
  loadTablonesNew() {
    this.fullArrayTablon = [];
    this.headTablones = [];
    this.fieldsTablones = [];

    var result = [];
    var arrayBuffer: any;

    this.showSpinner("Cargando Tablas...");
    if (this.fileListTablon != null) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        try {
          var workbook = XLSX.read(new Uint8Array(arrayBuffer), { type: 'array' });
        } catch (error) {
          if (error.message.includes("password-protected"))
            this.openSnackBar("El excel esta protegido por contraseña", "Error");
          else this.openSnackBar("Error: " + error.message, "Error");
          return null;
        }

        for (var tabo of workbook.SheetNames) {
          if (tabo == "DDNG-O")
            result[0] = XLSX.utils.sheet_to_json(workbook.Sheets[tabo], { defval: "" });
          if (tabo == "DDNG-F")
            result[1] = XLSX.utils.sheet_to_json(workbook.Sheets[tabo], { defval: "" });
        }
        this.buildFullTablonNew(result);
      }
      fileReader.readAsArrayBuffer(this.fileListTablon);
      this.openSnackBar("Archivo cargado correctamente", "Ok");
      setTimeout(() => { this.spinner.hide(); }, 2000);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
      this.spinner.hide();
    }
  }

  //Construye el objeto de un Tablon a cargar
  cleanRowJsonObject(result) {
    var tablonesNames = [];
    var objects = []
    for (let index = 0; index < result.length; index++) {
      const element = result[index];
      var row = [];
      for (const key in element) {
        if (element.hasOwnProperty(key)) {
          const item = element[key];
          row.push(item);
        }
      }
      var tag = String(element["TAGS"]).trim().toLowerCase();
      if (tablonesNames.indexOf(tag) == -1) {
        objects.push(row);
        tablonesNames.push(tag);
      }
    }
    return objects;
  }

  //Construye los fields de un Tablon a cargar
  cleanRowJsonFields(result: any[], objects: any[]) {
    var tablesNames = [];
    var fields = [];
    for (let index = 0; index < objects.length; index++) {
      fields[index] = [];
      tablesNames.push(objects[index][33]);
    }
    var fieldsObject = [];
    var tag = "";
    tag = String(result[0]["TAGS"]).trim().toLowerCase();
    for (let index = 0; index < result.length; index++) {
      const element = result[index];
      var tagTemp = String(element["TAGS"]).trim().toLowerCase();
      var row = []
      for (const key in element) {
        if (element.hasOwnProperty(key)) {
          const item = element[key];
          row.push(item);
        }
      }
      if (tag != tagTemp) {
        var indexObject = tablesNames.indexOf(tag)
        if (indexObject != -1) {
          fields[indexObject] = fieldsObject;
        }
        tag = tagTemp;
        fieldsObject = [];
        fieldsObject.push(row);
      } else {
        fieldsObject.push(row);
        if (index + 1 == result.length) {
          var indexObject = tablesNames.indexOf(tag)
          if (indexObject != -1) {
            fields[indexObject] = fieldsObject;
          }
        }
      }
    }
    return fields;
  }

  //Construye los tablones a cargar con formato de sophia en un archivo masivo
  //leyendo todos los campos que este contenga en el excel y construyendo formato para back.
  buildFullTablon(result: any) {
    this.newData = result;

    if (result[0])
      this.headTablones = this.cleanRowJsonObject(result[0]);
    if (result[1])
      this.fieldsTablones = this.cleanRowJsonFields(result[1], this.headTablones);

    for (let index = 0; index < this.headTablones.length; index++) {
      var tmpJson = {
        master_name: this.headTablones[index][2],
        baseName: this.headTablones[index][3],
        observationField: this.headTablones[index][4],
        alias: this.headTablones[index][33],
        column: index,
        fields: result[1] ? this.fieldsTablones[index].length : 99,
        periodicity: this.headTablones[index][17],
        uuaaMaster: this.headTablones[index][15],
        type: "T",
        new: false,
      }
      this.fullArrayTablon.push(tmpJson);
    }
    this.fullArrayTablon = [...this.fullArrayTablon];
    this.onFileChangeTablon(null);
  }

  //Carga del excel para subir tablones con formato Sophia para varios tablones a la vez.
  loadTablones() {
    this.fullArrayTablon = [];
    this.headTablones = [];
    this.fieldsTablones = [];
    this.showSpinner("Cargando Tablas...");
    var result = [];
    var tmpSheet: any;
    var arrayBuffer: any;
    var isFine = true;
    if (this.fileListTablon != null) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        try {
          var workbook = XLSX.read(data, { type: 'array' });
        } catch (error) {
          isFine = false;
          if (error.message.includes("password-protected"))
            this.openSnackBar("El excel esta protegido por contraseña", "Error");
          else this.openSnackBar("Error: " + error.message, "Error");
        }
        if (isFine) {
          for (var tabo of workbook.SheetNames) {
            tmpSheet = workbook.Sheets[tabo];
            switch (tabo) {
              case "DDNG-O":
                result[0] = XLSX.utils.sheet_to_json(tmpSheet, { defval: "" });
                break;
              case "DDNG-F":
                result[1] = XLSX.utils.sheet_to_json(tmpSheet, { defval: "" });
                break;
            }
          }
          this.buildFullTablon(result);
        }
      }
      fileReader.readAsArrayBuffer(this.fileListTablon);
      this.openSnackBar("Archivo cargado correctamente", "Ok");
      setTimeout(() => { this.spinner.hide(); }, 2000);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
      this.spinner.hide();
    }
  }

  //Envia la peticion al back para cargar un solo tablon
  sendLoadTablon(data: any, index: any) {
    this.rest.postRequest('projects/backlogs/dataTable/import/', data).subscribe(
      (res: any) => {
        this.fullArrayTablon.splice(index, 1);
        this.fullArrayTablon = [...this.fullArrayTablon];
        this.openSnackBar("El tablón " + data.data_table.object[33] + " fue cargado exitosamente", "Ok");
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  //Abre el dialog para cargar una tabla previamente detectada en el excel de carga
  loadTablon(row) {
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = row;
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "820px";
    dialogConfigRequest.minHeight = "345px";
    const dialogRef = this.dialog.open(LoadTableComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          let index = this.fullArrayTablon.indexOf(row);
          var carga = {
            project_id: result.projec_id,
            use_case_id: result.use_case_id,
            user: result.user,
            data_table: {
              object: this.headTablones[result.data.column],
              fields: this.fieldsTablones[result.data.column]
            },
            new: false,
            newData: this.newData
          }
          this.sendLoadTablon(carga, index);
        }
      });
  }

  cleanRowJsonBases(objFile) {
    //Construye el body del objeto de una base operacional a cargar.
    //:parametro: (objFile): Se refiere a la pagina 'DC-DD-O' del archivo de excel cargado 
    //:retorno: objects: Arreglo de objetos con la info de las filas
    var objects = []
    var basesNames = []

    for (let index = 1; index < objFile.length; index++) {
      const element = objFile[index]
      // var row = [];
      // for (const key in element) {
      //   if (element.hasOwnProperty(key)) {
      //     const item = element[key];
      //     row.push(item);
      //   }
      // }
      var tableName = String(element["Nombre físico objeto"]).trim().toLowerCase();
      if (tableName != '' && basesNames.indexOf(tableName) == -1) {
        // objects.push(row);
        objects.push(element)
        basesNames.push(tableName)
      }
    }
    return objects;
  }

  cleanRowJsonFieldsBases(fldFile, objects) {
    //Construye los fields de una base operacional a cargar
    //:parametro (fldFile): Se refiere a la pagina 'DC-DD-F' del archivo de excel cargado 
    //:parametro (objects): El arreglo de objetos con la informacion de la pagina 'DC-DD-O'
    //:retorno: fields: Arreglo de objetos con la info de las filas
    var fields = []
    var tablesNames = []

    for (let index = 0; index < objects.length; index++) {
      fields[index] = []; //crea los campos necesarios para almacenar los fields 
      tablesNames.push(objects[index]["Nombre físico objeto"].trim().toLowerCase());
    }

    var firstTableName = "";
    firstTableName = String(fldFile[1]["Nombre físico objeto"]).trim().toLowerCase();
    var fieldsObject = [];
    
    for (let index = 1; index < fldFile.length; index++) {
      const element = fldFile[index]; // Objeto que contiene la informacion de la fila
      var rowTableName = String(element["Nombre físico objeto"]).trim().toLowerCase();
      // var row = []
      // for (const key in element) {
      //   if (element.hasOwnProperty(key)) {
      //     const item = element[key];
      //     row.push(item);
      //   }
      // }

      if (firstTableName != rowTableName) {
        if (tablesNames.indexOf(firstTableName) != -1) {
          var indexObject = tablesNames.indexOf(firstTableName)
          fields[indexObject] = fieldsObject;
        }
        firstTableName = rowTableName;
        fieldsObject = [];
        fieldsObject.push(element);
      } else {
        fieldsObject.push(element);
        if (index + 1 == fldFile.length) {
          var indexObject = tablesNames.indexOf(firstTableName)
          if (indexObject != -1) {
            fields[indexObject] = fieldsObject;
          }
        }
      }
    }
    return fields;
  }

  //Construye el body de una nueva base operacional a cargar.
  buildFullBases(bases) {
    this.headBases = this.cleanRowJsonBases(bases[0]);
    this.fieldsBases = this.cleanRowJsonFieldsBases(bases[1], this.headBases);
    // console.log(this.headBases, this.fieldsBases)
    for (let index = 0; index < this.headBases.length; index++) {
      var tmpJson = {
        master_name: this.headBases[index]["Nombre físico objeto"], //1
        baseName: this.headBases[index]["Nombre lógico objeto"], //2
        observationField: this.headBases[index]["Descripción"], //3
        alias: "", // alias: this.headBases[index][28], //31
        column: index,
        fields: this.fieldsBases[index].length,
        periodicity: this.headBases[index]["Frecuencia"].toUpperCase(), //16
        uuaaMaster: this.headBases[index]["Código de sistema/uuaa"], //14
        type: "B"
      }
      this.fullArrayBases.push(tmpJson);
    }
    // console.log(this.fullArrayBases)
    this.fullArrayBases = [...this.fullArrayBases];
  }

  //Carga una base operacional
  loadBases() {
    this.fullArrayBases = [];
    this.headBases = [];
    this.fieldsBases = [];
    this.showSpinner("Cargando Tablas...");
    var result = [];
    var tmpSheet: any;
    var arrayBuffer: any;
    var isFine = true;
    if (this.fileListBases != null) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        try {
          var workbook = XLSX.read(data, { type: 'array' });
        } catch (error) {
          isFine = false;
          if (error.message.includes("password-protected"))
            this.openSnackBar("El excel esta protegido por contraseña", "Error");
          else this.openSnackBar("Error: " + error.message, "Error");
        }
        if (isFine) {
          for (var tabo of workbook.SheetNames) {
            tmpSheet = workbook.Sheets[tabo];
            switch (tabo) {
              case "DC-DD-O":
                result[0] = XLSX.utils.sheet_to_json(tmpSheet, { defval: "", blankrows: false, range: 3 });
                break;
              case "DC-DD-F":
                result[1] = XLSX.utils.sheet_to_json(tmpSheet, { defval: "", blankrows: false, range: 3 });
                break;
            }
          }
          this.buildFullBases(result);
        }
      }
      fileReader.readAsArrayBuffer(this.fileListBases);
      this.openSnackBar("Archivo cargado correctamente", "Ok");
      setTimeout(() => { this.spinner.hide(); }, 2000);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
      this.spinner.hide();
    }
  }

  //Peticion para cargar la base operacional.
  sendLoadBases(data, index) {
    this.rest.postRequest('operationalBase/file/', data).subscribe(
      (res: any) => {
        this.fullArrayBases.splice(index, 1);
        this.fullArrayBases = [...this.fullArrayBases];
        this.openSnackBar("LA BASE OP " + data.base.object[1] + " fue cargada exitosamente", "Ok");
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  //Carga una base operacional.
  loadOperaBase(row) {
    const dialogConfigBase = new MatDialogConfig();
    dialogConfigBase.disableClose = true;
    dialogConfigBase.autoFocus = true;
    dialogConfigBase.data = row;
    dialogConfigBase.width = "55%";
    dialogConfigBase.minWidth = "820px";
    dialogConfigBase.minHeight = "345px";
    const dialogRefBase = this.dialog.open(LoadTableComponent, dialogConfigBase);
    dialogRefBase.afterClosed().subscribe(
      result => {
        if (result != null) {
          let index = this.fullArrayBases.indexOf(row);
          var carga = {
            project_id: result.projec_id,
            user: result.user,
            base: {
              object: this.headBases[result.data.column],
              fields: this.fieldsBases[result.data.column]
            }
          }
          this.sendLoadBases(carga, index);
        }
      });
  }

  searchDialogConfig() {
    const dialogConfigBase = new MatDialogConfig();
    dialogConfigBase.disableClose = true;
    dialogConfigBase.autoFocus = false;
    dialogConfigBase.minWidth = "80%";
    dialogConfigBase.minHeight = "55%";
    dialogConfigBase.data = { searchType: "T" };
    //dialogConfigBase.panelClass = "repoDataClass";

    return dialogConfigBase;
  }

  //Abre el dialog para buscar tablas
  searchTables() {
    const dialogRef = this.dialog.open(RepoDatatableComponent, this.searchDialogConfig());
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        if (result["tables"].length > 0) {
          this.emptyMasterTables = false;
          this.masterTables = result["tables"]
        } else {
          this.emptyMasterTables = true;
        }

        
        
        console.log(result)
      }
    });
  }

  //Masteriza una tabla enviada
  masterizeTables(){
    for (let table of this.masterTables){
      console.log(table)
      this.masterTableRequest(table["_id"], table)
    }
    
  }

  masterTableRequest(idTable: string, tableData: any) {
    this.rest.putRequest('dataDictionaries/' + idTable + '/fases/',
    JSON.stringify({
      from: 'fieldsRaw',
      to: 'master',
      user_id: this.userId
    })
  ).subscribe(
    (data: any) => {
      this.generateExcels(idTable, tableData);
      this.openSnackBar('Se ha generado la fase MASTER.', 'Ok')
    },
    error => {
      this.openSnackBar('Error procesando petición al servidor. No se pudo generar MASTER', 'Error')
    })
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    let worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json)
    let workbook: XLSX.WorkBook = {
      Sheets: { 'DDNG-O': worksheet },
      SheetNames: ['DDNG-O']
    }
    XLSX.writeFile(workbook, `${excelFileName}_export_${new Date().getTime()}.xlsx`)
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json)
    const workbook: XLSX.WorkBook = {
      Sheets: { 'DDNG-F': worksheet },
      SheetNames: ['DDNG-F']
    }
    XLSX.writeFile(workbook, `${excelFileName}_export_${new Date().getTime()}.xlsx`)
  }

  generateObjectExcel(idTable: string, tableData: any) {
    this.rest
      .getRequest('dataDictionaries/' + idTable + '/excels/Object/')
      .subscribe(
        (data: any) => {
          this.exportAsExcelFileObject(
            data,
            tableData.baseName +
            '-' +
            tableData.alias +
            '-Object'
          )
          this.openSnackBar('Generación exitosa de los Object. ', 'Ok')
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }
  

  generateExcels(idTable: string, tableData: any) {
    this.generateObjectExcel(idTable, tableData)
    this.rest
      .getRequest('dataDictionaries/' + idTable + '/excels/Fields/')
      .subscribe(
        (data: any) => {
          this.exportAsExcelFileFields(
            data,
            tableData.baseName +
            '-' +
            tableData.alias +
            '-Fields'
          )
          this.openSnackBar('Generación exitosa de los Fields. ', 'Ok')
        },
        error => {
          this.openSnackBar('Error procesando petición al servidor.', 'Error')
        }
      )
  }


  //Crea un equipo TO BE DEPRECATED
  addTeam() {
    var team = { name: this.teamName, country: this.selteamCountry };
    this.rest.postRequest('users/teams/', JSON.stringify(team)).subscribe(
      (data: any) => {
        this.openSnackBar("Registro exitoso del equipo " + this.teamName + ".", "Ok");
        this.selteamCountry = "";
        this.teamName = "";
        this.selector.value = "";
      }, (error) => {
        this.openSnackBar("El equipo ya se encuentra registrado.", "Error");
      });
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    console.log(json)
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { "users": worksheet },
      SheetNames: ["users"],
    };
    XLSX.writeFile(workbook, `${excelFileName}_export_${new Date().getTime()}.xlsx`);
  }

  //Exporta y descarga un excel con el reporte de todos los usuarios de nebula y sus proyectos asociados.
  exportUsersReport() {
    let cleanedUsers: any = []
    for (let user of this.currRegisteredUsers) {
      cleanedUsers.push({
        "Nombre": user["name"],
        "email": user["email"],
        "Rol": this.prettyRol(user["rol"]),
        "Last Login": user["last_login"],
        "Vobos": user["vobos"].toString()
      })
    }
    this.exportAsExcelFile(cleanedUsers, "users_report");
    this.openSnackBar("Reporte Usuarios Generado", "Ok");
  }

  //this.projectSelected tiene que tener algo TO DO
  exportTablon() {
    console.error("Under construction.");
    this.openSnackBar("Under construction.", "Error");
  }

  exportOperationalBase() {
    console.error("Under construction.");
    this.openSnackBar("Under construction.", "Error");
  }

  //Actualiza el archivo actual (excel) por subir tablas
  onFileChange(event: any) {
    if (event && event.target.files.length >= 1) {
      this.fileList = event.target.files[0];
      //this.fileName = this.fileList.name;
    } else
      this.fileList = null;
  }

  //Actualiza el archivo actual (excel) por subir tablones
  onFileChangeTablon(event: any) {
    if (event && event.target.files.length >= 1)
      this.fileListTablon = event.target.files[0];
    else
      this.fileListTablon = null;
  }

  //Actualiza el archivo actual (excel) por subir las bases operacionales.
  onFileChangBases(event: any) {
    this.fileListBases = event.target.files[0];
  }

  //Selector para exportar tablones y/o bases op TO BE DEPRECATED
  selectedProjectBacklog(event: any) {
    this.getProjectUseCases(event._id);
    this.selectedProject(event);
  }

  //Selector para exportar tablones y/o bases op TO BE DEPRECATED
  selectedProject(event: any) {
    this.projectSelected = event;
  }

  //Selector para la creacion de un nuevo equipo TO BE DEPRECATED
  selectedCountryTeam(value: any) {
    this.selteamCountry = value;
  }

  //Links para las plantillas de carga de Tablas
  openTableLoadExamples(type: Number) {
    switch (type) {
      case 1: window.open("https://drive.google.com/drive/folders/1P0uxTWKDpOfx4Js92G3b60qIL2a3KXPV?usp=sharing", "_blank"); break;
      case 2: window.open("https://docs.google.com/spreadsheets/d/1vJxGUnZiiXKVZHumVym14WE73Ygec-D_5jPs5earg8Y/edit#gid=0", "_blank"); break;
    }
  }

  //Links para las plantillas de carga de Tablones
  openTablonLoadExamples(type: Number) {
    switch (type) {
      case 1: window.open("https://drive.google.com/drive/folders/1LbJUS13WpKbuYZ3iok8KcbfU2rIp4Ttr?usp=sharing", "_blank"); break;
    }
  }

  //Links para las plantillas de carga de Tablones
  openOpBaseExamples(type: Number) {
    switch (type) {
      case 1: window.open("https://drive.google.com/drive/folders/1rQzSDsCTgtNfv9nm3c7yxfegIFk2qhqV?usp=sharing", "_blank"); break;
    }
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else { config.panelClass = ['errorMessage']; }
    this.snackBar.open(message, action, config);
  }
}
