import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { RestService } from '../../rest.service';

//Hash MD5 de la constraseña "password" para reinicio
const defaultPass = "5f4dcc3b5aa765d61d8327deb882cf99";

@Component({
  selector: 'app-user-request',
  templateUrl: './user-request.component.html',
  styleUrls: ['./user-request.component.scss']
})
export class UserRequestComponent implements OnInit {

  rolSelect = new FormControl('');
  projSelect = new FormControl('');
  teamSelect = new FormControl('');

  allControls: any = [this.rolSelect, this.projSelect, this.teamSelect];

  title: string = "";
  data: any;
  rolOptions = [
    { value: 'I', viewValue: 'Ingesta' },
    { value: 'G', viewValue: 'Gobierno' },
    { value: 'PO', viewValue: 'Gestor de Proyectos' },
    { value: 'SPO', viewValue: 'Super Gestor' },
    { value: 'D', viewValue: 'Depreciado' }
  ];
  projectOptions: any = [];
  teamOptions: any = [];

  selectedRol: any = {};
  selectedProject: any = null;
  selectedTeam:any = null;

  voboProjects = []

  team_found: boolean = false;
  foundVobo: boolean = false;


  foundTeam: boolean = false;

  form_nombre = new FormControl();
  form_email = new FormControl();
  form_rol = new FormControl();

  admin_id: string;

  bk_name;
  bk_email;
  bk_rol;

  team_options = []
  user_teams = []

  new_user_flag: boolean = false;

  active_help: string = "";



  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public rest: RestService, public snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<UserRequestComponent>) { 
    this.admin_id = sessionStorage.getItem("userId");

    if ("new_user" in datas)
      this.new_user_flag = true;
    if (!("teams" in datas)) 
      datas["teams"] = [{_id: "5c2f821f698b6abf9fd5ffbf", name: "Gobierno"}]

    this.data = datas;
    this.bk_name = datas["name"];
    this.bk_email = datas["email"];
    this.bk_rol = datas["rol"];

    this.title = "Solicitud de creación usuario";
    this.getAllTeams();
    if (!this.new_user_flag) {
      this.getAllProjects();
      this.getVoboProjects();
    }
  }

  ngOnInit(){
  }

  getAllTeams(){
    this.rest.getRequest('users/teams/').subscribe(
      (data: any) => {
        this.team_options = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  getUserTeams(){
    this.rest.getRequest('users/teams/').subscribe(
      (data: any) => {
        this.user_teams = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  validateAll(){
    for(let vali of this.allControls){
      vali.markAsTouched();
      vali.updateValueAndValidity();
    }
    return this.rolSelect.valid && this.projSelect.valid  && this.teamSelect.valid;
  }

  confirm(){
    if (this.new_user_flag) {
      delete this.data["new_user"];
      this.rest.postRequest('users/'+ this.admin_id + '/create/', this.data ).subscribe(
        (data: any) => {
          this.sendClosedResponse(null);
          this.openSnackBar("Usuario Creado.", "Ok");
        });
    } else 
      this.rest.postRequest('users/'+ this.admin_id + '/changes/', this.data ).subscribe(
        (data: any) => {
          this.sendClosedResponse(null);
          this.openSnackBar("Usuario Actualizado.", "Ok");
        });
  }
  
  sendClosedResponse(response: any) {
    this.dialogRef.close(response);
  }

  selectedRolChange(value: any){
    this.selectedRol = value;
  }

  selectedProjectChange(value: any){
    this.selectedProject = value;
    this.getAllTeams();
    this.checkVoBoProject();
  }

  checkVoBoProject() {
    if (this.selectedProject["VoBo"].includes(this.data["_id"])) {
      this.foundVobo = true;
    } else 
      this.foundVobo = false;
  }

  editProjectVoBosRequest(action: string) {
    this.rest.postRequest('projects/updateVobo/'+ this.selectedProject["_id"] + '/', {"_id": this.data["_id"], "action": action}).subscribe(
      (data: any) => {
        this.selectedProject["VoBo"] = data;
        this.getVoboProjects();
        this.openSnackBar("VoBo Actualizado.", "Ok");
      });
  }

  editVoboToProject(type: string) {
    this.foundVobo = type == "add" ? true : type == "remove" ? false : !this.foundVobo;
    this.editProjectVoBosRequest(type);
  }


  selectedTeamChange(value: any){
    this.selectedTeam = value;
    let prev_team = this.data["teams"].find(filter => filter['_id'] == this.selectedTeam["_id"]);
    
    if (prev_team != undefined) {
      this.team_found = true;
    } else 
      this.team_found = false;
  }

  editTeamToUser(type: string) {    
    if (type == "add") {
      this.team_found = true;
      this.data["teams"].push(this.selectedTeam)
    } else if (type == "remove") {
      this.team_found = false;
      this.data["teams"].splice(this.data["teams"].indexOf(this.selectedTeam))
    }
  }

  resetPassword() {
    if (confirm("¿Está seguro de reiniciar la contraseña para el usuario " + this.data["name"] + "?")) {
      let new_user_data = { "user": this.data["_id"], "oldPasswrd": "reset", "newPasswrd": defaultPass };
      this.rest.postRequest('users/data/', new_user_data).subscribe(
        (data: any) => {
          this.openSnackBar("La contraseña se reinicio a 'password' sin comillas", "Ok");
        }, (error) => {
          this.openSnackBar("Error cambiando contraseña.", "Error");
        });
    }
  }

  getAllProjects() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
        //this.cleanUnassigned();
      });
    
  }

  getVoboProjects() {
    this.rest.getRequest('projects/checkVoBoUser/'+ this.data["_id"] + '/').subscribe(
      (data: any) => {
        this.voboProjects = data;
    });
  }

  cleanUnassigned() {
    for(var pos = 0 ; pos < this.projectOptions.length ; pos++){
      if(this.projectOptions[pos].name === "Unassigned"){
        this.projectOptions.splice(pos, 1);
        return;
      }      
    }
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") { config.panelClass = ['sucessMessage'];
    } else { config.panelClass = ['errorMessage']; }
    this.snackBar.open(message, action, config);
  }
}
