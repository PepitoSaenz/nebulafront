import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { RestService } from '../../rest.service';

@Component({
  selector: 'app-load-table',
  templateUrl: './load-table.component.html',
  styleUrls: ['./load-table.component.scss']
})
export class LoadTableComponent implements OnInit {

  title: string = "";
  projectOptions: any = [];
  useCaseOptions: any = [];
  idProject: string;
  selectedProject: any = undefined;
  selectedUC: any = undefined;
  backlogsUseCaseProject: any = [];
  selectedBacklog: any = { baseName: "", _id: "" };
  newBacklogName: string;

  periodOptions: any = [];

  usersOptions: any = [];
  selectedUser: any = undefined;

  data: any;
  uuaaRaw: string = "";

  type = '';
  useCaseId = "";

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public rest: RestService, public snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<LoadTableComponent>) {
    this.data = datas;
    if (datas.type != undefined) {
      if (datas.type == "DD") {
        this.type = "DD";
        this.title = "Carga de tabla 2: " + datas.alias;
      }
      if (datas.type == "B") {
        this.type = "B";
        this.title = "Carga de base operacional: " + datas.master_name;
      } else if (datas.type == "T") {
        this.type = "T";
        this.title = "Carga de tablón: " + datas.alias;
      }
    } else {
      this.title = "Carga de tabla: " + datas.alias;
      this.uuaaRaw = this.data.object.raw[1].split("_")[1].toLocaleUpperCase();
    }
  }

  ngOnInit() {
    this.getProjectOptions();
    this.getAllUsers();
  }

  //Obtiene la lista de todos los proyectos en nebula y elimina el proyecto unassigned de la lista
  getProjectOptions() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      }, (error) => {
        this.projectOptions = [];
      });
  }

  selectedProjectOption(event: any) {
    this.idProject = event["_id"];
    this.selectedProject = event;
    this.getProjectUseCases(this.idProject);
  }

  getProjectUseCases(idProject: string) {
    this.rest.getRequest('projects/' + idProject + '/useCases/').subscribe(
      (data: any) => {
        this.useCaseOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getPeriodicitesFull(idProject) {
    this.rest.getRequest('projects/backlogs/atributes/periodo/').subscribe(
      (data: any) => {
        this.periodOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  selectedUseCase(event) {
    this.selectedUC = event;
    this.rest.getRequest('projects/useCases/' + event._id + '/backlogs/').subscribe(
      (data: any) => {
        this.useCaseId = event._id;
        this.backlogsUseCaseProject = data;
        this.backlogsUseCaseProject.push({ baseName: "--NEW--", _id: "NEW" });
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getAllUsers() {
    this.rest.getRequest('users/').subscribe(
      (data: any) => {
        this.usersOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  periodicityMatch() {
    for (let peri in this.periodOptions) {
      if (peri["state"] == this.data.object.raw[16].toUpperCase())
        return peri["periodicity"];
    }
    return "DIARIO";
  }

  newPeriodicityMatch() {
    for (let peri in this.periodOptions) {
      if (peri["state"] == this.data.object.raw[21].toUpperCase())
        return peri["periodicity"];
    }
    return "DIARIO";
  }


  //D
  newBacklog() {
    var backlog = {
      baseName: this.newBacklogName.toUpperCase(),
      originSystem: this.data.object.raw[22].toUpperCase(),
      periodicity: this.periodicityMatch(),
      history: parseInt(this.data.object.raw[19].split(" ")[0]),
      observationField: this.data.object.raw[3],
      typeFile: this.data.object.raw[27].toLowerCase(),
      uuaaRaw: this.data.object.raw[14],
      uuaaMaster: (this.data.object.master[14] ? this.data.object.master[14] : this.data.object.raw[14]),
      tacticalObject: this.data.object.raw[33],
      perimeter: this.data.object.raw[6],
      request_date: new Date().toISOString().split("T")[0],
      modifications: {
        user_name: "",
        date: ""
      },
      created_time: new Date().toISOString().split("T")[0]
    };
    return backlog;
  }

  newArchiveBacklog() {
    var backlog = {
      baseName: this.newBacklogName.toUpperCase(),
      originSystem: this.data.object.raw[27].toUpperCase(),
      periodicity: this.newPeriodicityMatch(),
      history: parseInt(this.data.object.raw[24].split(" ")[0]),
      observationField: this.data.object.raw[3],
      typeFile: this.data.object.raw[32].toLowerCase(),
      uuaaRaw: this.data.object.raw[19],
      uuaaMaster: (this.data.object.master[19] ? this.data.object.master[19] : this.data.object.raw[19]),
      tacticalObject: this.data.object.raw[38],
      perimeter: this.data.object.raw[6],
      request_date: new Date().toISOString().split("T")[0],
      modifications: {
        user_name: "",
        date: ""
      },
      created_time: new Date().toISOString().split("T")[0]
    };
    return backlog;
  }

  acceptSol() {
    if (this.selectedBacklog !== undefined && this.selectedUser !== undefined) {
      this.data["backlog"] = this.selectedBacklog._id;
      this.data["user"] = this.selectedUser._id;
      if (this.selectedBacklog.baseName == "--NEW--") {
        this.data["newBacklog"] = this.newBacklog();
        this.data["newUseCase"] = this.selectedUC["_id"];
      }
      let response = {
        solResult: "approve",
        dialogTempData: this.data
      }
      this.sendClosedResponse(response);
    } else
      this.openSnackBar("Seleccione un Backlog y Usuario responsable de la tabla...", "Error");
  }

  acceptNewSol() {
    if (this.selectedBacklog !== undefined && this.selectedUser !== undefined) {
      this.data["backlog"] = this.selectedBacklog._id;
      this.data["user"] = this.selectedUser._id;
      if (this.selectedBacklog.baseName == "--NEW--") {
        this.data["newBacklog"] = this.newArchiveBacklog();
        this.data["newUseCase"] = this.selectedUC["_id"];
      }
      let response = {
        solResult: "approve",
        dialogTempData: this.data
      }
      this.sendClosedResponse(response);
    } else
      this.openSnackBar("Seleccione un Backlog y Usuario responsable de la tabla...", "Error");
  }

  acceptSolTablon() {
    if (this.useCaseId !== undefined && this.selectedUser !== undefined) {
      var response = {
        projec_id: this.idProject,
        use_case_id: this.useCaseId,
        user: this.selectedUser._id,
        solResult: "approve",
        data: this.data
      }
      this.sendClosedResponse(response);
    } else
      this.openSnackBar("Seleccione un caso de uso y usuario responsable del tablón...", "Error");
  }

  acceptSolBase() {
    if (this.idProject !== undefined && this.selectedUser !== undefined) {
      var response = {
        projec_id: this.idProject,
        use_case_id: this.useCaseId,
        user: this.selectedUser._id,
        solResult: "approve",
        data: this.data
      }
      this.sendClosedResponse(response);
    } else
      this.openSnackBar("Seleccione un proyecto y usuario responsable de la base operacional...", "Error");
  }

  sendClosedResponse(response: any) {
    this.dialogRef.close(response);
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else { config.panelClass = ['errorMessage']; }
    this.snackBar.open(message, action, config);
  }
}
