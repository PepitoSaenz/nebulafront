import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { RestService } from '../../rest.service';


@Component({
  selector: 'app-proyect-request',
  templateUrl: './proyect-request.component.html',
  styleUrls: ['./proyect-request.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProyectRequestComponent implements OnInit {

  admin_id: string;

  new_project_flag: boolean = false;

  bk_name: string;
  bk_desc: string;

  use_cases_data: any[] = []
  user_options: any[] = []
  team_options: any[] = []

  select_user: any = null;
  select_team: any = null;

  found_user: boolean = false;
  found_team: boolean = false;

  active_help: string = "";

  rol_options = [
    { value: '', viewValue: '' },
    { value: 'A', viewValue: 'Admin' },
    { value: 'I', viewValue: 'Ingesta' },
    { value: 'G', viewValue: 'Gobierno' },
    { value: 'RN', viewValue: 'VoBo Negocio' },
    { value: 'PO', viewValue: 'Gestor de Proyectos' },
    { value: 'SPO', viewValue: 'Super Gestor' }
  ];


  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public snackBar: MatSnackBar, public rest: RestService,
    private dialogRef: MatDialogRef<ProyectRequestComponent>) {
    this.admin_id = sessionStorage.getItem("userId");
    if ("new_project" in datas)
      this.new_project_flag = true;

    this.bk_name = datas["name"];
    this.bk_desc = datas["shortDesc"];
  }

  ngOnInit() {
    this.getAllRegisteredUsers();
    this.getTeamsOptions();
    this.getUseCaseData();
  }

  //Cierra el dialog
  sendClosedResponse(response: any) {
    this.dialogRef.close(response);
  }

  //Returns the name of a user given its ID
  prettyUserName(user_id: string) {
    let result = this.user_options.find(filter => filter['_id'] === user_id);
    return result != undefined ? result["name"] : "undefined";
  }

  //Returns the name of a team given its ID
  prettyTeamName(team_id: string) {
    let result = this.team_options.find(filter => filter['_id'] === team_id);
    return result != undefined ? result["name"] : "undefined";
  }

  //Trae todos los usuarios actualmente creados en Nebula para su manejo dentro del proyecto
  getAllRegisteredUsers() {
    this.rest.postRequest("users/listAll/", { "admin": true }).subscribe(
      (data: any) => {
        this.user_options = data;
      });
  }

  //Trae los equipos actualmente creados en Nebula para su manejo dentro del proyecto
  getTeamsOptions() {
    this.rest.getRequest("users/teams/").subscribe(
      (data: any) => {
        this.team_options = data;
      });
  }

  //Trae la informacion de un caso de uso especifico. Para mostrar el nombre del caso de uso exclusivamente.
  getUseCaseData() {
    if (this.datas["useCases"])
      for (let use_case of this.datas["useCases"])
        this.rest.getRequest("projects/useCases/" + use_case + "/").subscribe(
          (data: any) => {
            this.use_cases_data.push(data["name"]);
          });
  }

  //Selector del VoBo
  selectedUser(value: any) {
    this.select_user = value;
    this.found_user = (this.datas["VoBo"].includes(this.select_user["_id"]));
  }

  //Selector del equipo
  selectedTeam(value: any) {
    this.select_team = value;
    this.found_team = (this.datas["teams"].includes(this.select_team["_id"]));
  }

  //Edita el arreglo de los vobos del proyecto para agregar o quitar vobos
  editVoBos(type: string) {
    if (type == "add") {
      this.found_user = true;
      this.datas["VoBo"].push(this.select_user["_id"])
    } else if (type == "remove") {
      this.found_user = false;
      this.datas["VoBo"].splice(this.datas["VoBo"].indexOf(this.select_user["_id"]))
    }
  }

  //Edita el arreglo del equipo del proyecto para agregar o quitar equipos
  editTeams(type: string) {
    if (type == "add") {
      this.found_team = true;
      this.datas["teams"].push(this.select_team["_id"])
    } else if (type == "remove") {
      this.found_team = false;
      this.datas["teams"].splice(this.datas["teams"].indexOf(this.select_team["_id"]))
    }
  }

  //Crea o edita el proyecto confirmado para dar por finalizado el uso del dialog.
  confirm() {
    if (this.new_project_flag) {
      delete this.datas["new_project"];
      this.rest.postRequest('projects/' + this.admin_id + '/create/', this.datas).subscribe(
        (data: any) => {
          this.sendClosedResponse(null);
          this.openSnackBar("Proyecto Creado.", "Ok");
        });
    } else
      this.rest.postRequest('projects/' + this.admin_id + '/changes/', this.datas).subscribe(
        (data: any) => {
          this.sendClosedResponse(null);
          this.openSnackBar("Proyecto Actualizado.", "Ok");
        });
  }

  prettyRol(rol_value: String){
    let coso = this.rol_options.find(filter => filter['value'] === rol_value);
    return coso != undefined ? coso["viewValue"] : rol_value;
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else { config.panelClass = ['errorMessage']; }
    this.snackBar.open(message, action, config);
  }
}
