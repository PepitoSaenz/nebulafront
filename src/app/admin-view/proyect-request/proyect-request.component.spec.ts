import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectRequestComponent } from './proyect-request.component';

describe('ProyectRequestComponent', () => {
  let component: ProyectRequestComponent;
  let fixture: ComponentFixture<ProyectRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
