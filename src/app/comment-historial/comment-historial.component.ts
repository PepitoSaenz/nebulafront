import { MatSnackBar,MatSnackBarConfig } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-comment-historial',
  templateUrl: './comment-historial.component.html',
  styleUrls: []
})
export class CommentHistorialComponent implements OnInit {

    data:any;
    warning="";
    isEmpty=false;

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any,public rest:RestService, public snackBar: MatSnackBar) {
      this.warning="";
      this.isEmpty=false;
      this.data=datas;
      for(let i=0;i<datas.comment.length;i++){
        this.rest.getRequest('users/'+datas.comment[i].user+'/').subscribe(
              (dato: any) => {
                 if(dato!=[]){
                    this.data.comment[i].name=dato["name"];
                 }
              }, (error) =>{
                this.openSnackBar("Error procesando petición al servidor.","Error");
              }
        );
      }
      if(this.data.comment.length==0){
        this.isEmpty=true;
        this.warning="No hay comentarios.";
      }
   }

  ngOnInit() {

  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if(action=="Ok"){
        config.panelClass = ['sucessMessage'];
    }else{
        config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
