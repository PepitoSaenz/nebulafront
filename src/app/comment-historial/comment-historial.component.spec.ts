import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentHistorialComponent } from './comment-historial.component';

describe('CommentHistorialComponent', () => {
  let component: CommentHistorialComponent;
  let fixture: ComponentFixture<CommentHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
