import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartOpbaseComponent } from './start-opbase.component';

describe('StartOpbaseComponent', () => {
  let component: StartOpbaseComponent;
  let fixture: ComponentFixture<StartOpbaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartOpbaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartOpbaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
