import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { RestService } from '../rest.service';
import { RepoDatatableComponent } from './../repo-datatable/repo-datatable.component';
import { CdkPortal } from '@angular/cdk/portal';



@Component({
  selector: 'app-start-opbase',
  templateUrl: './start-opbase.component.html',
  styleUrls: ['./start-opbase.component.scss']
})
export class StartOpbaseComponent implements OnInit {

  @ViewChild('selectorAmbito') selectorAmbito: any;
  @ViewChild('selectorUUAA') selectorUUAA: any;

  formControlBaseName = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlAmbito = new FormControl('', [Validators.required, Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlPhysDesc = new FormControl('', Validators.compose([
    Validators.required,
    Validators.pattern('^[a-z0-9_]*$')
  ])); 
  formControlDBType = new FormControl('', [Validators.required]);
  formControlCategory = new FormControl('', [Validators.required]);

  formControlScope = new FormControl('', Validators.required);
  formControlPhysAlias = new FormControl('', [
    //Validators.required, 
    Validators.minLength(5)]);
  formControlAlias = new FormControl('', [Validators.required, Validators.minLength(4)]
    /*
    Validators.compose([
    Validators.required
    this.validateFormAlias()])*/
  );

  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlMark = new FormControl('', [Validators.required, Validators.minLength(2)]);
  formControlNumberField = new FormControl('', [Validators.required, Validators.minLength(1), Validators.min(1)]);

  formCDeliveryOwn= new FormControl('', Validators.compose([
    Validators.required,
    Validators.pattern('^[a-z0-9_.+-]+(@bbva\\.com)$')
	]));

  activeHelp: string = "";

  projectOptions: any = [];
  uuaaOptions: any = [];

  selectedProject: any = undefined;
  selectedUuaa: any = "";
  selectedScope: any;
  selectedAmbito: any = "";
  selectedDBType: any = "";

  newPhysName: string = "";
  newUuaa: string = "";
  newPhysDesc: string = "";
  newCategory: string = "";

  dbOptions: any = ["Cassandra", "Crossdata", "Elastic Search", "HDFS-Avro", "HDFS-Parquet",
  "Kafka", "MongoDB", "Oracle Physics", "PostgreSQL", "Ficheros Staging"];

  tablon: any;
  userId: string;
  fieldNumberField: any = 0;
  newBaseName: string = "";
  newDeliveryOwner: string = "";
  ambito: string = "";
  uuaa: string = "";
  newScope: string = "";
  physAlias: string = "";
  descAlias: string = "";
  alias: string = "";
  originsDataTable: any = ["MG", "PG", "ML", "PL"];
  maxLengthDesc: number = 22;

  numFields: any = 0;
  newDescription: string = "";
  newPeriodicity: string = "";
  newTactical: string = "";

  periodicityOptions = [];

  selectedOriginTables = [];
  selectedOriginOptions = [];
  originTablesIds = [];
  originTables = [];

  idProject: string;

  
  constructor(public rest: RestService, public snackBar: MatSnackBar, public dialog: MatDialog, private router: Router) { 
    this.userId = sessionStorage.getItem("userId");
    this.formControlAmbito.disable();
    this.formControlUUAA.disable();
    this.getProjectOptions();
    this.getPeriodicityOptions();
  }

  ngOnInit() {
  }
  
  checkFillStatus() {
    return false;
  }

  validateFormAlias(): ValidatorFn {
    return (control): { [key: string]: any } | null => {
      return !this.validateAlias(control.value, this.selectedUuaa, this.selectedScope) ?
        { 'validateFormAlias': { value: control.value } } : null;
    };
  }

  //Valida la estructura correcta del alias del tablon
  validateAlias(alias: string, uuaa: string, scope: string) {
    if (uuaa === undefined) uuaa = "";
    if (scope === undefined) scope = "";
    var flag = false;
    var pattern = "^(" + scope.toLowerCase() + uuaa.toLowerCase() + ")[a-z]*$";
    var regx = new RegExp(pattern);
    flag = regx.test(alias);
    return flag;
  }

  //Obtiene la lista de todos los proyectos en nebula y elimina el proyecto unassigned de la lista
  getProjectOptions() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
        this.projectOptions.splice(this.projectOptions.findIndex(
          projectOptions => projectOptions.name === "Unassigned"), 1);
      }, (error) => {
        this.projectOptions = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  getUuaas() {
    this.rest.getRequest('projects/uuaa/dataTable/' + this.selectedProject._id + '/').subscribe((data: any) => {
      this.uuaaOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  getPeriodicityOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicityOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  selectProjectOption(newProyect: any) {
    this.selectedProject = newProyect;
    this.idProject = newProyect._id;
    this.selectorAmbito.value = "";
    this.selectorUUAA.value = "";
    this.formControlAmbito.enable();
    this.formControlUUAA.enable();
    this.formControlAlias.updateValueAndValidity();
    this.getUuaas();
  }

  selectAmbito(value) {
    this.selectedAmbito = value;
    this.newUuaa = value + this.selectedUuaa;
    this.inputForNames();
    this.formControlAlias.updateValueAndValidity();
  }

  selectUuaa(value) {
    this.selectedUuaa = value;
    this.newUuaa = this.selectedAmbito + value;
    this.inputForNames();
    this.formControlAlias.updateValueAndValidity();
  }

  inputForNames() {
    this.newPhysName = "T_" + this.newUuaa + "_" + this.newPhysDesc.toUpperCase();
  }

  selectDBType(value){
    this.selectedDBType = value; 
  }

  createOpBase(){
    //this.createOriginTablesReq()
    let body = {
      user_id: this.userId,
      project_id: this.idProject,
      delivery_owner: this.newDeliveryOwner,
      db_type: this.selectedDBType,
      category_id: this.newCategory,
      alias: this.alias,
      fields: this.numFields,
      object_name: this.newPhysName,
      baseName: this.newBaseName,
      observationField: this.newDescription,
      uuaaMaster: this.newUuaa,
      periodicity: this.newPeriodicity,
      tacticalObject: this.newTactical,
      //origin_tables: this.originTables
    };
    console.log(body);
    this.rest.postRequest('operationalBase/', body).subscribe(
      (data: any) => {
        this.openSnackBar("Base OP creada", "Ok");
        this.router.navigate(['opbases/edit'], { queryParams: { idOpBase: data.newBase } });
      }, (error) => {
        console.log("ERR " + JSON.stringify(error));
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassRaw({ row, column, value }) {
    if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "RAW")
      return { 'selected': true };
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassMaster({ row, column, value }) {
    if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "MASTER")
      return { 'selected': true };
  }

  disabledFaseCheck(row: any, fase: string) {
    if (row.hasOwnProperty('faseSeleccionada'))
      if (row.faseSeleccionada !== fase)
        return true;
      else
        return false;
    else
      return true;
  }

  //Selecciona una fase Raw o Master para una TABLA y la marca para su posterior carga  
  selectFase(row: any, fase: string) {
    row.faseSeleccionada = fase;
    row.originType = "T";
  }

  //Funcion que llama al dialog de searchTable para agregar tablas de origen al tablon
  searchTables(searchType: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.maxWidth = "80%";
    dialogConfig.maxHeight = "90%";
    dialogConfig.data = { idProject: this.idProject, searchType: searchType };
    const dialogRef = this.dialog.open(RepoDatatableComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        console.log("RES " + JSON.stringify(result));
        this.prepareOriginTablesOptions(result.tableIds, searchType).then((res: any) => {
          if (searchType === "DT") this.selectFaseDT(res);
          this.selectedOriginOptions = (this.selectedOriginOptions.length < 1 ?
            res : this.selectedOriginOptions.concat(res));
          this.validateDuplicateOriginTables();
        });
      }
    });
  }

  //Prepara las promesas para cargar las tablas de origen al arreglo de options
  prepareOriginTablesOptions(tableIds, typeOrigin) {
    var promise = new Promise((resolve, reject) => {
      let promises = [];
      if (typeOrigin === 'DT')
        for (let id of tableIds)
          promises.push(this.requestPromiseDT(id))
      else
        for (let id of tableIds)
          promises.push(this.requestPromise(id))
      Promise.all(promises)
        .then((result) => {
          resolve(result);
        })
    });
    return promise;
  }

  //Peticion que llama la peticion para preparar la tabla de origen dependiendo de su fase
  requestPromise(tabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + tabId + '/head' + '/raw' + '/').toPromise()
        .then((res: any) => {
          res["_id"] = tabId;
          resolve(res);
        })
        .catch(res => {
          console.log("me jodi");
          resolve("");
        });
    });
    return promise;
  }

  inputValidatorNumberFields(event: any) {
    if (event.target.value.length > 4) {
      event.target.value = event.target.value.slice(0, 4);
    }
    if (event.target.value < 0)
      event.target.value = 0;
  }

  //Peticion que llama la peticion para preparar el tablon de origen
  requestPromiseDT(datatabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataTables/' + datatabId + '/head/').toPromise()
        .then((res: any) => {
          if (res !== null) resolve(res);
          else resolve({});
        });
    });
    return promise;
  }

  selectFaseDT(arr: any) {
    for (let obj of arr) {
      obj.faseSeleccionada = "DT";
      obj.originType = "DT";
    }
  }

  //Valida que la tabla origen que se quiera agregar no este repetida
  validateDuplicateOriginTables() {
    for (var first = 0; first < this.selectedOriginOptions.length - 1; first++) {
      for (var second = first + 1; second < this.selectedOriginOptions.length; second++) {
        if (this.selectedOriginOptions[first].baseName === this.selectedOriginOptions[second].baseName) {
          this.selectedOriginOptions.splice(second, 1);
          this.selectedOriginOptions = [...this.selectedOriginOptions]
        }
      }
    }
  }

  formatName(){

  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos
  //unicamente para la descripcion fisica
  public inputValidatorDesc(value: any) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(value)) value = value.replace(/[^a-z0-9_]/g, "");
    this.newPhysDesc = value;
    this.inputForNames();
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
