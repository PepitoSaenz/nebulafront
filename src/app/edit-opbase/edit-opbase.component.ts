import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, debounceTime, startWith } from 'rxjs/operators';
import { RestService } from '../rest.service';
import { CommentComponentComponent } from './../comment-component/comment-component.component';
import { ErrorsComponent } from './../errors/errors.component';
import * as XLSX from 'xlsx';

import { LoadOpbaseComponent } from './../load-opbase/load-opbase.component';



@Component({
  selector: 'app-edit-opbase',
  templateUrl: './edit-opbase.component.html',
  styleUrls: ['./edit-opbase.component.scss']
})
export class EditOpbaseComponent implements OnInit {

  @ViewChild('myTable') table: any;


  formControlBaseName = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlAmbito = new FormControl('', [Validators.required, Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlScope = new FormControl('', Validators.required);
  formControlAlias = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlDesc = new FormControl('', [Validators.required, Validators.minLength(4)]);
  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);

  //formControlDBType = new FormControl('', [Validators.required]);


  userId: string;
  userRol: string;
  idOpBase: string;
  idProject: string;
  baseInfo: any = {
    "modifications": {"user_name":"", "date":""},
    "created_time":""
  };
  baseFields: any = [];
  uuaaOptions: any = [];
  arraySuffixTmp: any = [];

  extraFields: any = {};
  currAmbito: string = "";
  currUUAA: string = "";
  descAlias: string = "";
  currIndex: string = "";

  arqFlag: boolean;

  stateTableClean: string = "";

  editing: any = {};

  rowsLimit: number = 5;
  activeHelp: string = "";
  fileList: File = null;
  suffixesOptions: any = [];
  periodicityOptions: any = [];
  frecuencyOptions: any = [];
  viewAuditFlag = false;
  objectTypeOptions: any = ["Table", "File"];
  dbOptions: any = ["Cassandra", "Crossdata", "Elastic Search", "HDFS-Avro", "HDFS-Parquet",
    "Kafka", "MongoDB", "Oracle Physics", "PostgreSQL", "Ficheros Staging"];

  constructor(public rest: RestService, private route: ActivatedRoute,
    private router: Router, public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.userId = sessionStorage.getItem("userId");
    this.userRol = sessionStorage.getItem("rol");
    this.arqFlag = this.userRol === "A";
    this.route.queryParams.subscribe(params => {
      this.idOpBase = params["idOpBase"];
      this.getInfoBaseOp();
      this.getPeriodicityOptions();
      this.getSuffixes();
    });
  }

  ngOnInit() {
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  private onPaginated(event) {
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  //Funcion que trae todas las uuaas que le corresponden al proyecto actual del tablon
  getUuaas() {
    this.rest.getRequest('projects/uuaa/dataTable/' + this.idProject + '/').subscribe(
      (data: any) => {
        this.uuaaOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Funcion que trae las opciones de periodicidad de la tabla/tablon
  getPeriodicityOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicityOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back que devuelve los tipos de frecuencias/periodicidad
  getFrecuency() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe((data: any) => {
      this.frecuencyOptions = data;
    }, (error) => { });
  }

  //Peticion al back que devuelve el arreglo con los posibles sufijos
  getSuffixes() {
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixesOptions = data;
      });
  }

  //Peticion al back que devuelve la información de la tabla que no tengan que ver con campos ni namings
  getInfoBaseOp() {
    this.rest.getRequest('operationalBase/' + this.idOpBase + '/head/').subscribe(
      (data: any) => {
        this.baseInfo = data;
        if(data.created_time != "" && 
          data.modifications.user_name != "" && 
          data.modifications.date != ""){
          this.viewAuditFlag = true;
        }
        this.cleanStatesTable();
        this.getProjectData();
        this.loadAmbito();
        this.loadUUAA();
        this.loadAliasDesc();
        this.getFieldsBaseOp();
        this.inputForNames();
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Peticion al back que devuelve la información de la tabla que no tengan que ver con campos ni namings
  getFieldsBaseOp() {
    this.rest.getRequest('operationalBase/' + this.idOpBase + '/fields/').subscribe(
      (data: any) => {
        this.baseFields = data.fields;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Obtiene la lista de todos los proyectos en nebula y elimina el proyecto unassigned de la lista
  getProjectData() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        for (var project of data)
          if (this.baseInfo.project_owner === project.name) {
            this.idProject = project._id;
            this.getUuaas();
          }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Imprime el estado del tablon natural dependiendo del codigo
  cleanStatesTable() {
    this.stateTableClean = (this.baseInfo.stateTable === "G" ? "Gobierno" : this.baseInfo.stateTable === "RN" ? "Visto Bueno" : this.baseInfo.requestStatus === "R" ? "Revisión Solicitud" : "Propuesta");
  }

  //Valida que la informacion de la tabla (objeto) tenga campos validos antes de enviar la peticion al back para actualizarlo
  validateObject(): boolean {
    var object = this.baseInfo;
    var flag = false;
    var tmp_errors = [];

    this.baseInfo.current_depth = Number(this.baseInfo.current_depth);
    this.baseInfo.estimated_volume_records = Number(this.baseInfo.estimated_volume_records);
    this.baseInfo.required_depth = Number(this.baseInfo.required_depth);

    if (object.object_name !== object.object_name.replace(/[^a-z0-9_]/g, "").trim())
      tmp_errors.push("El nombre físico del objeto continene caracteres especiales.");
    object.baseName = object.baseName.replace(/^\s+|\s+$/g, "").toUpperCase();
    if (object.baseName.length >= 1) {
      if (object.baseName.length < 60) {
        if (object.baseName != object.baseName.replace(/[^A-Z0-9_-]/g, " "))
          tmp_errors.push("El nombre base del objeto continene caracteres especiales.");
      } else
        tmp_errors.push("El nombre base del objeto no debe sobrepasar los 60 caracteres.");
    } else
      tmp_errors.push("El nombre base del objeto es obligatorio.");

    //if(object.observationField.length < 350 && object.observationField.length > 100)
    //tmp_errors.push("La descripción del objeto debe estar comprendida en una longitud de mínimo 100 caracteres y máximo 350 caracteres.");
    if (tmp_errors.length > 0) {
      tmp_errors.unshift("*- " + object.master_name + " -*");
      const dialogErr = new MatDialogConfig();
      dialogErr.maxHeight = "65%";
      dialogErr.minWidth = "500px";
      dialogErr.disableClose = true;
      dialogErr.autoFocus = true;
      dialogErr.data = tmp_errors;
      this.dialog.open(ErrorsComponent, dialogErr);
    } else
      flag = true;
    return flag;
  }

  updateObject() {
    //if (this.validateObject()) {
      let tmpObject = JSON.parse(JSON.stringify(this.baseInfo))
      tmpObject["user_id"] = this.userId;
      delete tmpObject["_id"];;
      this.rest.putRequest('operationalBase/' + this.idOpBase + '/head/', tmpObject).subscribe(
        (data: any) => {
          this.getInfoBaseOp();
          this.openSnackBar("Objeto Actualizado", "Ok");
        }, (error) => {
          console.log(error);
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
    //}
  }

  //Generar los archivos de la base operacional en formato Excel. 
  generateExcel(){
    this.rest.getRequest('operationalBase/' + this.idOpBase + '/excels/').subscribe(
      (data: any) => {
        console.log(data);
        this.exportAsExcelFileObject(data.object, this.baseInfo.object_name + "-Object");
        this.exportAsExcelFileObject(data.fields, this.baseInfo.object_name + "-Fields");
        this.openSnackBar("Archivos generados.", "Ok");
      }, (error) => {
        console.log(error);
        this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-O': worksheet }, SheetNames: ['DDNG-O'] };
    XLSX.writeFile(workbook, EditOpbaseComponent.toExportFileName(excelFileName));
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-F': worksheet }, SheetNames: ['DDNG-F'] };
    XLSX.writeFile(workbook, EditOpbaseComponent.toExportFileName(excelFileName));
  }

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  //Actualiza el archivo actual (excel) por subir
  onFileChange(event: any) {
    if (event.target.files.length >= 1)
      this.fileList = event.target.files[0];
    else
      this.fileList = null;
  }

  //Carga la tabla legacy desde un excel
  excelStructure() {
    var arrayBuffer: any;
    if (this.fileList != null) {
      let fileReader = new FileReader();
      let jsontmp;
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) {
          arr[i] = String.fromCharCode(data[i]);
        }
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, { type: "binary" });
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        jsontmp = XLSX.utils.sheet_to_json(worksheet, { raw: true });
        for (var j = 0; j != jsontmp.length; ++j) {
          if (jsontmp[j].catalogue == null) {
            jsontmp[j].catalogue = "empty";
          }
          if (jsontmp[j].origin == null) {
            jsontmp[j].origin = "empty";
          }
          if (jsontmp[j].length == null) {
            jsontmp[j].length = 0;
          }
          if (jsontmp[j].key == null || jsontmp[j].key == "N") {
            jsontmp[j].key = 0;
          } else if (jsontmp[j].key == "S") {
            jsontmp[j].key = 1;
          }
          if (jsontmp[j].mandatory == null || jsontmp[j].mandatory == "N") {
            jsontmp[j].mandatory = 0;
          } else if (jsontmp[j].mandatory == "S") {
            jsontmp[j].mandatory = 1;
          }
        }
        this.rest.putRequest('operationalBase/'+this.idOpBase+'/structure/', JSON.stringify({user_id: this.userId,fields: jsontmp})).subscribe(
          (data: any) => {
            this.getFieldsBaseOp();
            if (jsontmp.length != this.baseFields.length) {
              this.openSnackBar("Campos actualizados con diferencias encontradas. Longitud archivo:" + jsontmp.length + " " + "Longitud campos propuesta:" + this.baseFields.length, "Warning");
            } else {
              this.openSnackBar("Campos actualizados.", "Ok");
            }
          }, (error) => {
            this.openSnackBar(error.error.reason, "Error");
          }
        );
      }
      fileReader.readAsArrayBuffer(this.fileList);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
    }
  }
  
  openExternalTemplateLink(){
    window.open("https://docs.google.com/spreadsheets/d/17DLxrve0_J2Xhph2SquZhLO5B4QRl0exM4zyZHAjFkk/edit#gid=813161028", '_blank');
  }

  //Actualiza una propiedad de un naming y envia la peticion al back
  updateValue(event: any, row: any, property: string) {
    switch (property) {
      case "length":
        row.length = Number(event.target.value);
        this.updateValueGovernanceFormat(row);
        break;
      case "naming":
        if (this.validateNamingInput(row, event)) row.naming.naming = event.target.value.trim();
        break;
      case "logic":
        if (this.validateLogicNaming(row, event)) row.logic = this.clearLogic(event.target.value);
        break;
      case "dataType":
        row.dataType = event.target.value;
        if (row.dataType.match(/DATE/g)) row.length = 10;
        if (row.dataType.match(/TIMESTAMP/g)) row.length = 26;
        this.updateValueGovernanceFormat(row);
        break;
      case "key":
        if (row.key == 0 && event.target.value == 1) row.mandatory = 1;
        row.key = Number(event.target.value);
        break;
      case "mandatory":
        if (row.key == 1) {
          row.mandatory = 1;
          this.openSnackBar("No se puede cambiar el mandatory cuando el campo es llave", "Error");
        } else row.mandatory = Number(event.target.value);
        break;
      case "format_int":
        row.format = "(" + event.target.value + "," + this.getIntegerDecimals(row.format)[1] + ")";
        row.logicalFormat = "DECIMAL" + row.format;
        break;
      case "format_dec":
        row.format = "(" + this.getIntegerDecimals(row.format)[0] + "," + event.target.value + ")";
        row.logicalFormat = "DECIMAL" + row.format;
        break;
      default:
        row[property] = event.target.value;
    }
    this.updateValuesRequest();
  }

  //Peticion al back que actualiza toda la fila de un naming
  updateValuesRequest() {
    this.rest.putRequest('operationalBase/' + this.idOpBase + '/fields/',
      { user_id: this.userId, fields: this.baseFields }).subscribe(
        (data: any) => {
          //this.openSnackBar("FIELDS Modificado", "Ok");
        }, (error) => {
          console.error(JSON.stringify(error));
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
  }

  //Actualiza el formato de gobierno de la fila
  updateValueGovernanceFormat(row: any) {
    if (row.dataType.match(/STRING/g)) {
      row.logicalFormat = "ALPHANUMERIC(" + row.length + ")";
      row.format = "empty";
    } else if (row.dataType.match(/DATE/g)) {
      row.logicalFormat = "DATE";
      row.format = "yyyy-MM-dd";
    } else if (row.dataType.match(/INT64/g)) {
      row.logicalFormat = "NUMERIC BIG";
      row.format = "empty";
    } else if (row.dataType.match(/INT/g)) {
      row.logicalFormat = (row.length > 4 ? "NUMERIC LARGE" : "NUMERIC SHORT");
      row.format = "empty";
    } else if (row.dataType.match(/TIMESTAMP/g)) {
      row.logicalFormat = "TIMESTAMP";
      row.format = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    } else if (row.dataType.match(/DECIMAL/g)) {
      //if(row.decimals<1 && row.integers<1)
      //this.openSnackBar("Por favor, diligencie los valores decimales y enteros.", "Error");
      row.logicalFormat = "DECIMAL(0,0)";
      row.format = "(0,0)";
      //row.logicalFormat="DECIMAL("+(Number(row.integers)+Number(row.decimals))+","+row.decimals+")";
      //row.format="("+(Number(row.integers)+Number(row.decimals))+","+row.decimals+")";
    }
  }

  //Valida solo un naming y muestra el popup de los errores
  validateNaming(row: any) {
    var errors = this.validateNamingRow(row);
    const dialogErr = new MatDialogConfig();
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.minWidth = "50%";
    dialogErr.data = { validations: errors.errors };
    this.dialog.open(ErrorsComponent, dialogErr);
  }

  //Valida todos los namings del tablon
  validateAllNamings() {
    var tmpResp;
    var isAllValid = true;
    var fullErrors = [];
    fullErrors.push("*** Verificación Namings ***\n");
    for (var name of this.baseFields) {
      tmpResp = this.validateNamingRow(name);
      isAllValid = tmpResp.valid;
      if (tmpResp.errors.length === 1)
        tmpResp.errors[0] = tmpResp.errors[0] + " << OK >>";
      for (var tmp of tmpResp.errors)
        fullErrors.push(tmp);
    }
    const dialogErr = new MatDialogConfig();
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.minWidth = "50%";
    dialogErr.data = { validations: fullErrors };
    this.dialog.open(ErrorsComponent, dialogErr);
    return fullErrors.length === this.baseFields.length;
  }

  //Valida todos los campos de la fila del naming
  validateNamingRow(row: any) {
    var errors = [];
    let tmpSuffix = row.naming.naming.split('_');
    let suffix = tmpSuffix[tmpSuffix.length - 1];
    //Header errors
    errors.push("*" + (row.column + 1) + " - " + row.naming.naming + ":\n");
    if (row.length < 1) errors.push("Por favor, diligenciar la longitud del naming" + "\n");
    if (row.naming.naming == "empty") errors.push("Por favor, diligenciar el campo naming" + "\n");
    if (row.logic == "empty") errors.push("Por favor, diligenciar el campo nombre lógico" + "\n");
    if (row.description == "empty") errors.push("Por favor, diligenciar el campo descripción" + "\n");
    if (row.dataType == "empty") errors.push("Por favor, diligenciar el tipo de dato" + "\n");
    if (row.naming.isGlobal === 'N' && row.naming.naming.length > 30) errors.push("El naming no es global y tiene mas de 30 caracteres" + "\n");
    /*
    else if (row.naming.naming != "empty" && row.description != "empty" && row.logic != "empty" && row.operation != "") {
      var suffixTmp = undefined;
      for (let suff of this.suffixesOptions)
        if (suff.data[0].toLowerCase() === suffix)
          suffixTmp = suff;
      if (suffixTmp !== undefined) {
        if (row.naming.isGlobal !== 'Y') {
          this.addCheckSuffix(row, suffix);
          if (!suffixTmp.Logic.includes(row.logic.split(" ")[0]))
            errors.push("El nombre lógico no concuerda con el sufijo del naming." + "\n");
          if (!suffixTmp.DESCRIPTION.includes(row.description.split(" ")[0].toUpperCase()))
            errors.push("La descripción no concuerda con el sufijo del naming." + "\n");
          if (row.dataType.includes("("))
            if (!this.dataTypeList(row).includes(row.dataType.split("(")[0])) {
              errors.push("El tipo de dato no concuerda con el sufijo del naming.\n");
            } else if (!this.dataTypeList(row).includes(row.dataType))
              errors.push("El tipo de dato no concuerda con el sufijo del naming.\n");
        }
      } else errors.push("El súfijo no es válido." + "\n");
      if (row.description.toUpperCase() == row.logic)
        errors.push("El nombre lógico y la descripción son iguales." + "\n");
    }
    */
    //this.addCleanModification(row, errors.length - 1 === 0);
    this.updateValuesRequest();
    return { valid: errors.length - 1 === 0, errors: errors };
  }

  //Agrega un campo a la modificacion dependiendo con el resultado de la validacion del naming
  addCleanModification(row: any, validation: boolean) {
    var newModi = {
      startDate: new Date(),
      state: "",
      user: this.userId,
      stateValidation: (validation ? "YES" : "NO")
    };
    if (row.modification.length >= 5)
      row.modification.splice(4, row.modification.length - 1);
    row.modification.unshift(newModi);
  }

  //Valida el nuevo naming a modificar. No este repetido, si es global y si esta vacio
  //Solo cuando se llena el campo, no se verifica la integridad del naming
  validateNamingInput(row: any, event: any) {
    if (event.target.value.trim().length == 0) {
      event.target.value = "empty";
      return true;
    } else {
      if (!this.isNamingRepeated(event.target.value)) {
        this.baseFields[row.column].naming.naming = event.target.value.trim().toLowerCase();
        this.cleanDataTypeChange(row);
        this.checkNamingGlobal(row.column, event.target.value.trim().toLowerCase());
        return true;
      } else {
        this.openSnackBar("Naming repetido.", "Error");
        return false;
      }
    }
  }

  //Valida el nuevo nombre logico a modificar. No este repetido o si esta vacio
  validateLogicNaming(row: any, event: any) {
    if (event.target.value.trim().length == 0) {
      event.target.value = "empty";
      return true;
    } else {
      if (!this.isRepeatedLogic(this.clearLogic(event.target.value.trim()))) {
        //if(row.naming.isGlobal === 'Y') row.naming.isGlobal = 'N';
        return true;
      } else
        this.openSnackBar("Nombre lógico repetido.", "Error");
      return false;
    }
  }

  //Limpia el tipo de dato de salida del naming
  cleanDataTypeChange(row: any) {
    row.dataType = "empty";
    row.format = "empty";
    row.logicalFormat = "empty";
  }

  //Verifica si el naming esta repetido
  isRepeatedLogic(logicNaming: string) {
    for (let field of this.baseFields)
      if (field.logic === logicNaming)
        return true;
    return false;
  }

  //Limpia el nombre logico (?)
  clearLogic(logic: string) {
    let logicRem = logic;
    if (logic !== 'empty') {
      let tmp = logic.toUpperCase();
      logicRem = "";
      let i = 0;
      while (i < logic.length) {
        if (tmp.charCodeAt(i) == 209) {
          if (i + 1 < logic.length && tmp.charCodeAt(i + 1) == 73) {
            logicRem += "NI";
            i += 2;
          } else {
            logicRem += "NI";
            i++;
          }
        } else {
          logicRem += tmp.charAt(i);
          i++
        }
      }
    }
    return logicRem;
  }

  //Verifica que el naming enviado no exista en el arreglo de namings
  isNamingRepeated(naming: string) {
    for (var tab of this.baseFields)
      if (tab.naming.naming === naming)
        return true;
    return false;
  }

  //Verifica que el naming sea global, si lo es carga toda la info, si no, lo marca como no global.
  checkNamingGlobal(index: number, naming: string) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('namings/' + naming).subscribe(
        (data: any) => {
          var p = /~/gi;
          if (data != null) {
            this.baseFields[index].naming.isGlobal = data.naming.isGlobal;
            this.baseFields[index].logic = data.logic;
            this.baseFields[index].description = data.originalDesc.replace(p, "\n");
            this.baseFields[index].naming.code = data.code;
            this.baseFields[index].naming.codeLogic = data.codeLogic;
            this.baseFields[index].naming.Words = data.naming.Words;
            this.baseFields[index].naming.suffix = data.naming.suffix;
          } else {
            this.baseFields[index].naming.isGlobal = 'N';
            this.baseFields[index].naming.code = "empty";
            this.baseFields[index].naming.codeLogic = "empty";
            this.baseFields[index].naming.Words = [];
          }
          this.updateValuesRequest();
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    });
    return promise;
  }

  //Modifica la estructura del naming para agregar el sufix adecuado
  addCheckSuffix(row: any, suffix: string) {
    switch (suffix) {
      case "id": row.naming.suffix = 1; break;
      case "type": row.naming.suffix = 2; break;
      case "amount": row.naming.suffix = 3; break;
      case "date": row.naming.suffix = 4; break;
      case "name": row.naming.suffix = 5; break;
      case "desc": row.naming.suffix = 6; break;
      case "per": row.naming.suffix = 7; break;
      case "number": row.naming.suffix = 8; break;
    }
  }

  //Devuelve el entero o decimal calculado de un formato pero como tipo Number
  //Para input
  getNumberIntegerDecimals(format: string, pos: number) {
    return Number(this.getIntegerDecimals(format)[pos]);
  }

  //Calcula el entero y decimal de un formato de DECIMAL y los devuelve en un arreglo de dos posiciones
  //para el int(0) y dec(1) respectivamente
  getIntegerDecimals(format: string) {
    var res = [];
    var tmpInt = format.split(",")[0];
    var tmpDec = format.split(",")[1];
    res.push(tmpInt.split("(")[1]);
    res.push(tmpDec.split(")")[0]);
    return res;
  }

  //Metodo que cambia el logical format 
  changeDecimal(row: any, int: string, dec: string) {
    row.logicalFormat = "DECIMAL(" + int + "," + dec + ")";
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClass({ row, column, value }) {
    if (row.naming.naming !== "empty")
      //return { 'is-proposal': true };
      return { 'is-tempok': true };
      
    if (row.modification.length > 0) {
      if (row.modification[0].stateValidation)
        if (row.modification[0].stateValidation === 'NO')
          return { 'is-wrong': true };
      if (row.modification[0].state == 'GL') {
        return { 'is-global': true };
      } else if (row.modification[0].state == 'GC') {
        return { 'is-common-global': true };
      } else if (row.modification[0].state == 'PS') {
        //return { 'is-proposal': true };
      } else if (row.modification[0].state == 'PC') {
        return { 'is-common-proposal': true };
      }
    }
  }

  //Verifica el sufijo del naming actual
  checkNamingSuffix(row: any): string {
    let tmpSuffix = row.naming.naming.trim().split('_');
    let suffix = tmpSuffix[tmpSuffix.length - 1];
    let suffixIndex = -1;
    for (let i = 0; i < this.suffixesOptions.length; i++)
      if (this.suffixesOptions[i].suffix === suffix) {
        suffixIndex = i;
        break;
      }
    if (suffixIndex !== -1)
      return suffix;
    return "";
  }

  //Devuelve la lista de tipos de datos validos para el sufijo
  dataTypeList(row: any) {
    let suffix = this.checkNamingSuffix(row);
    let array = [];
    switch (suffix) {
      case "type": case "id": case "desc": case "name":
        array = ["STRING"];
        break;
      case "per": case "amount":
        array = ["DECIMAL"];
        break;
      case "number":
        array = ["INT", "INT64", "DECIMAL"];
        break;
      case "date":
        array = ["DATE", "TIMESTAMP_MILLIS"];
        break;
      case "":
        array = [];
        break;
    }
    this.arraySuffixTmp = array;
    return array;
  }

  //Modifica el nombre del tablon dependiendo del ambito, uuaa, alias y desc. del alias
  inputForNames() {
    switch (this.baseInfo.db_type) {
      case "ELASTIC SEARCH":
        this.baseInfo.object_name = ("s_" + this.baseInfo.uuaaMaster + "_" + this.descAlias).toLowerCase();
        this.currIndex = ("i_" + this.baseInfo.uuaaMaster + "_" + this.descAlias).toLowerCase();
        break;
      case "MONGODB":
        this.baseInfo.object_name = ("c_" + this.baseInfo.uuaaMaster + "_" + this.descAlias).toLowerCase();
        this.currIndex = ("i_" + this.baseInfo.uuaaMaster + "_" + this.descAlias + "(n)").toLowerCase();
        break;
      case "ORACLE PHYSICS":
        this.baseInfo.object_name = "T_" + this.baseInfo.uuaaMaster + "_" + this.descAlias.toUpperCase();
        this.currIndex = ("I_" + this.baseInfo.uuaaMaster + "_" + this.descAlias + "(N)").toUpperCase();
        break;
      default:
        this.baseInfo.object_name = ("t_" + this.baseInfo.uuaaMaster + "_" + this.descAlias).toLowerCase();

    }
  }

    //Agrega una fila una posicion arriba
    addAboveField(row: any) {
      this.rest.putRequest('operationalBase/' + this.idOpBase + '/fields/insert/',
        JSON.stringify({ column: row.column, user_id : this.userId}))
        .subscribe(
          (data: any) => {
            this.getFieldsBaseOp();
          }, (error) => {
            this.openSnackBar("No se ha podido agregar el campo.", "Error");
          }
        );
    }
  
    //Agrega una fila una posicion abajo
    addBelowField(row: any) {
      this.rest.putRequest('operationalBase/' + this.idOpBase + '/fields/insert/',
        JSON.stringify({ column: row.column + 1, user_id : this.userId}))
        .subscribe(
          (data: any) => {
            this.getFieldsBaseOp();
          }, (error) => {
            this.openSnackBar("No se ha podido agregar el campo.", "Error");
          }
        );
    }
  
    //Elimina la fila
    deleteField(row: any) {
      let index = this.baseFields.length === row.column + 1 ? row.column - 1 : row.column;
      this.rest.putRequest('operationalBase/' + this.idOpBase + '/fields/substract/',
        JSON.stringify({ column: row.column, user_id : this.userId})).subscribe(
          (data: any) => {
            this.getFieldsBaseOp();
            this.openSnackBar("Campo numero "+ row.column+1 + " eliminado", "Ok");
          }, (error) => {
            this.openSnackBar("No se ha podido eliminar el campo.", "Error");
          }
        );
    }

  changeDB() {
    this.inputForNames();
  }

  //Initializa el campo de ambito local dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAmbito() {
    this.currAmbito = this.baseInfo.uuaaMaster[0];
  }

  //Initializa el campo de la uuaa sin la letra del pais(ambito) dependiendo de la uuaa del tablon para su posible modificación y validación
  loadUUAA() {
    this.formControlUUAA.setValue(this.baseInfo.uuaaMaster.substring(1));
    this.currUUAA = this.baseInfo.uuaaMaster.substring(1);
  }

  //Initializa el campo de la descripcion del alias dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAliasDesc() {
    this.descAlias = this.baseInfo.object_name.substring(7);
  }

  //Actualiza el ambito y los campos que dependan de el
  selectedAmbito(event) {
    this.currAmbito = event;
    this.inputForUUAA();
    this.inputForNames();
  }

  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa(event) {
    this.currUUAA = event;
    this.inputForUUAA();
    this.inputForNames();
  }

  //Modifica la uuaa del tablon dependiendo del ambito y la uuaa sin codigo de pais
  inputForUUAA() {
    this.baseInfo.uuaaMaster = this.currAmbito + "" + this.currUUAA;
  }

  inputValidatorDescAlias(newVal: any) {
    const pattern = /^[a-zA-Z0-9_]*$/;
    if (!pattern.test(newVal))
      newVal = newVal.replace(/[^a-zA-Z0-9_]/g, "");
    this.descAlias = newVal.toUpperCase();
    this.inputForNames();
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  inputValidatorNamingS(event: any, cType: any) {
    var pattern: any;
    switch (cType) {
      case "naming":
        pattern = /^[a-z0-9_]*$/;
        break;
      case "logic":
        pattern = /^[ a-zA-Z0-9]*$/;
        break;
    }
    if (!pattern.test(event.target.value)) event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "").trim();
  }

  //Abre el dialog de la historia de los comentarios, la vista sin edicion de comentarios para arquitectura inicialmente
  openLoadSettings(row: any) {
    const dialogConfiguration = new MatDialogConfig();
    dialogConfiguration.minHeight = "400px";
    dialogConfiguration.minWidth = "950px";
    dialogConfiguration.disableClose = false;
    dialogConfiguration.autoFocus = true;
    dialogConfiguration.data = {};
    this.dialog.open(LoadOpbaseComponent, dialogConfiguration);
  }

  //Abre el dialog de la historia de los comentarios, la vista sin edicion de comentarios para arquitectura inicialmente
  historyComment(row: any) {
    const dialogConfiguration = new MatDialogConfig();
    dialogConfiguration.minHeight = "275px";
    dialogConfiguration.minWidth = "850px";
    dialogConfiguration.disableClose = false;
    dialogConfiguration.autoFocus = true;
    dialogConfiguration.data = {
      type: "comments",
      comments: row.check.comments
    };
    this.dialog.open(ErrorsComponent, dialogConfiguration);
  }

  //Abre el popup de los comentarios
  commentsWindows(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      user: sessionStorage.getItem("userId"),
      userRol: sessionStorage.getItem("rol"),
      row: row,
      rowArray: this.baseFields,
      idOpBase: this.idOpBase
    };
    this.dialog.open(CommentComponentComponent, dialogConfig);
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
