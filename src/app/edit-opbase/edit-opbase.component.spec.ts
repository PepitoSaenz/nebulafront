import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOpbaseComponent } from './edit-opbase.component';

describe('EditOpbaseComponent', () => {
  let component: EditOpbaseComponent;
  let fixture: ComponentFixture<EditOpbaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOpbaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOpbaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
