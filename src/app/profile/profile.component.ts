import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { RestService } from "../rest.service";
import { PasswordComponent } from "./password/password.component";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userId: string;
  userName: string;
  email: string;
  rol: string;
  rolName: string;
  userData: any = {};

  constructor(public rest: RestService, public dialog: MatDialog) {
    this.userId = sessionStorage.getItem("userId");
    this.userName = sessionStorage.getItem("userName");
    this.rol = sessionStorage.getItem("rol");
    this.email = sessionStorage.getItem("email");

    this.getUserData();
    this.getRolName();
  }

  ngOnInit() {}

  //Trae la información básica del usuario.
  getUserData(){
    this.rest.getRequest(`users/${this.userId}/`).subscribe(
      (data: any) => {
        this.userData = data;
      });
  }

  //Trae las opciones de los roles para usuarios y su nombre natural para mostrar.
  getRolName(){
    this.rest.getRequest('users/roles/').subscribe(
      (data: any) => {
        this.rolName = data.find(roles => {
          return roles.rol == this.rol;
        }).name;
      });
  }

  changePass() {
    const dialogPass = new MatDialogConfig();
    dialogPass.minWidth = "430px";
    dialogPass.width = "430px";
    dialogPass.minHeight = "45%";
    dialogPass.disableClose = true;
    dialogPass.autoFocus = false;
    this.dialog.open(PasswordComponent, dialogPass);
  }
}
