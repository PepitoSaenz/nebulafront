import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RestService } from '../../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { Md5 } from 'ts-md5/dist/md5';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})

export class PasswordComponent implements OnInit {
  data:any;
  oldPass:string;
  newPass:string;
  newPassRepeat:string;
  oldPassEncrpt:any;
  newPassEncrpt:any;
  hidePasswd1=true;
  hidePasswd2=true;
  hidePasswd3=true;
  messagesError:any=[];
  showDivError=false;

  constructor(private dialogRef: MatDialogRef<PasswordComponent>, @Inject(MAT_DIALOG_DATA) public datas: any, 
    public rest: RestService, public snackBar: MatSnackBar) {
  }

  ngOnInit() {}

  changePass(){
    this.messagesError=[];
    this.showDivError=false;

    if(this.newPass === this.newPassRepeat && this.newPass.length>=8){
      this.oldPassEncrpt = (Md5.hashStr(this.oldPass) as string);
      this.newPassEncrpt = (Md5.hashStr(this.newPass) as string);

      this.rest.postRequest('users/data/',
      JSON.stringify(
        { user: sessionStorage.getItem("userId"), 
          oldPasswrd: this.oldPassEncrpt,
          newPasswrd: this.newPassEncrpt
        })
      ).subscribe(
        (data: any) => {
          this.openSnackBar("Se han guardado los cambios.","Ok");
          this.closeDialog();
        }, (error) => {
          this.messagesError.push("Error en el cambio, intente de nuevo.");
          this.showDivError=true;
          this.updateSize();
        }
      );
    } else{
      this.oldPass = "";
      this.newPass = "";
      this.newPassRepeat = "";
      this.messagesError.push("La contraseña es igual a la anterior o tiene menos de 9 caracteres.");
      this.showDivError=true;
      this.updateSize();
    }
  }

  closeDialog() {
    this.dialogRef.close(null);
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  updateSize(){
    this.dialogRef.updateSize("430px", "460px");
  }

}
