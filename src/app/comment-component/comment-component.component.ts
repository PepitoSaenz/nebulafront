import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-comment-component',
  templateUrl: './comment-component.component.html',
  styleUrls: ['./comment-component.component.scss']
})

export class CommentComponentComponent implements OnInit {
  validate = false;
  reasons: any = [];
  data: any;
  newReason: string = "";
  newComment: string = "";
  names = new Map();

  dataTableFlag: boolean = false;
  opBaseFlag: boolean = false;

  voboFlag = false;
  funcGovFlag = false; 

  constructor(private datePipe: DatePipe, private dialogRef: MatDialogRef<CommentComponentComponent>, @Inject(MAT_DIALOG_DATA) public datas: any, public rest: RestService, public snackBar: MatSnackBar) {
    this.data = datas;
    this.dataTableFlag = this.data.hasOwnProperty("idTablon");
    this.opBaseFlag = this.data.hasOwnProperty("idOpBase");
    this.getReasons();

    if (this.data.hasOwnProperty("vobo")){
      this.voboFlag = true;
    } else if (this.data.hasOwnProperty("functional")){
      this.funcGovFlag = true;
    } else
      this.updateRealUserNames();
  }

  ngOnInit() {
    this.getReasons();
  }

  closeDialog() {
    this.dialogRef.close(null);
  }

  getUserName(user: any) {
    if (user["name"] && user["name"].length > 4)
      return user.name;
    return this.names.get(user.user);
  }

  splitCommentVoBo(comment: string){
    return comment.split("- por")[0];
  }

  splitUserNameVobo(comment: string){
    return comment.split("- por")[1];
  }

  addCommentVoBo(){
    if (this.newComment === "") {
      this.openSnackBar("El campo del comentario esta vacío", "Error");
    } else {
      let newComment = this.newComment + " - por " + this.data.user;
      this.data.row.comments.push(newComment);
      this.postChanges(this.data.row);
      this.newComment = "";
    }
  }

  addComment() {
    if (this.newReason === "OK") this.newComment = "OK";
    if (this.data.userRol === 'G' && this.newReason === "")
      this.openSnackBar("El campo de razon esta vacío", "Error");
    else if (this.newComment === "") {
      this.openSnackBar("El campo del comentario esta vacío", "Error");
    } else {
      var res = {
        comment: this.newComment,
        reasonReturn: (this.data.userRol === 'G' ? this.newReason : "OBSERVACIÓN"),
        checkComment: "",
        user: this.data.user,
        date: this.datePipe.transform(new Date(),"yyyy/MM/dd HH:mm:ss")
      };
      this.data.row.comments.push(res);
      this.updateRealUserNames();
      this.dataTableFlag ? this.pushDataTableChanges() 
        : this.opBaseFlag ? this.pushOpBaseChanges() 
          : this.pushCommentChanges();      
      
      this.newComment = "";
      this.newReason = "";
    }
  }

  checkDelete(index: any){
    if (this.voboFlag)
      return this.data.row.comments[index].split("- por")[1].trim() === this.data.user;
    else 
      return this.data.row.comments[index].user === this.data.user; 
  }

  deleteCommentVoBo(index: any){
    this.data.row.comments.splice(index, 1);
    this.postChanges(this.data.row); 
  }

  deleteComment(index: any){
    if (this.data.row.comments[index].user === this.data.user) {
      this.data.row.comments.splice(index, 1);
      this.dataTableFlag ? this.pushDataTableChanges() 
        : this.opBaseFlag ? this.pushOpBaseChanges() 
          : this.pushCommentChanges();
    } else {
      this.openSnackBar("No es posible eliminar comentarios de otros usuarios.", "Error");
    }
  }

  updateRealUserNames(){
    for (let i = 0; i < this.data.row.comments.length; i++) {
      if (!this.names.has(this.data.row.comments[i].user)) {
        this.rest.getRequest('users/' + this.data.row.comments[i].user + '/').subscribe(
          (dato: any) => {
            if (dato != []) {
              this.names.set(this.data.row.comments[i].user, dato["name"]);
            }
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          }
        );
      }
    }
  }

  postChanges(row: any){
    if(this.dataTableFlag)
      this.postDataTableComment(row);
    //else if(this.opBaseFlag)
      //this.postOpBaseComment(row);
    else {
      var data = {
        status: row.status,
        user: row.user,
        column: this.data.column,
        comments: row.comments,
      };
      this.rest.postRequest('dataDictionaries/' + this.data.idTable + '/validations/', data).subscribe(
        (data: any) => {
            this.openSnackBar("Campo actualizado.","Ok");
        }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.","Error");
        }
      );
    }
  }

  pushCommentChanges() {
    if (!this.funcGovFlag) {
      this.rest.postRequest('dataDictionaries/' + this.data.idTable + '/fase=' + this.data.fase + '&user=' + this.data.user + '/', JSON.stringify({ row: this.data.row })).subscribe(
        (data: any) => {
          this.openSnackBar("Campo actualizado.", "Ok");
          //this.updateFields(fase);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    } else {
      let rowArray = []; rowArray.push(this.data.row);
      this.rest.postRequest('dataDictionaries/' + this.data.idTable + '/functional/fields/', { user_id: this.data.user, user: this.data.user, row: rowArray }).subscribe(
        (data: any) => {
          this.openSnackBar("Campo actualizado.", "Ok");
          //this.updateFields(fase);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    }
  }

  //Peticion al back que actualiza toda la fila de un naming
  pushDataTableChanges() {
    let rowArray = []; rowArray.push(this.data.row);
    this.rest.postRequest('dataTables/' + this.data.idTablon + '/fields/', 
      { action: "row", user: this.data.user, row: rowArray }).subscribe(
        (data: any) => {
          this.openSnackBar("Campo actualizado.", "Ok");
        }, (error) => {
          console.error(JSON.stringify(error));
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
  }

  //Actualiza el naming con el comentario y estado del VoBo
  postDataTableComment(row: any){
    var data = {
      status: row.status,
      user: this.data.user,
      column: this.data.column,
      comments: row.comments
    };
    this.rest.postRequest('dataTables/' + this.data.idTablon + '/validations/', data).subscribe(
      (data) => { this.openSnackBar("Comentarios actualizados.", "Ok"); },
      (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); });
  }

  validateComment() {
    if (this.data.reason.length > 0) {
      this.dialogRef.close(this.data);
    } else {
      if (confirm("El comentario no tiene una razón seleccionada. ¿Desea salir sin guardar comentario?")) {
        this.dialogRef.close(null);
      }
    }
  }

  getReasons() {
    this.reasons = [];
    this.rest.getRequest('dataDictionaries/comments/reasons/').subscribe(
      (data: {}) => {
        this.reasons = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back que actualiza toda la fila de un naming
  pushOpBaseChanges() {
    var index = this.data.rowArray.indexOf(this.data.row);
    if (~index) this.data.rowArray[index] = this.data.row;
    let rowArray = []; rowArray.push(this.data.row);
    this.rest.putRequest('operationalBase/' + this.data.idOpBase + '/fields/',
    { user_id: this.data.user, fields: this.data.rowArray }).subscribe(
      (data: any) => {
          this.openSnackBar("Campo actualizado.", "Ok");
      }, (error) => {
        console.error(JSON.stringify(error));
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Actualiza el naming con el comentario y estado del VoBo
  postOpBaseChanges(row: any){
    var index = this.data.rowArray.indexOf(row);
    var newComment = {
      user: this.data.userId,
      comment: row.comments,
      reasonReturn: this.newReason,
      checkComment: ""
    };
    row.comments.push(newComment);
    if (~index) this.data.rowArray[index] = row;
    this.rest.putRequest('operationalBase/' + this.data.idOpBase + '/fields/',
    { user_id: this.data.userId, fields: this.data.rowArray }).subscribe(
      (data: any) => {
          this.openSnackBar("Campo actualizado.", "Ok");
      }, (error) => {
        console.error(JSON.stringify(error));
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });

  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
