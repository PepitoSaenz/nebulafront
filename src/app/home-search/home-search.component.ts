import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { RestService } from "../rest.service";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material";
import * as XLSX from "xlsx";

@Component({
  selector: "app-home-search",
  templateUrl: "./home-search.component.html",
  styleUrls: ["./home-search.component.scss"],
})
export class HomeSearchComponent implements OnInit {
  projectOptions: any = [];
  useCaseOptions: any = [];
  stateOptions: any = [];
  typeOptions: any = [
    { label: "Tabla", value: "DD" },
    { label: "Tablón", value: "DT" },
  ];

  selectedProject = "";
  selectedUseCase = undefined;
  selectedType: any = {};
  nameQuery = "";
  aliasQuery = "";
  logicNameQuery = "";
  logicDescQuery = "";
  legacyNameQuery = "";
  selectedState = "";
  tablesQuery = [];

  loadingExport: boolean = false;
  loadingIndicator: boolean = false;

  reviewDTFlag: boolean = false;
  expandIcon: string = "expand_more";

  @ViewChild("myTable") table: any;
  @ViewChild("selectProject") selectorProject: any;
  @ViewChild("selectState") selectorState: any;
  @ViewChild("selectorType") selectorType: any;

  @ViewChild("selectExport") selectorExport: any;

  states = [
    { value: "G", viewValue: "Gobierno" },
    { value: "N", viewValue: "En propuesta" },
    { value: "RN", viewValue: "VoBo Negocio" },
    { value: "I", viewValue: "Listo para ingestar" },
    { value: "P", viewValue: "Producción" },
    //{ value: 'D', viewValue: 'Descartados' }
  ];

  constructor(
    public rest: RestService,
    private router: Router,
    public snackBar: MatSnackBar
  ) {
    this.getAllProjects();
    //this.getSearchTables();
    this.stateOptions = this.states;
  }

  ngOnInit() { }

  getAllDatadictionaries() {
    this.rest
      .getRequest(
        "query/table_name=%20&alias=%20&project_id=" +
        "%20&table_state=%20&logic_name=%20&table_desc=%20&backlog=%20&type=%20/"
      )
      .subscribe(
        (data) => {
          this.loadingIndicator = false;
          this.tablesQuery = data;
        },
        (error) => {
          this.openSnackBar("Error servidor " + error, "Error");
        }
      );
  }

  getSearchTables() {
    this.loadingIndicator = true;
    if (
      this.selectedProject.length > 0 &&
      this.nameQuery.length > 0 &&
      this.aliasQuery.length > 0 &&
      this.selectedState.length > 0 &&
      this.logicNameQuery.length > 0 &&
      this.logicDescQuery.length > 0
    ) {
      this.tablesQuery = [];
      this.loadingIndicator = false;
      this.openSnackBar(
        "Por favor, indique un parámetro de búsqueda.",
        "Error"
      );
    } else {
      var projectTmp = this.selectedProject;
      var nameQueryTmp = this.nameQuery;
      var aliasTmp = this.aliasQuery;
      var legacyNameTmp = this.legacyNameQuery;
      var selectedStateTmp = this.selectedState;
      var logicNameTmp = this.logicNameQuery;
      var logicDescTemp = this.logicDescQuery;
      var typeTemp = this.selectedType.value;

      if (this.reviewDTFlag) {
        typeTemp = "DT";
        this.selectedType = this.typeOptions[1];
        legacyNameTmp = "%20";
      }

      if (!this.selectedType.hasOwnProperty("value")) typeTemp = "%20";
      if (this.selectedProject.length == 0) projectTmp = "%20";
      if (this.nameQuery.length == 0) nameQueryTmp = "%20";
      if (this.aliasQuery.length == 0) aliasTmp = "%20";
      if (this.selectedState.length == 0) selectedStateTmp = "%20";
      if (this.logicNameQuery.length == 0) logicNameTmp = "%20";
      if (this.logicDescQuery.length == 0) logicDescTemp = "%20";
      if (this.legacyNameQuery.length == 0) legacyNameTmp = "%20";
      else {
        typeTemp = "DD";
        this.reviewDTFlag = false;
        this.selectedType = this.typeOptions[0];
      }

      this.rest
        .getRequest(
          "query/table_name=" +
          nameQueryTmp +
          "&alias=" +
          aliasTmp +
          "&project_id=" +
          projectTmp +
          "&table_state=" +
          selectedStateTmp +
          "&logic_name=" +
          logicNameTmp +
          "&table_desc=" +
          logicDescTemp +
          "&backlog=" +
          legacyNameTmp +
          "&type=" +
          typeTemp +
          "/"
        )
        .subscribe(
          (data) => {
            this.loadingIndicator = false;
            this.tablesQuery = data;
            if (this.selectedUseCase !== undefined) console.log(data); //this.tablesQuery = this.cleanByUseCase();
            this.openSnackBar("Búsqueda finalizada", "Ok");
          },
          (error) => {
            this.openSnackBar(
              "Error procesando petición al servidor.",
              "Error"
            );
          }
        );
    }
  }

  //Limpia los parametros de búsqueda
  clean() {
    this.selectedProject = "";
    this.selectorProject.value = "";
    this.selectedUseCase = undefined;
    this.nameQuery = "";
    this.aliasQuery = "";
    this.legacyNameQuery = "";
    this.reviewDTFlag = false;
    this.selectedState = "";
    this.selectorState.value = "";
    this.logicNameQuery = "";
    this.logicDescQuery = "";
    this.selectedType = {};
    this.selectorType.value = "";

    this.stateOptions = this.states;
    this.getSearchTables();
  }

  //Limpia las busquedas por el caso de uso seleccionado si existe
  cleanByUseCase() {
    let tabsArray = [];
    for (let table of this.tablesQuery)
      if (
        this.selectedUseCase.tables.includes(table._id) ||
        this.selectedUseCase.tablones.includes(table._id)
      )
        tabsArray.push(table);
    return tabsArray;
  }

  getAllProjects() {
    this.rest.getRequest("projects/").subscribe((data: any) => {
      this.projectOptions = data;
    });
  }

  viewTable(row) {
    if (row.type === "T")
      this.router.navigate(["tables/view"], {
        queryParams: { idTable: row._id },
      });
    else if (row.type === "DT")
      this.router.navigate(["/tablon/view"], {
        queryParams: { idTablon: row._id },
      });
  }

  selectedTypeOption(value) {
    this.selectedType = value;
  }

  selectedStateOption(value) {
    this.selectedState = value;
    if (this.selectedState === "R") {
      this.reviewDTFlag = true;
      //this.selectedType = this.typeOptions[1];
    } else this.reviewDTFlag = false;
  }

  selectedProjectOption(value) {
    this.selectedProject = value;
    this.getProjectUseCases(value);
  }

  selectedUseCaseOption(value) {
    this.selectedUseCase = value;
  }

  getProjectUseCases(idProject) {
    this.rest.getRequest("projects/" + idProject + "/useCases/").subscribe(
      (data: any) => {
        this.useCaseOptions = data;
      },
      (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Metodo para el selector de exportacion de la data de busqueda
  exportData(option: number) {
    this.loadingExport = true;
    switch (option) {
      case 1:
        this.generateTablesReport();
        break;
      case 2:
        this.fullReportExport();
        break;
    }
    this.selectorExport.value = "";
  }

  //Genera un reporte de namings dados los parametros de busqueda actual
  cleanQueryforExport(dirtyArray: any) {
    var exporto = [];

    for (let table of this.tablesQuery) {
      let tmp = {
        Proyectos: table["project_name"].toString(),
        "Nombre Logico": table["baseName"],
        "Nombre Raw": table["raw_name"],
        "UUAA Raw": table["uuaaRaw"],
        "Nombre Master": table["master_name"],
        "UUAA Master": table["uuaaMaster"],
        "Nombre Legacy": table["legacy_name"],
        Alias: table["alias"],
        Tipo: table["type"] == "T" ? "Tabla" : "Tablon",
        Estado: table["stateTable"],
        Descripcion: table["observationField"],
      };
      exporto.push(tmp);
    }

    return exporto;
  }

  //Genera un reporte de namings dados los parametros de busqueda actual
  generateTablesReport() {
    this.exportAsExcelNamings(this.cleanQueryforExport(this.tablesQuery));
    this.openSnackBar("Generación exitosa de los Namings.", "Ok");
    this.loadingExport = false;
  }

  fullReportExport() {
    this.rest
      .getRequest(
        "query/table_name=%20&alias=%20&project_id=" +
        "%20&table_state=%20&logic_name=%20&table_desc=%20&backlog=%20&type=%20/"
      )
      .subscribe((data) => {
        this.exportAsExcelNamings(this.cleanQueryforExport(data));
        this.openSnackBar("Generación exitosa de los Namings.", "Ok");
        this.loadingExport = false;
      });
  }

  //Exporta y descarga una serie de datos en formato JSON a un archivo de Excel.
  exportAsExcelNamings(json: any[]): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { "DDNG-N": worksheet },
      SheetNames: ["DDNG-N"],
    };
    XLSX.writeFile(workbook, `nebula_data_export_${new Date().getTime()}.xlsx`);
  }

  toggleExpandRow(row, expanded) {
    this.table.rowDetail.toggleExpandRow(row);
    this.expandIcon = expanded ? "arrow_drop_down" : "arrow_drop_up";
  }

  onDetailToggle(event) { }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 1000;
    if (action == "Ok") {
      config.panelClass = ["sucessMessage"];
    } else {
      config.panelClass = ["errorMessage"];
    }
    this.snackBar.open(message, action, config);
  }
}
