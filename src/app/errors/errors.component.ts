import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})
export class ErrorsComponent implements OnInit {

  data:any;
  title = "";
  secondaryTitle = "";
  warning = "";
  isEmpty=false;

  singleFlag = false;

  prettyFlag = false;
  okArr = [];
  badArr = [];

  @ViewChild('okTable') okTable: any;


  constructor(@Inject(MAT_DIALOG_DATA) public datas: any) {
    this.data = datas;
    if(this.data.hasOwnProperty("pretty")){
      this.prettyFlag = true;
      this.cleanPretty();
    } else {
      if(this.data.hasOwnProperty('comments')){
        this.title = "Comentarios";
        this.data = datas.comments;
      } else if(this.data.hasOwnProperty('validations')){
        this.title = datas.validations[0];
        this.data = datas.validations;
        this.data.splice(0,1);
        if(this.data.length==0){
          this.isEmpty=true;
          this.warning="-- Campo Válido --";
        }
      } else {
        this.title = datas[0];
        this.data.splice(0,1);
        if(this.data.length==0){
          this.isEmpty=true;
          this.warning="No hay observaciones.";
        }
      }
    }
  }


  toggleGroup(group: any) {
    this.okTable.groupHeader.toggleExpandGroup(group);
  }

  cleanPretty(){
    let currNaming = "";
    let errArr = [];
    let tmpObj = {};
    if(this.data.subTitle) 
      this.secondaryTitle = " - " + this.data.subTitle;
    for(let err of this.data.errors) {
      if(err.includes('*')) {
        if(currNaming == "") {
          currNaming = err.split("*")[1].split(":")[0];
          tmpObj = {};
          errArr = [];
        } else {
          tmpObj["naming"] = currNaming;
          tmpObj["errors"] = errArr;
          if(errArr.length > 0)
            this.badArr.push(tmpObj);
          else
            this.okArr.push(tmpObj);
          currNaming = err.split("*")[1].split(":")[0];
          tmpObj = {};
          errArr = [];
        }
      } else {
        errArr.push(err);
      }
    }
    tmpObj["naming"] = currNaming;
    tmpObj["errors"] = errArr;
    if(errArr.length > 0)
      this.badArr.push(tmpObj);
    else
      this.okArr.push(tmpObj);

    if(this.okArr.length + this.badArr.length === 1)
      this.singleFlag = true;
  }

  ngOnInit() {
  }
}
