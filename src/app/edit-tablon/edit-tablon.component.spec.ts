import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTablonComponent } from './edit-tablon.component';

describe('EditTablonComponent', () => {
  let component: EditTablonComponent;
  let fixture: ComponentFixture<EditTablonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTablonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTablonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
