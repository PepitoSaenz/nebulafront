import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig } from '@angular/material';
import { RestService } from '../../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import * as CanvasJS from '../../statistics-dialog/canvasjs.min.js';
import { NamingInfoComponent } from '../naming-info/naming-info.component';



@Component({
  selector: 'app-comparison-info',
  templateUrl: './comparison-info.component.html',
  styleUrls: ['./comparison-info.component.scss']
})
export class ComparisonInfoComponent implements OnInit {

  @ViewChild('objectsTab') objectsTab: any;
  @ViewChild('fieldsTab') fieldsTab: any;

  idTablon: string;
  home: string;

  objects: any = [];
  fields: any = [];
  graphData: any = [];
  graphFieldsData: any = [];
  
  isAvailable: boolean = true;
  showGraph: boolean = true;
  showGraphFields: boolean = true;
  loadingIndicator: boolean = false;
  firstFieldLoad: boolean = false;


  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public dialog: MatDialog,
    public rest: RestService, public snackBar: MatSnackBar) {
    this.idTablon = datas.idTablon;
    this.home = this.rest.getHomeWebAddress();
    this.getComparisonsObject();
  }

  ngOnInit() {
  }

  onTabChange(event: any) {
    if(event.index === 1 && !this.firstFieldLoad){
      this.getComparisonsFields();
      this.firstFieldLoad = true;
    }
  }

  drawGraphTable(data: any, title: string, divshowgraphs: string){
    let seeData = new CanvasJS.Chart(divshowgraphs, {
      theme: "light2",
      animationEnabled: true,
      exportEnabled: true,
      title:{
        text: title
      },  
      data: [{
        type: "column",
        showInLegend: false,
        toolTipContent: "<b>{name}</b>: {y}%",
        //indexLabel: "{name} - {y}%",
        dataPoints: data
      }]
    });
    seeData.render();
  }

  cleanGraphData(array: any) {
    var resArr: any = [];
    let lowest, indexB;
    for(let obj of array.percentage){
      indexB = -1;
      lowest = obj.percentage;
      for(let index = 0; index < resArr.length; index++){
        if(resArr[index].percentage < lowest){
          lowest = resArr[index].percentage;
          indexB = index;
        }
      }
      let tmpObj = {
        name: obj.name,
        label: obj.name,
        y: obj.percentage
      }
      if(indexB > -1) resArr[indexB] = tmpObj;
      else resArr.push(JSON.parse(JSON.stringify(tmpObj)));
    }
    return resArr;
  }

  cleanGraphFieldData(array: any) {
    var resArr: any = [];
    let lowest, indexB;
    for(let obj of array.percentage){
      indexB = -1;
      lowest = obj.percentage;
      for(let index = 0; index < resArr.length; index++){
        if(resArr[index].percentage < lowest){
          lowest = resArr[index].percentage;
          indexB = index;
        }
      }
      let tmpObj = {
        name: obj.source,
        label: obj.source,
        y: obj.percentage
      }
      if(indexB > -1) resArr[indexB] = tmpObj;
      else resArr.push(JSON.parse(JSON.stringify(tmpObj)));
    }
    return resArr;
  }

  cleanObject(array: any) {
    var resultArr: any = [];
    var tmpOri: any = undefined;
    for (let ori of array.table)
      if (ori.sources.length > 0) {
        for (let source of ori.sources) {
          tmpOri = {};
          tmpOri.name = ori.name;
          tmpOri.source = source.alias;
          tmpOri._id = source._id;
          for (let per of array.percentage) {
            if (per.name.trim() == source.alias.trim()) {
              tmpOri.percentage = per.percentage;
              break;
            }
          }
          if (!tmpOri.hasOwnProperty("percentage"))
            tmpOri.percentage = "???";
          resultArr.push(JSON.parse(JSON.stringify(tmpOri)));
        }
      } else {
        tmpOri = {};
        tmpOri.name = ori.name;
        tmpOri.source = "-";
        tmpOri.percentage = "-";
        resultArr.push(JSON.parse(JSON.stringify(tmpOri)));
      }
    return resultArr;
  }

  getComparisonsObject() {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataTables/' + this.idTablon + '/comparisons/object/').subscribe(
        (data: any) => {
          this.objects = this.cleanObject(data);
          this.graphData = this.cleanGraphData(data);
          this.showGraph = this.graphData.length > 0;
          if(this.showGraph)
            this.drawGraphTable(JSON.parse(JSON.stringify(this.graphData)), "", "chartContainer");
        }, (error) => {
          //this.openSnackBar("Error procesando petición al servidor.", "Error");
          this.isAvailable = false;
          this.showGraph = false;
          this.openSnackBar(error.error.reason, "Error");
        });
    });
    return promise;
  }

  goToOrigin(row: any) {
    window.open(this.home + "tablon/view?idTablon=" + row._id, '_blank');
  }

  openNamingDetails(row: any) {
    this.getNamingInfo(row).then(res => {
      this.namingDetails(res["naming"], res["origin"]);
    });
  }

  //Muestra el popup de los detalles del naming de origen antes de agregarlo
  namingDetails(namingRow: any, originT: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "65%";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      naming: namingRow,
      idTable: originT._id,
      originType: originT.originType
    };
    this.dialog.open(NamingInfoComponent, dialogConfig);
  }

  getNamingInfo(naming){
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataTables/' + naming._id + '/fields/').subscribe(
        (data: any) => {
          for (var obj of data)
            if (obj.naming.naming === naming.naming) {
              let resu = { naming: obj, origin: { _id: naming.table_id, originType: "DT" } }
              resolve(resu);
            }
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
          resolve(error);
        });
    });
    return promise;
  }

  cleanFields(array: any) {
    var resultArr: any = [];
    var tmpOri: any = undefined;
    for (let ori of array.table) {
      if (ori.sources.length > 0)
        for (let source of ori.sources) {
          tmpOri = {};
          tmpOri.naming = ori.naming;
          tmpOri.source = source.source;
          tmpOri.type = source.type;
          tmpOri._id = source._id;
          resultArr.push(JSON.parse(JSON.stringify(tmpOri)));
        }
    }
    return resultArr;
  }

  getComparisonsFields() {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataTables/' + this.idTablon + '/comparisons/fields/').subscribe(
        (data: any) => {
          this.fields = this.cleanFields(data);
          this.graphFieldsData = this.cleanGraphFieldData(data);
          this.showGraphFields = this.graphFieldsData.length > 0;
          if(this.showGraphFields)
            this.drawGraphTable(JSON.parse(JSON.stringify(this.graphFieldsData)), "", "chartFieldContainer");
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
    });
    return promise;
  }

  toggleExpandObjectGroup(group: any, ok: boolean) {
    if (ok) this.objectsTab.groupHeader.toggleExpandGroup(group);
  }

  toggleExpandFieldsGroup(group: any) {
    this.fieldsTab.groupHeader.toggleExpandGroup(group);
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
