import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparisonInfoComponent } from './comparison-info.component';

describe('ComparisonInfoComponent', () => {
  let component: ComparisonInfoComponent;
  let fixture: ComponentFixture<ComparisonInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparisonInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparisonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
