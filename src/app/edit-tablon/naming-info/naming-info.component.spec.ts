import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamingInfoComponent } from './naming-info.component';

describe('NamingInfoComponent', () => {
  let component: NamingInfoComponent;
  let fixture: ComponentFixture<NamingInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamingInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
