import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RestService } from '../../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-naming-info',
  templateUrl: './naming-info.component.html',
  styleUrls: ['./naming-info.component.scss']
})

export class NamingInfoComponent implements OnInit {

  dataTableFlag: boolean;
  buttonMessage: string;

  home: string;
  data: any;
  idTable: string;
  oldPass: string;
  newPass: string;
  newPassRepeat: string;
  oldPassEncrpt: any;
  newPassEncrpt: any;
  hidePasswd1 = true;
  hidePasswd2 = true;
  hidePasswd3 = true;
  messagesError: any = [];
  showDivError = false;

  constructor(private dialogRef: MatDialogRef<NamingInfoComponent>, @Inject(MAT_DIALOG_DATA) public datas: any, 
    public rest: RestService, public snackBar: MatSnackBar) {
    this.data = datas.naming;
    this.idTable = datas.idTable;
    this.dataTableFlag = datas.originType && datas.originType === "DT";
    this.buttonMessage = this.dataTableFlag ? "Ver tablón origen" : "Ver tabla origen";
    this.home = this.rest.getHomeWebAddress();
    this.cleanData();
  }

  ngOnInit() {}

  closeDialog() {
    this.dialogRef.close(null);
  }

  cleanData() {
    this.data.naming.isGlobal = this.data.naming.isGlobal === "Y" ? "SI" : "NO";
    this.data.column += 1;
    this.data.key = this.data.key === "1" ? "SI" : "NO";
    this.data.mandatory = this.data.mandatory === "1" ? "SI" : "NO";
    this.data.tds = this.data.tds === "1" ? "SI" : "NO";
  }

  getGlobalNamingName(isGlobal: string){
    return (isGlobal === "Y" ? "SI" : "NO");
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  openOriginPage(){
    if(this.dataTableFlag)
      window.open(this.home + "tablon/edit?idTablon=" + this.idTable, '_blank');
    else
      window.open(this.home + "tables/view?idTable=" + this.idTable, '_blank');
  }

}
