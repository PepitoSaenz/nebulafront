import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, debounceTime, startWith } from 'rxjs/operators';
import { RestService } from '../rest.service';
import { CommentComponentComponent } from './../comment-component/comment-component.component';
import { NamingInfoComponent } from './naming-info/naming-info.component';
import { RepoDatatableComponent } from './../repo-datatable/repo-datatable.component';
import { ErrorsComponent } from './../errors/errors.component';
import { ComparisonInfoComponent } from './comparison-info/comparison-info.component';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-edit-tablon',
  templateUrl: './edit-tablon.component.html',
  styleUrls: ['./edit-tablon.component.scss']
})
export class EditTablonComponent implements OnInit {

  formControlBaseName = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlAmbito = new FormControl('', [Validators.required, Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlScope = new FormControl('', Validators.required);
  formControlAlias = new FormControl('', Validators.compose([
    Validators.required,
    this.validateFormAlias()
  ]));  
  formControlDesc = new FormControl('', [Validators.required, Validators.minLength(4)]);
  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);

  formControlPerimetro = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlLoadingT = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlTargetFileT = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlInfLevel = new FormControl('', [Validators.required, Validators.minLength(3)]);


  @ViewChild('myTable') table: any;
  @ViewChild('selectorExcel') selectorExcel: any;

  tablonInfo: any = {
    "modifications": {"user_name":"", "date":""},
    "created_time":""
  };
  viewAuditFlag = false;
  tablonFields: any = [];

  idTablon: string;
  idProject: string = "";
  idUseCase: string = "";
  currAmbito: string = "";
  currUUAA: string = "";
  aliasShort: string = "";
  descAlias: string = "";
  currScope: string = "";
  uuaaMaster: string = "";

  userId: any;
  userRol: string;
  editing: any = {};
  aliases: any = [];
  stateTableClean: string = "";
  requestStatusClean: string = "";
  isLoading: boolean = false;
  showAddOriginNaming: boolean = false;
  lastToggleNamingNumber: number = -1;

  selectedTable: any = {};
  originTables: any = [];
  originTablesIds: any = [];
  selectedOrigin: any;
  selectedOriginOptions: any = [];
  selOriginFlag: boolean = false;
  selectedNaming: any;
  namingOptions: any = [];
  selNamingFlag: boolean = false;

  originDiv: boolean = false;
  rowsLimit: number = 5;
  activeHelp: string = "";
  uuaaOptions: any = [];
  suffixesOptions: any = [];
  reasonsOptions: any = [];
  arraySuffixTmp: any = [];
  frecuencyOptions: any = [];
  periodicityOptions: any = [];
  objectTypesOptions: any = ["File", "Table"];
  loadingTypesOptions: any = ["incremental", "complete"];
  targetFileTypesOptions: any = ["Parquet", "CSV"];
  originsDataTable: any = ["MG", "PG", "ML", "PL"];
  maxLengthDesc: number = 22;
  poFlag: boolean = false;
  editReqFlag: boolean = true;
  idRequest: string;
  request: any = {};
  commentRequest: string = "";
  deployType_values = ["", "Global-Implementation", "Local"];
  logicalFormatDescOptions = ["ALPHANUMERIC", "CLOB"]
  tmpSecondaryLogicalF = "";

  originsControl = new FormControl();
  namingsControl = new FormControl();
  filteredNames: Observable<string[]>;
  filteredOrigins: Observable<string[]>;

  namingQuery = "";
  typeQuery = "";
  logicQuery = "";
  descQuery = "";
  stateQuery = "";

  fileList: File = null;

  filteredUUAAs: Observable<string[]>;
  uuaaControl = new FormControl();
  uuaaOptions2 = []
  ambitoOptions = []

  constructor(public rest: RestService, private fb: FormBuilder, private route: ActivatedRoute,
    private router: Router, public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.userId = sessionStorage.getItem("userId");
    this.userRol = sessionStorage.getItem("rol");
    this.route.queryParams.subscribe(params => {
      this.idTablon = params["idTablon"];
      this.idRequest = params["idReq"];

      this.updateUUAAInfo();
      this.getInfoTablon();
      this.getFieldsTablon();
      this.namingsControl.disable();
      this.getPeriodicityOptions();
      this.getSuffixes();
      this.getReasons();
      //this.cleanColumns();

      if(this.idRequest) {
        this.poFlag = this.userRol === "PO" || this.userRol === "SPO";
        this.getRequest();
      }
    });
  }

  //Inicializa los filtros de tablas de origen y namings
  ngOnInit() {
    this.filteredOrigins = this.originsControl.valueChanges
      .pipe(
        debounceTime(200),
        startWith(''),
        map(value => this.filterOriginTables(value))
      );
    this.filteredNames = this.namingsControl.valueChanges
      .pipe(
        debounceTime(200),
        startWith(''),
        map(value => this.filterNamings(value))
      );
    this.filteredUUAAs = this.uuaaControl.valueChanges.pipe(
        debounceTime(200),
        startWith(""),
        map((value) => this.filterUUAAs(value))
      );
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  private onPaginated(event) {
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
  }

  //Funcion que filtra las uuaas en el select a medida que se ingresen letras
  private filterUUAAs(value: any): string[] {
    if (value !== null) {
      if (value.hasOwnProperty("short_name")) value = value["short_name"];
      const filterValue = value.toLowerCase();
      let vau = this.uuaaOptions2.filter((option) =>
        option["short_name"].toLowerCase().includes(filterValue)
      );
      return vau;
    }
  }

  //Valida que el naming de origen este dentro de las opciones permitidas.
  //Para evitar que ingresen namings no permitidos
  checkSelectedUUAA() {
    if (this.uuaaControl.value != null)
      if (this.uuaaControl.value.hasOwnProperty("short_name")) {
        if (this.currUUAA !== this.uuaaControl.value["short_name"])
          this.resetEditUUAA();
      } else this.resetEditUUAA();
  }

  //Resetea los valores del namign de origen cuando se agrega.
  resetEditUUAA() {
    this.uuaaControl.setValue({ short_name: this.currUUAA });
  }

  validateFormAlias(): ValidatorFn {
    return (control): { [key: string]: any } | null => {
      return !this.validateAlias(control.value, this.uuaaMaster, this.currScope) ?
        { 'validateFormAlias': { value: control.value } } : null;
    };
  }

  //Funcion que filtra las tablas de origen en el select a medida que se ingresen letras
  private filterOriginTables(value: any): string[] {
    if (value !== null) {
      if (value.hasOwnProperty('originName'))
        value = this.calcAlias(value.originName, value.originType);
      const filterValue = value.toLowerCase();
      return this.originTables.filter(option =>
        this.calcAlias(option.originName, option.originType).toLowerCase().includes(filterValue));
    }
  }

  //Funcion que filtra los namings de origen en el select a medida que se ingresen letras
  private filterNamings(value: any): string[] {
    if (value !== null) {
      if (value.hasOwnProperty('naming'))
        value = value.naming.naming;
      const filterValue = value.toLowerCase();
      return this.namingOptions.filter(option => option.naming.naming.toLowerCase().includes(filterValue));
    }
  }

  //Valida los campos de Perimetro, Tipo de Carga, Formato de Salida y Nivel de Información
  //que son obligatorios pero no se solicitan al momento de crear el tablón
  validateTheAllFour(){
    this.formControlPerimetro.markAsTouched();
    this.formControlPerimetro.updateValueAndValidity();
    this.formControlLoadingT.markAsTouched();
    this.formControlLoadingT.updateValueAndValidity();
    this.formControlTargetFileT.markAsTouched();
    this.formControlTargetFileT.updateValueAndValidity();
    this.formControlInfLevel.markAsTouched();
    this.formControlInfLevel.updateValueAndValidity();
    return this.formControlPerimetro.valid && this.formControlLoadingT.valid 
      && this.formControlTargetFileT.valid && this.formControlInfLevel.valid;
  }

  //Peticion al back para traer la solicitud del tablon en el caso que sea necesario
  //ie. el usuario que entro es un SPO/PO
  getRequest() {
    this.rest.getRequest('requests/dataTable/' + this.idRequest + '/').subscribe(
      (data: any) => {
        this.request = data;
        this.editReqFlag = data.requestStatus === "P" && this.userRol === "SPO"; 
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Responde la solicitud de aprobación del tablón
  requestAnswer(isValid: boolean) {
    if(confirm("¿Desea " + (isValid ? "aceptar" : "rechazar") + " la solicitud de este Tablón?")){
      var request = {
        request_id: this.idRequest,
        action: isValid ? "A" : "P",
        body: this.tablonInfo,
        spo: this.userId,
        comment: this.commentRequest
      };
      this.rest.postRequest('requests/dataTable/' + this.idRequest + '/', JSON.stringify(request))
        .subscribe((data: any) => {
          this.editReqFlag = false;
          this.getRequest();
          this.openSnackBar("Solicitud - " + (isValid ? "aceptada" : "rechazada") + " exitosamente", "Ok");
        }, (error) => {
          this.openSnackBar(error.reason, "Error");
        }
      );
    }
  }

  //Agrega el alias de una tabla de origen a su estructura
  calcAlias(name: string, type: string) {
    var temp = name.split("_");
    temp.splice(0, 2);
    if (type === "DT")
      if (temp[0] && this.originsDataTable.includes(temp[0].toUpperCase()))
        return name.substring(10);
      //else
        //return name.substring(7);
    return temp[0];
  }


  //Calcula los alias de las tablas de origen de un naming y los agrega a un arreglo global sin campos repetidos
  calcUniqueAlias(row: any) {
    var tempArr = [];
    for (var tab of row.originNamings)
      if (tab.hasOwnProperty("originAlias")) {
        tempArr.push(tab.originAlias);
        //tempArr.push(JSON.parse(JSON.stringify({ alias: tab.originAlias, type: tab.originType })));
      } else
        tempArr.push(this.calcAlias(tab.table_name, tab.originType));
    this.aliases[row.column] = Array.from(new Set(tempArr));
  }

  //Toggle para el "div" que permite agregar namings de origen
  toggleAddOriginNaming() {
    this.showAddOriginNaming = !this.showAddOriginNaming;
  }

  //Valida que las particiones existan como namings en la tabla/fase
  validatePartitionNamings(partition: string, namingsTable: any) {
    var finished = false;
    var namesArr = partition.split(";");
    var valNaming: boolean;
    if (namesArr.length < 1){console.log("empty"); finished = true; }
    for (var i = 0; i < namesArr.length; i++) {
      valNaming = false;
      namingsTable.find(c => {
        if (c.naming.naming.trim() === namesArr[i].trim()){ console.log("found"); valNaming = true; }
      });
      if (valNaming === false) finished = true;
    }
    return !finished;
  }

  cleanPartitionRoute(route: string) {
    if (route.indexOf("${?YEAR}") > 0) {
      this.tablonInfo.master_path = route.substr(0, route.indexOf("${?YEAR}"));
      return route.substr(0, route.indexOf("${?YEAR}"));
    }
    return route;
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para el nombre del tablón
  inputValidatorBaseName(event: any) {
    const pattern = /[^a-zA-ZÀ-ÿ\u00f1\u00d1a-zA-Z0-9 _-]*$/;
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(/[^a-zA-ZÀ-ÿ\u00f1\u00d1a-zA-Z0-9 _-]/g, "");
    this.tablonInfo.baseName = event.target.value;
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  inputValidatorNamingS(event: any, cType: any) {
    var pattern: any;
    switch (cType) {
      case "naming":
        pattern = /^[a-z0-9_]*$/;
        break;
      case "logic":
        pattern = /^[ a-zA-Z0-9]*$/;
        break;
    }
    if (!pattern.test(event.target.value)) event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "").trim();
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para el nombre del tablón
  inputValidatorNaming(event: any, property: any, flag: boolean) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "");
      this[property] = event.target.value;
    }
    if (flag) this.inputForNames();
  }

  public inputValidatorNamingTsu(streing: any) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(streing))
      streing = streing.replace(/[^a-z0-9_]/g, "");
    return streing;
  }

  inputValidatorAliasLong(event: any) {
    const pattern = /^[a-z0-9]*$/;
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(pattern, "");
    this.tablonInfo.alias = event.target.value;
  }

  //Cierra la propuesta del tablon dandole paso a su productivizacion
  closeDataTable() {
    if (confirm("Se cerrara la propuesta del diccionario del tablón. Solo es posible volver a editarla cambiando el estado desde el Panel de Control.  ¿Desea continuar?")) {
      this.rest.putRequest('dataTables/' + this.idTablon + '/state/',
        JSON.stringify({ from: "architecture", to: "ingesta", user_id: this.userId })).subscribe(
        (data: any) => {
          this.openSnackBar("Se ha cerrado la propuesta del Tablón " + this.tablonInfo.alias, "Ok");
          this.router.navigate(['/tablon/view'], { queryParams: { idTablon: this.idTablon } });
        }, (error) => {
          this.openSnackBar("No se ha podido enviar la tabla para revisión en ingesta.", "Error");
        });
    }
  }

  updateUUAAInfo() {
    this.rest.postRequest("uuaa/update_object/", {"type": "DT", "id": this.idTablon}).subscribe(
        (data: any) => {
          console.log("")
        });
  }

  //Peticion al back que devuelve la información de la tabla que no tengan que ver con campos ni namings
  getInfoTablon() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/head/').subscribe(
      (data: any) => {
        this.tablonInfo = data;
        if(this.tablonInfo.stateTable !== "G" && this.tablonInfo.stateTable !== "N" && !this.poFlag) {
          this.router.navigate(['/tablon/view'], { queryParams: { idTablon: this.idTablon } });
          this.openSnackBar("El tablón no puede ser editado actualmente", "Error");
        }
        if(this.tablonInfo.created_time != "" &&
          this.tablonInfo.modifications.user_name != "" &&
          this.tablonInfo.modifications.date != ""){
          this.viewAuditFlag = true;
        }
        this.originTables = data.origin_tables;
        this.cleanStatesTable();
        this.loadOriginTablesOptions();
        this.getProjectData();
        this.loadAmbito();
        this.loadUUAA();
        this.loadAliasDesc();
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Imprime el estado del tablon natural dependiendo del codigo
  cleanStatesTable() {
    this.stateTableClean = (this.tablonInfo.stateTable === "G" ? "Gobierno"
      : this.tablonInfo.stateTable === "RN" ? "Visto Bueno" : this.tablonInfo.stateTable === "I" ? "Listo para Ingestar" 
      : this.tablonInfo.stateTable === "P" ? "Producción" : this.tablonInfo.stateTable === "R" ? "Revisión Solicitud" : "Propuesta");
    if (this.tablonInfo.requestStatus)
      this.requestStatusClean = (this.tablonInfo.requestStatus === "P" ? "Pendiente" : this.tablonInfo.requestStatus === "R" ?  "Revisión Solicitud" : "Ni idea");
  }

  //Peticion al back para que traiga todas las filas (namings) del tablón
  getFieldsTablon() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/fields/').subscribe(
      (data: any) => {
        this.tablonFields = data;
        for (var tab of this.tablonFields)
          this.calcUniqueAlias(tab);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Actualiza la información del object del tablón
  updateObject() {
    if (this.validateObject()) {
      let tmpObject = JSON.parse(JSON.stringify(this.tablonInfo))
      delete tmpObject["_id"];
      delete tmpObject["user"];
      delete tmpObject["project_owner"];
      tmpObject["user_id"] = this.userId;
      this.rest.postRequest('dataTables/' + this.idTablon + '/head/', tmpObject).subscribe(
        (data: any) => {
          this.openSnackBar("Objeto Actualizado", "Ok");
          this.getInfoTablon();
        }, (error) => {
          this.openSnackBar(error.error.reason, "Error");
        });
    }
  }

  //Peticion al back para confirmar que tablas/tablones de origen no se estan usando antes de enviar 
  //a gobierno
  checkOriginUses() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/originTables/').subscribe(
      (data: any) => {
        if (data.length === 0)
          data.unshift("¡¡¡Todas las tablas/tablones de origen estan en uso!!!" + "\n");
        data.unshift("*** Reporte de uso de origenes ***" + "\n");
        const dialogErr = new MatDialogConfig();
        dialogErr.maxHeight = "65%";
        dialogErr.minWidth = "500px";
        dialogErr.disableClose = true;
        dialogErr.autoFocus = true;
        dialogErr.data = data;
        this.dialog.open(ErrorsComponent, dialogErr);
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Elimina el origen y los namings asociados del tablon
  removeOriginTable(row: any) {
    if (confirm("¿Desea remover la tabla origen? Los namings asociados a este origen se eliminaran incluyendo los directos.")) {
      this.checkOriginUse(row);
      this.selectedOriginOptions.splice(
        this.selectedOriginOptions.indexOf(row), 1);
      this.deleteOrigin(row);
    }
    this.selectedOriginOptions = [...this.selectedOriginOptions]
  }

  //Elimina un tablon/tabla de origen SIN TOMAR EN CUENTA los namings para borrar, eso es del back
  deleteOrigin(row: any) {
    for (let index = 0; index < this.tablonInfo.origin_tables.length; index++)
      if (this.tablonInfo.origin_tables[index]._id === row._id) {
        this.tablonInfo.origin_tables.splice(index, 1);
        break;
      }
    this.updateObject();
  }

  checkOriginUse(origin: any) {
    var report = [];
    var tmpReport;
    for (let field of this.tablonFields) {
      tmpReport = [];
      tmpReport.push("* " + field.column + ": " + field.naming.naming + "\n");
      for (let ind = 0; ind < field.originNamings.length; ind++) {
        if (origin._id === field.originNamings[ind].table_id) {
          tmpReport.push("> " + field.originNamings[ind].naming + "\n");
          field.originNamings.splice(field.originNamings.indexOf(field.originNamings[ind]), 1);
          ind = ind - 1;
        }
      }
      if (tmpReport.length > 1) report.push(tmpReport);
      this.updateValuesRequest(field, false);
      for (var tab of this.tablonFields) this.calcUniqueAlias(tab);
    }
    report.unshift("*** Reporte de eliminación de origen - " + origin.baseName + " ***" + "\n");
    const dialogErr = new MatDialogConfig();
    dialogErr.maxHeight = "65%";
    dialogErr.minWidth = "500px";
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.data = report;
    this.dialog.open(ErrorsComponent, dialogErr);
  }

  //Verifica si el naming es directo y solicita una confirmacion antes de cualquier modificacion
  checkDirectNamingMod(row: any) {
    if (row.hasOwnProperty("direct") && row.direct === 1)
      return (confirm("El campo es directo, cualquier modificación le quitara el estado. ¿Desea continuar?"))
    return true;
  }

  getSecondaryLogicalF(row: any){
    if(row.logicalFormat.match(/ALPHANUMERIC/g)) {
      this.tmpSecondaryLogicalF = "ALPHANUMERIC";
      return "ALPHANUMERIC";
    } else if(row.logicalFormat.match(/CLOB/g)){
      this.tmpSecondaryLogicalF = "CLOB";
      return "CLOB";
    }
  }

  updateSecondaryLogicalF(value: string, row: any){
    switch (value) {
      case "ALPHANUMERIC":
        this.updateValueGovernanceFormat(row);
        break;
      case "CLOB":
        row.logicalFormat = "CLOB";
        break;
    }
    this.updateValuesRequest(row, true);
  }

  //Actualiza una propiedad de un naming y envia la peticion al back
  updateValue(value: any, row: any, property: string) {
    if (value.trim().length > 0) {
      if (this.checkDirectNamingMod(row)) {
        switch (property) {
          case "length":
            row.length = Number(value);
            this.updateValueGovernanceFormat(row);
            break;
          case "naming":
            if (this.validateNamingInput(row, event)) row.naming.naming = value.trim();
            break;
          case "logic":
            if (this.validateLogicNaming(row, event)) row.logic = this.clearLogic(value);
            break;
          case "dataType":
            row.dataType = value;
            if (row.dataType.match(/DATE/g)) row.length = 10;
            if (row.dataType.match(/TIMESTAMP/g)) row.length = 26;
            this.updateValueGovernanceFormat(row);
            break;
          case "key":
            if (row.key == 0 && value == 1) row.mandatory = 1;
            row.key = Number(value);
            break;
          case "mandatory":
            if (row.key == 1) {
              row.mandatory = 1;
              this.openSnackBar("No se puede cambiar el mandatory cuando el campo es llave", "Error");
            } else row.mandatory = Number(value);
            break;
          case "format_int":
            row.format = "(" + value + "," + this.getIntegerDecimals(row.format)[1] + ")";
            row.logicalFormat = "DECIMAL" + row.format;
            break;
          case "format_dec":
            row.format = "(" + this.getIntegerDecimals(row.format)[0] + "," + value + ")";
            row.logicalFormat = "DECIMAL" + row.format;
            break;
          case "tds":
            row.tds = Number(value);
            break;
          default:
            row[property] = value;
            break;
        }
        this.updateValuesRequest(row, true);
      }
    } else
      if (property == "default")
        if (value.trim().length === 0)
        row.default = "empty";
      else
        this.openSnackBar("El nuevo valor esta vacío", "Error");
  }

  //Devuelve el entero o decimal calculado de un formato pero como tipo Number
  //Para input
  getNumberIntegerDecimals(format: string, pos: number) {
    return Number(this.getIntegerDecimals(format)[pos]);
  }

  //Calcula el entero y decimal de un formato de DECIMAL y los devuelve en un arreglo de dos posiciones
  //para el int(0) y dec(1) respectivamente
  getIntegerDecimals(format: string) {
    var res = [];
    var tmpInt = format.split(",")[0];
    var tmpDec = format.split(",")[1];
    res.push(tmpInt.split("(")[1]);
    res.push(tmpDec.split(")")[0]);
    return res;
  }

  //Metodo que cambia el logical format 
  changeDecimal(row: any, int: string, dec: string) {
    row.logicalFormat = "DECIMAL(" + int + "," + dec + ")";
  }

  //Peticion al back que actualiza toda la fila de un naming
  updateValuesRequest(row: any, directCheck: boolean) {
    if (directCheck)
      if (row.hasOwnProperty("direct") && row.direct === 1) {
        row.direct = 0; //Cualquier modificacion sobre un campo directo lo quita
        row.operation = "";
      }
    let rowArray = []; rowArray.push(row);
    this.rest.postRequest('dataTables/' + this.idTablon + '/fields/',
      { action: "row", user: this.userId, row: rowArray }).subscribe(
        (data: any) => {
          //this.openSnackBar("Modificado", "Ok");
        }, (error) => {
          console.error(JSON.stringify(error));
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
  }

  //Initializa el campo de ambito local dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAmbito() {
    this.currAmbito = this.tablonInfo.uuaaMaster[0];
  }

  //Initializa el campo de la uuaa sin la letra del pais(ambito) dependiendo de la uuaa del tablon para su posible modificación y validación
  loadUUAA() {
    this.formControlUUAA.setValue(this.tablonInfo.uuaaMaster.substring(1));
    this.currUUAA = this.tablonInfo.uuaaMaster.substring(1);
    this.uuaaControl.setValue({ short_name: this.currUUAA });
  }

  //Actualiza los valores de la vista que corresponden a la uuaa de master y al origen del tablon
  updateUUAAProps(){
    this.uuaaMaster = this.tablonInfo.uuaaMaster;
    this.formControlAlias.markAsTouched();
    this.formControlAlias.updateValueAndValidity();
  }

  //Initializa el campo de la descripcion del alias dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAliasDesc() {
    this.uuaaMaster = this.tablonInfo.uuaaMaster;
    this.currScope = (this.tablonInfo.alias[0] + this.tablonInfo.alias[1]).toUpperCase();
    this.formControlScope.setValue(this.currScope);
    if (this.currAmbito === "C") {
      this.descAlias = this.tablonInfo.master_name.substring(10);
    } else {
      this.maxLengthDesc = 32;
      this.descAlias = this.tablonInfo.master_name.substring(7);
    }
  }

  //Modifica la uuaa del tablon dependiendo del ambito y la uuaa sin codigo de pais
  inputForUUAA() {
    this.tablonInfo.uuaaMaster = this.currAmbito + "" + this.currUUAA;
  }

  //Modifica el nombre del tablon dependiendo del ambito, uuaa, alias y desc. del alias
  inputForNames() {
    if (this.currAmbito === "C") {
      this.maxLengthDesc = 22;
      this.tablonInfo.master_name = "t_" + this.tablonInfo.uuaaMaster.toLowerCase()
        + "_" + this.currScope.toLowerCase() + "_" + this.descAlias;
    } else {
      this.maxLengthDesc = 32;
      this.tablonInfo.master_name = "t_" + this.tablonInfo.uuaaMaster.toLowerCase()
        + "_" + this.descAlias;
    }
    this.tablonInfo.master_name = this.inputValidatorNamingTsu(this.tablonInfo.master_name);
  }

  //Actualiza el ambito y los campos que dependan de el
  selectedAmbito(event) {
    this.currAmbito = event;
    this.inputForUUAA();
    this.inputForNames();
    this.updateUUAAProps();
  }


  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa2(event) {
    this.currUUAA = event.option.value["short_name"];
    this.uuaaControl.setValue(event.option.value);
    this.inputForUUAA();
    this.inputForNames();
    this.updateUUAAProps();
  }

  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa(event) {
    this.currUUAA = event;
    this.inputForUUAA();
    this.inputForNames();
    this.updateUUAAProps();
  }

  selectedScopeOrigin(event) {
    this.currScope = event;
    this.inputForNames();
    this.updateUUAAProps();
  }

  //Modifica la estructura del naming para agregar el sufix adecuado
  addCheckSuffix(row: any, suffix: string) {
    switch (suffix) {
      case "id": row.naming.suffix = 1; break;
      case "type": row.naming.suffix = 2; break;
      case "amount": row.naming.suffix = 3; break;
      case "date": row.naming.suffix = 4; break;
      case "name": row.naming.suffix = 5; break;
      case "desc": row.naming.suffix = 6; break;
      case "per": row.naming.suffix = 7; break;
      case "number": row.naming.suffix = 8; break;
    }
  }

  //Abre el popup con las comparaciones de los origenes y fields con otros tablones
  comparisonDialog() {
    const dialogErr = new MatDialogConfig();
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.minWidth = "50%";
    //dialogErr.minHeight = "50%";
    dialogErr.data = { idTablon: this.idTablon };
    this.dialog.open(ComparisonInfoComponent, dialogErr);
  }

  //Valida solo un naming y muestra el popup de los errores
  validateNaming(row: any) {
    var errors = this.validateNamingRow(row);
    const dialogErr = new MatDialogConfig();
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.minWidth = "50%";
    dialogErr.data = { validations: errors.errors };
    this.dialog.open(ErrorsComponent, dialogErr);
  }

  //Valida todos los namings del tablon
  validateAllNamings(showMessages: boolean) {
    var tmpResp;
    var isAllValid = true;
    var fullErrors = [];
    //fullErrors.push("*** Verificación Namings ***\n");
    for (var name of this.tablonFields) {
      tmpResp = this.validateNamingRow(name);
      if (tmpResp.hasOwnProperty("globalValid") || tmpResp.errors.length === 1)
        //tmpResp.errors[1] = "<<Campo válido>>\n";
        console.log("");
      else {
        //tmpResp.errors[tmpResp.errors.length] = "<<Campo inválido>>\n"; 
        isAllValid = false;
      }
      for (var tmp of tmpResp.errors)
        fullErrors.push(tmp);
    }
    const dialogErr = new MatDialogConfig();
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.minWidth = "50%";
    dialogErr.data = { pretty: showMessages, errors: fullErrors };
    this.dialog.open(ErrorsComponent, dialogErr);
    return isAllValid;
  }

  //Valida todos los campos de la fila del naming
  validateNamingRow(row: any) {
    var errors = [];
    let tmpSuffix = row.naming.naming.split('_');
    let suffix = tmpSuffix[tmpSuffix.length - 1];
    let globalErrL = 0;
    let globalFlag = row.naming.isGlobal == 'Y' || this.currScope == "MG";

    //Header errors
    errors.push("*" + (row.column + 1) + " - " + row.naming.naming + ":\n");
    if (row.length < 1 && this.getSecondaryLogicalF(row) !== 'CLOB') errors.push("Por favor, diligenciar la longitud del naming" + "\n");
    if (row.naming.naming == "empty") errors.push("Por favor, diligenciar el campo naming" + "\n");
    if (row.logic == "empty") errors.push("Por favor, diligenciar el campo nombre lógico" + "\n");
    if (row.description == "empty") errors.push("Por favor, diligenciar el campo descripción" + "\n");
    if (row.dataType == "empty") errors.push("Por favor, diligenciar el tipo de dato" + "\n");
    if ((row.operation == "" || row.operation == "empty") && row.direct === 0) errors.push("Por favor, diligenciar el campo de comentarios técnicos si no es directo" + "\n");
    if (row.naming.isGlobal === 'N' && row.naming.naming.length > 30) errors.push("El naming no es global y tiene mas de 30 caracteres" + "\n");
    else if (row.naming.naming != "empty" && row.description != "empty" && row.logic != "empty" && row.operation != "") {
      var suffixTmp = undefined;
      for (let suff of this.suffixesOptions)
        if (suff.data[0].toLowerCase() === suffix)
          suffixTmp = suff;
      if (suffixTmp !== undefined) {
        if (row.naming.isGlobal !== 'Y') {
          this.addCheckSuffix(row, suffix);
          if (!suffixTmp.Logic.includes(row.logic.split(" ")[0])) {
            errors.push("El nombre lógico no concuerda con el sufijo del naming." + "\n");
            globalErrL += 1;
          }
          if (!suffixTmp.DESCRIPTION.includes(row.description.split(" ")[0].toUpperCase())) {
            errors.push("La descripción no concuerda con el sufijo del naming." + "\n");
            globalErrL += 1;
          }
          if (row.dataType.includes("("))
            if (!this.dataTypeList(row, true).includes(row.dataType))
              if (!this.dataTypeList(row, true).includes(row.dataType.split("(")[0])) {
                errors.push("El tipo de dato no concuerda con el sufijo del naming.\n");
                globalErrL += 1;
              }
        }
      } else errors.push("El súfijo no es válido." + "\n");
    }
    this.addCleanModification(row, errors.length - 1 === 0);
    this.updateValuesRequest(row, false);
    if(globalFlag)
      if(globalErrL > 0) {
        errors.push("El naming tiene errores de sufijos y tipos de datos pero por ser del modelo global se ignoran.\n");
        return { globalValid: true, valid: errors.length - (globalErrL + 2) === 0, errors: errors };
      }
    return { valid: errors.length - 1 === 0, errors: errors };
  }

  //Limpia el tipo de dato de salida del naming
  cleanDataTypeChange(row: any) {
    row.dataType = "empty";
    row.format = "empty";
    row.logicalFormat = "empty";
  }

  //Valida el nuevo naming a modificar. No este repetido, si es global y si esta vacio
  //Solo cuando se llena el campo, no se verifica la integridad del naming
  validateNamingInput(row: any, event: any) {
    if (event.target.value.trim().length == 0) {
      event.target.value = "empty";
      return true;
    } else {
      if (!this.isNamingRepeated(event.target.value)) {
        this.tablonFields[row.column].naming.naming = event.target.value.trim().toLowerCase();
        this.cleanDataTypeChange(row);
        this.checkNamingGlobal(row.column, event.target.value.trim().toLowerCase());
        return true;
      } else {
        this.openSnackBar("Naming repetido.", "Error");
        return false;
      }
    }
  }

  //Valida el nuevo nombre logico a modificar. No este repetido o si esta vacio
  validateLogicNaming(row: any, event: any) {
    if (event.target.value.trim().length == 0) {
      event.target.value = "empty";
      return true;
    } else {
      if (!this.isRepeatedLogic(this.clearLogic(event.target.value.trim()))) {
        //if(row.naming.isGlobal === 'Y') row.naming.isGlobal = 'N';
        return true;
      } else
        this.openSnackBar("Nombre lógico repetido.", "Error");
      return false;
    }
  }

  //Verifica si el naming esta repetido
  isRepeatedLogic(logicNaming: string) {
    for (let field of this.tablonFields)
      if (field.logic === logicNaming)
        return true;
    return false;
  }

  //Limpia el nombre logico (?)
  clearLogic(logic: string) {
    let logicRem = logic;
    if (logic !== 'empty') {
      let tmp = logic.toUpperCase();
      logicRem = "";
      let i = 0;
      while (i < logic.length) {
        if (tmp.charCodeAt(i) == 209) {
          if (i + 1 < logic.length && tmp.charCodeAt(i + 1) == 73) {
            logicRem += "NI";
            i += 2;
          } else {
            logicRem += "NI";
            i++;
          }
        } else {
          logicRem += tmp.charAt(i);
          i++
        }
      }
    }
    return logicRem;
  }

  //Verifica que el naming enviado no exista en el arreglo de namings
  isNamingRepeated(naming: string) {
    for (var tab of this.tablonFields)
      if (tab.naming.naming === naming)
        return true;
    return false;
  }

  //Verifica que el naming sea global, si lo es carga toda la info, si no, lo marca como no global.
  checkNamingGlobal(index: number, naming: string) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('namings/' + naming).subscribe(
        (data: any) => {
          var p = /~/gi;
          if (data != null) {
            this.tablonFields[index].naming.isGlobal = data.naming.isGlobal;
            this.tablonFields[index].logic = data.logic;
            this.tablonFields[index].description = data.originalDesc.replace(p, "\n");
            this.tablonFields[index].naming.code = data.code;
            this.tablonFields[index].naming.codeLogic = data.codeLogic;
            this.tablonFields[index].naming.Words = data.naming.Words;
            this.tablonFields[index].naming.suffix = data.naming.suffix;
          } else {
            this.tablonFields[index].naming.isGlobal = 'N';
            this.tablonFields[index].naming.code = "empty";
            this.tablonFields[index].naming.codeLogic = "empty";
            this.tablonFields[index].naming.Words = [];
          }
          this.updateValuesRequest(this.tablonFields[index], false);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    });
    return promise;
  }

  //Trae namings de la tabla/tablon de origen
  getNamingsFromOrigin() {
    if (this.selectedOrigin.originType === "T") {
      let fase = (this.selectedOrigin.originRoute.includes("raw") ?
        "fieldsRaw" : "fieldsMaster");
      this.rest.getRequest('dataDictionaries/' + this.selectedOrigin["_id"] + '/fields/' + fase + '/').subscribe(
        (data: any) => {
          this.namingOptions = data;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
    } else if (this.selectedOrigin.originType === "DT") {
      this.rest.getRequest('dataTables/' + this.selectedOrigin["_id"] + '/fields/').subscribe(
        (data: any) => {
          this.namingOptions = data;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
    }
  }

  //Verifica el sufijo del naming actual
  checkNamingSuffix(row: any): string {
    let tmpSuffix = row.naming.naming.trim().split('_');
    let suffix = tmpSuffix[tmpSuffix.length - 1];
    let suffixIndex = -1;
    for (let i = 0; i < this.suffixesOptions.length; i++)
      if (this.suffixesOptions[i].suffix === suffix) {
        suffixIndex = i;
        break;
      }
    if (suffixIndex !== -1)
      return suffix;
    return "";
  }

  //Devuelve la lista de tipos de datos validos para el sufijo
  dataTypeList(row: any, globalFlag: boolean) {
    let array = [];
    let suffix = this.checkNamingSuffix(row);
    switch (suffix) {
      case "type":
      case "id":
      case "desc":
      case "name":
        array = ["STRING"];
        break;
      case "per":
        array = ["DECIMAL(p,s)", "FLOAT", "DOUBLE"];
        break;
      case "amount":
        array = ["STRING", "DECIMAL(p)", "DECIMAL(p,s)", "FLOAT", "DOUBLE"];
        break;
      case "number":
        array = [
          "INT32",
          "INT64",
          "DECIMAL(p)",
          "DECIMAL(p,s)",
          "FLOAT",
          "DOUBLE"
        ];
        break;
      case "date":
        array = ["DATE", "TIME", "TIMESTAMP"];
        break;
      case "ars":
        array = ["ARRAY", "STRING"];
        break;
      case "":
        array = ["STRING", "INT32", "INT64", "DECIMAL", "DATE", "TIME", "TIMESTAMP"];
        break;
    }
    this.arraySuffixTmp = array;
    return array;
  }

  //Resetea los valores para agregar naming de origen
  resetEditNamingAll() {
    this.selectedTable = '';
    this.selectedNaming = '';
    this.selNamingFlag = false;
    this.originsControl.setValue(null);
    this.namingsControl.setValue(null);
    this.namingsControl.disable();
  }

  //Resetea los valores de la tabla/tablon de origen para agregar namings de origen. Ej: cuando el input
  //de la tabla es invalido
  resetOrigin() {
    this.originsControl.setValue(null);
    this.namingsControl.disable();
    this.namingsControl.reset();
    this.resetEditNamingAll();
  }

  //Resetea los valores del namign de origen cuando se agrega.
  resetEditNaming() {
    this.selectedNaming = '';
    this.selNamingFlag = false;
    this.namingsControl.setValue(null);
  }

  //Valida que la tabla/tablon de origen del naming este dentro de las opciones permitidas.
  //Para evitar que ingresen tablas no permitidas
  checkSelectedOrigin() {
    if (!this.selectedOrigin || this.selectedOrigin !== this.originsControl.value)
      this.resetOrigin();
  }

  //Valida que el naming de origen este dentro de las opciones permitidas.
  //Para evitar que ingresen namings no permitidos
  checkSelectedNaming() {
    if (!this.selectedNaming || this.selectedNaming !== this.namingsControl.value)
      this.resetEditNaming();
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.duration = 12000;
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  //Peticion al back que devuelve los tipos de frecuencias/periodicidad
  getFrecuency() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe((data: any) => {
      this.frecuencyOptions = data;
    }, (error) => { });
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayOrigin(origin: any) {
    if (origin) {
      let originsDataTable: any = ["MG", "PG", "ML", "PL"];
      var temp = origin.originName.split("_");
      temp.splice(0, 2);
      if (origin.originType === "DT")
        if (originsDataTable.includes(temp[0].toUpperCase()))
          return origin.originName.substring(10);
        else
          return origin.originName.substring(7);
      return temp[0];
    }
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayNaming(naming: any) {
    if (naming) return naming.naming.naming;
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayUUAA(uuaa: any) {
    if (uuaa) return uuaa["short_name"];
  }

  //Actualiza el origen seleccionado y validado para agregar namings
  updateSelectedOrigin(event) {
    this.selectedOrigin = event.option.value;
    this.selOriginFlag = true;
    this.namingsControl.enable();
    this.namingsControl.reset();
    this.getNamingsFromOrigin();
  }

  //Actualiza el naming seleccionado y validado antes de agregar el naming de origen
  updateSelectedNaming(event) {
    this.selectedNaming = event.option.value;
    this.selNamingFlag = true;
  }

  //Calcula el alias de las tablas de origen para todas las filas despues de haber hecho alguna
  //modificacion (agregar/eliminar naming)
  updateAllAlias() {
    for (let row of this.tablonFields)
      this.calcUniqueAlias(row);
  }

  //Agregar un naming arriba de la fila actual
  addAboveFieldReview(row) {
    this.rest.postRequest('dataTables/' + this.idTablon + '/fields/actions/'
      , { action: "insert", column: row.column, user_id : this.userId}).subscribe(
        (data: any) => {
          this.tablonFields = data;
          this.updateAllAlias();
        }, (error) => {
          console.error(JSON.stringify(error));
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
  }

  //Agregar un naming abajo de la fila actual
  addBelowFieldReview(row) {
    this.rest.postRequest('dataTables/' + this.idTablon + '/fields/actions/'
      , { action: "insert", column: row.column + 1, user_id : this.userId}).subscribe(
        (data: any) => {
          this.tablonFields = data;
          this.updateAllAlias();
        }, (error) => {
          console.error(JSON.stringify(error));
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
  }

  //Elimina el naming seleccionado
  deleteRow(row) {
    this.rest.postRequest('dataTables/' + this.idTablon + '/fields/actions/'
      , { action: "substract", column: row.column, user_id : this.userId }).subscribe(
        (data: any) => {
          this.tablonFields = data;
          this.updateAllAlias();
        }, (error) => {
          console.error(JSON.stringify(error));
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        });
  }

  //Peticion al back que devuelve el arreglo con los posibles sufijos
  getSuffixes() {
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixesOptions = data;
      });
  }

  //Peticion al back que devuelve el arreglo con los posibles sufijos
  cleanColumns() {
    this.rest.getRequest('dataTables/clean_columns/' + this.idTablon + '/').subscribe(
      (data: {}) => {
        console.log("clean_columns: " + data)
      });
  }

  getReasons() {
    this.reasonsOptions = [];
    this.rest.getRequest('dataDictionaries/comments/reasons/').subscribe(
      (data: {}) => {
        this.reasonsOptions = data;
        this.reasonsOptions.unshift({ error: "NOK" });
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Agrega un naming de origen
  addNamingOrigin(row: any) {
    if (row.direct === 1) {
      if (confirm("El campo es directo, cualquier modificación le quitara el estado. ¿Desea continuar?")) {
        row.direct = 0;
        row.operation = "";
        if (!this.checkDuplicateOriginNamings(row))
          this.updateNamingOrigin(row);
        else
          this.openSnackBar("Ese naming esta repetido", "Error");
      }
    } else
      if (!this.checkDuplicateOriginNamings(row))
        this.updateNamingOrigin(row);
      else
        this.openSnackBar("Ese naming esta repetido", "Error");
  }

  //Verifica que no hayan namings de origen repetidos en el naming del tablón
  checkDuplicateOriginNamings(row: any) {
    for (let pos of row.originNamings) {
      if (pos.table_id === this.selectedOrigin["_id"]
        && pos.naming === this.selectedNaming.naming.naming) {
        return true;
      }
    }
    return false;
  }

  //Agrega un naming al arreglo de originNamings
  updateNamingOrigin(row: any) {
    let originNaming = {
      table_id: this.selectedOrigin["_id"],
      table_name: this.selectedOrigin.originName,
      naming: this.selectedNaming.naming.naming,
      type: this.selectedNaming.dataType
        ? this.selectedNaming.dataType : this.selectedNaming.destinationType.toUpperCase(),
      format: this.selectedNaming.format !== "empty" ? this.selectedNaming.format : "",
      originType: this.selectedOrigin.originType,
      originAlias: this.selectedOrigin.originAlias
    };
    row.originNamings.push(JSON.parse(JSON.stringify(originNaming)));
    this.updateNamingRow(row);
    this.calcUniqueAlias(row);
    this.resetEditNamingAll();
  }

  //Carga la info del naming 1 a 1 con la del origen
  updateNamingDirect(row: any) {
    row.direct = 1;
    row.operation = "<<DIRECTO>>";
    row.length = this.selectedNaming.length ? this.selectedNaming.length :
      Number(this.selectedNaming.logicalFormat.split("(")[1].split(")")[0]);
    row.naming = this.selectedNaming.naming;
    row.logic = this.selectedNaming.logic;
    row.description = this.selectedNaming.description;
    row.catalogue = this.selectedNaming.catalogue;
    row.key = this.selectedNaming.key;
    row.mandatory = this.selectedNaming.mandatory;
    row.dataType = this.selectedNaming.dataType
      ? this.selectedNaming.dataType : this.selectedNaming.outFormat;
    row.format = this.selectedNaming.format;
    row.logicalFormat = this.selectedNaming.logicalFormat;
    row.tds = this.selectedNaming.tds;
  }

  //Carga el naming de origen como directo cambiando el naming en si a este y tomandolo sin
  //procesamientos
  editDirectNaming(row: any) {
    if (confirm("¿Desea cargar el naming directo y sin modificaciones del origen?")) {
      this.updateNamingDirect(row);
      this.updateNamingOrigin(row);
    }
  }

  //Comprueba que si el naming actualmente es directo a su naming de origen. (Es el mismo sin procesamiento)
  checkDirectOriginNaming(row: any) {
    if (row.hasOwnProperty("direct"))
      return row.direct === 1;
    return false;
  }

  //Funcion que elimina una tabla/tablon de origen del tablon por completo
  //Falta validar cuando el origen tenga namings en uso
  removeOrigin(row: any, origin: any) {
    if (confirm("¿Desea remover el naming origen?")) {
      row.originNamings.splice(row.originNamings.indexOf(origin), 1);
      row.direct = 0;
      this.updateNamingRow(row);
    }
    this.calcUniqueAlias(row);
  }

  //Actualiza el formato de gobierno de la fila
  updateValueGovernanceFormat(row: any) {
    if (row.dataType.match(/STRING/g)) {
      row.logicalFormat = "ALPHANUMERIC(" + row.length + ")";
      row.format = "empty";
    } else if (row.dataType.match(/DATE/g)) {
      row.logicalFormat = "DATE";
      row.format = "yyyy-MM-dd";
    } else if (row.dataType.match(/INT64/g)) {
      row.logicalFormat = "NUMERIC BIG";
      row.format = "empty";
    } else if (row.dataType.match(/INT/g)) {
      row.logicalFormat = (row.length > 4 ? "NUMERIC LARGE" : "NUMERIC SHORT");
      row.format = "empty";
    } else if (row.dataType.match(/TIMESTAMP/g)) {
      row.logicalFormat = "TIMESTAMP";
      row.format = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    } else if (row.dataType.match(/DECIMAL/g)) {
      //if(row.decimals<1 && row.integers<1)
      //this.openSnackBar("Por favor, diligencie los valores decimales y enteros.", "Error");
      row.logicalFormat = "DECIMAL(0,0)";
      row.format = "(0,0)";
      //row.logicalFormat="DECIMAL("+(Number(row.integers)+Number(row.decimals))+","+row.decimals+")";
      //row.format="("+(Number(row.integers)+Number(row.decimals))+","+row.decimals+")";
    }
  }

  //Obtiene la lista de todos los proyectos en nebula y elimina el proyecto unassigned de la lista
  getProjectData() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        for (var project of data)
          if (this.tablonInfo.project_owner === project.name) {
            this.idProject = project._id;
            this.getProjectUseCases(this.idProject);
            this.getAmbitos();
            this.getUuaas();

          }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Peticion que trae un caso de uso relacionado con el proyecto del tablon
  //solamente para enviarlo al dialog de searchTable
  getProjectUseCases(idProject: string) {
    this.rest.getRequest('projects/' + idProject + '/useCases/').subscribe(
      (data: any) => {
        this.idUseCase = data[0];
      }, (error) => { this.openSnackBar("Error procesando petición al servidor.", "Error"); }
    );
  }

  getAmbitos() {
    this.rest.getRequest('uuaa/ambitos/').subscribe((data: any) => {
      this.ambitoOptions = data;
    });
  }


  getUuaas() {
    this.rest.getRequest("uuaa/master/all/").subscribe((data: any) => {
      this.uuaaOptions2 = data;
      this.uuaaOptions = data;
    });
  }

  //Funcion que trae las opciones de periodicidad de la tabla/tablon
  getPeriodicityOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicityOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Carga las tablas de origen en el arreglo de options para mostrar en la tabla de origen
  loadOriginTablesOptions() {
    let tableIds = [];
    let dataTablesIds = [];
    for (var tab of this.tablonInfo.origin_tables)
      tab.originType === "DT" ? dataTablesIds.push(tab._id) : tableIds.push(tab._id);

    this.prepareOriginTablesOptions(tableIds, "T").then((res: any) => {
      this.selectedOriginOptions = (this.selectedOriginOptions.length < 1 ?
        res : this.selectedOriginOptions.concat(res));
      this.prepareOriginTablesOptions(dataTablesIds, "DT").then((res: any) => {
        this.selectedOriginOptions = (this.selectedOriginOptions.length < 1 ?
          res : this.selectedOriginOptions.concat(res));
        this.validateDuplicateOriginTables();
      });
    });
  }

  //Prepara las promesas para cargar las tablas de origen al arreglo de options
  prepareOriginTablesOptions(tableIds, typeOrigin) {
    var promise = new Promise((resolve) => {
      let promises = [];
      if (typeOrigin === 'DT')
        for (let id of tableIds)
          promises.push(this.requestPromiseDT(id))
      else
        for (let id of tableIds)
          promises.push(this.requestPromise(id))
      Promise.all(promises)
        .then((result) => {
          resolve(result);
        })
    });
    return promise;
  }

  //Funcion que prepara la tabla de origen dependiendo de la fase que este seleccionada para mostrar
  //en la vista de tablas de origen
  checkOriginFase(row: any, id: string) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + id + '/head/' + 'FieldReview/').toPromise()
        .then(res => {
          let index = this.tablonInfo.origin_tables.map(function (e) { return e._id; }).indexOf(id);
          if (index >= 0) {
            let name = this.tablonInfo.origin_tables[index].originName;
            let fase = (name.includes(res.uuaaRaw.toLowerCase()) ? "RAW" : "MASTER");
            row.faseSeleccionada = fase;
            this.tablonInfo.origin_tables[index].originSystem = (fase === "RAW" ? "HDFS-Avro" : "HDFS-Parquet");
            this.tablonInfo.origin_tables[index].originType = "T";
          }
          row._id = id;
          resolve(row);
        })
    });
    return promise;
  }

  //Peticion que llama la peticion para preparar la tabla de origen dependiendo de su fase
  requestPromise(tabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + tabId + '/head/raw/').toPromise()
        .then((res: any) => {
          if (res !== null)
            this.checkOriginFase(res, tabId).then(row => {
              resolve(row);
            })
          else resolve({});
        });
    });
    return promise;
  }

  //Peticion que llama la peticion para preparar la tabla de origen dependiendo de su fase
  requestPromiseDT(datatabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataTables/' + datatabId + '/head/').toPromise()
        .then((res: any) => {
          if (res !== null) resolve(res);
          else resolve({});
        });
    });
    return promise;
  }

  //Funcion que llama al dialog de searchTable para agregar tablas de origen al tablon
  searchTables(searchType: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.minWidth = (searchType === "T" ? "80%" : "75%");
    dialogConfig.minHeight = "43rem";
    dialogConfig.width = "70rem";
    dialogConfig.data = { searchType: searchType };
    dialogConfig.panelClass = "repoDataClass";
    const dialogRef = this.dialog.open(RepoDatatableComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.prepareOriginTablesOptions(result.tableIds, searchType).then((res: any) => {
          this.selectedOriginOptions = (this.selectedOriginOptions.length < 1 ?
            res : this.selectedOriginOptions.concat(res));
          this.cleanEmptyTables();
          this.validateDuplicateOriginTables();
          if (searchType === "DT")
            for (let newDT of res)
              this.newOriginDT(newDT);
        });
      }
    });
  }

  //Limpia las tablas de origen que sacaron error en la carga
  //Posible agregar informe de las que fallaron.
  //Este metodo es mas una contingencia en caso que falle la carga de origenes
  cleanEmptyTables() {
    let flag = false;
    for (let tab of this.selectedOriginOptions)
      if (!tab.hasOwnProperty("baseName")) {
        this.selectedOriginOptions.splice(this.selectedOriginOptions.indexOf(tab), 1);
        flag = true;
      }
    if (flag) this.openSnackBar("Error procesando orígenes, algunas no se cargaron", "Error");
  }

  //Valida que la tabla origen que se quiera agregar no este repetida
  validateDuplicateOriginTables() {
    for (var first = 0; first < this.selectedOriginOptions.length - 1; first++) {
      for (var second = first + 1; second < this.selectedOriginOptions.length; second++) {
        if (this.selectedOriginOptions[first].baseName === this.selectedOriginOptions[second].baseName) {
          this.selectedOriginOptions.splice(second, 1);
          this.selectedOriginOptions = [...this.selectedOriginOptions]
        }
      }
    }
  }

  //Valida que la informacion de la tabla (objeto) tenga campos validos antes de enviar la peticion al back para actualizarlo
  validateObject(): boolean {
    var object = this.tablonInfo;
    var flag = false;
    var tmp_errors = [];

    this.tablonInfo.current_depth = Number(this.tablonInfo.current_depth);
    this.tablonInfo.estimated_volume_records = Number(this.tablonInfo.estimated_volume_records);
    this.tablonInfo.required_depth = Number(this.tablonInfo.required_depth);

    this.updateUUAAProps();
    if (this.tablonInfo.partitions.length > 1)
      if (!this.validatePartitionNamings(this.tablonInfo.partitions, this.tablonFields))
        tmp_errors.push("Los namings de particiones están erróneos, por favor revisar", "Error");
    if (object.master_name !== object.master_name.replace(/[^a-z0-9_]/g, "").trim())
      tmp_errors.push("El nombre físico del objeto continene caracteres especiales.");
    object.baseName = object.baseName.replace(/^\s+|\s+$/g, "").toUpperCase();
    if (object.baseName.length >= 1) {
      if (object.baseName.length < 60) {
        if (object.baseName != object.baseName.replace(/[^a-zA-ZÀ-ÿ\u00f1\u00d1a-zA-Z0-9 _-]/g, " "))
          tmp_errors.push("El nombre base del objeto continene caracteres especiales.");
      } else
        tmp_errors.push("El nombre base del objeto no debe sobrepasar los 60 caracteres.");
    } else
      tmp_errors.push("El nombre base del objeto es obligatorio.");
    if (!this.validateAlias(this.tablonInfo.alias, this.tablonInfo.uuaaMaster, this.currScope))
      tmp_errors.push("El Alias no cumple con la estructura requerida.");
    if (!this.validateTheAllFour())
      tmp_errors.push("Hay campos faltantes por llenar en el objeto.");

    if (object.deploymentType === "Global-Implementation") {
      if (!this.validateModelVersion(object)) tmp_errors.push("El campo Model Version tiene formato invalido.");
      if (!this.validateObjectVersion(object)) tmp_errors.push("El campo Object Version tiene formato invalido.");
    } else {
      if (object.hasOwnProperty('modelVersion') && object.modelVersion.trim() != "") tmp_errors.push("El campo Model Version tiene que ir vació si el Deployment Type NO es 'Global-Implementation'.");
      if (object.hasOwnProperty('objectVersion') && object.objectVersion.trim() != "") tmp_errors.push("El campo Object Version tiene que ir vació si el Deployment Type NO es 'Global-Implementation'.");
    }

    if (!object.hasOwnProperty('securityLevel'))
      tmp_errors.push("El campo Security Level es obligatorio.");
    if (tmp_errors.length > 0) {
      tmp_errors.unshift("" + object.master_name + "");
      const dialogErr = new MatDialogConfig();
      dialogErr.maxHeight = "65%";
      dialogErr.minWidth = "500px";
      dialogErr.disableClose = true;
      dialogErr.autoFocus = true;
      dialogErr.data = tmp_errors;
      this.dialog.open(ErrorsComponent, dialogErr);
    } else
      flag = true;
    return flag;
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassRaw({ row, column, value }) {
    if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "RAW")
      return { 'selected': true };
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassMaster({ row, column, value }) {
    if (!row.hasOwnProperty("uuaaRaw"))
      return { 'selectedDT': true };
    else if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "MASTER")
      return { 'selected': true };
  }

  //Devuelve la clase CSS dependiendo del estado del VoBo
  getStatusRow({ row, column, value }) {
    if (row.check.status == 'NOK') {
      return { 'is-nok': true };
    } else if (row.check.status == 'OK') {
      return { 'is-ok': true };
    }
  }

  //Abre el popup de los comentarios de VoBo
  commentsWindowsVoBo(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      vobo: true,
      fase: "fields",
      row: row.check,
      naming: row.naming,
      column: row.column,
      user: sessionStorage.getItem("userName"),
      userRol: sessionStorage.getItem("rol"),
      idTablon: this.idTablon
    };
    this.dialog.open(CommentComponentComponent, dialogConfig);
  }

  //Funcion que verifica a que fase pertenence la tabla de origen si es que pertenence a una
  disabledFaseCheck(row: any, fase: string) {
    if (row.hasOwnProperty('faseSeleccionada'))
      if (row.faseSeleccionada !== fase)
        return true;
      else
        return false;
    else
      return true;
  }

  //Funcion con la que se selecciona la fase que uno desee para la tabla de origen
  selectFase(row: any, fase: string) {
    if (!row.hasOwnProperty["faseSeleccionada"]) {
      row.faseSeleccionada = fase;
      this.newOrigin(row);
    }
  }

  //Agrega una nueva tabla de origen al tablon o edita uno actual a otra fase
  //Falta validar cuando hayan namings de la fase que se pierdan con este cambio
  newOrigin(row: any) {
    let existsFlag = false;
    for (var ind = 0; ind < this.tablonInfo.origin_tables.length; ind++)
      if (this.tablonInfo.origin_tables[ind]._id === row._id) {
        this.editExistingOrigin(row, ind);
        existsFlag = true;
      }
    if (!existsFlag && row._id) {
      var newOrigin = {
        _id: row._id,
        originSystem: (row.faseSeleccionada === "RAW" ? "HDFS-Avro" : "HDFS-Parquet"),
        originName: (row.faseSeleccionada === "RAW" ? row.raw_name : row.master_name),
        originAlias: this.calcAlias((row.faseSeleccionada === "RAW" ? row.raw_name : row.master_name), 'T'),
        originRoute: (row.faseSeleccionada === "RAW" ? row.raw_route : row.master_route),
        originType: "T"
      };
      this.tablonInfo.origin_tables.push(JSON.parse(JSON.stringify(newOrigin)));
      this.updateObject();
    }
  }

  //Agrega una nueva tabla de origen al tablon o edita uno actual a otra fase
  //Falta validar cuando hayan namings de la fase que se pierdan con este cambio
  newOriginDT(row: any) {
    let existsFlag = false;
    for (var ind = 0; ind < this.tablonInfo.origin_tables.length; ind++)
      if (this.tablonInfo.origin_tables[ind]._id === row._id) {
        existsFlag = true;
        this.openSnackBar("Tablon " + row.originName + " Repetido", "Error");
      }
    if (!existsFlag && row._id) {
      var newOrigin = {
        _id: row._id,
        originSystem: "HDFS-Parquet",
        originName: row.master_name,
        originAlias: row.alias,
        originRoute: row.master_path,
        originType: "DT"
      };
      this.tablonInfo.origin_tables.push(JSON.parse(JSON.stringify(newOrigin)));
      this.updateObject();
    }
  }

  //Edita el origen con la nueva fase
  editExistingOrigin(row: any, index: any) {
    this.tablonInfo.origin_tables[index].originName = (row.faseSeleccionada === "RAW" ?
      row.raw_name : row.master_name);
    this.tablonInfo.origin_tables[index].originRoute = (row.faseSeleccionada === "RAW" ?
      row.raw_route : row.master_route);
  }

  //Envia una peticion al back para actualizar los namings de origen de un naming del tablon
  updateNamingRow(row: any) {
    let rowArray = []; rowArray.push(row);
    let body = {
      action: "row",
      user: this.userId,
      row: rowArray
    }
    this.rest.postRequest('dataTables/' + this.idTablon + '/fields/', body).subscribe(
      (data: any) => {
        this.openSnackBar("Naming actualizado", "Ok");
        this.resetEditNamingAll();
        row.originNamings = [...row.originNamings];
      }, (error) => {
        console.error(JSON.stringify(error));
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Actualiza los valores de los namings de la tabla
  updateOrigin(event) {
    if (event.option.value) {
      this.selectedTable = event.option.value;
      this.namingOptions = event.option.value.naming;
      this.namingsControl.enable();
    }
  }

  //Toggle para el rowDetails
  toggleExpandRow(row) {
    if (this.lastToggleNamingNumber == row.column) {
      this.lastToggleNamingNumber = -1;
      this.table.rowDetail.collapseAllRows();
    } else {
      this.table.rowDetail.collapseAllRows();
      this.lastToggleNamingNumber = row.column;
      this.table.rowDetail.toggleExpandRow(row);
    }
    this.showAddOriginNaming = false;
    this.resetOrigin();
    this.resetEditNamingAll();
  }

  //Funcion visual para el toggle del row detail de los namings de origen
  activeDetailsCheck(column) {
    return this.lastToggleNamingNumber == column;
  }

  fieldsQuery() {
    if(this.namingQuery == "" && this.logicQuery == "" && this.descQuery == ""
      && this.stateQuery == "" && this.namingQuery == "" && this.typeQuery == "") {
        this.openSnackBar("Digite valores de búsqueda", "Error");
    } else {
      this.rest.getRequest('dataTables/' + this.idTablon 
        + '/suffix='+ (this.typeQuery == "" ? "%20" : this.typeQuery)
        + '&naming=' + (this.namingQuery == "" ? "%20" : this.namingQuery) 
        + '&logic=' + (this.logicQuery == "" ? "%20" : this.logicQuery)  
        + '&desc=' + (this.descQuery == "" ? "%20" : this.descQuery)  
        + '&state=' + (this.stateQuery == "" ? "%20" : this.stateQuery) + '/')
        .subscribe(
          (data: any) => {
            this.tablonFields = data;
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
            console.log(error);
          });
    }
  }

  cleanQuery(){
    this.namingQuery = "";
    this.logicQuery = "";
    this.descQuery = "";
    this.stateQuery = "";
    this.namingQuery = "";
    this.typeQuery = "";
    this.getFieldsTablon();
  }

  //Abre el popup de los comentarios
  commentsWindows(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      fase: "FieldReview", //PENDING
      user: sessionStorage.getItem("userId"),
      userRol: sessionStorage.getItem("rol"),
      row: row,
      idTablon: this.idTablon
    };
    this.dialog.open(CommentComponentComponent, dialogConfig);
  }

  checkGeneratedStatus(row: any) {
    if (row.originNamings.length === 0)
      if (row.default !== "empty" && row.default.trim().length > 0)
        return 1;
    if (row.originNamings.length > 0)
      return 2;
    return 0;
  }

  getGenCellDef({ row }) {
    if (row.originNamings.length > 0)
      return { 'generatedRow': true }
  }

  getGenCellOrigins({ row }) {
    if (row.originNamings.length === 0)
      if (row.default !== "empty" && row.default.trim().length > 0)
        return { 'generatedRow': true }
  }

  //Devuelve la clase CSS para colorear la celda del naming
  getCellClass({ row }) {
    if (row.originNamings.length === 0) {
      if (row.default === "empty")
        return { 'generatedRow': true }
      if (row.modification.length > 0 && row.modification[0].stateValidation) {
        if (row.modification[0].stateValidation === 'YES')
          return { 'is-valid-proposal': true }
        else
          return { 'is-wrong': true };
      } else
        return { 'is-wrong': true };
    }
    if (row.hasOwnProperty("direct"))
      if (row.direct === 1)
        return { 'is-directnaming': true };
    if (row.naming.isGlobal === 'Y')
      return { 'is-global': true };
    return { 'is-proposal': true };
  }

  //Agrega un campo a la modificacion dependiendo con el resultado de la validacion del naming
  addCleanModification(row: any, validation: boolean) {
    var newModi = {
      startDate: new Date(),
      state: "",
      user: this.userId,
      stateValidation: (validation ? "YES" : "NO")
    };
    if (row.modification.length >= 5)
      row.modification.splice(4, row.modification.length - 1);
    row.modification.unshift(newModi);
  }

  //Muestra el popup de los detalles del naming de origen antes de agregarlo
  namingDetails(namingRow: any, originT: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "65%";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      naming: namingRow,
      idTable: originT._id,
      originType: originT.originType
    };
    this.dialog.open(NamingInfoComponent, dialogConfig);
  }

  //Funcion que abre el popup para los detalles del naming de origen llamando la peticion que trae la info completa
  //y abriendo el popup con un vinculo a la tabla original
  namingDetailsFromOrigin(naming: any) {
    this.getNamingFromOrigin(naming).then(res => {
      this.namingDetails(res["naming"], res["origin"]);
    });
  }

  //Peticion que trae la info completa del naming de origen
  getNamingFromOrigin(naming: any) {
    if (naming.originType && naming["originType"] === "DT") {
      var promise = new Promise((resolve, reject) => {
        this.rest.getRequest('dataTables/' + naming.table_id + '/fields/').subscribe(
          (data: any) => {
            for (var obj of data)
              if (obj.naming.naming === naming.naming) {
                let resu = { naming: obj, origin: { _id: naming.table_id, originType: "DT" } }
                resolve(resu);
              }
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
            resolve(error);
          });
      });
      return promise;
    } else {
      var promise = new Promise((resolve, reject) => {
        this.rest.getRequest('dataDictionaries/' + naming.table_id + '/head/' + 'FieldReview' + '/').toPromise()
          .then(res => {
            let fase = (naming.table_name.includes(res.uuaaRaw.toLowerCase()) ? "fieldsRaw" : "fieldsMaster");
            this.rest.getRequest('dataDictionaries/' + naming.table_id + '/fields/' + fase + '/').subscribe(
              (data: any) => {
                for (var obj of data)
                  if (obj.naming.naming === naming.naming) {
                    let resu = { naming: obj, origin: { _id: naming.table_id, originType: "T" } }
                    resolve(resu);
                  }
              }, (error) => {
                this.openSnackBar("Error procesando petición al servidor.", "Error");
                resolve(error);
              });
          }, (error) => {
            this.openSnackBar("Error con origen de tablón, se sugiere volver a cargar este naming.", "Error");
            resolve(error);
          });
      });
      return promise;
    }
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/;
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
  }

  //Valida la estructura correcta del alias del tablon
  validateAlias(alias: string, uuaa: string, scope: string) {
    if (uuaa === undefined) uuaa = "";
    if (scope === undefined) scope = "";
    var flag = false;
    var pattern = "^(" + scope.toLowerCase() + uuaa.toLowerCase() + ")[a-z0-9]*$";
    var regx = new RegExp(pattern);
    flag = regx.test(alias);
    return flag;
  }

  validateOriginExistence() {
    if (this.tablonInfo.origin_tables.length < 1)
        return false;
    return true;
  }

  validateModelVersion(object: any) {
    var regexo: RegExp = /^[v][\d]*[.][\d]*[.][\d]*/;
    return regexo.test(object.modelVersion);
  }

  validateObjectVersion(object: any) {
    var regexo: RegExp = /^[\d]*[.][\d]*[.][\d]*/;
    return regexo.test(object.objectVersion);
  }

  sendReview() {
    if (this.validateOriginExistence()) {
      if (this.validateAllNamings(false)) {
        if (confirm("Todos los namings están validos, ¿Desea devolver el tablón a Propuesta?"))
          this.rest.putRequest('dataTables/' + this.idTablon + '/state/',
            JSON.stringify({ from: "governance", to: "ingesta", user_id: this.userId})).subscribe(
              (data: any) => {
                this.openSnackBar("Se ha enviado la tabla para propuesta de namings.", "Ok");
                this.getInfoTablon();
              }, (error) => {
                console.log(error);
                this.openSnackBar("No se pudo enviar la tabla para propuesta de namings. " + error.message, "Error");
              });
      } else
        this.openSnackBar("Todavía existen namings sin validar", "Error");
    } else
      this.openSnackBar("El tablón necesita al menos un origen, ya sea Tabla o Tablón antes de pasar de fase", "Error");
  }

  sendGovernance() {
    if (this.validateOriginExistence()) {
      if (this.validateAllNamings(false)) {
        if (confirm("Todos los namings están validos, ¿Desea enviar el tablón a Gobierno?"))
          this.rest.putRequest('dataTables/' + this.idTablon + '/state/',
            JSON.stringify({ from: "ingesta", to: "governance", user_id: this.userId })).subscribe(
              (data: any) => {
                this.openSnackBar("Se ha enviado la tabla para revisión en gobierno", "Ok");
              }, (error) => {
                this.rest.putRequest('dataTables/' + this.idTablon + '/state/',
                  JSON.stringify({ from: "ingesta", to: "review" })).subscribe(
                  (data: any) => {
                    this.openSnackBar("Se ha enviado la tabla para revisión de la solicitud en gobierno", "Ok");
                    this.router.navigate(['/home']);
                  }, (error) => {
                    this.openSnackBar("" + error.error.reason, "Error");
                  });
              });
      } else
        this.openSnackBar("Todavía existen namings sin validar", "Error");
    } else
      this.openSnackBar("El tablón necesita al menos un origen, ya sea Tabla o Tablón antes de pasar de fase", "Error");
  }

  sendVobo() {
    if (this.validateOriginExistence()) {
      if (this.validateAllNamings(false)) {
        if (confirm("Todos los namings están validos, ¿Desea enviar el tablón a VoBo de Negocio?"))
          this.rest.putRequest('dataTables/' + this.idTablon + '/state/',
            JSON.stringify({ from: "governance", to: "business" , user_id: this.userId})).subscribe(
              (data: any) => {
                this.openSnackBar("Se ha enviado la tabla para revisión en VoBo. ", "Ok");
                this.router.navigate(['/home']);
              }, (error) => {
                this.openSnackBar("No se ha podido enviar la tabla para revisión en VoBo.", "Error");
              });
      } else
        this.openSnackBar("Todavía existen namings sin validar", "Error")
    } else
      this.openSnackBar("El tablón necesita al menos un origen, ya sea Tabla o Tablón antes de pasar de fase", "Error");
  }

  sendDoneDT(){
    if (confirm("Todos los namings están validos, ¿Desea dar por cerrado el Tablón?"))
      this.rest.putRequest('dataTables/' + this.idTablon + '/state/',
        JSON.stringify({ from: "architecture", to: "ingesta", user_id: this.userId })).subscribe(
          (data: any) => {
            this.openSnackBar("Se ha cerrado la propuesta del tablón. ", "Ok");
            this.router.navigate(['/home']);
          }, (error) => {
            this.openSnackBar("No se ha podido dar por cerrado el Tablón...", "Error");
          });
  }

  getExcels() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/excels/Object/').subscribe(
      (data: any) => {
        this.exportAsExcelFileObject(data, "Tablon-Objects " + this.tablonInfo.baseName);
        this.openSnackBar("Generación exitosa de los Object. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
    this.rest.getRequest('dataTables/' + this.idTablon + '/excels/Fields/').subscribe(
      (data: any) => {
        this.exportAsExcelFileFields(data, "Tablon-Fields " + this.tablonInfo.baseName);
        this.openSnackBar("Generación exitosa de los Fields. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-O': worksheet }, SheetNames: ['DDNG-O'] };
    XLSX.writeFile(workbook, EditTablonComponent.toExportFileName(excelFileName));
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    console.log(json);
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-F': worksheet }, SheetNames: ['DDNG-F'] };
    XLSX.writeFile(workbook, EditTablonComponent.toExportFileName(excelFileName));
  }

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  uploadStructureTablones() {
    var arrayBuffer: any;
    if (this.fileList != null) {
      let fileReader = new FileReader();
      let jsontmp;
      fileReader.onload = (e) => {
        arrayBuffer = fileReader.result;
        var data = new Uint8Array(arrayBuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) {
          arr[i] = String.fromCharCode(data[i]);
        }
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, { type: "binary" });
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        jsontmp = XLSX.utils.sheet_to_json(worksheet, { raw: true })
        var typeInfo = "origins";

        console.log(jsontmp)
        if (jsontmp[0].field_position == null) {
          typeInfo = "structure";
          for (var k = 0; k != jsontmp.length; ++k) {
            if (jsontmp[k].__rowNum__ != k + 1) {
              jsontmp.splice(k, 0, { naming: "", length: 0, decimals: 0, integers: 0, key: 0, mandatory: 0, direct: 0, operation: "" });
            }
          }
          for (var j = 0; j != jsontmp.length; ++j) {
            if (jsontmp[j].naming == null) {
              jsontmp[j].naming = "";
            } else {
              if (jsontmp[j].catalogue == null) jsontmp[j].catalogue = "";
              if (jsontmp[j].length == null) jsontmp[j].length = 0;
              if (jsontmp[j].decimals == null) jsontmp[j].decimals = 0;
              if (jsontmp[j].integers == null) jsontmp[j].integers = 0;
              if (jsontmp[j].data_type == null) jsontmp[j].data_type = "";
              if (jsontmp[j].catalogue == null) jsontmp[j].catalogue = "";
              jsontmp[j].key = (jsontmp[j].key == null || jsontmp[j].key == "N") ? 0 : 1;
              jsontmp[j].mandatory = (jsontmp[j].mandatory == null || jsontmp[j].mandatory == "N") ? 0 : 1;

              jsontmp[j].direct = (jsontmp[j].direct == null || jsontmp[j].direct == "N") ? 0 : 1;
              if (jsontmp[j].operation == null) jsontmp[j].operation = "";
            }
          }
          this.rest.putRequest('dataTables/' + this.idTablon + '/information/' + typeInfo + '/', JSON.stringify({"fields": jsontmp, "user_id": this.userId})).subscribe(
            (data: any) => {
              this.getFieldsTablon();
              if (jsontmp.length != this.tablonFields.length) {
                this.openSnackBar("Campos actualizados con diferencias encontradas. Longitud archivo:" + jsontmp.length + " " + "Longitud campos propuesta:" + this.tablonFields.length, "Warning");
              } else {
                this.openSnackBar("Campos actualizados.", "Ok");
              }
            }, (error) => {
              this.openSnackBar(error.error.reason, "Error");
            }
          );
        } else {
          this.rest.putRequest('dataTables/' + this.idTablon + '/information/' + typeInfo + '/', JSON.stringify({"fields": jsontmp, "user_id": this.userId})).subscribe(
            (data: any) => {
              this.selectedOriginOptions = []
              this.getInfoTablon();
              this.getFieldsTablon();
              this.openSnackBar("Campos actualizados.", "Ok");
            }, (error) => {
              this.openSnackBar(error.error.reason, "Error");
            }
          );
        }
      }
      fileReader.readAsArrayBuffer(this.fileList);
    } else {
      this.openSnackBar("Por favor, suba un archivo.", "Error");
    }
  }

  genExcels(option: number) {
    switch (option) {
      case 1: this.getExcels();
        break;
      case 2: this.getExcelsVobo();
        break;
    }
    this.selectorExcel.value = "";
  }

  getExcelsVobo() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/excels/VoBo/').subscribe(
      (data: any) => {
        this.exportAsExcelFileFields(data, "Tablon-Fields-VoBo " + this.tablonInfo.baseName);
        this.openSnackBar("Generación exitosa de los Fields con VoBo. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }



  //Actualiza el archivo actual (excel) por subir
  onFileChange(event: any) {
    if (event.target.files.length >= 1)
      this.fileList = event.target.files[0];
    else
      this.fileList = null;
  }

  //Abre el enlace externo para el template de las plantillas de carga
  openExternalTemplateLink(){
    window.open("https://docs.google.com/spreadsheets/d/17DLxrve0_J2Xhph2SquZhLO5B4QRl0exM4zyZHAjFkk/edit#gid=813161028", '_blank');
  }

  getRowHeight(row) {
    console.log('ROW', row);

    if (row.length % 2 == 0)
      return 50 

    return 'auto';
  }
}
