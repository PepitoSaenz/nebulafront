import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

//const homeAddress = "http://82.250.88.212:8001/";
const homeAddress = "http://localhost:4200/";

//const endpoint = 'http://82.250.88.212/';
const endpoint = 'http://localhost:8000/';

@Injectable({
  providedIn: 'root'
})

export class RestService {

  headers: HttpHeaders;

  constructor(private http: HttpClient, private router: Router, public snackBar: MatSnackBar) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Access-Control-Allow-Methods', 'GET,POST,PUT');
  }

  getHomeWebAddress(){
    return homeAddress;
  }

  getEndpoint(){
    return endpoint;
  }

  getRequest(route: string): Observable<any> {
    if(this.checkSessionStorage())
      return this.http.get(endpoint + route, { headers: this.headers });
  }

  postRequest(route: string, body: any): Observable<any> {
    if(this.checkSessionStorage())
      return this.http.post(endpoint + route, body, { headers: this.headers });
  }

  putRequest(route:string, body:any): Observable<any> {
    if(this.checkSessionStorage())
      return this.http.put(endpoint + route, body, { headers: this.headers });
  }

  //Realiza peticiones HTTP sin verificar que este logeado (Usar con precaución)
  getNoCheckRequest(route: string): Observable<any> {
    return this.http.get(endpoint + route, { headers: this.headers });
  }

  postNoCheckRequest(route: string, body: any): Observable<any> {
    return this.http.post(endpoint + route, body, { headers: this.headers });
  }

  putNoCheckRequest(route:string, body:any): Observable<any> {
    return this.http.put(endpoint + route, body, { headers: this.headers });
  }

  checkSessionStorage():boolean{
    if(sessionStorage.getItem("userName") !== null)
      return true;
    
    sessionStorage.clear();
    this.router.navigate(["/login"]);
    this.openSnackBar("Sesión caducada, vuelva a ingresar", "Error");
    return false;
  }

  sendHome(rol: string){
    if(rol=="A"){
      this.router.navigate(['/home/admin']);
    }else if(rol=="G" || rol=="I" || rol=="AR" || rol=="F"){
      this.router.navigate(['/home']);
    }else if(rol=="PO"){
      this.router.navigate(['/home/productowner']);
    }else if(rol=="SPO"){
      this.router.navigate(['/home/sproductowner']);
    }else if(rol=="RN"){
      this.router.navigate(['/tables/vobo']);
    }else{
      this.router.navigate(['/login']);
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
