import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTablonComponent } from './search-tablon.component';

describe('SearchTablonComponent', () => {
  let component: SearchTablonComponent;
  let fixture: ComponentFixture<SearchTablonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTablonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTablonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
