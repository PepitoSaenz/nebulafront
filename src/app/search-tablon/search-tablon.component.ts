
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RestService } from '../rest.service';
import { MatSnackBar,MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-search-tablon',
  templateUrl: './search-tablon.component.html',
  styleUrls: ['./search-tablon.component.scss']
})
export class SearchTablonComponent implements OnInit {

  useCaseId = "";
  projectId = "";

  masterName = "";
  alias = "";
  description = "";
  uuaa = "";
  selectedUuaa = "";
  ambito = "";

  uuaaOptions = [];
  tablones = [];
  selectedTablones = [];

  @ViewChild('uuaaSelectorOption') selectorUuaaMaster;
  @ViewChild('selectorAmbito') selectorAmbito: any;

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any,public rest:RestService, 
  public snackBar: MatSnackBar, private dialogRef: MatDialogRef<SearchTablonComponent>) {  
      this.useCaseId = datas.use_case_id;
      this.projectId = datas.project_id;
      this.getUuaas();
      this.getTablonesSearch(false);
  }

  ngOnInit() {
  }

  getTablonesSearch(param){
    var masterNameTmp = this.masterName;
    var aliasTmp = this.alias;
    var descriptionTmp = this.description;
    var uuaaMasterTmp = this.selectedUuaa + this.ambito;

    if(masterNameTmp.length==0){masterNameTmp="%20"; }
    if(aliasTmp.length==0){ aliasTmp="%20"; }
    if(descriptionTmp.length==0){ descriptionTmp="%20";}
    if(uuaaMasterTmp.length==0){ uuaaMasterTmp="%20";}

    if(descriptionTmp.length==0){ descriptionTmp="%20";}
    this.rest.getRequest('projects/query/dataTables/name='+masterNameTmp+'&tag='+aliasTmp+'&description='+descriptionTmp+'&uuaa='+uuaaMasterTmp+'/').subscribe(
        (data: any) => {
          this.tablones = data;
          if (param) this.openSnackBar("Busqueda finalizada", "Ok");
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.","Error");
        }
      );
  }
 
  getUuaas() {
    this.rest.getRequest('projects/uuaa/dataTable/' + this.projectId + '/').subscribe(
      (data: any) => {
        this.uuaaOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  selectedUuaaOption(event){
    this.selectedUuaa = event;
  }

  selectedAmbito(event) {
    this.ambito = event;
  }

  onSelect({ selected }) {
    this.selectedTablones=[];
    this.selectedTablones.push(...selected);
  }

  validateTablones() {
    var resultIds = [];
    if (this.selectedTablones.length>0){
      for(var j = 0; j<this.selectedTablones.length; j++){
        if (resultIds.indexOf(this.selectedTablones[j]["_id"])) resultIds.push(this.selectedTablones[j]["_id"]);
      }
      this.dialogRef.close(resultIds);
    }else {
      this.dialogRef.close(null);
    }
  }
  
  clean() {
    this.masterName = "";
    this.alias = "";
    this.description = "";
    this.uuaa = "";
    this.selectedUuaa = "";
    this.selectorUuaaMaster.value = "";
    this.selectorAmbito.value = "";
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if(action=="Ok"){
        config.panelClass = ['sucessMessage'];
    }else{
        config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
