import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
  MatSnackBar,
  MatSnackBarConfig,
} from "@angular/material";
import {
  FormControl,
  FormBuilder,
  Validators,
  ValidatorFn,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { map, debounceTime, startWith } from "rxjs/operators";
import { RestService } from "../rest.service";
import { UuaaDetailComponent } from "./uuaa-detail/uuaa-detail.component";

import * as XLSX from "xlsx";

@Component({
  selector: "app-view-uuaas",
  templateUrl: "./view-uuaas.component.html",
  styleUrls: ["./view-uuaas.component.scss"],
})
export class ViewUuaasComponent implements OnInit {
  userRol: string;

  queryUuaasRaw = [];
  queryUuaasMaster = [];

  uuaa_master = ""
  system_name = "";
  data_source = "";
  description = "";
  level1 = "";
  level2 = "";
  country = "";

  selected_uuaa_name_Raw = "";
  selected_app_name = "";
  selected_level1_rawname = "";
  selected_level2_rawname = "";

  selected_origin_name = "";
  selected_scope_name = "";

  selected_uuaa_name_Master = "";

  selected_desc_Master = "";
  selected_descEng_Master = "";

  selected_datasource_name = "";
  selected_level1_name = "";
  selected_level2_name = "";

  selected_level3_name = " ";

  scopeOptions = ["", "Local", "Global"];
  level2Options = [];
  level3Options = [];
  appOptions = [];
  originOptions = [];

  expandIcon: string = "expand_more";

  countryOptions = [];
  deployCountryOptions = [];
  dataSourceOptions = [];
  level1MasterOptions = [];
  level2MasterOptions = [];

  dataSourceControl = new FormControl();
  countryControl = new FormControl();
  deployCountryControl = new FormControl();
  level1MasterControl = new FormControl();
  level2MasterControl = new FormControl();

  filteredDataSource: Observable<string[]>;
  filteredLevel1Master: Observable<string[]>;
  filteredLevel2Master: Observable<string[]>;

  @ViewChild("tableMaster") tableMaster: any;
  @ViewChild("tableRaw") tableRaw: any;

  constructor(
    public rest: RestService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.userRol = sessionStorage.getItem("rol");

    this.query_uuaas_masterNEW();
    this.getSearchOptions();


    this.getOptionsCountries();
    //this.getOptions_DataSource();
    this.getOptions_LevelsMaster();
    //this.query_uuaas_raw();
    //this.query_uuaas_master();
  }

  ngOnInit() {
    this.initAutoCompletes();
  }

  initAutoCompletes() {
    this.filteredDataSource = this.dataSourceControl.valueChanges.pipe(
      debounceTime(200),
      startWith(""),
      map((value) => this.filterDataSource(value))
    );
    this.filteredLevel1Master = this.level1MasterControl.valueChanges.pipe(
      debounceTime(200),
      startWith(""),
      map((value) => this.filterLevel1Master(value))
    );
    this.filteredLevel2Master = this.level2MasterControl.valueChanges.pipe(
      debounceTime(200),
      startWith(""),
      map((value) => this.filterLevel2Master(value))
    );
  }

  //Funcion que filtra las tablas de origen en el select a medida que se ingresen letras
  private filterDataSource(value: any): string[] {
    if (value !== null) {
      const filterValue = value.toLowerCase();
      return this.dataSourceOptions.filter((option) =>
        option.toLowerCase().includes(filterValue)
      );
    }
  }

  //Funcion que filtra las tablas de origen en el select a medida que se ingresen letras
  private filterLevel1Master(value: any): string[] {
    if (value !== null) {
      const filterValue = value.toLowerCase();
      return this.level1MasterOptions.filter((option) =>
        option.toLowerCase().includes(filterValue)
      );
    }
  }

  //Funcion que filtra las tablas de origen en el select a medida que se ingresen letras
  private filterLevel2Master(value: any): string[] {
    if (value !== null) {
      const filterValue = value.toLowerCase();
      return this.level2MasterOptions.filter((option) =>
        option.toLowerCase().includes(filterValue)
      );
    }
  }

  //Peticion al back
  getSearchOptions() {
    this.rest.getRequest("uuaa/master/search_props/").subscribe(
      (data: any) => {
        this.dataSourceOptions = data[0];
        this.countryOptions = data[1];
      });
  }

  getOptionsCountries() {
    this.rest
      .getRequest("uuaa/master/options/country/")
      .subscribe((data: any) => {
        this.countryOptions = data.filter(function (value: string) {
          return value.trim().length > 0;
        });
      });

    this.rest
      .getRequest("uuaa/master/options/deploy_country/")
      .subscribe((data: any) => {
        this.deployCountryOptions = data.filter(function (value: string) {
          return value.trim().length > 0;
        });
      });
  }

  //Peticion al back
  getOptions_DataSource() {
    this.rest
      .getRequest("uuaa/master/options/dataSource/")
      .subscribe((data: any) => {
        this.dataSourceOptions = data.filter(function (value: string) {
          return value.trim().length > 0;
        });
      });
  }

  //Peticion al back
  getOptions_LevelsMaster() {
    this.rest
      .getRequest("uuaa/master/options/level1/")
      .subscribe((data: any) => {
        this.level1MasterOptions = data.filter(function (value: string) {
          return value.trim().length > 0;
        });
      });
    this.rest
      .getRequest("uuaa/master/options/level2/")
      .subscribe((data: any) => {
        this.level2MasterOptions = data.filter(function (value: string) {
          return value.trim().length > 0;
        });
      });
  }

  //Peticion al back
  getOptions_Apps() {
    this.rest
      .getRequest("functionalMap/raw/appOptions/")
      .subscribe((data: any) => {
        this.appOptions = data;
        this.appOptions.push("");
      });
  }

  //Peticion al back
  getOptions_Origins() {
    this.rest
      .getRequest("functionalMap/raw/originOptions/")
      .subscribe((data: any) => {
        this.originOptions = data;
        this.originOptions.push("");
      });
  }

  //Peticion al back
  getOptions_Levels() {
    this.rest
      .getRequest("functionalMap/master/levelOptions/")
      .subscribe((data: any) => {
        this.level2Options = data["level2"];
        this.level3Options = data["level3"];
        this.level2Options.push("");
        this.level3Options.push("");
      });
  }

  toggleExpandRow(row, expanded) {
    if (row.functional_group_data_source.trim().length < 1)
      row.functional_group_data_source = "<< VACIO >>";
    this.tableMaster.rowDetail.toggleExpandRow(row);
    this.expandIcon = expanded ? "arrow_drop_down" : "arrow_drop_up";
  }

  toggleExpandRowRaw(row, expanded) {
    if (
      row.descriptionFunctional &&
      row.descriptionFunctional.trim().length < 1
    )
      row.descriptionFunctional = "<< VACIO >>";
    this.tableRaw.rowDetail.toggleExpandRow(row);
    this.expandIcon = expanded ? "arrow_drop_down" : "arrow_drop_up";
  }

  //Peticion al back
  query_uuaas_raw() {
    let query_uuaa_name = "%20";
    let query_app_name = "%20";
    let query_origin_name = "%20";

    if (this.selected_uuaa_name_Raw.length > 0)
      query_uuaa_name = this.selected_uuaa_name_Raw;
    if (this.selected_app_name.length > 0)
      query_app_name = this.selected_app_name;
    if (this.selected_origin_name.length > 0)
      query_origin_name = this.selected_origin_name;

    this.rest
      .getRequest(
        "functionalMap/raw/uuaa=" +
        query_uuaa_name +
        "&app=" +
        query_app_name +
        "&origin=" +
        query_origin_name +
        "/"
      )
      .subscribe(
        (data: any) => {
          this.queryUuaasRaw = data;
          this.openSnackBar("Búsqueda finalizada", "Ok");
        },
        (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
  }

  //Peticion al back
  query_uuaas_master() {
    let query_uuaa_name = "%20";
    let query_scope_name = "%20";
    let query_level2_name = "%20";
    let query_level3_name = "%20";

    if (this.selected_uuaa_name_Master.length > 0)
      query_uuaa_name = this.selected_uuaa_name_Master;
    if (this.selected_scope_name.length > 0)
      query_scope_name = this.selected_scope_name;
    if (this.selected_level2_name.length > 0)
      query_level2_name = this.selected_level2_name;
    if (this.selected_level3_name.length > 0)
      query_level3_name = this.selected_level3_name;

    this.rest
      .getRequest(
        "functionalMap/master/uuaa=" +
        query_uuaa_name +
        "&scope=" +
        query_scope_name +
        "&level2=" +
        query_level2_name +
        "&level3=" +
        query_level3_name +
        "/"
      )
      .subscribe(
        (data: any) => {
          this.queryUuaasMaster = data;
          this.openSnackBar("Búsqueda finalizada", "Ok");
        },
        (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
  }

  //Peticion al back
  query_uuaas_masterNEW() {
    let query_uuaa_name = this.uuaa_master.length > 0 ? this.uuaa_master : "%20";
    let query_system_name = this.system_name.length > 0 ? this.system_name : "%20";
    let query_data_source = this.data_source.length > 0 ? this.data_source : "%20";
    let query_desc = this.description.length > 0 ? this.description : "%20";
    let query_level1 = this.level1.length > 0 ? this.level1 : "%20";
    let query_level2 = this.level2.length > 0 ? this.level2 : "%20";
    let query_country = this.country.length > 0 ? this.country : "%20";

    this.rest.getRequest(
        "uuaa/master/uuaa=" + query_uuaa_name +
        "&system_name=" + query_system_name +
        "&data_source=" + query_data_source +
        "&desc=" + query_desc +
        "&level1=" + query_level1 +
        "&level2=" + query_level2 +
        "&country=" + query_country +
        "/"
      )
      .subscribe(
        (data: any) => {
          this.queryUuaasMaster = data;
          this.openSnackBar("Búsqueda finalizada", "Ok");
        });
  }

  //Peticion al back
  query_uuaas_rawNEW() {
    let query_uuaa_name = "%20";
    let query_app_name = "%20";
    let query_level1_rawname = "%20";
    let query_level2_rawname = "%20";

    if (this.selected_uuaa_name_Master.length > 0)
      query_uuaa_name = this.selected_uuaa_name_Master;
    if (this.selected_app_name.length > 0)
      query_app_name = this.selected_app_name;
    if (this.selected_level1_rawname.length > 0)
      query_level1_rawname = this.selected_level1_rawname;
    if (this.selected_level2_rawname.length > 0)
      query_level2_rawname = this.selected_level2_rawname;

    this.rest
      .getRequest(
        "uuaa/raw/uuaa=" +
        query_uuaa_name +
        "&app=" +
        query_app_name +
        "&level1=" +
        query_level1_rawname +
        "&level2=" +
        query_level2_rawname +
        "/"
      )
      .subscribe(
        (data: any) => {
          this.queryUuaasRaw = data;
          this.openSnackBar("Búsqueda finalizada", "Ok");
        },
        (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
  }

  clearQueryRaw() {
    this.selected_uuaa_name_Raw = " ";
    this.selected_app_name = "";
    this.selected_level1_rawname = "";
    this.selected_level2_rawname = "";
    this.query_uuaas_rawNEW();
  }

  clearQueryMaster() {
    this.uuaa_master = "";
    this.system_name = "";
    this.data_source = "";
    this.description = "";
    this.level1 = "";
    this.level2 = "";
    this.country = "";

    this.query_uuaas_masterNEW();
  }

  view_uuaaDetail(row: any, type: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "65%";
    dialogConfig.width = "1000px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      type: type,
      row: row,
    };
    this.dialog.open(UuaaDetailComponent, dialogConfig);
  }

  new_uuaa(type: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.maxHeight = "60%";
    dialogConfig.maxWidth = "65%";
    dialogConfig.width = "1000px";
    dialogConfig.disableClose = false;
    dialogConfig.data = { newType: type, type: "new", row: {} };
    this.dialog.open(UuaaDetailComponent, dialogConfig);
  }

  exportExcelMaster() {
    let arr = this.queryUuaasMaster;
    arr.forEach(function (uuaa) {
      delete uuaa["_id"];
      delete uuaa["short_name"];
    });

    this.exportExcel(arr);
  }

  exportReportTables() {
    this.rest.getRequest("uuaa/report_tables/%20/").subscribe(
      (data: any) => {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
        const workbook: XLSX.WorkBook = {
          Sheets: { Tables: worksheet },
          SheetNames: ["Tables"],
        };
        XLSX.writeFile(
          workbook, `${"REPORT_ua_tables"}_export_${new Date().getTime()}.xlsx`
        );
      });
  }

  exportExcel(jsonData: any[]) {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonData);
    const workbook: XLSX.WorkBook = {
      Sheets: { MASTER: worksheet },
      SheetNames: ["MASTER"],
    };
    XLSX.writeFile(
      workbook,
      `${"FunctionalMap_Nebula"}_export_${new Date().getTime()}.xlsx`
    );
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ["sucessMessage"];
    } else if (action == "Warning") {
      config.duration = 12000;
      config.panelClass = ["warnningMessage"];
    } else {
      config.panelClass = ["errorMessage"];
    }
    this.snackBar.open(message, action, config);
  }
}
