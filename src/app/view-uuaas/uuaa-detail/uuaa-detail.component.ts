import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RestService } from '../../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-uuaa-detail',
  templateUrl: './uuaa-detail.component.html',
  styleUrls: ['./uuaa-detail.component.scss']
})
export class UuaaDetailComponent implements OnInit {

  userId: string;
  userRol: string;
  uuaaData: any;

  flagType = "";
  newType = false;

  constructor(private dialogRef: MatDialogRef<UuaaDetailComponent>, @Inject(MAT_DIALOG_DATA) public datas: any, 
    public rest: RestService, public snackBar: MatSnackBar) {
      this.userRol = sessionStorage.getItem("rol");
      if (datas.type === "new"){
        this.flagType = datas["newType"];
        this.newType = true;
        this.new_uuaa(this.newType);
      } else {
        this.flagType = datas["type"];
        this.uuaaData = datas.row;
        this.get_uuaaInfo(datas.row);
      }
      console.log(this.flagType);
      this.userId = sessionStorage.getItem("userId");
    }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(null);
  }

  get_uuaaInfo(row: any) {
    this.rest.getRequest('uuaa/'+ this.flagType + '/' + row["_id"] + '/').subscribe(
      (data: any) => { this.uuaaData = data; });
  }

  update_uuaaRequest(){
    this.uuaaData["user_id"] = this.userId;
    this.rest.putRequest('uuaa/' + this.flagType + '/update/' + this.uuaaData["_id"] + '/', JSON.stringify(this.uuaaData)).subscribe(
      (data: any) => {
        this.uuaaData = data;
        this.openSnackBar("Campos actualizados", "Ok");
      },
      (error) => {
        this.openSnackBar(error.error.reason, "Error");
      }
    );
  }

  validateNewUUAA() {
    if (this.flagType == "master")
      if (this.uuaaData["uuaa"].trim().length < 3) {
        this.openSnackBar("La UUAA debe tener exactamente 3 caracteres", "Error");
        return false;
      }
    if (this.flagType == "raw")
      if (this.uuaaData["uuaa"].trim().length < 4) {
        this.openSnackBar("La UUAA debe tener exactamente 4 caracteres", "Error");
        return false;
      }

    this.uuaaData["uuaa"] = this.uuaaData["uuaa"].trim().toUpperCase();
    return true;
  }
  
  new_uuaaRequest() {
    if (this.validateNewUUAA()) {
      this.uuaaData["user_id"] = this.userId;
      this.rest.postRequest('uuaa/'+ this.flagType + '/new/', JSON.stringify(this.uuaaData)).subscribe(
        (data: any) => {
          this.uuaaData = data;
          this.openSnackBar("UUAA Creada", "Ok");
          this.closeDialog();
        },
        (error) => {
          this.openSnackBar(error.error.reason, "Error");
        }
      );
    }
  }

  new_uuaa(newType){
    if (this.flagType === "raw"){
      let newUa = {
        uuaa: "",
        level1: "",
        level2: "",
        level3: "",
        nameSimple: "",
        descriptionApp: "",
        descriptionFunctional: "",
        geography: "",
        area: "",
        originCountry: "",
        example: "",
        route: "",
        tecnicalResponsible: "datahub.co.group@bbva.com",
        modifications: []
      };
      this.uuaaData = newUa;
    } else if (this.flagType === "master"){
      let newUa = {
        uuaa: "",
        level1: "",
        level2: "",
        example: "",
        dataSource: "",
        functionalGroupDataSource: "",
        systemDescription: "",
        systemDescriptionEng: "",
        systemName: "",
        route: "",
        tecnicalResponsible: "datahub.co.group@bbva.com",
        modifications: []
      };
      this.uuaaData = newUa;
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
