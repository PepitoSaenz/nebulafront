import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UuaaDetailComponent } from './uuaa-detail.component';

describe('UuaaDetailComponent', () => {
  let component: UuaaDetailComponent;
  let fixture: ComponentFixture<UuaaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UuaaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UuaaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
