import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUuaasComponent } from './view-uuaas.component';

describe('ViewUuaasComponent', () => {
  let component: ViewUuaasComponent;
  let fixture: ComponentFixture<ViewUuaasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUuaasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUuaasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
