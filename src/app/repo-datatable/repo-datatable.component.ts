
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-repo-datatable',
  templateUrl: './repo-datatable.component.html',
  styleUrls: ['./repo-datatable.component.scss']
})
export class RepoDatatableComponent implements OnInit {

  searchType: string = "";
  mainTitle: string = "Buscador";
  firstTime: boolean = true;
  data: any;
  dataTables: any = [];

  masterName: string = "";
  alias: string = "";
  baseName: string = "";
  logic: string = "";
  observationField: string = "";
  selectedUuaaRaw: string = "";
  selectedUuaaMaster = null;

  selectedTables: any = [];
  uuaaOptions: any = [];

  loadingFlag: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public datas: any, public rest: RestService, public snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<RepoDatatableComponent>) {
    this.data = datas;
    this.searchType = datas.searchType;
    this.mainTitle = this.searchType === "DT"
      ? "Buscador de tablones de origen" : "Buscador de tablas de origen";
    this.getUuaas();
    this.getTablesSearch();
  }

  ngOnInit() {
  }

  getUuaas() {
    this.rest.getRequest("uuaa/master/allfull/").subscribe((data: any) => {
      this.uuaaOptions = data;
    });
  }

  selectedUuaaMasterOption(event) {
    this.selectedUuaaMaster = event;
  }

  onSelect({ selected }) {
    this.selectedTables = [];
    this.selectedTables.push(...selected);
  }

  clean() {
    this.masterName = "";
    this.alias = "";
    this.baseName = "";
    this.logic = "";
    this.observationField = "";
    this.selectedUuaaRaw = ""
    this.selectedUuaaMaster = null;
    this.getTablesSearch();
  }

  //Devuelve el nombre sin el pedazo de t_uuaa para las tablas
  shortName(longName) {
    return longName.slice(7, longName.length);
  }

  getTablesSearch() {
    this.loadingFlag = true;

    var aliasTmp = this.alias.length > 0 ? this.alias : "%20";
    var baseNameTmp = this.baseName.length > 0 ? this.baseName : "%20";
    var logicTmp = this.logic.length != 0 ? this.logic : "%20";
    var uuaaMasterTmp = this.selectedUuaaMaster != null ? this.selectedUuaaMaster["full_name"] : "%20";

    if (this.searchType === "T") {
      var uuaaRawTmp = this.selectedUuaaRaw.length != 0 ? this.selectedUuaaRaw : "%20";
      this.tablesSearchRequest(aliasTmp, baseNameTmp, logicTmp, uuaaRawTmp, uuaaMasterTmp);
    } else if (this.searchType === "DT")
      this.dataTablesSearchRequest(aliasTmp, baseNameTmp, logicTmp, uuaaMasterTmp);
  }

  tablesSearchRequest(aliasTmp: string, baseNameTmp: string, logicTmp: string, uuaaRawTmp: string, uuaaMasterTmp: string) {
    this.rest.getRequest("dataTables/tables/origin/alias=" + aliasTmp + "&datio=" + baseNameTmp + "&uuaa_raw=" + uuaaRawTmp + "&uuaa_master=" + uuaaMasterTmp + "&logic=" + logicTmp + "/").subscribe(
      (data: any) => {
        this.dataTables = data;
        if (!this.firstTime)
          this.openSnackBar("Busqueda finalizada", "Ok");
        this.loadingFlag = false;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
        this.loadingFlag = false;
      }
    );
  }

  dataTablesSearchRequest(aliasTmp: string, baseNameTmp: string, logicTmp: string, uuaaMasterTmp: string) {
    this.rest.getRequest(`dataTables/tablones/origin/alias=${aliasTmp}&datio=${baseNameTmp}&uuaa=${uuaaMasterTmp}&logic=${logicTmp}/`).subscribe(
      (data: any) => {
        this.dataTables = data;
        if (!this.firstTime)
          this.openSnackBar("Busqueda finalizada", "Ok");
        this.loadingFlag = false;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
        this.loadingFlag = false;
      }
    );
  }


  validateTables() {
    var resultIds = [];
    if (this.selectedTables.length > 0) {
      for (var j = 0; j < this.selectedTables.length; j++) {
        resultIds.push(this.selectedTables[j]["_id"]);
      }
      var res = {
        tableIds: resultIds,
        tables: this.selectedTables
      }
      this.dialogRef.close(res);
    } else {
      this.dialogRef.close(null);
    }
  }

  sendClosedResponse(response: any) {
    this.dialogRef.close(response);
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
