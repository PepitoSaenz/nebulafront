import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepoDatatableComponent } from './repo-datatable.component';

describe('RepoDatatableComponent', () => {
  let component: RepoDatatableComponent;
  let fixture: ComponentFixture<RepoDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepoDatatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepoDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
