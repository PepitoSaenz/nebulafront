import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestUsecaseComponent } from './request-usecase.component';

describe('RequestUsecaseComponent', () => {
  let component: RequestUsecaseComponent;
  let fixture: ComponentFixture<RequestUsecaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestUsecaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestUsecaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
