import {FormControl, Validators} from '@angular/forms';
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material';
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-request-usecase',
  templateUrl: './request-usecase.component.html',
  styleUrls: ['./request-usecase.component.scss']
})

export class RequestUsecaseComponent implements OnInit {

  requiredFormControlName = new FormControl('', [Validators.required]);
  requiredFormControlDesc = new FormControl('', [Validators.required]);
  dateFormControlStart = new FormControl(new Date(),[Validators.required]);
  dateFormControlEnd = new FormControl(new Date(),[Validators.required]);
  requiredFormControlComment = new FormControl('', [Validators.required, Validators.minLength(2)]);

  rol: string;
  formatStartDate: Date;
  formatFinishDate: Date;
  messagesError = [];
  showDivError = false;
  messageWarning ="Solicitud realizada el día ";
  oldUseCaseFlag = false;
  oldUseCase = {};
  use_case: any;
  TMPuse_case = {
    use_case: { 
                name: "",
                description: "",
                startDate: "",
                finishDate: "",
                modifications:{
                  user_name:"",
                  date:""
                }
              },
    created_time: ""
  };
  viewOnlyFlag = false;
  initialCheck = false;
  closedFlag = false;
  answerFlag = false;
  modifyFlag = false;
  titleType = "";
  answerResult = "";
  
  viewAuditFlag = false;
  
  @ViewChild('picker1') startDate: any;

  filterWeekends = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }

  constructor(private dialogRef: MatDialogRef<RequestUsecaseComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any, public rest:RestService, public snackBar: MatSnackBar) {
    this.use_case = data;
    this.rol = sessionStorage.getItem("rol");
    this.initDialogState(data);
    if(data.hasOwnProperty("oldUseCase")){
      this.oldUseCaseFlag = true;
      this.oldUseCase = data.oldUseCase;
    }
    if(data.hasOwnProperty("viewOnly")){
      this.viewOnlyFlag = true;
      this.disableAllFields();
    }
    if(this.use_case.use_case.created_time != "" && 
        this.use_case.use_case.modifications.user_name != "" && 
        this.use_case.use_case.modifications.date != ""){
      this.viewAuditFlag = true;
    }
    this.formatStartDate = new Date(this.use_case.use_case.startDate);
    this.formatStartDate.setDate(this.formatStartDate.getDate() + 1); 
    this.formatFinishDate = new Date(this.use_case.use_case.finishDate);
    this.formatFinishDate.setDate(this.formatFinishDate.getDate() + 1); 

    //TO DELETE
    if (!this.use_case.hasOwnProperty("comment")) this.use_case["comment"] = "";
    if (!this.use_case.hasOwnProperty("spo")) this.use_case["spo"] = "pepe@nada.com";
  }

  ngOnInit() {}

  initDialogState(datas: any) {
    if (datas.hasOwnProperty("requestStatus")) {
      if (datas.requestStatus === "A" || datas.requestStatus === "D") {
        this.answerFlag = true;
        this.disableAllFields();
        this.requiredFormControlComment.disable();
        if (datas.requestStatus === "A") {
          this.answerResult = "Aceptada";
          this.titleType = "Aceptada - Tabla: " ;
        } else {
          this.answerResult = "Rechazada";
          this.titleType = "Rechazada - Tabla: " ;
        }
      } else {
        if (this.rol === "PO") {
          this.closedFlag = false;
          this.modifyFlag = true;
        } else
          this.closedFlag = true;
        this.titleType = (datas.action === "Add" ? "Agregar Tabla: " :
          (datas.action === "Del" ? "Eliminar Tabla: " : "Modificar Tabla: "));
      }
    } else {
      this.titleType = (datas.action === "backlog" ? "Modificar: " + "this.backlog.baseName" : "Nueva Tabla");
    }
  }
  
  disableAllFields(){
    this.requiredFormControlName.disable();
    this.requiredFormControlDesc.disable();
    this.dateFormControlStart.disable();
    this.dateFormControlEnd.disable();
  }

  dateChanger(event: any, type: number){
    if(type === 0){
      this.use_case.use_case.startDate = new Date(this.formatStartDate.toString()).toISOString().slice(0,10);
    } else {
      this.use_case.use_case.finishDate = new Date(this.formatFinishDate.toString()).toISOString().slice(0,10);
    }
  }

  validateUseCase(){
    if (this.updateUseCase())
      this.sendClosedResponse(this.use_case);
  }

  approveSol() {
    if (this.updateUseCase()) {
      if (this.use_case.comment.length === 0) this.use_case.comment = "Ok";
      let response = {
        solResult: "approve",
        dialogTempData: this.use_case
      }
      this.sendClosedResponse(response);
    }
  }

  rejectSol() {
    if (this.requiredFormControlComment.hasError('required') && this.rol !== "PO") {
      this.openSnackBar("El comentario es obligatorio cuando se rechaza una solicitud", "Error");
    } else {
      let response = {
        solResult: "discard",
        dialogTempData: this.use_case
      }
      this.sendClosedResponse(response);
    }
  }

  updateUseCase(){
    this.messagesError=[];
    this.showDivError=false;
    if(this.use_case.use_case.name.length>0 && this.use_case.use_case.description.length>0 && 
    this.use_case.use_case.finishDate!="" && this.use_case.use_case.startDate !=""){
        this.messagesError=[];
        this.showDivError=false;
        // Give the format yyyy-mm-dd
        this.use_case.use_case.startDate=new Date(this.use_case.use_case.startDate.toString()).toISOString().slice(0,10);
        this.use_case.use_case.finishDate=new Date(this.use_case.use_case.finishDate.toString()).toISOString().slice(0,10);
        this.use_case.use_case.name=this.use_case.use_case.name.toUpperCase();
        return true;
    }else{
      this.messagesError.push("Por favor, verifique los campos requeridos (*).");
      this.showDivError=true;
      return false;
    }
  } 

  sendClosedResponse(response: any){
    this.dialogRef.close(response);
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}