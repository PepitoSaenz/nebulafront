import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTabChangeEvent, MatDialog, MatDialogConfig } from '@angular/material';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { RestService } from '../rest.service';
import { SearchTableComponent } from './../search-table/search-table.component';
import { RequestTableComponent } from './../request-table/request-table.component';
import { RequestTablonComponent } from './../request-tablon/request-tablon.component';
import { RequestUsecaseComponent } from './../request-usecase/request-usecase.component';
import { RepoDatatableComponent } from './../repo-datatable/repo-datatable.component';
import { StatisticsDialogComponent } from './../statistics-dialog/statistics-dialog.component';

@Component({
  selector: 'app-backlog',
  templateUrl: './backlog.component.html',
  styleUrls: ['./backlog.component.scss']
})

export class BacklogComponent implements OnInit {

  viewOnlyFlag = false;

  index = 0;
  rol = "";
  userId: string;
  userMail: string;
  toggleUseCase: any;
  loadingFlag= false;

  lastToggleUsecase = { _id: undefined };
  usecasesProject = [];
  backlogsUseCaseProject = [];
  dataTablesUseCaseProject: any = [];

  requestsTables = [];
  requestsUseCases = [];
  requestsUseCasesTmp = [];

  searchUseCases = "";

  projectOptions = [];
  projectFilterBacklogOptions = [];

  selectedFilterTableProject = "";
  selectedFilterTableState = "";
  filterTableUseCaseName = "";
  filterTableName = "";
  useCaseId = "";
  showSelectedProject = false;
  flagQuery = false;
  flagUnassigned = false;

  projectFilterDataTablesOptions = [];
  selectedFilterDataTableProject = "";
  filterDataTableUseCaseName = "";
  filterDataTableName = "";
  selectedDataTableFilterState = "";
  requestsDataTables = [];

  //Functional Variables
  voboList: any[] = [] 
  editableProject: boolean = false;

  @ViewChild('selectRequestTableProject') selectorRequestTableProject: any;
  @ViewChild('selectRequestDataTableProject') selectorRequestDataTableProject: any;
  @ViewChild('selectRequestTableAction') selectorRequestTableAction: any;
  @ViewChild('selectRequestDataTableAction') selectorRequestDataTableAction: any;
  @ViewChild('selectStateInsideUseCase') selectorStateInsideUseCase: any;
  @ViewChild('selectTable') selectorStateTableInsideUseCase: any;
  @ViewChild('useCasesProject') usecasesProjectTable: any;
  selectedProject={ _id: undefined, name: "", shortDesc: ""};
  @ViewChild('useCases') useCasesTable: any;

  @ViewChild('insideDataTableStateTable') insideDataTableStateTable: any;
  @ViewChild('insideDataTableStatus') insideDataTableStatus: any;

  
  filterQueryUseCaseName = "";
  filterQueryBacklogName = "";
  filterQueryDataTableName = "";

  filterInsideUseCaseSelectedState = "";
  filterInsideUseCaseSelectedStateTable = "";
  filterInsideUseCaseTable = "";

  filterInDataTableStateTable = "";
  filterInDataTableStatus = "";
  filterInDataTableName = "";

  //Nuevas variables para filtro interno
  filterInsideName: string = "";
  filterInsideState: string = "";
  filterInsideRequest: string = "";

  //Variables de apariencia y/o funcionamiento
  loadingBarInside: boolean = false;

  ticketsUseCases = [];
  ticketsUseCasesTmp =[];
  ticketsBacklogs = [];
  ticketsDatatable = [];
  searchUseCasesTickets = "";
  @ViewChild('ticketsUseCasesTable') useCasesTicketsTable: any;

  filterTicketUseCaseName = "";
  filterTicketTableName = "";
  selectedFilterTicketState = "";
  selectedFilterTicketProject = "";
  selectedFilterTicketStatusReq = "";
  projectFilterTicketsOptions = [];
  @ViewChild('selectTicktesTableProject') selectorTicketsTableProject: any;
  @ViewChild('selectTicketsTableAction') selectorTicketsTableAction: any;
  @ViewChild('selectTicketsReq') selectorTicketsStatusReq: any;

  selectedProjectTicketsDataTable = "";
  filterTicketDatatableUseCaseName = "";
  filterTicketDataTableName = "";
  @ViewChild('selectorDatatableTickets') selectorTicketsDataTableProject: any;
  @ViewChild('selectTicketsDataTableAction') selectorTicketsDataTableAction: any;
  filterTicketDataTableAction = "";
  @ViewChild('selectTicketsDataTableReq') selectorTicketsDataTableReq: any;
  filterTicketDataTableReq = "";
  viewAuditFields = false;

  constructor(public rest: RestService,
    private router: Router, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.flagQuery = false;
    this.rol = sessionStorage.getItem("rol");
    this.userId = sessionStorage.getItem("userId");
    if(this.rol != 'PO' && this.rol != 'SPO' && this.userId != '5c350591698b6a8b84532e63'){
      this.viewOnlyFlag = true;
      //this.openSnackBar("Ventana no permitida en el rol actual", "Error");
      //this.rest.sendHome(this.rol);
    }
    this.getProjectOptions();
    this.getProjectOptionsFitlerBacklogs();
    this.getProjectOptionsFitlerDataTables();
    this.getProjectOptionsTicketsBacklogs();
    this.getAllBacklogsTickets();

    this.getUserVobos();
  }

  ngOnInit() { }

  getUserVobos() {
    this.rest.getRequest(`users/${this.userId}/vobos/`).subscribe(
      (data: any) => {
        this.voboList = data;
      });
  }

  //Comprueba si el usuario actual puede realizar solicitudes 
  checkUseableProject(row: any) {
    let result = this.voboList.find(filter => filter['_id'] === row._id);
    return result != undefined;
  }

  getProjectOptions() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      },
      (error) => {
        this.projectOptions = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getProjectOptionsFitlerBacklogs() {
    this.rest.getRequest('projects/requests/options/').subscribe(
      (data: any) => {
        this.projectFilterBacklogOptions = data;
      },
      (error) => {
        this.projectFilterBacklogOptions = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  } 

  getProjectOptionsFitlerDataTables() {
    this.rest.getRequest('projects/requests/dataTables/options/').subscribe(
      (data: any) => {
        this.projectFilterDataTablesOptions = data;
      },
      (error) => {
        this.projectFilterDataTablesOptions = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getProjectOptionsTicketsBacklogs() {
    this.rest.getRequest('projects/tickets/options/').subscribe(
      (data: any) => {
        this.projectFilterTicketsOptions = data;
      },
      (error) => {
        this.projectFilterTicketsOptions = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getAllBacklogs() {
    this.rest.getRequest('projects/requests/backlogs/').subscribe(
      (data: any) => {
        this.requestsTables = data;
      }, (error) => {
        this.requestsTables = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getAllDataTables() {
    this.rest.getRequest('projects/requests/dataTables/').subscribe(
      (data: any) => {
        this.requestsDataTables = data;
      }, (error) => {
        this.requestsDataTables = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getAllUseCases() {
    this.rest.getRequest('projects/requests/useCases/').subscribe(
      (data: any) => {
        this.requestsUseCases = data;
        this.requestsUseCasesTmp = data;
      }, (error) => {
        this.requestsUseCases = [];
        this.requestsUseCasesTmp = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getAllUseCasesTickets(){
    this.rest.getRequest('projects/tickets/useCases/').subscribe(
      (data: any) => {
        this.ticketsUseCases = data;
        this.ticketsUseCasesTmp = data;
      }, (error) => {
        this.ticketsUseCases = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getAllBacklogsTickets(){
    this.rest.getRequest('projects/tickets/backlogs/').subscribe(
      (data: any) => {
        this.ticketsBacklogs = data;
      }, (error) => {
        this.ticketsBacklogs = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getAllDatatableTickets(){
    this.rest.getRequest('projects/tickets/dataTables/').subscribe(
      (data: any) => {
        this.ticketsDatatable = data;
      }, (error) => {
        this.ticketsDatatable = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getFilterTablesRequests() {
    this.loadingFlag = true;
    var useCaseNameTmp = "%20"; var tableNameTmp = "%20";
    var projectTmp = "%20"; var actionTmp = "%20";
    var flag = false;
    if (this.filterTableName.length > 0) { tableNameTmp = this.filterTableName; flag = true; }
    if (this.filterTableUseCaseName.length > 0) { useCaseNameTmp = this.filterTableUseCaseName; flag = true; }
    if (this.selectedFilterTableProject.length > 0) { projectTmp = this.selectedFilterTableProject; flag = true; }
    if (this.selectedFilterTableState.length > 0) { actionTmp = this.selectedFilterTableState; flag = true; }
    if (flag) {
      this.rest.getRequest('projects/requests/project=' + projectTmp + '&usecase=' + useCaseNameTmp + '&backlog=' + tableNameTmp + '&action=' + actionTmp + '/').subscribe(
        (data: any) => {
          this.requestsTables = data;
          this.loadingFlag = false;
          this.openSnackBar("Búsqueda finalizada", "Ok");
        },
        (error) => {
          this.requestsTables = [];
          this.openSnackBar("Error procesando petición al servidor.", "Error");
          this.loadingFlag = false;
        }
      );
    } else {
      this.openSnackBar("Por favor, diligenciar los campos de búsqueda.", "Error");
      this.loadingFlag = false;
    }
  }

  getFilterTablesTickets() {
    var useCaseNameTmp = "%20"; var tableNameTmp = "%20";
    var projectTmp = "%20"; var actionTmp = "%20";
    var statusReqTmp = "%20";
    var flag = false;
    if (this.filterTicketTableName.length > 0) { tableNameTmp = this.filterTicketTableName; flag = true; }
    if (this.filterTicketUseCaseName.length > 0) { useCaseNameTmp = this.filterTicketUseCaseName; flag = true; }
    if (this.selectedFilterTicketProject.length > 0) { projectTmp = this.selectedFilterTicketProject; flag = true; }
    if (this.selectedFilterTicketState.length > 0) { actionTmp = this.selectedFilterTicketState; flag = true; }
    if (this.selectedFilterTicketStatusReq.length > 0) { statusReqTmp = this.selectedFilterTicketStatusReq; flag = true; }
    if (flag) {
      this.rest.getRequest('projects/tickets/project=' + projectTmp + '&usecase=' + useCaseNameTmp + '&backlog=' + tableNameTmp + '&action=' + actionTmp + '&status='+statusReqTmp+'/').subscribe(
        (data: any) => {
          this.ticketsBacklogs = data;
        },
        (error) => {
          this.ticketsBacklogs = [];
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    } else {
      this.openSnackBar("Por favor, diligenciar los campos de búsqueda.", "Error");
    }
  }

  getFilterDataTablesRequests(){
    var useCaseNameTmp = "%20"; var tableNameTmp = "%20";
    var projectTmp = "%20"; var actionTmp = "%20";
    var flag = false;
    if (this.filterDataTableName.length > 0) { tableNameTmp = this.filterDataTableName; flag = true; }
    if (this.filterDataTableUseCaseName.length > 0) { useCaseNameTmp = this.filterDataTableUseCaseName; flag = true; }
    if (this.selectedFilterDataTableProject.length > 0) { projectTmp = this.selectedFilterDataTableProject; flag = true; }
    if (this.selectedDataTableFilterState.length > 0) { actionTmp = this.selectedDataTableFilterState; flag = true; }
    if (flag) {
      this.rest.getRequest('projects/dataTables/requests/project='+projectTmp+'&usecase='+useCaseNameTmp+'&dataTable='+tableNameTmp+'&action='+actionTmp+'/').subscribe(
        (data: any) => {
          this.requestsDataTables=data;
        },
        (error) => {
          this.requestsDataTables=[]
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    } else {
      this.openSnackBar("Por favor, diligenciar los campos de búsqueda.", "Error");
    }
  }

  getFilterDataTablesTickets(){
    var useCaseNameTmp = "%20"; var tableNameTmp = "%20";
    var projectTmp = "%20"; var actionTmp = "%20"; var reqTmp = "%20";

    var flag = false;
    if (this.filterTicketDataTableName.length > 0) { tableNameTmp = this.filterTicketDataTableName; flag = true; }
    if (this.filterTicketDatatableUseCaseName.length > 0) { useCaseNameTmp = this.filterTicketDatatableUseCaseName; flag = true; }
    if (this.selectedProjectTicketsDataTable.length > 0) { projectTmp = this.selectedProjectTicketsDataTable; flag = true; }
    if (this.filterTicketDataTableAction.length > 0) { actionTmp = this.filterTicketDataTableAction; flag = true; }
    if (this.filterTicketDataTableReq.length > 0) { reqTmp = this.filterTicketDataTableReq; flag = true; }
    if (flag) {
      this.rest.getRequest('projects/dataTables/tickets/project='+projectTmp+'&usecase='+useCaseNameTmp+'&dataTable='+tableNameTmp+'&action='+actionTmp+'&status='+reqTmp+'/').subscribe(
        (data: any) => {
          this.ticketsDatatable = data;
        },
        (error) => {
          this.ticketsDatatable = []
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    }else{
      this.openSnackBar("Por favor, diligenciar los campos de búsqueda.", "Error");
    }
  }

  clean() {
    this.filterTableUseCaseName = "";
    this.filterTableName = "";
    this.selectedFilterTableState = "";
    this.selectedFilterTableProject = "";
    this.selectorRequestTableProject.value = "";
    this.selectorRequestTableAction.value = "";
    this.getAllBacklogs();
  }

  cleanDataTable() {
    this.selectedFilterDataTableProject = "";
    this.filterDataTableUseCaseName = "";
    this.filterDataTableName = "";
    this.selectorRequestDataTableProject.value = "";
    this.selectorRequestDataTableAction.value = "";
    this.selectedDataTableFilterState="";
    this.getAllDataTables();
  }

  cleanTickets() {
    this.filterTicketUseCaseName = "";
    this.filterTicketTableName = "";
    this.selectedFilterTicketState = "";
    this.selectedFilterTicketProject = "";
    this.selectedFilterTicketStatusReq="";
    this.selectorTicketsTableProject.value = "";
    this.selectorTicketsTableAction.value = "";
    this.selectorTicketsStatusReq.value = "";
    this.getAllBacklogsTickets();
  }

  cleanTicketsDataTables() {
    this.selectedProjectTicketsDataTable = "";
    this.selectorTicketsDataTableProject.value = "";
    this.filterTicketDatatableUseCaseName = "";
    this.filterTicketDataTableName = "";
    this.selectorTicketsDataTableAction.value = "";
    this.filterTicketDataTableAction = "";
    this.filterTicketDataTableReq = "";
    this.selectorTicketsDataTableReq.value = "";
    this.getAllDatatableTickets();
  }

  updateFilterUseCase(event) {
    this.requestsUseCases = this.requestsUseCasesTmp;
    this.searchUseCases = event.target.value.toLowerCase();
    const val = event.target.value.toLowerCase();
    const temp = this.requestsUseCases.filter(function (d) {
      return d.use_case.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.requestsUseCases = temp;
    this.useCasesTable.offset = 0;
  }

  updateFilterUseCaseTickets(event) {
    this.ticketsUseCases = this.ticketsUseCasesTmp;
    this.searchUseCasesTickets = event.target.value.toLowerCase();
    const val = event.target.value.toLowerCase();
    const temp = this.ticketsUseCases.filter(function (d) {
      return d.use_case.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.ticketsUseCases = temp;
    this.useCasesTicketsTable.offset = 0;
  }

  cleanFilterUseCase() {
    this.searchUseCases = "";
    this.getAllUseCases();
    this.useCasesTable.offset = 0;
  }

  cleanFilterUseCaseTickets() {
    this.searchUseCasesTickets = "";
    this.getAllUseCasesTickets();
    this.useCasesTicketsTable.offset = 0;
  }

  editRequestUseCase(row) {
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = row;
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "770px";
    dialogConfigRequest.minHeight = "410px";
    const dialogRef = this.dialog.open(RequestUsecaseComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          if (result.hasOwnProperty("dialogTempData")){
            if(result.solResult === "approve"){
              this.updateUseCase(result.dialogTempData);
              this.approvedRequestUseCase(result.dialogTempData);
            } else 
              this.deletedRequestUseCase(result.dialogTempData);
          } else{
            result.user = this.userId,
            this.updateUseCase(result);
          }
        }
      });
  }

  searchTables(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "80%";
    dialogConfig.minHeight = "43rem";
    dialogConfig.width = "70rem";
    dialogConfig.data = { searchType: "T" , use_case: row, project_id: this.selectedProject._id };
    const dialogRef = this.dialog.open(SearchTableComponent, dialogConfig);
    dialogRef.afterClosed().subscribe( result => {
      if (result != null)
        this.updateUseCaseTables(result.tableIds, row);
    });
  }

  searchDataTables(row){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.maxWidth = "80%";
    dialogConfig.maxHeight = "90%";
    dialogConfig.data = { idProject: this.selectedProject._id }
    const dialogRef = this.dialog.open(RepoDatatableComponent, dialogConfig);
  }

  updateTablesUseCaseRow(row) {
    var tmp = []
    for (let i = 0; i < this.backlogsUseCaseProject.length; i++) {
      tmp.push(this.backlogsUseCaseProject[i]._id);
    }
    row.tables = tmp;
  }

  editUseCaseProject(row) {
    var use_case = {
      user: this.userId,
      idProject: this.selectedProject._id,
      use_case: row,
      action: "head",
      created_time: new Date().toISOString().split("T")[0]
    };
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = (row.hasOwnProperty("use_case") ? row : use_case);
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "770px";
    dialogConfigRequest.minHeight = "380px";
    const dialogRef = this.dialog.open(RequestUsecaseComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          if (result.hasOwnProperty("dialogTempData")){
            if(result.solResult === "approve"){
              this.updateBacklog(result.dialogTempData);
              this.approvedRequestUseCase(result.dialogTempData);
            } else 
              this.deletedRequestUseCase(result.dialogTempData);
          } else{
            result.user = this.userId,
            this.updateUseCase(result);
          }
        }
      });
  }

  checkUseCaseProject(row: any){
    var use_case = {
      viewOnly: true,
      user: this.userId,
      idProject: this.selectedProject._id,
      use_case: row,
      action: "head",
      created_time: new Date().toISOString().split("T")[0]
    };
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = (row.hasOwnProperty("use_case") ? row : use_case);
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "770px";
    dialogConfigRequest.minHeight = "380px";
    const dialogRef = this.dialog.open(RequestUsecaseComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
      });
  }

  updateUseCase(row) {
    var useCasetmp = {
      user: row.user,
      idProject: row.idProject,
      action: "head",
      use_case_id: row.use_case._id,
      use_case_body: {
        name: row.use_case.name,
        description: row.use_case.description,
        startDate: row.use_case.startDate,
        finishDate: row.use_case.finishDate
      }
    }
    //POST: para actualizar caso de uso existente.
    this.rest.postRequest('projects/requests/useCases/', JSON.stringify(useCasetmp)).subscribe(
      (data: any) => {
        this.openSnackBar("Caso de uso actualizado exitosamente.", "Ok");
      }, (error) => {
        this.updateTabBacklog();
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  updateUseCaseTables(tables, row) {
    var useCasetmp = {
      user: this.userId,
      idProject: this.selectedProject._id,
      action: "tables",
      use_case_id: this.useCaseId,
      old_tables: tables,
      new_tables: []
    }
    //POST: para actualizar caso de uso existente.
    this.rest.postRequest('projects/requests/useCases/', JSON.stringify(useCasetmp)).subscribe(
      (data: any) => {
        this.clean();
        this.backlogsUseCaseProject = data;
        this.openSnackBar("Caso de uso actualizado exitosamente.", "Ok");
        this.updateTablesUseCaseRow(row);
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  requestNewUseCase() {
    var use_case = {
      user: this.userId,
      idProject: this.selectedProject._id,
      use_case: {
        name: "", description: "",
        startDate: "", finishDate: "",
        modifications:{
          user_name:"",
          date:""
        },
        created_time: ""
      },
      action: "new_use_case",
      created_time: new Date().toISOString().split("T")[0]
    }
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.width = "55%";
    dialogConfigRequest.minWidth = "770px";
    dialogConfigRequest.minHeight = "380px";
    dialogConfigRequest.data = use_case;
    const dialogRef = this.dialog.open(RequestUsecaseComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          use_case.use_case = result.use_case;
          this.createNewUseCase(use_case);
        }
      });
  }

  createNewUseCase(row) {
    var useCasetmp = {
      user: row.user,
      idProject: row.idProject,
      action: "new_use_case",
      use_case_body: {
        name: row.use_case.name,
        description: row.use_case.description,
        startDate: row.use_case.startDate,
        finishDate: row.use_case.finishDate,
        tables: []
      },
      old_tables: [],
      new_tables: []
    }
    //PUT: Para crear la solicitud de un nuevo caso de uso.
    this.rest.putRequest('projects/requests/useCases/', JSON.stringify(useCasetmp)).subscribe(
      (data: any) => {
        this.usecasesProject.push({
          _id: data,
          name: row.use_case.name,
          description: row.use_case.description,
          startDate: row.use_case.startDate,
          finishDate: row.use_case.finishDate,
          request_date: new Date().toISOString().split("T")[0],
          tables: []
        });
        this.getProjectUseCases(row.idProject);
        this.cleanFilterUseCase();
        this.openSnackBar("Caso de uso actualizado exitosamente.", "Ok");
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  requestNewBacklog(row) {
    var backlogrequest = {
      user: this.userId,
      idProject: this.selectedProject._id,
      backlog: {
        baseName: "",
        originSystem: "",
        periodicity: "",
        history: 0,
        observationField: "",
        typeFile: "",
        uuaaRaw: "",
        uuaaMaster: "",
        tacticalObject: "",
        perimeter: "",
        modifications: {
          user_name:"",
          date:""
        },
        created_time: ""
      },
      created_time: new Date().toISOString().split("T")[0]
    }
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = backlogrequest;
    dialogConfigRequest.minWidth = "1100px";
    dialogConfigRequest.minHeight = "380px";
    const dialogRef = this.dialog.open(RequestTableComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          this.createNewTable(result, row);
        }
      });
  }

  createNewTable(result, row) {
    var backlogtmp = {
      user: this.userId,
      idProject: this.selectedProject._id,
      action: "tables",
      use_case_id: this.useCaseId,
      old_tables: [],
      new_tables: [result.backlog]
    }
    //POST: para actualizar caso de uso existente.
    this.rest.postRequest('projects/requests/useCases/', JSON.stringify(backlogtmp)).subscribe(
      (data: any) => {
        this.backlogsUseCaseProject = data;
        this.clean();
        this.openSnackBar("Caso de uso actualizado exitosamente.", "Ok");
        this.updateTablesUseCaseRow(row);
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      });
  }

  editRequestBacklog(row) {
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.minWidth = "1100px";
    dialogConfigRequest.minHeight = "380px";
    dialogConfigRequest.data = row;
    const dialogRef = this.dialog.open(RequestTableComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          if (result.hasOwnProperty("dialogTempData")){
            if(result.solResult === "approve"){
              this.updateBacklog(result.dialogTempData);
              this.approvedRequestBacklog(result.dialogTempData);
            } else 
              this.deletedRequestBacklog(result.dialogTempData);
          } else
            this.updateBacklog(result);
        }
      });
  }

  editBacklogUseCase(row) {
    if (row.stateTable == "N" || row.stateTable == "") {
      var edit_backlog = {
        use_case_id: this.useCaseId,
        action: "backlog",
        backlog: row,
        user: this.userId,
        idProject: this.selectedProject._id,
        created_time: new Date().toISOString().split("T")[0]
      }
      const dialogConfigRequest = new MatDialogConfig();
      dialogConfigRequest.disableClose = true;
      dialogConfigRequest.autoFocus = true;
      dialogConfigRequest.data = edit_backlog;
      const dialogRef = this.dialog.open(RequestTableComponent, dialogConfigRequest);
      dialogRef.afterClosed().subscribe(
        result => {
          if (result != null) {
            result.user = this.userId,
            this.updateBacklog(result);
          }
        });
    } else {
      this.openSnackBar("No es posible editar la tabla, se encuentra en proceso de ingesta.", "Error");
    }
  }

  viewRequestDataTable(row) {
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = row;
    dialogConfigRequest.maxWidth = "70%";
    dialogConfigRequest.minWidth = "770px";
    dialogConfigRequest.minHeight = "410px";
    this.dialog.open(RequestTablonComponent, dialogConfigRequest);
  }

  updateBacklog(row) {
    row["action"] = "backlog";
    this.rest.postRequest('projects/requests/backlogs/', JSON.stringify(row)).subscribe(
      (data: any) => {
        this.clean();
        this.openSnackBar("Tabla actualizada exitosamente.", "Ok");
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
      }
    );
  }

  deleteBacklogUseCase(row) {
    if (confirm("¿Desea solicitar la eliminación de la tabla?")) {
      var request = {
        action: "delete",
        use_case_id: this.useCaseId,
        user: this.userId,
        idProject: this.selectedProject._id,
        backlog: {
          _id: row._id
        }
      }
      this.rest.postRequest('projects/requests/backlogs/', JSON.stringify(request)).subscribe(
        (data: any) => {
          this.clean();
          this.openSnackBar("Solicitud eliminación tabla - " + row.baseName + " - creada exitosamente.", "Ok");
        }, (error) => {
          this.openSnackBar(error.error.reason, "Error");
        }
      );
    }
  }

  deleteUseCase(row) {
    if (confirm("¿Desea solicitar la eliminación del caso de uso?")) {
      var request = {
        action: "delete",
        use_case_id: row._id,
        user_id: this.userId,
        project_id: this.selectedProject._id,
      }
      this.rest.postRequest('projects/requests/useCases/', JSON.stringify(request)).subscribe(
        (data: any) => {
          this.cleanFilterUseCase();
          this.openSnackBar("Solicitud eliminación caso de uso - " + row.name + " - creada exitosamente.", "Ok");
        }, (error) => {
          this.openSnackBar(error.error.reason, "Error");
        }
      );
    }
  }

  queryFilterUseCaseBacklog() {
    this.loadingFlag = true;
    var useCaseNameTmp = "%20";
    var tableNameTmp = "%20";
    var dataTableNameTmp = "%20";
    this.flagQuery = false;
    if (this.filterQueryUseCaseName.length > 0) { useCaseNameTmp = this.filterQueryUseCaseName; this.flagQuery = true; }
    if (this.filterQueryBacklogName.length > 0) { tableNameTmp = this.filterQueryBacklogName; this.flagQuery = true; }
    if (this.filterQueryDataTableName.length > 0) { dataTableNameTmp = this.filterQueryDataTableName; this.flagQuery = true; }
    if (this.flagQuery) {
      this.rest.getRequest('projects/' + this.selectedProject._id + '/query/useCase=' + useCaseNameTmp + '&backlog=' + tableNameTmp + '&tablon=' + dataTableNameTmp +'/').subscribe(
        (data: any) => {
          this.usecasesProject = data;
          this.dataTablesUseCaseProject = data.tablones;
          this.usecasesProjectTable.rowDetail.collapseAllRows();
          this.openSnackBar("Búsqueda finalizada", "Ok");
          this.loadingFlag = false;
        },
        (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
          this.loadingFlag = false;
        }
      );
    } else {
      this.openSnackBar("Por favor, diligenciar los campos de búsqueda.", "Error");
      this.loadingFlag = false;
    }
  }

  cleanFilterUseCaseBacklogs() {
    this.filterQueryUseCaseName = "";
    this.filterQueryBacklogName = "";
    this.filterQueryDataTableName = "";
    this.flagQuery = false;
    this.getProjectUseCases(this.selectedProject._id);
  }

  queryInsideUseCaseBacklog(row: any, searchType: string) {
    this.loadingBarInside = true;
    var filterName = this.filterInsideName != "" ? this.filterInsideName : "%20"
    var filterState = this.filterInsideState != "" ? this.filterInsideState : "%20"
    var filterRequest = this.filterInsideRequest != "" ? this.filterInsideRequest : "%20"

    var queryType = searchType == "T" ? "backlog" : "dataTable";
    var url = `projects/useCases/${row._id}/query/${queryType}=${filterName}&request=${filterRequest}&state=${filterState}/`

    this.rest.getRequest(url).subscribe(
      (data: any) => {
        if (searchType == "T") {
          this.backlogsUseCaseProject = data.tables;
        } else if (searchType == "DT") {
          this.dataTablesUseCaseProject = data.tablones;
          for(var tabo of this.dataTablesUseCaseProject)
            tabo["originSystem"] = tabo["originSystem"] ? tabo["originSystem"] : "HDFS-Parquet";
        }
        this.openSnackBar("Búsqueda finalizada", "Ok");
        this.loadingBarInside = false;
    });
  }

  queryInsideUseCaseDataTable(row) {
    var stateDTTmp = this.filterInDataTableStateTable.length > 0 
      ? this.filterInDataTableStateTable : "%20" ;
    var nameDTTmp = this.filterInDataTableName.length > 0 
      ? this.filterInDataTableName : "%20" ;
    var statusDTTmp = this.filterInDataTableStatus.length > 0 
      ? this.filterInDataTableStatus : "%20" ;

    if (stateDTTmp !==  "%20" || nameDTTmp !==  "%20" || statusDTTmp !==  "%20") {
      this.rest.getRequest('projects/useCases/' + row._id + '/query/dataTable=' + nameDTTmp + '&request=' + statusDTTmp + '&state=' + stateDTTmp + '/').subscribe((data: any) => {
        this.dataTablesUseCaseProject = data.tablones;
        for(var tabo of this.dataTablesUseCaseProject)
          tabo["originSystem"] = tabo["originSystem"] ? tabo["originSystem"] : "HDFS-Parquet";
        this.openSnackBar("Búsqueda finalizada", "Ok");
      },
        (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    } else {
      this.openSnackBar("Por favor, diligenciar los campos de búsqueda.", "Error");
    }
  }

  cleanFilterInsideUseCase() {
    this.backlogsUseCaseProject = [];
    this.dataTablesUseCaseProject = [];
    this.filterInsideUseCaseSelectedState = "";
    this.filterInsideUseCaseTable = "";
    this.selectorStateInsideUseCase.value = "";
    this.selectorStateTableInsideUseCase.value = "";
    this.getUseCasesBacklogs(this.useCaseId);

    this.filterInsideName = "";
    this.filterInsideState = "";
    this.filterInsideRequest = "";
    this.getUseCasesBacklogs(this.useCaseId);
  }

  cleanFilterInsideUseCaseDataTable() {
    this.insideDataTableStateTable.value = "";
    this.insideDataTableStatus.value = "";
    this.filterInDataTableStateTable = "";
    this.filterInDataTableStatus = "";
    this.filterInDataTableName = "";
    this.getUseCasesBacklogs(this.useCaseId);
  }

  getUseCasesBacklogs(use_case_id) {
    this.rest.getRequest('projects/useCases/' + use_case_id + '/backlogs/').subscribe(
      (data: any) => {
        this.backlogsUseCaseProject = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
    this.rest.getRequest('projects/useCases/' + use_case_id + '/dataTables/').subscribe(
      (data: any) => {
        this.dataTablesUseCaseProject = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getProjectUseCases(idProject) {
    this.rest.getRequest('projects/' + idProject + '/useCases/').subscribe(
      (data: any) => {
        this.usecasesProject = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  queryStatisticUseCase(row) {
    this.rest.getRequest('statistics/status/where=use_case&code=' + row._id).subscribe(
      (data: any) => {
        const dialogConfigUse = new MatDialogConfig();
        dialogConfigUse.disableClose = true;
        dialogConfigUse.autoFocus = true;
        dialogConfigUse.minWidth = "600px";
        dialogConfigUse.minHeight = "500px";
        dialogConfigUse.maxWidth = "45%";
        dialogConfigUse.maxHeight = "60%";
        var useData = {
          name: row.name,
          data: data
        }
        dialogConfigUse.data = useData;
        this.dialog.open(StatisticsDialogComponent, dialogConfigUse);
      }
    );
  }

  editRequestTablon(row){
    if(this.rol === "PO") row["viewOnly"] = true;
    const dialogConfigRequest = new MatDialogConfig();
    dialogConfigRequest.disableClose = true;
    dialogConfigRequest.autoFocus = true;
    dialogConfigRequest.data = row;
    dialogConfigRequest.maxWidth = "70%";
    dialogConfigRequest.minWidth = "770px";
    dialogConfigRequest.minHeight = "410px";
    const dialogRef = this.dialog.open(RequestTablonComponent, dialogConfigRequest);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null) {
          console.log(result);
          /*if (result.hasOwnProperty("dialogTempData")){
            var flag;
            if(result.solResult === "approve"){ if (confirm("¿Desea aprobar la solicitud?")) flag = "A";
            } else if (result.solResult === "reject"){ if (confirm("¿Desea rechazar la solicitud?")) flag = "P";
            } else if (confirm("¿Desea descartar la solicitud?")) { flag = "D"; }
            this.updateRequestDataTable(result.dialogTempData._id, result.dialogTempData.backlog,
              result.dialogTempData.comment, flag);
          }*/
        }
      });
  }

  selectedProjectOption(event) {
    this.editableProject = this.checkUseableProject(event);
    this.selectedProject = event;
    this.showSelectedProject = true;
    this.getProjectUseCases(this.selectedProject._id);
    if(this.selectedProject.name == 'Unassigned') this.flagUnassigned = true;
    else this.flagUnassigned = false;
    this.viewAuditFields = true;
  }

  selectedProjectOptionBacklogFilter(event) {
    this.selectedFilterTableProject = event;
  }

  selectedProjectOptionDataTableFilter(event) {
    this.selectedFilterDataTableProject = event;
  }

  selectedProjectOptionTickets(event) {
    this.selectedFilterTicketProject = event;
  }

  selectedStateOption(event) {
    this.selectedFilterTableState = event;
  }

  selectedDataStateOption(event) {
    this.selectedDataTableFilterState = event;
  }

  selectedStateOptionTickets(event) {
    this.selectedFilterTicketState = event;
  }

  selectedStateInsideUseCase(event) {
    this.filterInsideUseCaseSelectedState = event;
  }

  selectedTableState(event) {
    this.filterInsideUseCaseSelectedStateTable = event;
  }

  selectedStateOptionTicketsStatusReqs(event){
    this.selectedFilterTicketStatusReq = event;
  }

  selectedTicketDatatableProject(event){
    this.selectedProjectTicketsDataTable = event;
  }

  selectedStateOptionDataTableTickets(event){
    this.filterTicketDataTableAction = event;
  }

  selectedReqDataTableTickets(event){
    this.filterTicketDataTableReq = event;
  }

  selectedDataTableState(event) {
    this.filterInDataTableStateTable = event;
  }

  selectedDataTableStatus(event) {
    this.filterInDataTableStatus = event;
  }

  toggleExpandRow(row) {
    if (this.lastToggleUsecase._id == row._id) {
      this.lastToggleUsecase = { _id: "" };
      this.usecasesProjectTable.rowDetail.collapseAllRows();
    } else {
      this.usecasesProjectTable.rowDetail.collapseAllRows();
      this.lastToggleUsecase = row;
      this.usecasesProjectTable.rowDetail.toggleExpandRow(row);
    }
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, (500));
  }

  onDetailToggle(event: any) {
    if (typeof (event.value._id) != "undefined") {
      this.useCaseId = event.value._id;
      if (this.flagQuery) {
        this.backlogsUseCaseProject = event.value.tables;
        this.dataTablesUseCaseProject = event.value.tablones;
      } else {
        this.getUseCasesBacklogs(this.useCaseId);
      }
    }
  }

  showDataDictionary(row) {
    window.open(this.rest.getHomeWebAddress() + "tables/view?idTable=" + row._id, '_blank');
    //this.router.navigate(['tables/view'], { queryParams: { idTable: row._id } });
  }

  showDataTable(row: any, viewFlag: boolean) {
    if(viewFlag)
      window.open(this.rest.getHomeWebAddress() + "tablon/view?idTablon=" + (row.backlog ? row.backlog._id : row._id) , '_blank');
    else 
      this.router.navigate(['/tablon/edit'], { queryParams: { idTablon: row.backlog._id, idReq: row._id } });
  }

  deletedRequestUseCase(row) {
    if (confirm("¿Desea descartar la solicitud?")) {
      if (this.rol == "SPO") {
        this.updateRequest(row._id, row, row, "P", "U", "discard");
      } else if (this.rol == "PO") {
        this.updateRequest(row._id, row.use_case, row, "D", "U", "discard");
      }
    }
  }

  deletedRequestBacklog(row) {
    if (confirm("¿Desea descartar la solicitud?")) {
      if (this.rol == "SPO") {
        this.updateRequest(row._id, row, row, "P", "B", "discard");
      } else if (this.rol == "PO") {
        this.updateRequest(row._id, row.backlog, row, "D", "B", "discard");
      }
    }
  }

  //----SPO
  approvedRequestUseCase(row) {
    if (confirm("¿Desea aprobar la solicitud?")) {
      if (this.rol == "SPO") {
        this.updateRequest(row._id, row.use_case, row, "A", "U", "approve");
      } else {
        this.openSnackBar("Solicitud inválida bajo su rol", "Error");
      }
    }
  }

  approvedRequestBacklog(row) {
    if (confirm("¿Desea aprobar la solicitud?")) {
      if (this.rol == "SPO") {
        this.updateRequest(row._id, row.backlog, row, "A", "B", "approve");
      } else {
        this.openSnackBar("Solicitud inválida bajo su rol", "Error");
      }
    }
  }

  updateTabBacklog(){
    if (typeof(this.selectedProject._id) != "undefined" && this.selectedProject._id.length>0) {
      this.getProjectUseCases(this.selectedProject._id); 
    }
  }

  updateRequest(id, row, fullRow, action, tab, result: string) {
    var request = {
      request_id: id,
      action: action,
      body: row,
      spo: this.userId,
      comment: fullRow.comment
    }
    this.rest.postRequest('requests/' + id + '/', JSON.stringify(request)).subscribe(
      (data: any) => {
        if (tab == "B") {
          this.clean();
        } else if (tab == "U") {
          this.cleanFilterUseCase();
        }
        if(result === "approve")
          this.openSnackBar("Solicitud - " + ( row.hasOwnProperty("baseName")  ? row.baseName : row.name) + " -  aceptada exitosamente.", "Ok");
        if(result === "discard")
          this.openSnackBar("Solicitud - " + (row.action =='Add'  ? 'Addición' : row.action =='Del'  ? 'Eliminación' : 'Modificación')
           + " -  descartada exitosamente.", "Ok");
      }, (error) => {
        this.openSnackBar(error.reason, "Error");
      }
    );
  }

  updateRequestDataTable(id, row, comment, action) {
    var request = {
      request_id: id,
      action: action,
      body: row,
      spo: this.userId,
      comment: comment
    }
    this.rest.postRequest('requests/dataTable/' + id + '/', JSON.stringify(request)).subscribe(
      (data: any) => {
        this.cleanDataTable();
        var mess = action === "A" ? "aceptada" : action === "P" ? "rechazada" : "descartada";
        this.openSnackBar("Solicitud - " + row.alias + " - " + mess + " exitosamente.", "Ok");
      }, (error) => {
        this.openSnackBar(error.reason, "Error");
      }
    );
  }

  //Cambia la fase actual dependiendo de la pestaña en la que se encuentre
  onTabChange(event: MatTabChangeEvent) {
    switch (event.tab.textLabel) {
      case "Solicitudes de casos de uso":
        this.index = 1;
        this.cleanFilterUseCase();
        break;
      case "Solicitudes de tablas":
        this.index = 2;
        this.clean();
        break;
      case "Backlog":
        this.index = 0;
        this.updateTabBacklog();
        break;
      case "Solicitudes cerradas":
        this.index = 3;
        this.getAllBacklogsTickets();
        this.getAllUseCasesTickets();
        this.getAllDatatableTickets();
        this.getProjectOptionsFitlerBacklogs();
        this.getProjectOptionsFitlerDataTables();
        break;
      case "Solicitudes de tablones":
        this.getAllDataTables();
        break;
    }
  }

  //OnTabChange event para la tabla interna de los backlogs que contiene las tablas y tablones del caso de uso
  //  para reiniciar los valores de busqueda.
  //(param) event: El evento a disparar, en este caso no se le tiene uso
  onTabChangeInside(event: MatTabChangeEvent) {
    this.cleanFilterInsideUseCase()
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, (400));
  }

  //Devuelve el color de la fila si este es generado
  getRowClass(row: any) {
    return {
      generatedRow: row.origin == ''
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
