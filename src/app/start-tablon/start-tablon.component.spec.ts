import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartTablonComponent } from './start-tablon.component';

describe('StartTablonComponent', () => {
  let component: StartTablonComponent;
  let fixture: ComponentFixture<StartTablonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartTablonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartTablonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
