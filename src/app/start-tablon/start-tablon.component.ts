import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { RestService } from '../rest.service';
import { RepoDatatableComponent } from './../repo-datatable/repo-datatable.component';



@Component({
  selector: 'app-start-tablon',
  templateUrl: './start-tablon.component.html',
  styleUrls: ['./start-tablon.component.scss']
})
export class StartTablonComponent implements OnInit {

  @ViewChild('selectorAmbito') selectorAmbito: any;
  @ViewChild('selectorUUAA') selectorUUAA: any;

  formControlBaseName = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlAmbito = new FormControl('', [Validators.required, Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlScope = new FormControl('', Validators.required);
  formControlPhysAlias = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlDesc = new FormControl('', [Validators.required, Validators.minLength(4)]);
  formControlAlias = new FormControl('', Validators.compose([
    Validators.required,
    this.validateFormAlias()
  ]));

  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);
  requiredFormControlMark = new FormControl('', [Validators.required, Validators.minLength(2)]);
  formControlNumberField = new FormControl('', [Validators.required, Validators.minLength(1), Validators.min(1)]);


  tablon: any;
  userId: string;
  fieldNumberField: any = 0;
  newBaseName: string = "";
  ambito: string = "";
  uuaa: string = "";
  newUuaa: string = "";
  newScope: string = "";
  physAlias: string = "";
  descAlias: string = "";
  alias: string = "";
  originsDataTable: any = ["MG", "PG", "ML", "PL"];
  maxLengthDesc: number = 22;

  numFields: any = 0;
  newDescription: string = "";
  newPhysName: any;
  newPeriodicity: string = "";
  newTactical: string = "";

  projectOptions = [];
  useCaseOptions = [];
  ambitoOptions = [];
  uuaaOptions = [];
  periodicityOptions = [];

  selectedOriginTables = [];
  selectedOriginOptions = [];
  originTablesIds = [];
  originTables = [];

  selectedProject: any = undefined;
  selectedUseCase: any = undefined;
  idProject: string;

  activeHelp: any;

  constructor(public rest: RestService, public snackBar: MatSnackBar, public dialog: MatDialog, private router: Router) {
    this.userId = sessionStorage.getItem("userId");
    this.formControlAmbito.disable();
    this.formControlUUAA.disable();
    this.getProjectOptions();
    this.getPeriodicityOptions();
    this.getAmbitos();
  }

  ngOnInit() {
  }

  inputForNames() {
    if(this.ambito === "C"){
      this.maxLengthDesc = 22;
      this.newPhysName = ("t_" + this.newUuaa
      + "_" + this.newScope + "_" + this.descAlias).toLowerCase();
    } else {
      this.maxLengthDesc = 32;
      this.newPhysName = ("t_" + this.newUuaa
      + "_" + this.descAlias).toLowerCase();
    }
  }

  checkFillStatus() {
    if (this.uuaa.length > 0 && this.ambito.length > 0 && this.alias.length > 0 && this.descAlias.length > 0
      && this.numFields > 0 && this.newPeriodicity.length > 0 && this.newTactical.length > 0
      && this.newBaseName.length > 0 && this.newDescription.length > 0 && this.selectedUseCase !== undefined
      && this.validateOriginTables() && this.formControlAlias.valid && this.formControlScope.valid)
      return false;
    return true;
  }

  validateOriginTables() {
    for (var tab of this.selectedOriginOptions)
      if (!tab.hasOwnProperty("faseSeleccionada"))
        return false;
    return true;
  }

  validateFormAlias(): ValidatorFn {
    return (control): { [key: string]: any } | null => {
      return !this.validateAlias(control.value, this.newUuaa, this.newScope) ?
        { 'validateFormAlias': { value: control.value } } : null;
    };
  }

  //Valida la estructura correcta del alias del tablon
  validateAlias(alias: string, uuaa: string, scope: string) {
    if (uuaa === undefined) uuaa = "";
    if (scope === undefined) scope = "";
    var flag = false;
    var pattern = "^(" + scope.toLowerCase() + uuaa.toLowerCase() + ")[a-z0-9]*$";
    var regx = new RegExp(pattern);
    flag = regx.test(alias);
    return flag;
  }

  checkProjectUseCase() {
    return this.selectedUseCase !== undefined
  }

  selectedProjectOption(event) {
    this.selectedProject = event;
    this.idProject = event["_id"];
    this.getProjectUseCases(this.idProject);
    this.selectorAmbito.value = "";
    this.selectorUUAA.value = "";
    this.formControlAmbito.enable();
    this.formControlUUAA.enable();
    this.formControlAlias.updateValueAndValidity();
    this.getUuaas();
  }

  selectedAmbito(event) {
    console.log("coso")
    console.log(event)

    this.ambito = event;
    this.newUuaa = event + this.uuaa;
    this.formControlAlias.updateValueAndValidity();
    this.inputForNames();
  }

  selectedUuaa(event: object) {
    this.uuaa = event["short_name"];
    this.newUuaa = this.ambito + event["short_name"];
    this.formControlAlias.updateValueAndValidity();
    this.inputForNames();
  }

  selectedScopeOrigin(event) {
    this.newScope = event;
    this.formControlAlias.updateValueAndValidity();
    this.inputForNames();
  }

  //Funcion que llama al dialog de searchTable para agregar tablas de origen al tablon
  searchTables(searchType: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.maxWidth = "80%";
    dialogConfig.maxHeight = "90%";
    dialogConfig.data = { idProject: this.idProject, searchType: searchType };
    const dialogRef = this.dialog.open(RepoDatatableComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.prepareOriginTablesOptions(result.tableIds, searchType).then((res: any) => {
          if (searchType === "DT") this.selectFaseDT(res);
          this.selectedOriginOptions = (this.selectedOriginOptions.length < 1 ?
            res : this.selectedOriginOptions.concat(res));
          this.validateDuplicateOriginTables();
        });
      }
    });
  }

  selectFaseDT(arr: any) {
    for (let obj of arr) {
      obj.faseSeleccionada = "DT";
      obj.originType = "DT";
    }
  }

  //Valida que la tabla origen que se quiera agregar no este repetida
  validateDuplicateOriginTables() {
    for (var first = 0; first < this.selectedOriginOptions.length - 1; first++) {
      for (var second = first + 1; second < this.selectedOriginOptions.length; second++) {
        if (this.selectedOriginOptions[first].baseName === this.selectedOriginOptions[second].baseName) {
          this.selectedOriginOptions.splice(second, 1);
          this.selectedOriginOptions = [...this.selectedOriginOptions]
        }
      }
    }
  }

  createOriginTablesReq() {
    let index = 0;
    let origin_tables = {
      _id: "",
      originSystem: "",
      originName: "",
      originRoute: "",
      originType: ""
    };
    for (let tab of this.selectedOriginOptions) {
      origin_tables._id = (tab._id ? tab._id : this.originTablesIds[index]);
      origin_tables.originType = tab.originType;
      if (tab.originType === "T") {
        origin_tables.originSystem = tab.originSystem;
        origin_tables.originName = (
          tab.faseSeleccionada === "RAW" ? tab.raw_name : tab.master_name);
        origin_tables.originRoute = (
          tab.faseSeleccionada === "RAW" ? tab.raw_route : tab.master_route);
      } else if (tab.originType === "DT") {
        origin_tables.originSystem = "HDFS-Parquet";
        origin_tables.originName = tab.master_name;
        origin_tables.originRoute = tab.master_path;
      }
      this.originTables.push(JSON.parse(JSON.stringify(origin_tables)));
      index += 1;
    }
  }

  //Peticion que llama la peticion para preparar la tabla de origen dependiendo de su fase
  requestPromise(tabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataDictionaries/' + tabId + '/head' + '/raw' + '/').toPromise()
        .then((res: any) => {
          res["_id"] = tabId;
          resolve(res);
        })
        .catch(res => {
          resolve("");
        });
    });
    return promise;
  }

  //Peticion que llama la peticion para preparar el tablon de origen
  requestPromiseDT(datatabId) {
    var promise = new Promise((resolve, reject) => {
      this.rest.getRequest('dataTables/' + datatabId + '/head/').toPromise()
        .then((res: any) => {
          if (res !== null) resolve(res);
          else resolve({});
        });
    });
    return promise;
  }

  //Prepara las promesas para cargar las tablas de origen al arreglo de options
  prepareOriginTablesOptions(tableIds, typeOrigin) {
    var promise = new Promise((resolve, reject) => {
      let promises = [];
      if (typeOrigin === 'DT')
        for (let id of tableIds)
          promises.push(this.requestPromiseDT(id))
      else
        for (let id of tableIds)
          promises.push(this.requestPromise(id))
      Promise.all(promises)
        .then((result) => {
          resolve(result);
        })
    });
    return promise;
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassRaw({ row, column, value }) {
    if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "RAW")
      return { 'selected': true };
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClassMaster({ row, column, value }) {
    if (row.hasOwnProperty("faseSeleccionada")
      && row.faseSeleccionada === "MASTER")
      return { 'selected': true };
  }

  disabledFaseCheck(row: any, fase: string) {
    if (row.hasOwnProperty('faseSeleccionada'))
      if (row.faseSeleccionada !== fase)
        return true;
      else
        return false;
    else
      return true;
  }

  //Selecciona una fase Raw o Master para una TABLA y la marca para su posterior carga  
  selectFase(row: any, fase: string) {
    row.faseSeleccionada = fase;
    row.originType = "T";
  }

  removeOriginTable(row: any) {
    if (confirm("¿Desea remover la tabla origen?"))
      this.selectedOriginOptions.splice(
        this.selectedOriginOptions.indexOf(row), 1);
    this.selectedOriginOptions = [...this.selectedOriginOptions]
  }

  //Llama a la peticion al back para la creación del tablon dados sus campos llenos y validados
  createTablon() {
    this.createOriginTablesReq();
    let insideBody = {
      alias: this.alias,
      numFields: this.numFields,
      master_name: this.newPhysName,
      baseName: this.newBaseName,
      observationField: this.newDescription,
      uuaaMaster: this.newUuaa,
      periodicity: this.newPeriodicity,
      tacticalObject: this.newTactical,
      project_owner: this.idProject,
      origin_tables: this.originTables
    };
    let body = {
      user: this.userId,
      idUsecase: this.selectedUseCase["_id"],
      idProject: this.idProject,
      action: "new",
      data: insideBody
    };
    this.rest.postRequest('projects/requests/dataTables/', body).subscribe(
      (data: any) => {
        this.openSnackBar("Tablón creado", "Ok");
        this.router.navigate(['tablon/edit'], { queryParams: { idTablon: data } });
        //Mandar a edit tablon
      }, (error) => {
        this.openSnackBar(error.error.reason, "Error");
        //this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Obtiene la lista de todos los proyectos en nebula y elimina el proyecto unassigned de la lista
  getProjectOptions() {
    this.rest.getRequest('users/' + this.userId + '/projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      }, (error) => {
        this.projectOptions = [];
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  getProjectUseCases(idProject) {
    this.rest.getRequest('projects/' + idProject + '/useCases/').subscribe(
      (data: any) => {
        this.useCaseOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getUuaas() {
    this.rest.getRequest("uuaa/master/all/").subscribe((data: any) => {
      this.uuaaOptions = data;
    });
  }

  getAmbitos() {
    this.rest.getRequest('uuaa/ambitos/').subscribe((data: any) => {
      this.ambitoOptions = data;
    });
  }

  getPeriodicityOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicityOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  inputValidatorNumberFields(event: any) {
    if (event.target.value.length > 4) {
      event.target.value = event.target.value.slice(0, 4);
    }
    if (event.target.value < 0)
      event.target.value = 0;
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  inputValidator(event: any, property: any) {
    switch (property) {
      case "alias":
        break;
    }
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "");
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  public inputValidatorNaming(event: any, property: any) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(event.target.value))
      event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "");
    if (property) {
      this[property] = event.target.value;
      this.inputForNames();
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }
}
