import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { MatDialog, MatDialogConfig, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { TableInfoComponent } from './table-info/table-info.component';
import { TablonInfoComponent } from './tablon-info/tablon-info.component';

import * as XLSX from 'xlsx';


@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent implements OnInit {

  rol: any;
  restOb: any;

  projectOptions: any = [];
  useCaseOptions: any = [];
  stateOptions: any = [];
  typeOptions: any = [{ label: "Tabla", value: "DD" }, { label: "Tablón", value: "DT" }];

  selectedProject = "";
  selectedUseCase = undefined;
  selectedType: any = {};
  nameQuery = "";
  aliasQuery = "";
  logicNameQuery = "";
  logicDescQuery = "";
  legacyNameQuery = "";
  selectedState = "";
  tablesQuery = [];

  reviewDTFlag: boolean = false;
  loadingIndicator: boolean = false;
  expandIcon: string = "expand_more";

  @ViewChild('myTable') table: any;
  @ViewChild('selectProject') selectorProject: any;
  @ViewChild('selectState') selectorState: any;
  @ViewChild('selectorType') selectorType: any;

  states = [
    { value: 'N', viewValue: 'En propuesta' },
    { value: 'G', viewValue: 'Gobierno' },
    { value: 'RN', viewValue: 'VoBo Negocio' },
    { value: 'Q', viewValue: 'Calidad' },
    { value: 'I', viewValue: 'Listo para ingestar' },
    { value: 'A', viewValue: 'Arquitectura' },
    { value: 'P', viewValue: 'Producción' },
    { value: 'R', viewValue: 'Revisión de Tablones' },
    { value: 'D', viewValue: 'Descartados' }
  ];

  statesAlt = [
    { value: 'N', viewValue: 'En propuesta' },
    { value: 'G', viewValue: 'Gobierno' },
    { value: 'RN', viewValue: 'VoBo Negocio' },
    { value: 'Q', viewValue: 'Calidad' },
    { value: 'I', viewValue: 'Listo para ingestar' },
    { value: 'A', viewValue: 'Arquitectura' },
    { value: 'P', viewValue: 'Producción' },
    { value: 'D', viewValue: 'Descartados' }
  ];


  constructor(public rest: RestService, private router: Router, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.rol = sessionStorage.getItem("rol");
    if (this.rol !== 'G'){
      this.openSnackBar("Restringida a solo Gobierno", "Error");
      this.router.navigate(["/home"]);
    } else {
      this.getAllProjects();
      this.getAllDatadictionaries();
    }
    this.stateOptions = this.states;
   }


  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
  }
  

  toggleExpandRow(row, expanded) {
    this.table.rowDetail.toggleExpandRow(row);
    this.expandIcon = (expanded ? "arrow_drop_down" : "arrow_drop_up");
  }

  getAllDatadictionaries() {
    this.loadingIndicator = true;
    this.restOb = this.rest.getRequest('query/table_name=%20&alias=%20&project_id='
      + '%20&table_state=%20&logic_name=%20&table_desc=%20&backlog=%20&type=%20/').subscribe(
        (data) => {
          this.tablesQuery = data;
          this.loadingIndicator = false;
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
  }

  selectedNewState(value, row){
    let newVal = "";
    let queryType = "";
    let messageObj = "";

    switch (value) {
      case "En propuesta": newVal="N"; break;
      case "Gobierno": newVal="G"; break;
      case "Producción": newVal="A"; break;
      case "VoBo Negocio": newVal="RN"; break;
      case "Listo para ingestar": newVal="I"; break;
    }
    if (row.type === "T") {
      queryType = "dataDictionaries";
      messageObj = "Tabla";
    } else if (row.type === "DT") {
      queryType = "dataTables";
      messageObj = "Tablón"
    }
    this.rest.postRequest(`${queryType}/${row._id}/state/`, { newState: newVal }).subscribe(
      (data: any) => {
        this.openSnackBar(`${messageObj} ${row.master_name} modificada > ${value}`, "Ok");
      });
  }

  viewTable(row: any){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = "80%";
    dialogConfig.width = "80%";
    dialogConfig.maxHeight = "100%";
    dialogConfig.maxWidth = "100%";
    dialogConfig.disableClose = false;
    if (row.type === "T") {
      this.rest.getRequest('dataDictionaries/' + row._id + '/head/' + "complete" + '/').subscribe(
        (data: any) => {
          dialogConfig.data = {
            row: data
          };
          this.dialog.open(TableInfoComponent, dialogConfig);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    } else if (row.type === "DT") {
      this.rest.getRequest('dataTables/' + row._id + '/head/').subscribe(
        (data: any) => {
          console.log(data);
          dialogConfig.data = { row: data };
          this.dialog.open(TablonInfoComponent, dialogConfig);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    }   
  } 

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'TODO': worksheet }, SheetNames: ['TODO'] };
    XLSX.writeFile(workbook, ControlPanelComponent.toExportFileName(excelFileName));
  }

  generateExcelsDT() {
    this.rest.getRequest('dataTables/todo/excels/').subscribe(
      (data: any) => {
        this.exportAsExcelFileObject(data, "TODOS_dt");
        this.openSnackBar("Generación exitosa del EXCELOSO ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  cleanExportData(data) {
    var cleanData = [];
    for (let row of data) {
      cleanData.push({
        "Alias": row.alias,
        "Nombre Base": row.baseName,
        "Nombre Legacy": row.legacy_name,
        "Nombre Raw": row.raw_name,
        "Nombre Master": row.master_name,
        "Descripcion": row.observationField,
        "UUAA Raw": row.uuaaRaw,
        "UUAA Master": row.uuaaMaster,
        "Proyectos": row.project_name.toString(),
        "Estado Nebula": row.stateTable,
        "Tipo Objeto": row.type === "T" ? "Tabla" : "Tablon",
        "Usuario Responsable": row.user
      })
    }
    return cleanData;
  }

  exportData() {
    if (this.tablesQuery.length > 0) {
      this.exportAsExcelFileObject(this.cleanExportData(this.tablesQuery), "ExportData_Nebula");
      this.openSnackBar("Generación exitosa del EXCELOSO ", "Ok");
    } else {
      this.openSnackBar("Realice una busqueda de datos antes de exportar el excel", "Error");
    }
  }

  
  getSearchTables() {
    this.loadingIndicator = true;
    if (this.selectedProject.length > 0 && this.nameQuery.length > 0 && this.aliasQuery.length > 0 &&
      this.selectedState.length > 0 && this.logicNameQuery.length > 0 && this.logicDescQuery.length > 0) {
      this.tablesQuery = [];
      this.loadingIndicator = false;
      this.openSnackBar("Por favor, indique un parámetro de búsqueda.", "Error");
    } else {
      var projectTmp = this.selectedProject;
      var nameQueryTmp = this.nameQuery;
      var aliasTmp = this.aliasQuery;
      var legacyNameTmp = this.legacyNameQuery;
      var selectedStateTmp = this.selectedState;
      var logicNameTmp = this.logicNameQuery;
      var logicDescTemp = this.logicDescQuery;
      var typeTemp = this.selectedType.value;

      if (this.reviewDTFlag){
        typeTemp = "DT";
        this.selectedType = this.typeOptions[1];
        legacyNameTmp = "%20";
      }

      if (!this.selectedType.hasOwnProperty("value")) typeTemp = "%20";
      if (this.selectedProject.length == 0) projectTmp = "%20";
      if (this.nameQuery.length == 0) nameQueryTmp = "%20";
      if (this.aliasQuery.length == 0) aliasTmp = "%20";
      if (this.selectedState.length == 0) selectedStateTmp = "%20";
      if (this.logicNameQuery.length == 0) logicNameTmp = "%20";
      if (this.logicDescQuery.length == 0) logicDescTemp = "%20";
      if (this.legacyNameQuery.length == 0) legacyNameTmp = "%20";
      else {
        typeTemp = "DD";
        this.reviewDTFlag = false;
        this.selectedType = this.typeOptions[0];
      }

      if (this.loadingIndicator) 
        this.restOb.unsubscribe();
        
      this.restOb = this.rest.getRequest('query/table_name=' + nameQueryTmp + '&alias=' + aliasTmp + '&project_id=' + projectTmp + '&table_state=' + selectedStateTmp
      + '&logic_name=' + logicNameTmp + '&table_desc=' + logicDescTemp + '&backlog=' + legacyNameTmp + '&type=' + typeTemp + '/').subscribe(
        (data) => {
          this.loadingIndicator = false;
          this.tablesQuery = data;
          if (this.selectedUseCase !== undefined){
            console.log(data);
            //this.tablesQuery = this.cleanByUseCase();
          }
          this.openSnackBar("Búsqueda finalizada", "Ok");
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    }
  }

  //Limpia los parametros de búsqueda
  clean() {
    this.selectedProject = "";
    this.selectorProject.value = "";
    this.selectedUseCase = undefined;
    this.nameQuery = "";
    this.aliasQuery = "";
    this.legacyNameQuery = "";
    this.reviewDTFlag = false;
    this.selectedState = "";
    this.selectorState.value = "";
    this.logicNameQuery = "";
    this.logicDescQuery = "";
    this.selectedType = {};
    this.selectorType.value = "";

    this.stateOptions = this.states;
    this.getSearchTables();
  }

  //Limpia las busquedas por el caso de uso seleccionado si existe
  cleanByUseCase() {
    let tabsArray = [];
    for (let table of this.tablesQuery)
      if (this.selectedUseCase.tables.includes(table._id) || this.selectedUseCase.tablones.includes(table._id))
        tabsArray.push(table);
    return tabsArray;
  }

  getAllProjects() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        this.projectOptions = data;
      },
      (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }


  selectedTypeOption(value) {
    this.selectedType = value;
  }

  selectedStateOption(value) {
    this.selectedState = value;
    if (this.selectedState === "R"){
      this.reviewDTFlag = true;
      //this.selectedType = this.typeOptions[1];
    }else
      this.reviewDTFlag = false;
  }

  selectedProjectOption(value) {
    this.selectedProject = value;
    this.getProjectUseCases(value);
  }

  selectedUseCaseOption(value) {
    this.selectedUseCase = value;
  }

  inputLegacy(value: any) {
    if (value.data === null)
      this.stateOptions = this.states;
    else {
      this.stateOptions = this.statesAlt;
      this.selectedState = "";
      this.selectorState.value = "";
    }
  }

  getProjectUseCases(idProject) {
    this.rest.getRequest('projects/' + idProject + '/useCases/').subscribe(
      (data: any) => {
        this.useCaseOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 8000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }


}
