import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { debounceTime, map, startWith } from "rxjs/operators";
import { RestService } from '../../rest.service';

@Component({
  selector: 'app-table-info',
  templateUrl: './table-info.component.html',
  styleUrls: ['./table-info.component.scss']
})

export class TableInfoComponent implements OnInit {

  dataTableFlag: boolean;
  buttonMessage: string;

  home: string;
  data: any;
  idTable: string;
  oldPass: string;
  newPass: string;
  newPassRepeat: string;
  oldPassEncrpt: any;
  newPassEncrpt: any;
  hidePasswd1 = true;
  hidePasswd2 = true;
  hidePasswd3 = true;
  messagesError: any = [];
  showDivError = false;

  currAmbito = "";
  currUUAA = "";

  frecuencyOptions = [];
  objectTypesOptions = ["File", "Table"];
  fileTypeOptions = ["", "csv", "fixed"];
  loadingTypesOptions = ["incremental", "complete"];
  originSystemOptions = [];
  deployType_values = ["", "Global-Implementation", "Local"];


  userId;
  tableObject: any;
  showHostTableFields = false;

  uuaaOptions;
  ambitoOptions = []
  uuaaOptions2 = []

  uuaaShortRaw;
  aliasLongTablaRaw;

  uuaaShortMaster;
  aliasLongTablaMaster;

  uuaaControl = new FormControl();
  filteredUUAAs: Observable<string[]>;

  constructor(private dialogRef: MatDialogRef<TableInfoComponent>, @Inject(MAT_DIALOG_DATA) public datas: any,
    public rest: RestService, public snackBar: MatSnackBar) {
    this.userId = sessionStorage.getItem("userId");
    this.tableObject = datas.row;
    this.idTable = datas.row["_id"];
    this.getInfoTable();
  }

  ngOnInit() {
    this.filteredUUAAs = this.uuaaControl.valueChanges.pipe(
      debounceTime(200),
      startWith(""),
      map((value) => this.filterUUAAs(value))
    );
  }

  closeDialog() {
    this.dialogRef.close(null);
  }

  //Funcion que filtra las uuaas en el select a medida que se ingresen letras
  private filterUUAAs(value: any): string[] {
    if (value !== null) {
      if (value.hasOwnProperty('short_name')) value = value['short_name']
      const filterValue = value.toLowerCase()
      let vau = this.uuaaOptions2.filter(option =>
        option['short_name'].toLowerCase().includes(filterValue)
      )
      return vau
    }
  }

  //Valida que el naming de origen este dentro de las opciones permitidas.
  //Para evitar que ingresen namings no permitidos
  checkSelectedUUAA() {
    if (this.uuaaControl.value != null)
      if (this.uuaaControl.value.hasOwnProperty('short_name')) {
        if (this.currUUAA !== this.uuaaControl.value['short_name'])
          this.resetEditUUAA()
      } else this.resetEditUUAA()
  }

  //Resetea los valores del namign de origen cuando se agrega.
  resetEditUUAA() {
    this.uuaaControl.setValue({ short_name: this.currUUAA })
  }

  //Actualiza el ambito y los campos que dependan de el
  selectedAmbito(event: any) {
    this.currAmbito = event;
    this.inputUUAA();
  }

  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa(event: any) {
    this.currUUAA = event.option.value.short_name
    this.uuaaControl.setValue(event.option.value);
    this.inputUUAA();
  }

  //Input
  inputUUAA() {
    this.tableObject.uuaaMaster = this.currAmbito + this.currUUAA;
  }

  //Calcula el nombre del alias cuando la fase es propuesta (el objeto no tiene el nombre fisico solito)
  calcAliasLong(fase: string) {
    if (fase === "raw") {
      var temp = this.tableObject.raw_name.split("_");
      this.uuaaShortRaw = temp[0] + "_" + temp[1] + "_";
      temp.splice(0, 2);
      this.aliasLongTablaRaw = temp.join("_");
    } else if (fase === "master") {
      var temp = this.tableObject.master_name.split("_");
      this.uuaaShortMaster = temp[0] + "_" + temp[1] + "_";
      temp.splice(0, 2);
      this.aliasLongTablaMaster = temp.join("_");
    }
    this.currAmbito = this.tableObject.uuaaMaster.charAt(0);
    this.currUUAA = this.tableObject.uuaaMaster.substring(1);
    this.uuaaControl.setValue({ short_name: this.currUUAA })
  }

  getAmbitos() {
    this.rest.getRequest('uuaa/ambitos/').subscribe((data: any) => {
      this.ambitoOptions = data;
    });
  }

  getMasterUUAAs() {
    this.rest.getRequest("uuaa/master/all/").subscribe((data: any) => {
      this.uuaaOptions2 = data;
    });
  }

  getFrecuencyOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe((data: any) => {
      this.frecuencyOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  getOrigSysOptions() {
    this.rest.getRequest('projects/backlogs/atributes/originSystem/').subscribe(
      (data: any) => {
        this.originSystemOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Peticion al back para traer la información del objeto de la tabla
  getInfoTable() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/head/' + "complete" + '/').subscribe(
      (data: any) => {
        this.tableObject = data;
        if (data.originSystem.match(/HOST/g))
          this.showHostTableFields = true;
        this.calcAliasLong("raw");
        this.calcAliasLong("master");
        this.getAmbitos();
        this.getMasterUUAAs();
        this.getFrecuencyOptions();
        this.getOrigSysOptions();
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  updateObject() {
    this.tableObject["user_id"] = this.userId;
    this.tableObject["fase"] = "complete"
    delete this.tableObject["user"]
    this.rest.putRequest('dataDictionaries/' + this.idTable + '/head/', JSON.stringify(this.tableObject)).subscribe(
      (data: any) => {
        this.getInfoTable();
        this.openSnackBar("Campos actualizados", "Ok");
      },
      (error) => {
        this.openSnackBar(error.error.reason, "Error");
      }
    );
  }

  cleanData() {
    this.data.naming.isGlobal = this.data.naming.isGlobal === "Y" ? "SI" : "NO";
    this.data.column += 1;
    this.data.key = this.data.key === "1" ? "SI" : "NO";
    this.data.mandatory = this.data.mandatory === "1" ? "SI" : "NO";
    this.data.tds = this.data.tds === "1" ? "SI" : "NO";
  }

  getGlobalNamingName(isGlobal: string) {
    return (isGlobal === "Y" ? "SI" : "NO");
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayUUAA(uuaa: any) {
    if (uuaa) return uuaa['short_name']
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }



}
