import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablonInfoComponent } from './tablon-info.component';

describe('TablonInfoComponent', () => {
  let component: TablonInfoComponent;
  let fixture: ComponentFixture<TablonInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablonInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
