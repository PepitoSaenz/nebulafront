import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Observable } from "rxjs";
import { debounceTime, map, startWith } from "rxjs/operators";
import { RestService } from '../../rest.service';


@Component({
  selector: 'app-tablon-info',
  templateUrl: './tablon-info.component.html',
  styleUrls: ['./tablon-info.component.scss']
})
export class TablonInfoComponent implements OnInit {

  formControlBaseName = new FormControl('', [Validators.required, Validators.minLength(5)]);
  formControlAmbito = new FormControl('', [Validators.required, Validators.minLength(1)]);
  formControlUUAA = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlScope = new FormControl('', Validators.required);
  formControlAlias = new FormControl('', Validators.compose([
    Validators.required,
    this.validateFormAlias()
  ]));
  formControlDesc = new FormControl('', [Validators.required, Validators.minLength(4)]);
  requiredFormControlPer = new FormControl('', [Validators.required, Validators.minLength(2)]);

  formControlPerimetro = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlLoadingT = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlTargetFileT = new FormControl('', [Validators.required, Validators.minLength(3)]);
  formControlInfLevel = new FormControl('', [Validators.required, Validators.minLength(3)]);

  dataTableFlag: boolean;
  buttonMessage: string;

  home: string;
  data: any;
  idTablon: string;
  oldPass: string;
  newPass: string;
  newPassRepeat: string;
  oldPassEncrpt: any;
  newPassEncrpt: any;
  hidePasswd1 = true;
  hidePasswd2 = true;
  hidePasswd3 = true;
  messagesError: any = [];
  showDivError = false;

  frecuencyOptions = [];
  objectTypesOptions = ["File", "Table"];
  fileTypeOptions = ["", "csv", "fixed"];
  loadingTypesOptions = ["incremental", "complete"];
  originSystemOptions = [];
  deployType_values = ["", "Global-Implementation", "Local"];

  suffixesOptions: any = [];
  reasonsOptions: any = [];
  arraySuffixTmp: any = [];
  periodicityOptions: any = [];
  targetFileTypesOptions: any = ["Parquet", "CSV"];
  originsDataTable: any = ["MG", "PG", "ML", "PL"];

  userId;
  tablonObject: any;
  showHostTableFields = false;

  uuaaOptions: any = [];

  uuaaShortRaw;
  aliasLongTablaRaw;

  uuaaShortMaster;
  aliasLongTablaMaster;

  currAmbito: string = "";
  currUUAA: string = "";
  aliasShort: string = "";
  descAlias: string = "";
  currScope: string = "";
  uuaaMaster: string = "";

  idProject;
  maxLengthDesc: number = 22;

  ambitoOptions = []
  uuaaOptions2 = []  
  uuaaControl = new FormControl();
  filteredUUAAs: Observable<string[]>;

  constructor(private dialogRef: MatDialogRef<TablonInfoComponent>, @Inject(MAT_DIALOG_DATA) public datas: any,
    public rest: RestService, public snackBar: MatSnackBar) {
    this.userId = sessionStorage.getItem("userId");
    this.tablonObject = datas.row;
    this.idTablon = datas.row["_id"];
    this.getInfoTablon();
    this.getPeriodicityOptions();
    this.getProjectData();
  }

  ngOnInit() {
    this.filteredUUAAs = this.uuaaControl.valueChanges.pipe(
      debounceTime(200),
      startWith(""),
      map((value) => this.filterUUAAs(value))
    );
  }

    //Funcion que filtra las uuaas en el select a medida que se ingresen letras
    private filterUUAAs(value: any): string[] {
      if (value !== null) {
        if (value.hasOwnProperty("uuaa")) value = value.uuaa;
        const filterValue = value.toLowerCase();
        let vau = this.uuaaOptions2.filter((option) =>
          option.uuaa.toLowerCase().includes(filterValue)
        );
        return vau;
      }
    }

    //Valida que el naming de origen este dentro de las opciones permitidas.
    //Para evitar que ingresen namings no permitidos
    checkSelectedUUAA() {
      if (this.uuaaControl.value != null)
        if (this.uuaaControl.value.hasOwnProperty("uuaa")) {
          if (this.currUUAA !== this.uuaaControl.value.uuaa)
            this.resetEditUUAA();
        } else this.resetEditUUAA();
    }
  
    //Resetea los valores del namign de origen cuando se agrega.
    resetEditUUAA() {
      //this.currUUAA = this.activeObject.uuaaMaster.substring(1);
      this.uuaaControl.setValue({ uuaa: this.currUUAA });
    }

  //Valida la estructura correcta del alias del tablon
  validateAlias(alias: string, uuaa: string, scope: string) {
    if (uuaa === undefined) uuaa = "";
    if (scope === undefined) scope = "";
    var flag = false;
    var pattern = "^(" + scope.toLowerCase() + uuaa.toLowerCase() + ")[a-z]*$";
    var regx = new RegExp(pattern);
    flag = regx.test(alias);
    return flag;
  }

  validateFormAlias(): ValidatorFn {
    return (control): { [key: string]: any } | null => {
      return !this.validateAlias(control.value, this.uuaaMaster, this.currScope) ?
        { 'validateFormAlias': { value: control.value } } : null;
    };
  }


  //Obtiene la lista de todos los proyectos en nebula y elimina el proyecto unassigned de la lista
  getProjectData() {
    this.rest.getRequest('projects/').subscribe(
      (data: any) => {
        for (var project of data)
          if (this.tablonObject.project_owner === project.name) {
            this.idProject = project._id;
          }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  //Funcion que trae las opciones de periodicidad de la tabla/tablon
  getPeriodicityOptions() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe(
      (data: any) => {
        this.periodicityOptions = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  //Initializa el campo de ambito local dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAmbito() {
    this.currAmbito = this.tablonObject.uuaaMaster[0].trim();
  }

  //Initializa el campo de la uuaa sin la letra del pais(ambito) dependiendo de la uuaa del tablon para su posible modificación y validación
  loadUUAA() {
    this.formControlUUAA.setValue(this.tablonObject.uuaaMaster.substring(1));
    this.currUUAA = this.tablonObject.uuaaMaster.substring(1);
    this.uuaaControl.setValue({ uuaa: this.currUUAA });

  }

  //Actualiza los valores de la vista que corresponden a la uuaa de master y al origen del tablon
  updateUUAAProps() {
    this.uuaaMaster = this.tablonObject.uuaaMaster;
    this.formControlAlias.markAsTouched();
    this.formControlAlias.updateValueAndValidity();
  }

  //Initializa el campo de la descripcion del alias dependiendo de la uuaa del tablon para su posible modificación y validación
  loadAliasDesc() {
    this.uuaaMaster = this.tablonObject.uuaaMaster;
    this.currScope = (this.tablonObject.alias[0] + this.tablonObject.alias[1]).toUpperCase();
    this.formControlScope.setValue(this.currScope);
    if (this.currAmbito === "C") {
      this.descAlias = this.tablonObject.master_name.substring(10);
    } else {
      this.maxLengthDesc = 32;
      this.descAlias = this.tablonObject.master_name.substring(7);
    }
  }

  //Modifica la uuaa del tablon dependiendo del ambito y la uuaa sin codigo de pais
  inputForUUAA() {
    this.tablonObject.uuaaMaster = this.currAmbito + "" + this.currUUAA;
  }

  //Modifica el nombre del tablon dependiendo del ambito, uuaa, alias y desc. del alias
  inputForNames() {
    if (this.currAmbito === "C") {
      this.maxLengthDesc = 22;
      this.tablonObject.master_name = "t_" + this.tablonObject.uuaaMaster.toLowerCase()
        + "_" + this.currScope.toLowerCase() + "_" + this.descAlias;
    } else if (this.currAmbito === "K") {
      this.maxLengthDesc = 32;
      this.tablonObject.master_name = "t_" + this.tablonObject.uuaaMaster.toLowerCase()
        + "_" + this.descAlias;
    }
    this.tablonObject.master_name = this.inputValidatorNamingTsu(this.tablonObject.master_name);
  }

  //Actualiza el ambito y los campos que dependan de el
  selectedAmbito(event) {
    this.currAmbito = event;
    this.inputForUUAA();
    this.inputForNames();
    this.updateUUAAProps();
  }

  //Actualiza la uuaa y los campos que dependan de el
  selectedUuaa(event) {
    this.currUUAA = event.option.value.uuaa;
    this.uuaaControl.setValue(event.option.value);
    this.inputForUUAA();
    this.inputForNames();
    this.updateUUAAProps();
  }

  selectedScopeOrigin(event) {
    this.currScope = event;
    this.inputForNames();
    this.updateUUAAProps();
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para el nombre del tablón
  inputValidatorNaming(event: any, property: any, flag: boolean) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "");
      this[property] = event.target.value;
    }
    if (flag) this.inputForNames();
  }

  public inputValidatorNamingTsu(streing: any) {
    const pattern = /^[a-z0-9_]*$/;
    if (!pattern.test(streing))
      streing = streing.replace(/[^a-z0-9_]/g, "");
    return streing;
  }

  //Peticion al back que devuelve la información de la tabla que no tengan que ver con campos ni namings
  getInfoTablon() {
    this.rest.getRequest('dataTables/' + this.idTablon + '/head/').subscribe(
      (data: any) => {
        this.tablonObject = data;
        this.getAmbitos();
        this.getMasterUUAAs();
        this.loadAmbito();
        this.loadUUAA();
        this.loadAliasDesc();
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  getAmbitos() {
    this.rest.getRequest('uuaa/ambitos/').subscribe((data: any) => {
      this.ambitoOptions = data;
    });
  }

  getMasterUUAAs() {
    this.rest.getRequest("uuaa/master/all/").subscribe((data: any) => {
      this.uuaaOptions2 = data;
    });
  }

  updateObject(){
    let tmpObject = JSON.parse(JSON.stringify(this.tablonObject))
    delete tmpObject["_id"];
    delete tmpObject["user"];
    delete tmpObject["project_owner"];
    tmpObject["user_id"] = this.userId;
    tmpObject["complete"] = true;
    this.rest.postRequest('dataTables/' + this.idTablon + '/head/', JSON.stringify(tmpObject)).subscribe(
      (data: any) => {
        this.getInfoTablon();
        this.openSnackBar("Campos actualizados", "Ok");
      },
      (error) => {
        this.openSnackBar(error.error.reason, "Error");
      }
    );
  }

  //Funcion que devuelve el alias del origen para mostrar en el select de origenes al agregar namings
  displayUUAA(uuaa: any) {
    if (uuaa) return uuaa.uuaa;
  }

  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.duration = 12000;
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

}
