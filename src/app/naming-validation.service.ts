import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class NamingValidationService {

  suffixesOptions: any = [];

  constructor(public rest: RestService, public snackBar: MatSnackBar) {
    this.getSuffixes();
  }


  //Abre el snack bar de la pagina
  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 6000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else if (action == "Warning") {
      config.panelClass = ['warnningMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  //Peticion al back que devuelve el arreglo con los posibles sufijos
  getSuffixes() {
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixesOptions = data;
      });
  }

  ///////////////////////////////////////////////
  namingValidationForLoad(row: any, tableFields: any, originHostFlag: boolean) {
    if (row.naming.trim().length < 1)
      row.naming = "empty";
    this.validateNamingInput(row, row.naming, tableFields)
  }

  //Solo cuando se llena el campo, no se verifica la integridad del naming
  validateNamingInput(row: any, value: any, tableFields: any) {
    row.naming = value.trim().toLowerCase();
    //this.cleanDataTypeChange(row);
    this.checkNamingGlobal(row);
    this.updateValueGovernanceFormat(row);
    return true;
  }

  //Verifica que el naming enviado no exista en el arreglo de namings
  isNamingRepeated(naming: string, tableFields) {
    for (var tab of tableFields)
      if (tab.naming === naming) {
        return true;
      }
    return false;
  }

  //Limpia el tipo de dato de salida del naming
  cleanDataTypeChange(row: any) {
    row.dataType = "empty";
    row.format = "empty";
    row.logicalFormat = "empty";
  }

  //Valida el nuevo nombre logico a modificar. No este repetido o si esta vacio
  validateLogicNaming(row: any, value: any, tableFields: any) {
    if (value.trim().length == 0) {
      value = "empty";
      return true;
    } else {
      if (!this.isRepeatedLogic(this.clearLogic(value.trim()), tableFields)) {
        //if(row.naming.isGlobal === 'Y') row.naming.isGlobal = 'N';
        return true;
      } else
        this.openSnackBar("Nombre lógico repetido.", "Error");
      return false;
    }
  }

  //Limpia el nombre logico (?)
  clearLogic(logic: string) {
    let logicRem = logic;
    if (logic !== 'empty') {
      let tmp = logic.toUpperCase();
      logicRem = "";
      let i = 0;
      while (i < logic.length) {
        if (tmp.charCodeAt(i) == 209) {
          if (i + 1 < logic.length && tmp.charCodeAt(i + 1) == 73) {
            logicRem += "NI";
            i += 2;
          } else {
            logicRem += "NI";
            i++;
          }
        } else {
          logicRem += tmp.charAt(i);
          i++
        }
      }
    }
    return logicRem;
  }

  //Verifica si el naming esta repetido
  isRepeatedLogic(logicNaming: string, tableFields: any) {
    for (let field of tableFields)
      if (field.logic === logicNaming)
        return true;
    return false;
  }

  //Verifica los namings repetidos dentro de un arreglo de namings
  //Devuelve un arreglo de los namings repetidos con solo una iteracion
  checkNamingRepeated(tableFields: any) {
    const namings = [];
    for (var tab of tableFields) {
      namings.push(tab.naming.naming);
    }
    const set = new Set(namings);
    const duplicates = namings.filter(item => {
      if (set.has(item))
          set.delete(item);
      else
          return item;
    });
    return duplicates;
  }

  updateValueGovernanceFormat(row: any) {
    row.outFormat = "ALPHANUMERIC(" + row.length + ")";
    row.governanceFormat = row.outFormat;
    if (row.destinationType.match(/STRING/g) || row.destinationType.match(/CHAR/g)) {
      row.destinationType = "STRING";
      row.governanceFormat = "ALPHANUMERIC(" + row.length + ")";
      row.format = "empty";
    } else if (row.destinationType.match(/DATE/g)) {
      row.governanceFormat = "DATE";
      row.format = "yyyy-MM-dd";
    } else if (row.destinationType.match(/INT64/g)) {
      row.governanceFormat = "NUMERIC BIG";
      row.format = "empty";
    } else if (row.destinationType.match(/INT/g)) {
      row.destinationType = "INT32";
      row.governanceFormat = "NUMERIC SHORT";
      row.format = "empty";
      if (row.length > 4) row.governanceFormat = "NUMERIC LARGE";
    } else if (row.destinationType.match(/TIMESTAMP/g)) {
      row.governanceFormat = "TIMESTAMP";
      row.format = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    } else if (row.destinationType.match(/DECIMAL/g)) {
      row.governanceFormat = "DECIMAL(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
      row.format = "(" + (Number(row.integers) + Number(row.decimals)) + "," + row.decimals + ")";
    }
  }

  basicNamingValidation(row: any, options: any, tableFields) {
    var errors = [];
    let titleFlag = !(options.hasOwnProperty("title") && options.title === false); //default true
    let globalFlag = !(options.hasOwnProperty("global") && options.global === false); //def true
    let dataTFlag = options.hasOwnProperty("dataType") && options.dataType; //def false

    //Check if the arrary of namings has one repited
    const dupFields = this.checkNamingRepeated(tableFields);
    const isDup = dupFields.indexOf(row.naming.naming);

    //Title flag
    if (titleFlag)
      errors.push("*" + (row.column + 1) + " - " + row.naming.naming + ":");

    //Validate empty fields
    if (options["fase"] && options["fase"] !== "fieldsMaster")
      if (row.length < 1) errors.push("Por favor, diligenciar la longitud del naming");
    if (row.naming.naming == "empty") errors.push("Por favor, diligenciar el campo naming");
    if (row.logic == "empty") errors.push("Por favor, diligenciar el campo nombre lógico");
    if (row.description == "empty") errors.push("Por favor, diligenciar el campo descripción");
    if (isDup !== -1) errors.push("Ya existe un campo con el mismo naming, favor modificar!");

    if (globalFlag)
      this.checkNamingGlobal(row);

    //Data type flag
    errors = errors.concat(this.validateDataTypeNaming(row, options, dataTFlag));

    return { valid: errors.length - (titleFlag ? 1 : 0) === 0, errors: errors };
  }

  validateDataTypeNaming(rowNaming: any, options: any, dataTFlag: boolean) {
    var errors: string[] = [];
    var suffixTmp = undefined;
    let tmpSuffix = rowNaming.naming.naming.split('_');
    let suffix = tmpSuffix[tmpSuffix.length - 1];

    for (let suff of this.suffixesOptions)
      if (suff.data[0].toLowerCase() === suffix)
        suffixTmp = suff;

    if (suffixTmp !== undefined) {
      if (rowNaming.naming.isGlobal != undefined && rowNaming.naming.isGlobal !== "Y") {
        if (rowNaming.naming.naming.length > 30)
          errors.push("El naming tiene mas de 30 caracteres y no es del Repositorio Global.");
        if (dataTFlag)
          if (rowNaming[options.dataType] && rowNaming[options.dataType].includes("(")) {
            if (!this.dataTypeList(rowNaming).includes(rowNaming[options.dataType].toUpperCase().split("(")[0]))
              errors.push("El tipo de dato no concuerda con el sufijo del naming '()'.");
          } else if (!this.dataTypeList(rowNaming).includes(rowNaming[options.dataType].toUpperCase()))
            errors.push("El tipo de dato no concuerda con el sufijo del naming.");
        if (!suffixTmp.Logic.includes(rowNaming.logic.split(" ")[0]))
          errors.push("El nombre lógico no concuerda con el sufijo del naming.");
        if (!suffixTmp.DESCRIPTION.includes(rowNaming.description.split(" ")[0].toUpperCase())){
          errors.push("La descripción no concuerda con el sufijo del naming.");
        }
      }
    } else errors.push("El súfijo no es válido." + "\n");
    if (options["length"])
      errors = errors.concat(this.basicLengthValidation(rowNaming, options.dataType))

    return errors;
  }

  //Hace una verificacion a la longitud de varios campos basicos en el caso de un retraso de actualizacion
  //o de un naming no global
  basicLengthValidation(rowNaming: any, dataType: string) {
    var errors: string[] = [];

    if (rowNaming[dataType].trim().length == 0)
        errors.push("El tipo de dato esta vacio.");
    if (rowNaming.logic.trim().length == 0)
      errors.push("El nombre lógico esta vacio.");
    if (rowNaming.description.trim().length == 0)
      errors.push("La descripción esta vacia.");

    return errors
  }

  //Busca si el naming esta en el repositorio global para su carga previa
  checkNamingGlobal(row: any) {
    if (!row.naming.naming)
      row.naming = { naming: row.naming };
    this.rest.getRequest('namings/' + row.naming.naming).subscribe(
      (data: any) => {
        var p = /~/gi;
        if (data != null) {
          row.naming.isGlobal = data.naming.isGlobal;
          row.logic = data.logic;
          row.description = data.originalDesc.replace(p, "\n");
          row.naming.code = data.code;
          row.naming.codeLogic = data.codeLogic;
          row.naming.Words = data.naming.Words;
          row.naming.suffix = data.naming.suffix;
          row.naming.hierarchy = data.level;
        } else {
          row.naming.isGlobal = 'N';
          row.naming.code = "empty";
          row.naming.codeLogic = "empty";
          row.naming.hierarchy = "";
          row.naming.Words = [];
        }
      }, (error) => {
          row.naming.isGlobal = 'N';
          row.naming.code = "empty";
          row.naming.codeLogic = "empty";
          row.naming.hierarchy = "";
          row.naming.Words = [];
      }
    );
  }

  //Modifica la estructura del naming para agregar el sufix adecuado
  addCheckSuffix(row: any, suffix: string) {
    switch (suffix) {
      case "id": row.naming.suffix = 1; break;
      case "type": row.naming.suffix = 2; break;
      case "amount": row.naming.suffix = 3; break;
      case "date": row.naming.suffix = 4; break;
      case "name": row.naming.suffix = 5; break;
      case "desc": row.naming.suffix = 6; break;
      case "per": row.naming.suffix = 7; break;
      case "number": row.naming.suffix = 8; break;
    }
  }

  //Verifica el sufijo del naming actual
  checkNamingSuffix(row: any): string {
    let tmpSuffix = row.naming.naming.trim().split('_');
    let suffix = tmpSuffix[tmpSuffix.length - 1];
    let suffixIndex = -1;
    for (let i = 0; i < this.suffixesOptions.length; i++)
      if (this.suffixesOptions[i].suffix === suffix) {
        suffixIndex = i;
        break;
      }
    if (suffixIndex !== -1)
      return suffix;
    return "";
  }

  //Devuelve la lista de tipos de datos validos para el sufijo
  dataTypeList(row: any) {
    let suffix = this.checkNamingSuffix(row);
    let array = [];
    switch (suffix) {
      case "type": case "id": case "desc": case "name":
        array = ["STRING", "XML", "ARRAY"];
        break;
      case "per": case "amount":
        array = ["DECIMAL"];
        break;
      case "number":
        array = ["INT", "INT32", "INT64", "DECIMAL"];
        break;
      case "date":
        array = ["DATE", "TIMESTAMP", "STRING"];
        break;
      case "ars":
        array = ["ARRAY", "STRING"];
        break;
      case "": default:
        array = [];
        break;
    }
    return array;
  }
}
