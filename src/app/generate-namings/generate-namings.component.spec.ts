import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateNamingsComponent } from './generate-namings.component';

describe('GenerateNamingsComponent', () => {
  let component: GenerateNamingsComponent;
  let fixture: ComponentFixture<GenerateNamingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateNamingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateNamingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
