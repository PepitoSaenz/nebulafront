import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { CommentHistorialComponent } from './../comment-historial/comment-historial.component';
import { CommentComponentComponent } from './../comment-component/comment-component.component';
import { ErrorsComponent } from './../errors/errors.component';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-generate-namings',
  templateUrl: './generate-namings.component.html',
  styleUrls: ['./generate-namings.component.scss']
})

export class GenerateNamingsComponent implements OnInit {

  appliValues = [
    "BA",
    "BG",
    "BT",
    "CA",
    "CN",
    "G2",
    "CX",
    "EF",
    "EY",
    "FP",
    "GF",
    "GI",
    "GP",
    "GT",
    "HA",
    "HR",
    "HX",
    "IC",
    "IM",
    "IO",
    "JG",
    "JI",
    "KN",
    "LS",
    "MC",
    "OG",
    "OZ",
    "PE",
    "PM",
    "QH",
    "RC",
    "RG",
    "RI",
    "RV",
    "SM",
    "SL",
    "SP",
    "S4",
    "TC",
    "TP",
    "UG",
    "V7",
    "W9",
    "XC",
    "XG",
    "QG",
    "MK",
    "KP",
    "FI",
    "CT",
    "UB",
    "PF",
    "PI",
    "DI",
    "TN",
    "US",
    "SR",
    "QR",
    "SF",
    "EX",
    "BR",
    "WP",
    "DG",
    "RR",
    "MF",
    "CO",
    "DE",
    "MC"
  ];

  loadingIndicatorV: boolean = false;
  loadingIndicatorR: boolean = false;
  loadingIndicatorM: boolean = false;
  tableOffset = 0;
  tableOffsetR = 0;
  tableOffsetM = 0;
  fileList: File = null;
  arrayBuffer: any;
  objectTypesOptions = ["File", "Table"];
  loadingTypesOptions = ["incremental", "complete"];
  reasons: any;
  frecuencyOptions;
  aliasLongTabla = "";

  userId = "0";

  tempFields = [];
  //Data table.
  fieldsTable: any = [];
  tableData: any = [];

  rawObject: any = [];
  fieldsRaw: any = [];
  alias_descR = "";
  uuaaRaw = "";

  masterObject: any = [];
  fieldsMaster: any = [];
  alias_descM = "";
  uuaaMaster = "";

  idTable: string = "";

  suffixes: any = [];

  editing = [];
  blockers = [];
  editingRaw = [];
  editingMaster = [];

  rowsLimit = 5;
  showTabRaw = false;
  showTabMaster = false;
  observationFlag = true;

  errors = [];

  @ViewChild('table') table: any;
  @ViewChild('tableRaw') tableRaw: any;
  @ViewChild('tableMaster') tableMaster: any;

  @ViewChild('selectSuffixP') selectorSuffixP: any;
  @ViewChild('selectSuffixR') selectorSuffixR: any;
  @ViewChild('selectSuffixM') selectorSuffixM: any;

  @ViewChild('objectType') selectorObjectTypeRaw: any;
  @ViewChild('loadingType') selectorLoadingTypeRaw: any;
  @ViewChild('periodicity') selectorFrecuencyRaw: any;

  @ViewChild('uuaaMasters') selectorUuaaMaster: any;
  @ViewChild('objectType2') selectorObjectTypeMaster: any;
  @ViewChild('loadingType2') selectorLoadingTypeMaster: any;
  @ViewChild('periodicity2') selectorFrecuencyMaster: any;

  @ViewChild('stateFilterP') selectorFilterP: any;
  @ViewChild('stateFilterR') selectorFilterR: any;
  @ViewChild('stateFilterM') selectorFilterM: any;

  requiredFormControlRaw = new FormControl('', [Validators.required, Validators.minLength(2), this.firstLetterVal(), this.applicationCodeVal()]);


  NamingGlobalSearchR = "";
  LogicGlobalSearchR = "";
  DescGlobalSearchR = "";
  LegacySearchR = "";
  DescLegacySearchR = "";
  selectedTypeR = "";
  selectedStateFilterR = "";

  NamingGlobalSearchM = "";
  LogicGlobalSearchM = "";
  DescGlobalSearchM = "";
  LegacySearchM = "";
  DescLegacySearchM = "";
  selectedTypeM = "";
  selectedStateFilterM = "";

  NamingGlobalSearchP = "";
  LogicGlobalSearchP = "";
  DescGlobalSearchP = "";
  LegacySearchP = "";
  DescLegacySearchP = "";
  selectedTypeP = "";
  selectedStateFilterP = "";

  showAllColumnsP = true;
  showAllColumnsR = true;
  showAllColumnsM = true;
  uuaaOptions = [];

  isCommentsRawOk = false;
  isCommentsMasterOk = false;
  isCommentsFieldReviewOk = false;

  columns = [
    { prop: "column", name: "No.", visible: true },
    { prop: "startPosition", name: "Inicio", visible: true },
    { prop: "endPosition", name: "Final", visible: true },
    { prop: "length", name: "Longitud", visible: true },
    { prop: "outLength", name: "Longitud H.", visible: true }, //HOST
    { prop: "naming.naming", name: "Naming", visible: true },
    { prop: "logic", name: "Nombre Lógico", visible: true },
    { prop: "description", name: "Descripción", visible: true },
    { prop: "catalogue", name: "Catálogo", visible: true },
    { prop: "legacy", name: "Legacy", visible: true },
    { prop: "legacyDescription", name: "Descripción Legacy", visible: true },
    { prop: "example", name: "Ejemplo", visible: true },
    { prop: "key", name: "Key", visible: true },
    { prop: "mandatory", name: "Mandatory", visible: true },
    { prop: "destinationType", name: "Tipo destino", visible: true },
    { prop: "governanceFormat", name: "Formato Gobierno", visible: true },
    { prop: "", name: "Edición", visible: true },
    { prop: "", name: "Observaciones", visible: true },
    { prop: "", name: "Comentarios", visible: true },
    { prop: "", name: "Acciones", visible: true },
    { prop: "naming.governanceState", name: "Estado Naming Gobierno", visible: true }, //20
  ];

  columnsRaw = [
    { prop: "column", name: "No.", visible: true }, //0
    { prop: "code", name: "Código", visible: true },
    { prop: "naming.naming", name: "Naming", visible: true },
    { prop: "logic", name: "Lógico", visible: true },
    { prop: "description", name: "Descripción", visible: true },
    { prop: "catalogue", name: "Catálogo", visible: true },
    { prop: "destinationType", name: "Tipo de dato", visible: true },
    { prop: "logicalFormat", name: "Formato lógico", visible: true },
    { prop: "key", name: "Key", visible: true },
    { prop: "mandatory", name: "Mandatory", visible: true },
    { prop: "sourceField", name: "Campo origen", visible: true }, //10
    { prop: "sourceFieldDesc", name: "Descripción campo origen", visible: true },
    { prop: "tokenization", name: "Tokenización", visible: true },
    { prop: "VoBo", name: "VoBo", visible: true },
    { prop: "tds", name: "TDS", visible: true },
    { name: "Edición", visible: true },
    { name: "Observaciones", visible: true },
    { name: "Comentarios", visible: true },
    { name: "Acciones", visible: true },
    { prop: "naming.hierarchy", name: "Jerarquía", visible: true }, //19
    { prop: "naming.governanceState", name: "Estado Naming Gobierno", visible: true }
  ];

  columnsMaster = [
    { prop: "column", name: "No.", visible: true }, //0
    { prop: "code", name: "Código", visible: true },
    { prop: "naming.naming", name: "Naming", visible: true },
    { prop: "logic", name: "Lógico", visible: true },
    { prop: "description", name: "Descripción", visible: true },
    { prop: "catalogue", name: "Catálogo", visible: true },
    { prop: "destinationType", name: "Tipo de dato", visible: true },
    { prop: "logicalFormat", name: "Formato lógico", visible: true },
    { prop: "format", name: "Formato", visible: true },
    { prop: "key", name: "Key", visible: true },
    { prop: "mandatory", name: "Mandatory", visible: true }, //10
    { prop: "sourceField", name: "Campo origen", visible: true },
    { prop: "tokenization", name: "Tokenización", visible: true },
    { prop: "tds", name: "TDS", visible: true },
    { name: "Edición", visible: true },
    { prop: "commentResponse", name: "Observaciones", visible: true },
    { name: "Comentarios", visible: true },
    { name: "Acciones", visible: true },
    { prop: "naming.hierarchy", name: "Jerarquía", visible: true }, //18
    { prop: "naming.governanceState", name: "Estado Naming Gobierno", visible: true } //19
  ];

  showHostTableFields = false;
  stateArchitecture = false;

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router, private changeDetectorRef: ChangeDetectorRef, public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.loadingIndicatorV = false;
    this.loadingIndicatorR = false;
    this.loadingIndicatorM = false;
    this.tableOffset = 0;
    this.tableOffsetR = 0;
    this.tableOffsetM = 0;
    this.userId = sessionStorage.getItem("userId");
    this.route.queryParams.subscribe(params => {
      this.idTable = params["idTable"];
      this.getInfoTable("FieldReview");
      this.getFields("FieldReview");
      this.isGenerateRaw();
      this.isGenerateMaster();
    });
    if (this.idTable.length == 0) {
      this.router.navigate(['/tables']);
    }
    this.getSuffixes();
    this.getUuaas();
    this.getFrecuency();
    this.getReasons();
  }

  ngOnInit() { }

  //Valida que la primera letra de la UUAA de Raw empiece por "C"
  firstLetterVal(): ValidatorFn {
    var nameRe: RegExp = /^[C][A-Z0-9]+$/;
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = nameRe.test(control.value);
      return forbidden ? null : {'firstLetter': {value: control.value}};
    };
  }

  //Valida que la UUAA de Raw posea el código de un applicativo correcto
  applicationCodeVal(): ValidatorFn {
    var found: boolean = false;
    var tmp: string = "";
    var nameRe: RegExp = /^[A-Z][A-Z]+$/;
    return (control: AbstractControl): {[key: string]: any} | null => {
      for(var app of this.appliValues){
        tmp = "^[A-Z][" + app[0] + "][" + app[1] + "][A-Z]+$";
        nameRe = new RegExp(tmp,"g");
        found = nameRe.test(control.value);
        if(found) break;
      }
      return found ? null : {'appCode': {value: control.value}};
    };
  }

  selectedLoadingType(value) {
    this.rawObject.loading_type = value;
    this.masterObject.loading_type = value;
  }

  selectedObjectType(value) {
    this.rawObject.objectType = value;
    this.masterObject.objectType = value;
  }

  selectedFrecuency(value) {
    this.rawObject.periodicity = value;
  }

  selectedFrecuencyMaster(value) {
    this.masterObject.periodicity_master = value;
  }

  selectedSuffixP(value) {
    this.selectedTypeP = value;

  }
  selectedSuffixR(value) {
    this.selectedTypeR = value;
  }

  selectedSuffixM(value) {
    this.selectedTypeM = value;
  }

  selectedStateP(value) {
    this.selectedStateFilterP = value;
  }

  selectedStateR(value) {
    this.selectedStateFilterR = value;
  }

  selectedStateM(value) {
    this.selectedStateFilterM = value;
  }

  selectedUuaaMaster(event) {
    this.masterObject.uuaaMaster = event;
  }

  getUuaas() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/uuuaa/').subscribe((data: any) => {
      this.uuaaOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  getFrecuency() {
    this.rest.getRequest('projects/backlogs/atributes/periodicity/').subscribe((data: any) => {
      this.frecuencyOptions = data;
    }, (error) => {
      this.openSnackBar("Error procesando petición al servidor.", "Error");
    });
  }

  getReasons() {
    this.reasons = [];
    this.rest.getRequest('dataDictionaries/comments/reasons/').subscribe(
      (data: {}) => {
        this.reasons = data;
        this.reasons.unshift({error: "NOK"});
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  isGenerateRaw() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/fieldsRaw/')
      .subscribe(
        (data: any) => {
          if (data.length > 0) {
            this.getInfoTable("raw");
            this.getFields("fieldsRaw");
            this.showTabRaw = true;
          }
        }, (error) => {
          this.showTabRaw = false;
        }
      );
  }

  isGenerateMaster() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/fieldsMaster/')
      .subscribe(
        (data: any) => {
          if (data.length > 0) {
            this.getInfoTable("master");
            this.getFields("fieldsMaster");
            this.showTabMaster = true;
          }
        }, (error) => {
          this.showTabMaster = false;
        }
      );
  }

  updateValueTds(event, row, fase) {
    if (fase == 'fieldsRaw') {
      this.editingRaw[row.column] = false;
      if (event.length == 0) {
        event = "empty";
      } else {
        this.fieldsRaw[row.column].tds = event;
        this.fieldsRaw = [...this.fieldsRaw];
      }
    } else {
      this.editingMaster[row.column] = false;
      if (event.length == 0) {
        event = "empty";
      } else {
        this.fieldsMaster[row.column].tds = event;
        this.fieldsMaster = [...this.fieldsMaster];
      }
    }
    this.validateFieldWithOutStatistics(fase == 'FieldReview' 
        ? this.fieldsTable[row.column] : fase == 'fieldsRaw' ? this.fieldsRaw[row.column] 
        : this.fieldsMaster[row.column], fase);
  }

  checkNamingGlobal(index, naming, fase) {
    this.rest.getRequest('namings/' + naming).subscribe(
      (data: any) => {
        var p = /~/gi;
        //console.log(data.originalDesc.replace(p, "\n"));
        //data.originalDesc.replace(p, "\n")
        if (fase == 'FieldReview') {
          if (data != null) {
            this.fieldsTable[index].naming.isGlobal = data.naming.isGlobal;
            this.fieldsTable[index].logic = data.logic;
            this.fieldsTable[index].description = data.originalDesc.replace(p, "\n");
            this.fieldsTable[index].naming.code = data.code;
            this.fieldsTable[index].naming.codeLogic = data.codeLogic;
            this.fieldsTable[index].naming.Words = data.naming.Words;
            this.fieldsTable[index].naming.suffix = data.naming.suffix;
          } else {
            this.fieldsTable[index].naming.isGlobal = 'N';
            this.fieldsTable[index].naming.code = "empty";
            this.fieldsTable[index].naming.codeLogic = "empty";
            this.fieldsTable[index].naming.Words = [];
          }
        } else if (fase == 'fieldsRaw') {
          if (data != null) {
            this.fieldsRaw[index].naming.isGlobal = data.naming.isGlobal;
            this.fieldsRaw[index].logic = data.logic;
            this.fieldsRaw[index].description = data.originalDesc.replace(p, "\n");
            this.fieldsRaw[index].naming.code = data.code;
            this.fieldsRaw[index].naming.codeLogic = data.codeLogic;
            this.fieldsRaw[index].naming.Words = data.naming.Words;
            this.fieldsRaw[index].naming.suffix = data.naming.suffix;
          } else {
            this.fieldsRaw[index].naming.isGlobal = 'N';
            this.fieldsRaw[index].naming.code = "empty";
            this.fieldsRaw[index].naming.codeLogic = "empty";
            this.fieldsRaw[index].naming.Words = [];
          }
        } else if (fase == 'fieldsMaster') {
          if (data != null) {
            this.fieldsMaster[index].naming.isGlobal = data.naming.isGlobal;
            this.fieldsMaster[index].logic = data.logic;
            this.fieldsMaster[index].description = data.originalDesc.replace(p, "\n");
            this.fieldsMaster[index].naming.code = data.code;
            this.fieldsMaster[index].naming.codeLogic = data.codeLogic;
            this.fieldsMaster[index].naming.Words = data.naming.Words;
            this.fieldsMaster[index].naming.suffix = data.naming.suffix;
          } else {
            this.fieldsMaster[index].naming.isGlobal = 'N';
            this.fieldsMaster[index].naming.code = "empty";
            this.fieldsMaster[index].naming.codeLogic = "empty";
            this.fieldsMaster[index].naming.Words = [];
          }
        }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  updateValueNaming(event, cell, row, fase) {
    if (fase == 'FieldReview') {
      if (!this.blockers[row.column]) {
        this.editing[row.column + '-' + cell] = false;
        if (event.target.value.trim().length == 0){
          event.target.value = "empty";
          this.fieldsTable[row.column].naming.naming = event.target.value;
        } else {
          if (!this.isNamingRepeated(row.column, event.target.value.trim(), fase)) {
            this.fieldsTable[row.column].naming.naming = event.target.value.trim().toLowerCase();
            this.checkNamingGlobal(row.column, event.target.value.trim().toLowerCase(), fase);
          } else {
            this.openSnackBar("Naming repetido.", "Error");
          }
        }
        this.validateFieldWithOutStatistics(this.fieldsTable[row.column], fase);
      }
    } else if (fase == 'fieldsRaw') {
      if (!this.blockers[row.column]) {
        this.editingRaw[row.column + '-' + cell] = false;
        if (event.target.value.trim().length == 0) {
          event.target.value = "empty";
          this.fieldsRaw[row.column].naming.naming = event.target.value;
        } else {
          if (!this.isNamingRepeated(row.column, event.target.value.trim(), fase)) {
            this.fieldsRaw[row.column].naming.naming = event.target.value.toLowerCase().trim();
            this.checkNamingGlobal(row.column, event.target.value.toLowerCase().trim(), fase);
          } else {
            this.openSnackBar("Naming repetido.", "Error");
          }
        }
        this.validateFieldWithOutStatistics(this.fieldsRaw[row.column], fase);
      }
    } else if (fase == 'fieldsMaster') {
      if (!this.blockers[row.column]) {
        this.editingMaster[row.column + '-' + cell] = false;
        if (event.target.value.trim().length == 0) {
          event.target.value = "empty";
          this.fieldsMaster[row.column].naming.naming = event.target.value;
        } else {
          if (!this.isNamingRepeated(row.column, event.target.value.trim(), fase)) {
            this.fieldsMaster[row.column].naming.naming = event.target.value.toLowerCase().trim();
            this.checkNamingGlobal(row.column, event.target.value.toLowerCase().trim(), fase);
          } else {
            this.openSnackBar("Naming repetido.", "Error");
          }
        }
        this.validateFieldWithOutStatistics(this.fieldsMaster[row.column], fase);
      }
    }
  }

  clearLogic(logic) {
    let tmp = logic.toUpperCase();
    let logicRem = "";
    let i = 0;
    while (i < logic.length) {
      if (tmp.charCodeAt(i) == 209) {
        if (i + 1 < logic.length && tmp.charCodeAt(i + 1) == 73) {
          logicRem += "NI";
          i += 2;
        } else {
          logicRem += "NI";
          i++;
        }
      } else {
        logicRem += tmp.charAt(i);
        i++
      }
    }
    return logicRem;
  }

  isRepeatedLogic(index, logic, fase) {
    let flag = false;
    if (fase == 'FieldReview') {
      for (let i = 0; i < this.fieldsTable.length && !flag; i++) {
        if (this.fieldsTable[i].logic === logic && index != i) {
          flag = true;
        }
      }
      if (!flag) {
        this.fieldsTable[index].logic = logic.toUpperCase();
        this.fieldsTable = [...this.fieldsTable];
      }
    } else if (fase == 'fieldsRaw') {
      for (let i = 0; i < this.fieldsRaw.length && !flag; i++) {
        if (this.fieldsRaw[i].logic === logic && index != i) {
          flag = true;
        }
      }
      if (!flag) {
        this.fieldsRaw[index].logic = logic.toUpperCase();
        this.fieldsRaw = [...this.fieldsRaw];
      }
    } else if (fase == 'fieldsMaster') {
      for (let i = 0; i < this.fieldsMaster.length && !flag; i++) {
        if (this.fieldsMaster[i].logic === logic && index != i) {
          flag = true;
        }
      }
      if (!flag) {
        this.fieldsMaster[index].logic = logic.toUpperCase();
        this.fieldsMaster = [...this.fieldsMaster];
      }
    }
    return flag;
  }

  updateValueLogic(event, cell, row, fase) {
    if (fase == 'FieldReview') {
      if (!this.blockers[row.column]) {
        this.editing[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          this.fieldsTable[row.column].logic = "empty";
        } else {
          this.fieldsTable[row.column].logic = this.clearLogic(event.target.value);
          if (this.isRepeatedLogic(row.column, this.fieldsTable[row.column].logic, fase)) {
            this.openSnackBar("Nombre lógico repetido.", "Error");
          }
        }
        this.validateFieldWithOutStatistics(this.fieldsTable[row.column], fase);
      }

    } else if (fase == 'fieldsRaw') {
      if (!this.blockers[row.column]) {
        this.editingRaw[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          this.fieldsRaw[row.column].logic = "empty";
        } else {
          this.fieldsRaw[row.column].logic = this.clearLogic(event.target.value);
          if (this.isRepeatedLogic(row.column, this.fieldsRaw[row.column].logic, fase)) {
            this.openSnackBar("Nombre lógico repetido.", "Error");
          }
        }
        this.validateFieldWithOutStatistics(this.fieldsRaw[row.column], fase);
      }
    } else if (fase == 'fieldsMaster') {
      if (!this.blockers[row.column]) {
        this.editingMaster[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          this.fieldsMaster[row.column].logic = "empty";
        } else {
          this.fieldsMaster[row.column].logic = this.clearLogic(event.target.value);
          if (this.isRepeatedLogic(row.column, this.fieldsMaster[row.column].logic, fase)) {
            this.openSnackBar("Nombre lógico repetido.", "Error");
          }
        }
        this.validateFieldWithOutStatistics(this.fieldsMaster[row.column], fase);
      }
    }
  }

  updateValueDescription(event, cell, row, fase) {
    if (fase == 'FieldReview') {
      if (!this.blockers[row.column]) {
        this.editing[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          event.target.value = "empty";
          this.fieldsTable[row.column].description = event.target.value;
        } else {
          this.fieldsTable[row.column].description = event.target.value;
          this.validateFieldWithOutStatistics(this.fieldsTable[row.column], fase);
          this.fieldsTable = [...this.fieldsTable];
        }
      }
    } else if (fase == 'fieldsRaw') {
      if (!this.blockers[row.column]) {
        this.editingRaw[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          event.target.value = "empty";
          this.fieldsRaw[row.column].description = event.target.value;
        } else {
          this.fieldsRaw[row.column].description = event.target.value;
          this.validateFieldWithOutStatistics(this.fieldsRaw[row.column], fase);
          this.fieldsRaw = [...this.fieldsRaw];
        }
      }
    } else if (fase == 'fieldsMaster') {
      if (!this.blockers[row.column]) {
        this.editingMaster[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          event.target.value = "empty";
          this.fieldsMaster[row.column].description = event.target.value;
        } else {
          this.fieldsMaster[row.column].description = event.target.value;
          this.validateFieldWithOutStatistics(this.fieldsMaster[row.column], fase);
          this.fieldsMaster = [...this.fieldsMaster];
        }
      }
    }
  }

  updateValueCatalogue(event, cell, row, fase) {
    if (fase == 'FieldReview') {
      if (!this.blockers[row.column]) {
        this.editing[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          this.fieldsTable[row.column].catalogue = "N/A";
        } else {
          this.fieldsTable[row.column].catalogue = event.target.value;
        }
        this.validateFieldWithOutStatistics(this.fieldsTable[row.column], fase);
      }
    } else if (fase == 'fieldsRaw') {
      if (!this.blockers[row.column]) {
        this.editingRaw[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          this.fieldsRaw[row.column].catalogue = "N/A";
        } else {
          this.fieldsRaw[row.column].catalogue = event.target.value;
        }
        this.validateFieldWithOutStatistics(this.fieldsRaw[row.column], fase);
      }
    } else if (fase == 'fieldsMaster') {
      if (!this.blockers[row.column]) {
        this.editingMaster[row.column + '-' + cell] = false;
        if (event.target.value.length == 0) {
          this.fieldsMaster[row.column].catalogue = "N/A";
        } else {
          this.fieldsMaster[row.column].catalogue = event.target.value;
        }
        this.validateFieldWithOutStatistics(this.fieldsMaster[row.column], fase);
      }
    }
  }

  updateValueMandatory(event, row, fase) {
    if (!this.blockers[row.column]) {
      if (row.key == 1) {
        row.mandatory = 1;
        this.openSnackBar("No se puede cambiar el mandatory cuando el campo es llave", "Error");
      } else {
        row.mandatory = event.target.value;
      }
      this.validateFieldWithOutStatistics(fase == 'FieldReview' 
        ? this.fieldsTable[row.column] : fase == 'fieldsRaw' ? this.fieldsRaw[row.column] 
        : this.fieldsMaster[row.column], fase);
    }
  }

  updateValueKey(event, row, fase) {
    if (fase == 'FieldReview') {
      if (!this.blockers[row.column]) {
        this.fieldsTable[row.column].key = event.target.value;
        if (this.fieldsTable[row.column].key == 1) {
          this.fieldsTable[row.column].madatory = 1;
          row.mandatory = 1;
        }
      }
    } else if (fase == 'fieldsRaw') {
      if (!this.blockers[row.column]) {
        this.fieldsRaw[row.column].key = event.target.value;
        if (this.fieldsRaw[row.column].key == 1) {
          this.fieldsRaw[row.column].madatory = 1;
          row.mandatory = 1;
        }
      }
    } else if (fase == 'fieldsMaster') {
      if (!this.blockers[row.column]) {
        this.fieldsMaster[row.column].key = event.target.value;
        if (this.fieldsMaster[row.column].key == 1) {
          this.fieldsMaster[row.column].madatory = 1;
          row.mandatory = 1;
        }
      }
    }
    this.validateFieldWithOutStatistics(fase == 'FieldReview' 
      ? this.fieldsTable[row.column] : fase == 'fieldsRaw' ? this.fieldsRaw[row.column] 
      : this.fieldsMaster[row.column], fase);
  }

  updateValueToken(event, row, fase) {
    if (fase == 'FieldReview') {
      if (!this.blockers[row.column]) {
        this.fieldsTable[row.column].token = event.target.value;
      }
    } else if (fase == 'fieldsRaw') {
      if (!this.blockers[row.column]) {
        this.fieldsRaw[row.column].token = event.target.value;
      }
    } else if (fase == 'fieldsMaster') {
      if (!this.blockers[row.column]) {
        this.fieldsMaster[row.column].token = event.target.value;
      }
    }
    this.validateFieldWithOutStatistics(fase == 'FieldReview' 
        ? this.fieldsTable[row.column] : fase == 'fieldsRaw' ? this.fieldsRaw[row.column] 
        : this.fieldsMaster[row.column], fase);
  }

  updateValueTokenizationType(event, cell, row, fase) {
    if (fase == 'fieldsRaw') {
      this.editingRaw[row.column + '-' + cell] = false;
      if (event.target.value.length == 0) {
        this.fieldsRaw[row.column].tokenization = "empty";
      } else {
        this.fieldsRaw[row.column].tokenization = event.target.value;
      }
    } else {
      this.editingMaster[row.column + '-' + cell] = false;
      if (event.target.value.length == 0) {
        this.fieldsMaster[row.column].tokenization = "empty";
      } else {
        this.fieldsMaster[row.column].tokenization = event.target.value;
      }
    }
    this.validateFieldWithOutStatistics(fase == 'FieldReview' 
        ? this.fieldsTable[row.column] : fase == 'fieldsRaw' ? this.fieldsRaw[row.column] 
        : this.fieldsMaster[row.column], fase);
  }

  updateValueGovernanceState(event: any, row: any, fase: string) {
    row.naming.governanceState = event.target.value;
    this.blockers[row.column] = row.naming.governanceState === "C" ? true : false;
    this.validateFieldWithOutStatistics(row, fase);
  }

  validateFieldWithOutStatistics(row, fase) {
    this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=%20/',
      JSON.stringify({ notSave: "notSave", row: row }))
      .subscribe(
        (data: any) => {}
      );
  }

  objectValidationRaw(): boolean {
    var flag = false;
    this.rawObject.raw_name = this.uuaaRaw + this.alias_descR.replace(/\s/g, '');
    var tmp_errors = [];
    var tmp_structure_logic = this.rawObject.raw_name.split("_");
    if (tmp_structure_logic.length >= 4) {
      this.rawObject.raw_name = this.rawObject.raw_name.toLowerCase();
      var tmp_logic = this.rawObject.raw_name.replace(/[^a-z0-9_]/g, "");
      if (this.rawObject.raw_name != tmp_logic) {
        tmp_errors.push("El nombre físico del objeto continene caracteres especiales.");
      }
    } else {
      tmp_errors.push("El nombre físico del objeto no cumple con el estándar t_uuaa_alias_descripcion");
    }
    this.rawObject.baseName = this.rawObject.baseName.replace(/^\s+|\s+$/g, "");
    this.rawObject.baseName = this.rawObject.baseName.toUpperCase()
    var tmp_base_name = this.rawObject.baseName.replace(/[^A-Z0-9_-]/g, " ");
    if (this.rawObject.baseName.length < 60) {
      if (this.rawObject.baseName != tmp_base_name) {
        tmp_errors.push("El nombre lógico del objeto continene caracteres especiales.");
      }
    } else {
      tmp_errors.push("El nombre lógico no debe sobrepasar los 60 caracteres.");
    }
    if (this.rawObject.baseName.length < 350 && this.rawObject.baseName.length > 100) {
      tmp_errors.push("La descripción del objeto debe estar comprendida en una longitud de mínimo 100 caracteres y máximo 350 caracteres.");
    }
    this.rawObject.uuaaRaw = this.rawObject.uuaaRaw.toUpperCase();
    if (this.rawObject.uuaaRaw.length != 4) {
      tmp_errors.push("El código de sistema/uuaa es un campo obligatorio.");
    }
    if (!this.requiredFormControlRaw.valid){
      tmp_errors.push("El código de sistema/uuaa está incorrecto");
    }
    if (this.rawObject.periodicity.length < 2) {
      tmp_errors.push("La frecuencia del objeto es un campo obligatorio.");
    }
    if (this.rawObject.perimeter.length < 2) {
      tmp_errors.push("El perímetro del objeto es un campo obligatorio.");
    }
    if (this.rawObject.information_level.length < 2) {
      tmp_errors.push("El nivel información del objeto es un campo obligatorio.");
    }
    if (this.rawObject.objectType.length < 2) {
      tmp_errors.push("El tipo del objeto es un campo obligatorio.");
    }
    if (this.rawObject.loading_type.length < 2) {
      tmp_errors.push("El tipo de carga del objeto es un campo obligatorio.");
    }
    if (tmp_errors.length > 0) {
      tmp_errors.unshift(this.rawObject.raw_name + " - RAW");
      const dialogErr = new MatDialogConfig();
      dialogErr.height = "87%";
      dialogErr.width = "60%";
      dialogErr.disableClose = true;
      dialogErr.autoFocus = true;
      dialogErr.data = tmp_errors;
      this.dialog.open(ErrorsComponent, dialogErr);
      flag = false;
    } else {
      flag = true;
    }
    return flag;
  }

  objectValidationMaster(): boolean {
    var flag = false;
    this.masterObject.master_name = this.uuaaMaster + this.alias_descM.replace(/\s/g, '');
    var tmp_errors = [];
    var tmp_structure_logic = this.masterObject.master_name.split("_");
    if (tmp_structure_logic.length >= 4) {
      this.masterObject.master_name = this.masterObject.master_name.toLowerCase();
      var tmp_logic = this.masterObject.master_name.replace(/[^a-z0-9_]/g, "");
      if (this.masterObject.master_name != tmp_logic) {
        tmp_errors.push("El nombre físico del objeto continene caracteres especiales.");
      }
    } else {
      tmp_errors.push("El nombre físico del objeto no cumple con el estándar t_uuaa_alias_descripcion");
    }
    this.masterObject.baseName = this.masterObject.baseName.replace(/^\s+|\s+$/g, "");
    this.masterObject.baseName = this.masterObject.baseName.toUpperCase()
    var tmp_base_name = this.masterObject.baseName.replace(/[^A-Z0-9_-]/g, " ");
    if (this.masterObject.baseName.length < 60) {
      if (this.masterObject.baseName != tmp_base_name) {
        tmp_errors.push("El nombre lógico del objeto continene caracteres especiales.");
      }
    } else {
      tmp_errors.push("El nombre lógico no debe sobrepasar los 60 caracteres.");
    }
    if (this.masterObject.baseName.length < 350 && this.masterObject.baseName.length > 100) {
      tmp_errors.push("La descripción del objeto debe estar comprendida en una longitud de mínimo 100 caracteres y máximo 350 caracteres.");
    }
    this.masterObject.uuaaRaw = this.masterObject.uuaaRaw.toUpperCase();
    if (this.masterObject.uuaaRaw.length != 4) {
      tmp_errors.push("El código de sistema/uuaa es un campo obligatorio.");
    }
    if (this.masterObject.periodicity.length < 2) {
      tmp_errors.push("La frecuencia del objeto es un campo obligatorio.");
    }
    if (this.masterObject.perimeter.length < 2) {
      tmp_errors.push("El perímetro del objeto es un campo obligatorio.");
    }
    if (this.masterObject.information_level.length < 2) {
      tmp_errors.push("El nivel información del objeto es un campo obligatorio.");
    }
    if (this.masterObject.objectType.length < 2) {
      tmp_errors.push("El tipo del objeto es un campo obligatorio.");
    }
    if (this.masterObject.loading_type.length < 2) {
      tmp_errors.push("El tipo de carga del objeto es un campo obligatorio.");
    }
    if (tmp_errors.length > 0) {
      tmp_errors.unshift(this.masterObject.master_name + " - MASTER");
      const dialogErr = new MatDialogConfig();
      dialogErr.height = "87%";
      dialogErr.width = "60%";
      dialogErr.disableClose = true;
      dialogErr.autoFocus = true;
      dialogErr.data = tmp_errors;
      this.dialog.open(ErrorsComponent, dialogErr);
      flag = false;
    } else {
      flag = true;
    }
    return flag;
  }

  updateRawObject() {
    delete this.rawObject["_id"];
    delete this.rawObject["user"];
    delete this.rawObject["sprint"];
    delete this.rawObject["stateTable"];
    delete this.rawObject["project_name"];
    this.rawObject["fase"] = "Raw";
    this.rawObject.current_depth = Number(this.rawObject.current_depth);
    this.rawObject.estimated_volume_records = Number(this.rawObject.estimated_volume_records);
    this.rawObject.required_depth = Number(this.rawObject.required_depth);
    if (this.objectValidationRaw()) {
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/head/', JSON.stringify(this.rawObject)).subscribe(
        (data: any) => {
          this.getInfoTable("raw");
          this.openSnackBar("Campos actualizados.", "Ok");
        }, (error) => {
          this.openSnackBar(error.error.reason, "Error");
        }
      );
    }
  }

  updateMasterObject() {
    delete this.masterObject["_id"];
    delete this.masterObject["user"];
    delete this.masterObject["sprint"];
    delete this.masterObject["stateTable"];
    delete this.masterObject["project_name"];
    this.masterObject["fase"] = "Master";
    this.masterObject.current_depth = Number(this.masterObject.current_depth);
    this.masterObject.estimated_volume_records = Number(this.masterObject.estimated_volume_records);
    this.masterObject.required_depth = Number(this.masterObject.required_depth);
    if (this.objectValidationMaster()) {
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/head/', JSON.stringify(this.masterObject)).subscribe(
        (data: any) => {
          this.getInfoTable("master");
          this.openSnackBar("Campos actualizados.", "Ok");
        },
        (error) => {
          this.openSnackBar(error.error.reason, "Error");
        }
      );
    }
  }

  getSuffixes() {
    this.suffixes = [];
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixes = data;
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  getInfoTable(fase) {
    let uuaaShort;
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/head/' + fase + '/').subscribe(
      (data: any) => {
        if (fase == "raw") {
          this.rawObject = data;
          this.rawObject.baseName = this.rawObject.baseName.toUpperCase();
          var temp = this.rawObject.raw_name.split("_");
          this.uuaaRaw = temp[0] + "_" + temp[1] + "_";
          temp.splice(0, 1);
          temp.splice(0, 1);
          this.alias_descR = temp.join("_");
          this.aliasLongTabla = this.alias_descR;
          //this.selectorObjectTypeRaw.value = this.rawObject.objectType;
        } else if (fase == "master") {
          this.masterObject = data;
          this.masterObject.baseName = this.masterObject.baseName.toUpperCase();
          var temp = this.masterObject.master_name.split("_");
          this.uuaaMaster = temp[0] + "_" + temp[1] + "_";
          temp.splice(0, 1);
          temp.splice(0, 1);
          this.alias_descM = temp.join("_");
          this.aliasLongTabla = this.alias_descM;
        } else if (fase == "FieldReview") {
          this.tableData = data;
          if (this.tableData.stateTable !== "G" && this.tableData.stateTable !== "N"){
            this.rest.sendHome(sessionStorage.getItem("rol"));
            this.openSnackBar("Esa tabla no esta disponible para editar", "Error");
          }
          var temp = this.tableData['raw_name'].split("_");
          uuaaShort = temp[0] + "_" + temp[1] + "_";
          temp.splice(0, 2);
          this.aliasLongTabla = temp.join("_");
          if (data.stateTable != "G") {
            this.stateArchitecture = true;
          }
        }
        if (data.originSystem.match(/HOST/g)) {
          this.showHostTableFields = true;
        }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      });
  }

  getFields(fase) {
    this.loadingIndicatorV = true;
    this.loadingIndicatorR = true;
    this.loadingIndicatorM = true;
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + fase + '/')
      .subscribe(
        (data: any) => {
          if (fase == "fieldsRaw") {
            this.fieldsRaw = data;
            if (this.fieldsRaw.length > 0) 
              this.showTabRaw = true;
            this.getBlockers(this.fieldsRaw);
          } else if (fase == "fieldsMaster") {
            this.fieldsMaster = data;
            if (this.fieldsMaster.length > 0)
              this.showTabMaster = true;
            this.getBlockers(this.fieldsMaster);
          } else if (fase == "FieldReview") {
            this.fieldsTable = data;
            this.getBlockers(this.fieldsTable);
          }
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    this.loadingIndicatorV = false;
    this.loadingIndicatorR = false;
    this.loadingIndicatorM = false;
  }

  getBlockers(arrTable: any){
    this.blockers = [];
    for(let i = 0; i < arrTable.length; i++){
      this.blockers[i] = arrTable[i].naming.governanceState === "C" ? true : false;
    }
  }

  isNamingRepeated(index, naming, fase) {
    let flag = false;
    if (fase == 'FieldReview') {
      for (let i = 0; i < this.fieldsTable.length && !flag; i++) {
        if (this.fieldsTable[i].naming.naming === naming && index != i) {
          flag = true;
          break;
        }
      }
    } else if (fase == 'fieldsRaw') {
      for (let i = 0; i < this.fieldsRaw.length && !flag; i++) {
        if (this.fieldsRaw[i].naming.naming === naming && index != i) {
          flag = true;
          break;
        }
      }
    } else if (fase == 'fieldsMaster') {
      for (let i = 0; i < this.fieldsMaster.length && !flag; i++) {
        if (this.fieldsMaster[i].naming.naming === naming && index != i) {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  commentHistorial(row, fase) {
    const dialogConfiguration = new MatDialogConfig();
    dialogConfiguration.disableClose = true;
    dialogConfiguration.autoFocus = true;
    dialogConfiguration.data = {
      column: row.column + 1,
      comment: row.comments
    };
    this.dialog.open(CommentHistorialComponent, dialogConfiguration);
  }

  updateFields(fase) {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/fields/' + fase + '/').subscribe(
      (data: any) => {
        if (fase == "fieldsRaw") {
          this.fieldsRaw = data;
        } else if (fase == "fieldsMaster") {
          this.fieldsMaster = data;
        } else if (fase == "FieldReview") {
          this.fieldsTable = data;
        }
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  validateNamingLogicDescriptionFieldReviewGlobal(row, fase) {
    let isValidate = false;
    if (this.isNamingRepeated(row.column, row.naming.naming, fase)) {
      this.errors.push("Naming repetido." + "\n");
      isValidate = false;
    } else {
      if (this.fieldsTable[row.column].length <= 0) this.errors.push("Por favor, diligenciar el campo longitud." + "\n");
      if (this.fieldsTable[row.column].originType == "empty") this.errors.push("Por favor, diligenciar el campo tipo de origen." + "\n");
      if (this.fieldsTable[row.column].destinationType == "empty") this.errors.push("Por favor, diligenciar el campo longitud." + "\n");
      if (this.fieldsTable[row.column].legacy.legacy == "empty") this.errors.push("Por favor, diligenciar el campo legacy." + "\n");
      if (this.fieldsTable[row.column].legacy.legacyDescription == "empty") this.errors.push("Por favor, diligenciar el campo descripción legacy." + "\n");

      if (this.fieldsTable[row.column].length > 0 &&
        this.fieldsTable[row.column].originType != "empty" &&
        this.fieldsTable[row.column].destinationType != "empty" &&
        this.fieldsTable[row.column].legacy.legacy != "empty" &&
        this.fieldsTable[row.column].legacy.legacyDescription != "empty") {
        if (this.fieldsTable[row.column].destinationType.search("DECIMAL") >= 0) {
          if (this.fieldsTable[row.column].decimals < 1 && this.fieldsTable[row.column].integers < 1) {
            isValidate = false;
            this.errors.push("Por favor, diligencie los valores decimales y enteros." + "\n");
          } else {
            isValidate = true;
          }
        } else {
          isValidate = true;
        }
      } else {
        isValidate = false

      }
    }
    return isValidate;
  }

  validateNamingLogicDescriptionRawGlobal(row, fase) {
    let isValidate = false;
    if (this.isNamingRepeated(row.column, row.naming.naming, fase)) {
      this.errors.push("Naming repetido." + "\n");
      isValidate = false;
    } else {
      if (this.fieldsRaw[row.column].legacy.legacy == "empty") this.errors.push("Por favor, diligenciar el campo tipo de destino." + "\n");
      if (this.fieldsRaw[row.column].legacy.legacyDescription == "empty") this.errors.push("Por favor, diligenciar el campo del nombre origen del naming." + "\n");

      if (this.fieldsRaw[row.column].legacy.legacy != "empty" &&
        this.fieldsRaw[row.column].legacy.legacyDescription != "empty") {
        isValidate = true;
      } else {
        isValidate = false
        this.errors.push("El campo del nombre origen del naming y el tipo destino deben diligenciarse." + "\n");
      }
    }
    return isValidate;
  }

  validateNamigLogicDescriptionMasterGlobal(row, fase) {
    let isValidate = false;
    if (this.isNamingRepeated(row.column, row.naming.naming, fase)) {
      this.errors.push("Naming repetido." + "\n");
      isValidate = false;
    } else {
      if (this.fieldsMaster[row.column].destinationType == "empty") this.errors.push("Por favor, diligenciar el campo tipo de destino." + "\n");
      if (this.fieldsMaster[row.column].legacy.legacy == "empty") this.errors.push("Por favor, diligenciar el campo del nombre origen del naming." + "\n");
      if (
        this.fieldsMaster[row.column].destinationType != "empty" &&
        this.fieldsMaster[row.column].legacy.legacy != "empty") {

        isValidate = true;

      } else {
        isValidate = false
        this.errors.push("El campo del nombre origen del naming y el tipo destino deben diligenciarse." + "\n");
      }
    }
    return isValidate;
  }

  commentsWindowsVoBo(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      vobo: true,
      fase: "fieldsRaw",
      user: sessionStorage.getItem("userName"),
      userRol: sessionStorage.getItem("rol"),
      row: row.check,
      naming: row.naming,
      column: row.column,
      idTable: this.idTable
    };
    this.dialog.open(CommentComponentComponent, dialogConfig);
  }

  validateNamigLogicDescriptionFieldReview(row, suffix) {
    let isValidate = false;
    if (this.fieldsTable[row.column].naming.naming == "empty") this.errors.push("Por favor, diligenciar el campo naming" + "\n");
    if (this.fieldsTable[row.column].logic == "empty") this.errors.push("Por favor, diligenciar el campo nombre lógico" + "\n");
    if (this.fieldsTable[row.column].description == "empty") this.errors.push("Por favor, diligenciar el campo descripción" + "\n");

    if (this.fieldsTable[row.column].naming.naming != "empty" &&
      this.fieldsTable[row.column].description != "empty" &&
      this.fieldsTable[row.column].logic != "empty") {
      if (this.fieldsTable[row.column].description.toUpperCase() == this.fieldsTable[row.column].logic) {
        isValidate = false;
        this.errors.push("El nombre logico y la descripcion son iguales." + "\n");
      } else {
        for (let i = 0; i < this.suffixes.length; i++) {
          if (suffix == this.suffixes[i].suffix) {
            let flagS = false;
            let flagD = false;
            for (let j = 0; j < this.suffixes[i].Logic.length && !flagS; j++) {
              if (this.fieldsTable[row.column].logic.startsWith(this.suffixes[i].Logic[j])) {
                flagS = true;
              }
            }
            for (let j = 0; j < this.suffixes[i].DESCRIPTION.length && !flagD; j++) {
              if (this.fieldsTable[row.column].description.toUpperCase().startsWith(this.suffixes[i].DESCRIPTION[j])) {
                flagD = true;
              }
            }
            if (!flagS) {
              this.errors.push("El nombre lógico no concuerda con el sufijo del naming." + "\n");
            }
            if (!flagD) {
              this.errors.push("La descripción no concuerda con el sufijo del naming." + "\n");
            }
            if (flagS && flagD) {
              isValidate = true;
            }
            break;
          }
        }
      }
    } else {
      isValidate = false;
    }
    return isValidate;
  }

  validateNamigLogicDescriptionRaw(row, suffix) {
    let isValidate = false;
    if (this.fieldsRaw[row.column].naming.naming == "empty") this.errors.push("Por favor, diligenciar el campo naming" + "\n");
    if (this.fieldsRaw[row.column].logic == "empty") this.errors.push("Por favor, diligenciar el campo nombre lógico" + "\n");
    if (this.fieldsRaw[row.column].description == "empty") this.errors.push("Por favor, diligenciar el campo descripción" + "\n");

    if (this.fieldsRaw[row.column].naming.naming != "empty" &&
      this.fieldsRaw[row.column].description != "empty" &&
      this.fieldsRaw[row.column].logic != "empty") {
      if (this.fieldsRaw[row.column].description.toUpperCase() == this.fieldsRaw[row.column].logic) {
        isValidate = false;
        this.errors.push("El nombre logico y la descripcion son iguales." + "\n");
      } else {
        for (let i = 0; i < this.suffixes.length; i++) {
          if (suffix == this.suffixes[i].suffix) {
            let flagS = false;
            let flagD = false;
            for (let j = 0; j < this.suffixes[i].Logic.length && !flagS; j++) {
              if (this.fieldsRaw[row.column].logic.startsWith(this.suffixes[i].Logic[j])) {
                flagS = true;
              }
            }
            for (let j = 0; j < this.suffixes[i].DESCRIPTION.length && !flagD; j++) {
              if (this.fieldsRaw[row.column].description.toUpperCase().startsWith(this.suffixes[i].DESCRIPTION[j])) {
                flagD = true;
              }
            }
            if (!flagS) {
              this.errors.push("El nombre lógico no concuerda con el sufijo del naming." + "\n");
            }
            if (!flagD) {
              this.errors.push("La descripción no concuerda con el sufijo del naming." + "\n");
            }
            if (flagS && flagD) {
              isValidate = true;
            }
            break;
          }
        }
      }
    } else {
      isValidate = false;
    }
    return isValidate;
  }

  validateNamigLogicDescriptionMaster(row, suffix) {
    let isValidate = false;
    if (this.fieldsMaster[row.column].naming.naming == "empty") this.errors.push("Por favor, diligenciar el campo naming" + "\n");
    if (this.fieldsMaster[row.column].logic == "empty") this.errors.push("Por favor, diligenciar el campo nombre lógico" + "\n");
    if (this.fieldsMaster[row.column].description == "empty") this.errors.push("Por favor, diligenciar el campo descripción" + "\n");

    if (this.fieldsMaster[row.column].naming.naming != "empty" &&
      this.fieldsMaster[row.column].description != "empty" &&
      this.fieldsMaster[row.column].logic != "empty") {
      if (this.fieldsMaster[row.column].description.toUpperCase() == this.fieldsMaster[row.column].logic) {
        isValidate = false;
        this.errors.push("El nombre logico y la descripcion son iguales." + "\n");
      } else {
        for (let i = 0; i < this.suffixes.length; i++) {
          if (suffix == this.suffixes[i].suffix) {
            let flagS = false;
            let flagD = false;
            for (let j = 0; j < this.suffixes[i].Logic.length && !flagS; j++) {
              if (this.fieldsMaster[row.column].logic.startsWith(this.suffixes[i].Logic[j])) {
                flagS = true;
              }
            }
            for (let j = 0; j < this.suffixes[i].DESCRIPTION.length && !flagD; j++) {
              if (this.fieldsMaster[row.column].description.toUpperCase().startsWith(this.suffixes[i].DESCRIPTION[j])) {
                flagD = true;
              }
            }
            if (!flagS) {
              this.errors.push("El nombre lógico no concuerda con el sufijo del naming." + "\n");
            }
            if (!flagD) {
              this.errors.push("La descripción no concuerda con el sufijo del naming." + "\n");
            }
            if (flagS && flagD) {
              isValidate = true;
            }
            break;
          }
        }
      }
    } else {
      isValidate = false;
      this.errors.push("Por favor, diligencie el naming, el nombre lógico y la descripción. " + "\n");
    }
    return isValidate;
  }

  rowValidation(row, fase): boolean {
    let isValidate = false;
    this.errors.push(row.column + 1 + " : \n");
    if (fase == 'FieldReview') {
      this.validateFieldWithOutStatistics(row, fase);
      //let tmpSuffix = this.fieldsTable[row.column].naming.naming.split('_');
      let tmpSuffix = row.naming.naming.split('_');
      let index = tmpSuffix.indexOf('');
      let suffix = tmpSuffix[tmpSuffix.length - 1];
      if (index === -1) {
        if (row.naming.naming != "empty") {
          this.checkNamingGlobal(row.column, row.naming.naming.trim(), fase);
          if (row.naming.isGlobal == 'N') {
            isValidate = this.validateNamigLogicDescriptionFieldReview(row, suffix);
          } else {
            isValidate = this.validateNamingLogicDescriptionFieldReviewGlobal(row, fase);
          }
        } else {
          this.errors.push("El naming propuesto para el campo, se encuentra vacío. \n");
          isValidate = false;
        }
      } else {
        this.errors.push("Por favor revisar el naming. El símbolo '_' está repetido. \n");
      }
    } else if (fase == 'fieldsRaw') {
      this.validateFieldWithOutStatistics(row, fase);
      let tmpSuffix = row.naming.naming.split('_');
      let index = tmpSuffix.indexOf('');
      let suffix = tmpSuffix[tmpSuffix.length - 1];
      if (index === -1) {
        if (row.naming.naming != "empty") {
          this.checkNamingGlobal(row.column, row.naming.naming.trim(), fase);
          if (row.naming.isGlobal == 'N') {
            isValidate = this.validateNamigLogicDescriptionRaw(row, suffix);
          } else {
            isValidate = this.validateNamingLogicDescriptionRawGlobal(row, fase);
          }
        } else {
          this.errors.push("El naming propuesto para el campo, se encuentra vacío. \n");
          isValidate = false;
        }
      } else {
        this.errors.push("Por favor revisar el naming. El símbolo '_' está repetido. \n");
      }

    } else if (fase == 'fieldsMaster') {
      this.validateFieldWithOutStatistics(row, fase);
      let tmpSuffix = row.naming.naming.split('_');
      let index = tmpSuffix.indexOf('');
      let suffix = tmpSuffix[tmpSuffix.length - 1];
      if (index === -1) {
        if (row.naming.naming != "empty") {
          this.checkNamingGlobal(row.column, row.naming.naming.trim(), fase);
          if (row.naming.isGlobal == 'N') {
            isValidate = this.validateNamigLogicDescriptionMaster(row, suffix);
          } else {
            isValidate = this.validateNamigLogicDescriptionMasterGlobal(row, fase);
          }
        } else {
          this.errors.push("El naming propuesto para el campo, se encuentra vacío. \n");
          isValidate = false;
        }
      } else {
        this.errors.push("Por favor revisar el naming. El símbolo '_' está repetido. \n");
      }
    }
    return isValidate;
  }


  stateValidation(row, fase) {
    this.loadingIndicatorV = true;
    this.loadingIndicatorR = true;
    this.loadingIndicatorM = true;
    this.errors = [];
    if (this.rowValidation(row, fase)) {
      this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=' + this.userId + '/', JSON.stringify({ row: row })).subscribe(
        (data: any) => {
          this.errors.push("Campo actualizado." + "\n");
          this.updateFields(fase);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    }
    this.errors.unshift("Campos válidos actualizados." + "\n");
    const dialogErr = new MatDialogConfig();
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.data = this.errors;
    this.dialog.open(ErrorsComponent, dialogErr);
    this.loadingIndicatorV = false;
    this.loadingIndicatorR = false;
    this.loadingIndicatorM = false;
  }

  allValidationsFieldReview() {
    var arr = [];
    this.loadingIndicatorV = true;
    this.errors = [];
    var final = 0;
    if ((this.tableOffset + 1) * 5 > this.fieldsTable.length) {
      final = this.fieldsTable.length;
    } else {
      final = (this.tableOffset + 1) * 5;
    }
    //for (let i = ((this.tableOffset + 1) * 5) - 5; i < final; i++) {
    for(let i = 0 ; i < this.fieldsTable.length ; i++){
      if (this.rowValidation(this.fieldsTable[i], 'FieldReview')) {
        this.errors.push("Campo válido." + "\n");
        arr.push(this.fieldsTable[i]);
      } else {
        this.errors.push("Campo inválido." + "\n");
      }
    }
    this.updateAllValidations(arr, 'FieldReview');
    this.loadingIndicatorV = false;
  }

  allValidationsRaw() {
    var arr = [];
    this.loadingIndicatorR = true;
    this.errors = [];
    var final = 0;
    if ((this.tableOffsetR + 1) * 5 > this.fieldsRaw.length) {
      final = this.fieldsRaw.length;
    } else {
      final = (this.tableOffsetR + 1) * 5;
    }
    //for (let i = ((this.tableOffsetR + 1) * 5) - 5; i < final; i++) {
    for(let i = 0 ; i < this.fieldsRaw.length ; i++){
      if (this.rowValidation(this.fieldsRaw[i], 'fieldsRaw')) {
        this.errors.push("Campo válido." + "\n");
        arr.push(this.fieldsRaw[i]);
      } else {
        this.errors.push("Campo inválido." + "\n");
      }
    }
    this.updateAllValidations(arr, 'fieldsRaw');
    this.loadingIndicatorR = false;
  }

  allValidationsMaster() {
    var arr = [];
    this.loadingIndicatorM = true;
    this.errors = [];
    var final = 0;
    if ((this.tableOffsetM + 1) * 5 > this.fieldsMaster.length) {
      final = this.fieldsMaster.length;
    } else {
      final = (this.tableOffsetM + 1) * 5;
    }
    //for (let i = ((this.tableOffsetM + 1) * 5) - 5; i < final; i++) {
    for(let i = 0 ; i < this.fieldsMaster.length ; i++){
      if (this.rowValidation(this.fieldsMaster[i], 'fieldsMaster')) {
        this.errors.push("Campo válido." + "\n");
        arr.push(this.fieldsMaster[i]);
      } else {
        this.errors.push("Campo inválido." + "\n");
      }
    }
    this.updateAllValidations(arr, 'fieldsMaster');
    this.loadingIndicatorM = false;
  }

  updateAllValidations(fields, fase) {
    if (fields.length > 0) {
      this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=' + this.userId + '/', JSON.stringify({ all: "", row: fields }))
        .subscribe(
          (data: any) => {
            this.updateFields(fase);
            this.errors.push("Campos válidos actualizados." + "\n");
          }, (error) => {
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          }
        );
    }
  }

  allValidations(fase) {
    this.loadingIndicatorV = true;
    this.loadingIndicatorR = true;
    this.loadingIndicatorM = true;
    if (fase == 'FieldReview') {
      this.allValidationsFieldReview();
    } else if (fase == 'fieldsRaw') {
      this.allValidationsRaw();
    } else if (fase == 'fieldsMaster') {
      this.allValidationsMaster();
    }
    this.errors.unshift("Campos válidos actualizados." + "\n");
    const dialogErr = new MatDialogConfig();
    dialogErr.height = "87%";
    dialogErr.width = "60%";
    dialogErr.disableClose = true;
    dialogErr.autoFocus = true;
    dialogErr.data = this.errors;
    this.dialog.open(ErrorsComponent, dialogErr);
    this.loadingIndicatorV = false;
    this.loadingIndicatorR = false;
    this.loadingIndicatorM = false;
  }

  cleanP() {
    this.NamingGlobalSearchP = "";
    this.LogicGlobalSearchP = "";
    this.DescGlobalSearchP = "";
    this.LegacySearchP = "";
    this.DescLegacySearchP = "";
    this.selectedTypeP = "";
    this.selectorSuffixP.value = "";
    this.selectorFilterP.value = "";
    this.selectedStateFilterP = "";
    this.table.offset = 0;
    this.getFields('FieldReview');
  }

  cleanR() {
    this.NamingGlobalSearchR = "";
    this.LogicGlobalSearchR = "";
    this.DescGlobalSearchR = "";
    this.LegacySearchR = "";
    this.DescLegacySearchR = "";
    this.selectedTypeR = "";
    this.selectorSuffixR.value = "";
    this.selectorFilterR.value = "";
    this.selectedStateFilterR = "";
    this.tableRaw.offset = 0;
    this.getFields('fieldsRaw');
  }

  cleanM() {
    this.NamingGlobalSearchM = "";
    this.LogicGlobalSearchM = "";
    this.DescGlobalSearchM = "";
    this.LegacySearchM = "";
    this.DescLegacySearchM = "";
    this.selectedTypeM = "";
    this.selectorSuffixM.value = "";
    this.selectorFilterM.value = "";
    this.selectedStateFilterM = "";
    this.tableMaster.offset = 0;
    this.getFields('fieldsMaster');
  }

  getFieldsQueryP() {
    this.loadingIndicatorV = true;
    var NamingGlobalSearchTmp = this.NamingGlobalSearchP;
    var LogicGlobalSearchTmp = this.LogicGlobalSearchP;
    var DescGlobalSearchTmp = this.DescGlobalSearchP;
    var LegacySearchTmp = this.LegacySearchP;
    var DescLegacyTmp = this.DescLegacySearchP;

    if (LegacySearchTmp.length == 0 && DescLegacyTmp.length < 2 && NamingGlobalSearchTmp.length == 0 && LogicGlobalSearchTmp.length == 0 && DescGlobalSearchTmp.length < 2 && this.selectedTypeP.length < 0 && this.selectedStateFilterP.length < 0) {
      this.loadingIndicatorV = false;
      this.cleanP();
      this.openSnackBar("Por favor, ingrese los datos de búsqueda.", "Error");
    } else {
      if (NamingGlobalSearchTmp.length == 0) {
        NamingGlobalSearchTmp = "%20";
      }
      if (LogicGlobalSearchTmp.length == 0) {
        LogicGlobalSearchTmp = "%20";
      }
      if (DescGlobalSearchTmp.length == 0) {
        DescGlobalSearchTmp = "%20";
      }
      if (LegacySearchTmp.length == 0) {
        LegacySearchTmp = "%20";
      }
      if (DescLegacyTmp.length == 0) {
        DescLegacyTmp = "%20";
      }
      if (this.selectedTypeP.length == 0) {
        this.selectedTypeP = "%20";
      }
      if (this.selectedStateFilterP.length == 0) {
        this.selectedStateFilterP = "%20";
      }

      this.rest.getRequest('dataDictionaries/' + this.idTable + '/fases/FieldReview/naming=' + NamingGlobalSearchTmp + '&logic=' + LogicGlobalSearchTmp + '&desc=' + DescGlobalSearchTmp + '&state=' + this.selectedStateFilterP + '&legacy=' + LegacySearchTmp + '&descLegacy=' + DescLegacyTmp + '&suffix=' + this.selectedTypeP + '/')
        .subscribe(
          (data: any) => {
            this.fieldsTable = data;
            setTimeout(() => { this.loadingIndicatorV = false; }, 1000);
          }, (error) => {
            this.loadingIndicatorV = false;
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          });
    }
    this.table.offset = 0;
  }

  getFieldsQueryRaw() {
    this.loadingIndicatorR = true;
    var NamingGlobalSearchTmp = this.NamingGlobalSearchR;
    var LogicGlobalSearchTmp = this.LogicGlobalSearchR;
    var DescGlobalSearchTmp = this.DescGlobalSearchR;
    var LegacySearchTmp = this.LegacySearchR;
    var DescLegacyTmp = this.DescLegacySearchR;
    if (LegacySearchTmp.length == 0 && DescLegacyTmp.length < 2 && NamingGlobalSearchTmp.length == 0 && LogicGlobalSearchTmp.length == 0 && DescGlobalSearchTmp.length < 2 && this.selectedTypeR.length < 0 && this.selectedStateFilterR.length < 0) {
      this.loadingIndicatorR = false;
      this.cleanR();
      this.openSnackBar("Por favor, ingrese los datos de búsqueda.", "Error");
    } else {

      if (NamingGlobalSearchTmp.length == 0) {
        NamingGlobalSearchTmp = "%20";
      }
      if (LogicGlobalSearchTmp.length == 0) {
        LogicGlobalSearchTmp = "%20";
      }
      if (DescGlobalSearchTmp.length == 0) {
        DescGlobalSearchTmp = "%20";
      }
      if (LegacySearchTmp.length == 0) {
        LegacySearchTmp = "%20";
      }
      if (DescLegacyTmp.length == 0) {
        DescLegacyTmp = "%20";
      }
      if (this.selectedTypeR.length == 0) {
        this.selectedTypeR = "%20";
      }
      if (this.selectedStateFilterR.length == 0) {
        this.selectedStateFilterR = "%20";
      }
      this.rest.getRequest('dataDictionaries/' + this.idTable + '/fases/fieldsRaw/naming=' + NamingGlobalSearchTmp + '&logic=' + LogicGlobalSearchTmp + '&desc=' + DescGlobalSearchTmp + '&state=' + this.selectedStateFilterR + '&legacy=' + LegacySearchTmp + '&descLegacy=' + DescLegacyTmp + '&suffix=' + this.selectedTypeR + '/')
        .subscribe(
          (data: any) => {
            this.fieldsRaw = data;
            setTimeout(() => { this.loadingIndicatorR = false; }, 1000);
          }, (error) => {
            this.loadingIndicatorV = false;
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          });
    }
    this.tableRaw.offset = 0;
  }

  getFieldsQueryMaster() {
    this.loadingIndicatorM = true;
    var NamingGlobalSearchTmp = this.NamingGlobalSearchM;
    var LogicGlobalSearchTmp = this.LogicGlobalSearchM;
    var DescGlobalSearchTmp = this.DescGlobalSearchM;
    var LegacySearchTmp = this.LegacySearchM;
    var DescLegacyTmp = this.DescLegacySearchM;
    if (LegacySearchTmp.length == 0 && DescLegacyTmp.length < 2 && NamingGlobalSearchTmp.length == 0 && LogicGlobalSearchTmp.length == 0 && DescGlobalSearchTmp.length < 2 && this.selectedTypeM.length < 0 && this.selectedStateFilterM.length < 0) {
      this.loadingIndicatorM = false;
      this.cleanM();
      this.openSnackBar("Por favor, ingrese los datos de búsqueda.", "Error");
    } else {

      if (NamingGlobalSearchTmp.length == 0) {
        NamingGlobalSearchTmp = "%20";
      }
      if (LogicGlobalSearchTmp.length == 0) {
        LogicGlobalSearchTmp = "%20";
      }
      if (DescGlobalSearchTmp.length == 0) {
        DescGlobalSearchTmp = "%20";
      }
      if (LegacySearchTmp.length == 0) {
        LegacySearchTmp = "%20";
      }
      if (DescLegacyTmp.length == 0) {
        DescLegacyTmp = "%20";
      }
      if (this.selectedTypeM.length == 0) {
        this.selectedTypeM = "%20";
      }
      if (this.selectedStateFilterM.length == 0) {
        this.selectedStateFilterM = "%20";
      }
      this.rest.getRequest('dataDictionaries/' + this.idTable + '/fases/fieldsMaster/naming=' + NamingGlobalSearchTmp + '&logic=' + LogicGlobalSearchTmp + '&desc=' + DescGlobalSearchTmp + '&state=' + this.selectedStateFilterM + '&legacy=' + LegacySearchTmp + '&descLegacy=' + DescLegacyTmp + '&suffix=' + this.selectedTypeM + '/')
        .subscribe(
          (data: any) => {
            this.fieldsMaster = data;
            setTimeout(() => { this.loadingIndicatorM = false; }, 1000);
          }, (error) => {
            this.loadingIndicatorV = false;
            this.openSnackBar("Error procesando petición al servidor.", "Error");
          });
    }
    this.tableMaster.offset = 0;
  }

  toggle(col) {
    this.columns.find(c => {
      if (c.name === col) return c.visible = !c.visible;
    });
  }

  isChecked(col) {
    return this.columns.find(c => {
      if (c.name === col) return c.visible;
    });
  }

  toggleAllP() {
    this.showAllColumnsP = !this.showAllColumnsP;
    if (this.showAllColumnsP) {
      for (let i = 0; i < this.columns.length; i++) {
        this.columns[i].visible = true;
      }
    } else {
      for (let i = 0; i < this.columns.length; i++) {
        this.columns[i].visible = false;
      }
    }
    this.fieldsTable = [...this.fieldsTable];
    this.changeDetectorRef.detectChanges();
    return this.showAllColumnsP;
  }

  isCheckedAllP() {
    return this.showAllColumnsP;
  }

  toggleRaw(col) {
    this.columnsRaw.find(c => {
      if (c.name === col) return c.visible = !c.visible;
    });
  }

  isCheckedRaw(col) {
    return this.columnsRaw.find(c => {
      if (c.name === col) return c.visible;
    });
  }

  toggleAllR() {
    this.showAllColumnsR = !this.showAllColumnsR;
    if (this.showAllColumnsR) {
      for (let i = 0; i < this.columnsRaw.length; i++) {
        this.columnsRaw[i].visible = true;
      }
    } else {
      for (let i = 0; i < this.columnsRaw.length; i++) {
        this.columnsRaw[i].visible = false;
      }
    }
    this.fieldsRaw = [...this.fieldsRaw];
    this.changeDetectorRef.detectChanges();
    return this.showAllColumnsR;
  }

  isCheckedAllR() {
    return this.showAllColumnsR;
  }

  toggleMaster(col) {
    this.columnsMaster.find(c => {
      if (c.name === col) return c.visible = !c.visible;
    });
  }

  isCheckedMaster(col) {
    return this.columnsMaster.find(c => {
      if (c.name === col) return c.visible;
    });
  }

  toggleAllM() {
    this.showAllColumnsM = !this.showAllColumnsM;
    if (this.showAllColumnsM) {
      for (let i = 0; i < this.columnsMaster.length; i++) {
        this.columnsMaster[i].visible = true;
      }
    } else {
      for (let i = 0; i < this.columnsMaster.length; i++) {
        this.columnsMaster[i].visible = false;
      }
    }
    this.fieldsMaster = [...this.fieldsMaster];
    this.changeDetectorRef.detectChanges();
    return this.showAllColumnsM;
  }

  isCheckedAllM() {
    return this.showAllColumnsM;
  }

  onChange(event) {
    this.tableOffset = event.offset;
  }

  onChangeR(event) {
    this.tableOffsetR = event.offset;
  }

  onChangeM(event) {
    this.tableOffsetM = event.offset;
  }

  private _changeRowLimits(event) {
    this.rowsLimit = parseInt(event.target.value, 10);
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
    this.tableRaw.limit = this.rowsLimit;
    this.tableRaw.recalculate();
    this.tableMaster.limit = this.rowsLimit;
    this.tableMaster.recalculate();
  }
  
  onPaginated(event) {
    this.table.limit = this.rowsLimit;
    this.table.recalculate();
    this.tableRaw.limit = this.rowsLimit;
    this.tableRaw.recalculate();
    this.tableMaster.limit = this.rowsLimit;
    this.tableMaster.recalculate();
  }

  tabChanged(event) {
    if(event.tab.textLabel == 'Raw') {
      this.getInfoTable("raw");
      this.getFields("fieldsRaw");
    } else if (event.tab.textLabel == 'Master') {
      this.getInfoTable("master");
      this.getFields("fieldsMaster");
    } else if (event.tab.textLabel == 'Propuesta') {
      this.getInfoTable("FieldReview");
      this.getFields("FieldReview");
    }
  }

  addComments(row, fase) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = "275px";
    dialogConfig.minWidth = "1050px";
    dialogConfig.disableClose = false;
    dialogConfig.data = {
      fase: fase,
      user: sessionStorage.getItem("userId"),
      userRol: sessionStorage.getItem("rol"),
      row: row,
      idTable: this.idTable,
      comment: '',
      reason: '',
      checkComment: 0 //Possibly deprecated last three
    };
    const dialogRef = this.dialog.open(CommentComponentComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        var res = {
          comment: result.comment,
          reasonReturn: result.reason,
          checkComment: result.checkComment,
          user: this.userId
        };
        if (fase == 'FieldReview') {
          this.fieldsTable[row.column].comments.push(res);
          this.snackBarValidateComments(row, fase);
        } else if (fase == 'fieldsRaw') {
          this.fieldsRaw[row.column].comments.push(res);
          this.snackBarValidateComments(row, fase);
        } else if (fase == 'fieldsMaster') {
          this.fieldsMaster[row.column].comments.push(res);
          this.snackBarValidateComments(row, fase);
        }
      }
    });
  }

  snackBarValidateComments(row, fase) {
    var a = this.rowValidation(row, fase);
    if (a) {
      this.rest.postRequest('dataDictionaries/' + this.idTable + '/fase=' + fase + '&user=' + this.userId + '/', JSON.stringify({ row: row })).subscribe(
        (data: any) => {
          this.openSnackBar("Campo actualizado.", "Ok");
          this.updateFields(fase);
        }, (error) => {
          this.openSnackBar("Error procesando petición al servidor.", "Error");
        }
      );
    }
  }

  commentsValidationRaw() {
    this.isCommentsRawOk = true;
    let tmpR = [];
    tmpR.push("Reporte fase: Raw \n");
    for (let i = 0; i < this.fieldsRaw.length; i++) {
      if (this.rowValidation(this.fieldsRaw[i], 'fieldsRaw')) {
        if (this.fieldsRaw[i].comments.length <= 0 ||
          this.fieldsRaw[i].comments[this.fieldsRaw[i].comments.length - 1].reasonReturn != "OK") {
          tmpR.push("1:" + "\n");
          tmpR.push("El campo no tiene un comentario aprobatorio" + "\n");
          this.isCommentsRawOk = false;
        }
      } else {
        for (let j = 0; j < this.errors.length; j++) {
          tmpR.push(this.errors[j]);
        }
        this.errors = [];
      }
    }
    if (this.isCommentsRawOk) {
      tmpR = [];
    }
    return tmpR;
  }

  commentsValidationMaster() {
    this.isCommentsMasterOk = true;
    let tmpR = [];
    tmpR.push("Reporte fase: Master \n");
    for (let i = 0; i < this.fieldsMaster.length; i++) {
      if (this.rowValidation(this.fieldsMaster[i], 'fieldsMaster')) {
        if (this.fieldsMaster[i].comments.length <= 0 ||
          this.fieldsMaster[i].comments[this.fieldsMaster[i].comments.length - 1].reasonReturn != "OK") {
          tmpR.push("1:" + "\n");
          tmpR.push("El campo no tiene un comentario aprobatorio" + "\n");
          this.isCommentsMasterOk = false;
        }
      } else {
        for (let j = 0; j < this.errors.length; j++) {
          tmpR.push(this.errors[j]);
        }
        this.errors = [];
      }
    }
    if (this.isCommentsMasterOk) {
      tmpR = [];
    }
    return tmpR;
  }

  historyComment(row) {
    const dialogConfiguration = new MatDialogConfig();
    dialogConfiguration.disableClose = true;
    dialogConfiguration.autoFocus = true;
    dialogConfiguration.data = row.check.comments
    this.dialog.open(ErrorsComponent, dialogConfiguration);
  }

  commentsValidationFieldReview() {
    this.isCommentsFieldReviewOk = true;
    let tmpR = [];
    tmpR.push("Reporte fase: FieldReview \n");
    for (let i = 0; i < this.fieldsTable.length; i++) {
      if (this.rowValidation(this.fieldsTable[i], 'FieldReview')) {
        if (this.fieldsTable[i].comments.length <= 0 ||
          this.fieldsTable[i].comments[this.fieldsTable[i].comments.length - 1].reasonReturn != "OK") {
          tmpR.push("1:" + "\n");
          tmpR.push("El campo no tiene un comentario aprobatorio" + "\n");
          this.isCommentsFieldReviewOk = false;
        }
      } else {
        for (let j = 0; j < this.errors.length; j++) {
          tmpR.push(this.errors[j]);
        }
        this.errors = [];
      }
    }
    if (this.isCommentsFieldReviewOk) {
      tmpR = [];
    }
    return tmpR;
  }

  sendTableArq() {
    if (confirm("Se enviará la tabla para revisión en arquitectura. ¿Desea continuar?")) {
      if (!this.stateArchitecture) {
        this.rest.putRequest('dataDictionaries/' + this.idTable + '/state/', JSON.stringify({ from: "governance", to: "architecture" }))
          .subscribe(
            (data: any) => {
              this.openSnackBar("Se ha enviado la tabla para revisión en arquitectura. ", "Ok");
              this.stateArchitecture = true;
              this.router.navigate(['/home']);
            }, (error) => {
              this.openSnackBar("No se ha podido enviar la tabla para revisión en arquitectura.", "Error");
            });

      } else {
        this.openSnackBar("La tabla ya fue enviada a revisión.", "Error");
      }
    }
  }

  closeTable() {
    if (confirm("Se enviará la tabla al estado 'lista para ingestar'. ¿Desea continuar?")) {
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/state/', JSON.stringify({ from: "architecture", to: "ingesta" })).subscribe(
        (data: any) => {
          this.openSnackBar("Se ha enviado la tabla para revisión en ingesta. ", "Ok");
          this.stateArchitecture = true;
          this.router.navigate(['/home']);
        }, (error) => {
          this.openSnackBar("No se ha podido enviar la tabla para revisión en ingesta.", "Error");
        });
    }
  }

  sendTableIng() {
    if (confirm("Se devolverá la tabla para revisión en Ingesta. ¿Desea continuar?")) {
      if (!this.stateArchitecture) {
        this.rest.putRequest('dataDictionaries/' + this.idTable + '/state/', JSON.stringify({ from: "governance", to: "ingesta" })).subscribe(
          (data: any) => {
            this.openSnackBar("Se ha enviado la tabla para revisión en ingesta. ", "Ok");
            this.stateArchitecture = true;
            this.router.navigate(['/home']);
          }, (error) => {
            this.openSnackBar("No se ha podido enviar la tabla para revisión en ingesta.", "Error");
          });
      } else {
        this.openSnackBar("La tabla ya fue enviada a revisión.", "Error");
      }
    }
  }

  sendTableVobo() {
    if (confirm("La tabla será enviada para VoBo de Negocio. ¿Desea continuar?")) {
      this.rest.putRequest('dataDictionaries/' + this.idTable + '/state/', JSON.stringify({ from: "governance", to: "business" })).subscribe(
        (data: any) => {
          this.openSnackBar("Se ha enviado la tabla para revisión en ingesta. ", "Ok");
          this.stateArchitecture = true;
          this.router.navigate(['/home']);
        }, (error) => {
          this.openSnackBar("No se ha podido enviar la tabla para revisión en ingesta.", "Error");
        });
    }
  }

  generateExcels() {
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/excels/Object/').subscribe(
      (data: any) => {
        data = this.cleanNewValuesExcelsObject(data);
        this.exportAsExcelFileObject(data, this.tableData.project_name + "-" + this.tableData.alias + "-Object");
        this.openSnackBar("Generación exitosa de los Object. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
    this.rest.getRequest('dataDictionaries/' + this.idTable + '/excels/Fields/').subscribe(
      (data: any) => {
        data = this.cleanNewValuesExcelsFields(data);
        this.exportAsExcelFileFields(data, this.tableData.project_name + "-" + this.tableData.alias + "-Fields");
        this.openSnackBar("Generación exitosa de los Fields. ", "Ok");
      }, (error) => {
        this.openSnackBar("Error procesando petición al servidor.", "Error");
      }
    );
  }

  cleanNewValuesExcelsFields(data: any){
    for (let row of data)
      if(row["STORAGE ZONE"])
        if(row["STORAGE ZONE"] === "RAWDATA")
          if(!row["TOKENIZED AT DATA SOURCE"])
            row["TOKENIZED AT DATA SOURCE"] = "NO"
    
    return data;
  }

  cleanNewValuesExcelsObject(data: any){
    for (let row of data)
      if(!row["SECURITY LEVEL"])
        row["SECURITY LEVEL"] = "";
    
    return data;
  }

  exportAsExcelFileObject(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-O': worksheet }, SheetNames: ['DDNG-O'] };
    XLSX.writeFile(workbook, GenerateNamingsComponent.toExportFileName(excelFileName));
  }

  exportAsExcelFileFields(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'DDNG-F': worksheet }, SheetNames: ['DDNG-F'] };
    XLSX.writeFile(workbook, GenerateNamingsComponent.toExportFileName(excelFileName));
  }


  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }


  onFileChange(event) {
    this.fileList = event.target.files[0];
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 1000;
    if (action == "Ok") {
      config.panelClass = ['sucessMessage'];
    } else {
      config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }

  //Devuelve el color de la fila si este es generado
  getRowClass(row: any) {
    return {
      'generatedRow': row.origin == ""
    };
  }

  //Devuelve la clase CSS dependiendo del tipo de naming
  getCellClass({ row, column, value }) {
    if(row.modification.length > 0){
      if(row.modification[0].stateValidation)
        if(row.modification[0].stateValidation === 'NO')
          return { 'is-wrong': true };
      if (row.modification[0].state == 'GL'){
        return { 'is-global': true };
      } else if (row.modification[0].state == 'GC'){
        return { 'is-common-global': true };
      } else if (row.modification[0].state == 'PS'){
        return { 'is-proposal': true };
      } else if (row.modification[0].state == 'PC'){
        return { 'is-common-proposal': true };
      } 
    }
  }

  getCellClassState({ row, column, value }) {
    return row.naming.governanceState === "C" ? {'naming-closed': true} :  {'naming-open': true};  
  }

  getCellClassLegacy({ row, column, value }) {
    if (row.legacy.repeat == 'Y') {
      return { 'legacy-repeated': true }
    }
  }

  getStatusRow({ row, column, value }) {
    if (row.check.status == 'NOK') {
      return { 'is-nok': true };
    } else if (row.check.status == 'OK') {
      return { 'is-ok': true };
    }
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para namings
  public inputValidatorNaming(event: any) {
    const pattern = /^[a-z0-9_]*$/;   
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-z0-9_]/g, "");
    }
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos para logicos
  public inputValidatorLogic(event: any) {
    const pattern = /^[a-zA-Z0-9 ]*$/;   
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9 ]/g, "");
    }
  }

  //Valida la entrada y modifica el texto para que no se digiten caracteres incorrectos aparte de numeros
  public inputValidatorNumbers(event: any) {
    const pattern = /^[0-9]*$/;   
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
  }
  

}

