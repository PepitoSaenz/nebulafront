
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestService } from '../rest.service';
import * as CanvasJS from './canvasjs.min';

@Component({
  selector: 'app-statistics-dialog',
  templateUrl: './statistics-dialog.component.html',
  styleUrls: ['./statistics-dialog.component.scss']
})
export class StatisticsDialogComponent implements OnInit {

  usecase:any;

  constructor(@Inject(MAT_DIALOG_DATA) public usecases: any,public rest:RestService) { 
    this.usecase = usecases;
  }

  ngOnInit() {
    this.drawStateTable(this.usecase.data, "", "chartContainer");
  }

  drawStateTable(dataToPaint, title, divshowgraphs){
    let seeData = new CanvasJS.Chart(divshowgraphs, {
      theme: "light2",
		  animationEnabled: true,
		  exportEnabled: true,
		  title:{
			  text: title
      },
      data:[{
        type: "pie",
        showInLegend: true,
        toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
        indexLabel: "{name} - #percent%",
        dataPoints: dataToPaint
      }]
    });
    seeData.render();
  }
}
