import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../rest.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import * as XLSX from "xlsx";


@Component({
  selector: 'app-global-query',
  templateUrl: './global-query.component.html',
  styleUrls: ['./global-query.component.scss']
})
export class GlobalQueryComponent implements OnInit {

  namingSearch: string = "";
  logicSearch: string = "";
  descriptionSearch: string = "";
  selectedType: string = "";
  selectedLevel: string = undefined;

  fields: any = [];
  suffixes: any = [];
  levels: any = [];
  generalStatInfo: any = {};
  
  fieldsNumber: string;
  expandIcon = "expand_more";
  loadingExport: boolean = false;
  loadingIndicator: boolean = false;

  restObject: any;
  barConfig: MatSnackBarConfig = new MatSnackBarConfig();

  @ViewChild('myTable') table: any;
  @ViewChild('selectSuffix') selectorSuffix: any;
  @ViewChild('selectLevel') selectorLevel: any;
  @ViewChild('selectExport') selectorExport: any;


  constructor(public rest: RestService, private changeDetectorRef: ChangeDetectorRef, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getSuffixesAndHierarchiesAndGeneralStats();
    this.getFieldsQuery();
  }

  //Brings the list of available hierarchy levels and naming suffixes from the DB for queries and sorting
  //Trae estadisticas basi  cas del back sobre las funcionalidades de Nebula
  getSuffixesAndHierarchiesAndGeneralStats() {
    this.rest.getRequest('namings/suffixes/').subscribe(
      (data: {}) => {
        this.suffixes = data;
        this.suffixes.unshift({ suffix: " " });
      });
    this.rest.getRequest('namings/levels/').subscribe(
      (data: {}) => {
        this.levels = data;
        this.levels.unshift("");
      });
    this.rest.getRequest('statistics/general/').subscribe(
      (data: {}) => {
        this.generalStatInfo = data;
      });
  }

  //Verifica si hay una peticion actual sin terminar para finalizarla y dar paso a una nueva peticion al back
  checkCurrentRequestStatus() {
    if (this.loadingIndicator)
      this.restObject.unsubscribe();
    else
      this.loadingIndicator = true;
  }

  //Devuelve la busqueda de namings. Si no hay parametros de busqueda devuelve los primeros 20000 namings en orden.
  //Queries the naming search to the server. if there are not query params the server will return the first 20000 namings in order.
  getFieldsQuery() {
    this.checkCurrentRequestStatus();

    var namingSearchTmp = this.namingSearch.length == 0 ? "%20" : this.namingSearch;
    var descriptionSearchTmp = this.descriptionSearch.length == 0 ? "%20" : this.descriptionSearch;
    var logicSearchTmp = this.logicSearch.length == 0 ? "%20" : this.logicSearch;
    var selectedTypeTmp = this.selectedType.length == 0 ? "%20" : this.selectedType;
    var selectedLevelTmp = this.selectedLevel == undefined || this.selectedLevel.length == 0 ? "%20" : this.selectedLevel;

    this.restObject = this.rest.getRequest('namings/naming=' + namingSearchTmp + '&logic=' + logicSearchTmp
      + '&desc=' + descriptionSearchTmp + '&type=' + selectedTypeTmp + '&level=' + selectedLevelTmp + '/').subscribe(
        (data: any) => {
          for (let i = 0; i < data.length; i++)
            data[i]["originalDesc"] = data[i]["originalDesc"].replace(/~/gi, "\n");
          this.fields = data;
          this.fieldsNumber = Number(this.fields.length).toLocaleString();
          this.loadingIndicator = false;
        });
  }

  //Verifica que los parametros de busqueda tengan algo para buscar, al menos alguno de ellos.
  checkNotEmptySearchParams() {
    if (this.namingSearch.length == 0 && this.selectedType.length == 0
      && this.logicSearch.length == 0 && this.descriptionSearch.length == 0) {
      this.openSnackBar("Indique un naming, lógico o descripción para la búsqueda.", "Error");
      return false;
    }
    return true;
  }

  //Solicita la busqueda inteligente de namings al server. Solo acepta busquedas con parametros en comparacion con el otro tipo de busqueda.
  getFieldsQueryMath() {
    if (this.checkNotEmptySearchParams()) {
      this.checkCurrentRequestStatus();

      var namingSearchTmp = this.namingSearch.length == 0 ? "%20" : this.namingSearch;
      var descriptionSearchTmp = this.descriptionSearch.length == 0 ? "%20" : this.descriptionSearch;
      var selectedTypeTmp = this.selectedType.length == 0 ? "%20" : this.selectedType;

      this.restObject = this.rest.getRequest('namings/machine/naming=' + namingSearchTmp + '&suffix=' + selectedTypeTmp + '&description=' + descriptionSearchTmp + '/').subscribe(
        (data: any) => {
          for (let i = 0; i < data.length; i++)
            data[i]["originalDesc"] = data[i]["originalDesc"].replace(/~/gi, "\n");
          this.fields = data;
          this.fieldsNumber = Number(this.fields.length).toLocaleString();
          this.loadingIndicator = false;
        }, (error) => { this.openSnackBar("Error procesando petición al servidor. " + error, "Error"); });
    }
  }

  //Metodo que selecciona la variable para el select del nivel de jerarquia
  selectedHierarchy(value: any) {
    this.selectedLevel = (value == " " ? "" : value);
  }

  //Metodo que selecciona la variable para el select del suffijo del naming
  selectedSuffix(value: any) {
    this.selectedType = (value == " " ? "" : value);
  }

  //Metodo para el selector de exportacion de namings
  exportNamings(option: number) {
    this.loadingExport = true;
    switch (option) {
      case 1: this.generateNamingsReport();
        break;
      case 2: this.doubleNamingsReport();
        break;
    }
    this.selectorExport.value = "";
  }

  //Exporta y descarga una serie de datos en formato JSON a un archivo de Excel.
  exportAsExcelNamings(json: any[]): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { "DDNG-N": worksheet },
      SheetNames: ["DDNG-N"],
    };
    XLSX.writeFile(workbook, `nebula_namings_export_${new Date().getTime()}.xlsx`);
  }

  //Genera un reporte de namings dados los parametros de busqueda actual
  generateNamingsReport() {
    var params = {
      "naming": this.namingSearch,
      "logic": this.logicSearch,
      "desc": this.descriptionSearch,
      "type": this.selectedType,
      "level": this.selectedLevel == undefined ? "" : this.selectedLevel
    }
    this.rest.postRequest('namings/export/', params).subscribe(
      (data: any) => {
        this.loadingExport = false;
        this.exportAsExcelNamings(data);
        this.openSnackBar("Generación exitosa de los Namings.", "Ok");
      }, (error) => { this.openSnackBar("Error generación " + error, "Error"); });
  }

  //Genera un reporte de namings mas largo con dos peticiones al back
  doubleNamingsReport() {
    var all = []
    this.rest.getRequest('namings/export/limit/100000').subscribe(
      (data: any) => {
        all = data;
        this.rest.getRequest('namings/export/skip/100000').subscribe(
          (data: any) => {
            all = all.concat(data);
            this.loadingExport = false;
            this.exportAsExcelNamings(all);
            this.openSnackBar("Generación exitosa de TODOS los Namings.", "Ok");
          });
      });
  }

  //Limpia y reinicia los valores de búsqueda
  clean() {
    this.fields = [];
    this.namingSearch = "";
    this.logicSearch = "";
    this.descriptionSearch = "";
    this.selectedType = "";
    this.selectedLevel = undefined;
    this.selectorSuffix.value = "";
    this.selectorLevel.value = undefined;
  }

  //Devuelve la clase CSS con el color correspondiente al nivel de jerarquia del naming (row)
  getCellClass({ row }) {
    var level = row.level.toUpperCase();
    switch (level) {
      case "PRINCIPAL": return { 'principal': true };
      case "SECONDARY": return { 'secondary': true };
      case "GLOBAL MODEL": return { 'global-model': true };
      case "GLOBAL MODEL SECONDARY": return { 'global-model-secondary': true };
      case "SPECIFIC": return { 'specific': true };
    }
  }

  //Abre el snack bar para avisos y mensajes cortos
  openSnackBar(message: string, action: string) {
    this.barConfig.duration = 1000;
    if (action == "Ok") { this.barConfig.panelClass = ['sucessMessage']; }
    else { this.barConfig.panelClass = ['errorMessage']; }
    this.snackBar.open(message, action, this.barConfig);
  }

  //Sobreescritura de metodo para ordenar los namings en la tabla de busquedas
  onSort(event: any) {
    this.loadingIndicator = true;
    setTimeout(() => {
      const rows = [...this.fields];
      const sort = event.sorts[0];
      rows.sort((a, b) => {
        let collator = new Intl.Collator();
        return collator.compare(a[sort.prop], b[sort.prop]) * (sort.dir === 'desc' ? -1 : 1);
      });

      this.fields = rows;
      this.loadingIndicator = false;
    }, 1000);
  }

  //Expande la fila actual para ver sus detalles
  toggleExpandRow(row: any, expanded: boolean) {
    this.table.rowDetail.toggleExpandRow(row);
    this.expandIcon = (expanded ? "arrow_drop_down" : "arrow_drop_up");
  }

  //Metodo que copia en el portapapeles la fila enviada (DEPRECATED)
  copyField(row) {
    localStorage.setItem("row", JSON.stringify(row));
    this.openSnackBar("Se ha copiado el campo.", "Ok");
  }

  onDetailToggle(event: any) { }
}

