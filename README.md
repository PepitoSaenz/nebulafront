# GovernanceProjectFrontRest - NEBULA

Este proyecto esta pensado y diseñado como una herramienta que ayude al gobierno de datos en diversas tareas, principalmente en la propuesta de namings del diccionario de objetos logicos como Tablas (DataDictionary) y Tablones (DataTable).
Especificamente es el FrontEnd de este proyecto completo.


## Requerimientos técnicos

El proyecto es generado con una version local de Angular en el mismo repositorio, version 7.0.2., sin embargo es necesario tener instalada una version de Angular mas reciente en el equipo para la administracion de las librerias que se necesitan.

Se necesita NodeJS como base para AngularCLI y un administrador de paquetes como npm o chocolatey. 
La ejecución de `npm install` deberia solucionar las dependencias requeridas. Mas información en el archivo "package-lock.json".

## Conexiones

Este repositorio se concentra en el FrontEnd de NEBULA por lo que las conexiones se hacen al servidor, BackEnd, de Python. Esta conexion actualmente se realiza todo en una maquina local por lo que es necesario tener los tres componentes principales en el mismo equipo, incluyendo aqui a la 
base de datos que se administra con MongoDB.

En el root, se encuentra el archivo `rest.service.ts` el cual maneja las conexiones. El "homeAddress" señala la direccion donde va a estar alojada este proyecto, por defecto y Angular la direccion es [http://localhost:4200/] y la variable "endpoint" señala la IP del servidor BackEnd,
por defecto, [http://localhost:8000/].

Todas las conexiones se hacen mediante servicios REST de parte del BackEnd con transmición de objetos por JSON sin encriptar, con excepcion de datos sensibles como las contraseñas las cuales van a estar codificadas con un hash MD32.

Los otros valores son constantes que se usan para su despliegue en producción, especificamente en el servidor de DATIO actual.

## Detalles técnicos y desarrollo

Este proyecto es la versión de desarrollo del FrontEnd, por lo que para su ejecucion se ejecuta `ng serve` para montar el servidor de desarrollo local, el cual va a estar alojado en la IP por defecto [http://localhost:4200/]. 

Cuando sea momento de construir la version para produccion se necesita el comando `ng build`, sin embargo este proyecto necesita muchos recursos por lo que es recomendable hacer uso del comando 
`node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/ng build --prod --build-optimizer` al momento de construir. Esto permite incrementar el limite del proyectoa mas de 8MB que es lo minímo actual.

Al generar el construido para produccion del proyecto este va a estar en la carpeta `dist` dentro del proyecto el cual se monta en el servidor que ejecuta Nginx para su despliegue. Este repositorio NO aloja este construido, por favor no dejarlo dentro del repo.


## Proyecto y Distribución de los módulos

Como proyecto de Angular este esta construido con componentes que permiten su funcionamiento, por lo que se definieron una serie de reglas básicas para su organizacion.

Los componentes estan divididos en varias principalidades, [edit-OBJECT], [view-OBJECT], [home-OBJECT], [request-OBJECT], [OBJECT-search].

- [view-OBJECT] son aquellos que son vistas para consultar datos, en estos componentes no deberia existir ningun tipo de procesamiento de datos ni envio de informacion al back. 
- [edit-OBJECT] son aquellos que si se encargan de su edición y procesamiento. En estos componentes se manejan extensas validaciones para asegurar la calidad de los datos enviados al BackEnd. Estos componentes tambien se apoyan del validador de namings en el root [naming-validation.service.ts].
- [home-OBJECT] son aquellos que se usan como ventanas de inicio para los distinos roles que acceden a la herramienta. Estos componentes se usan de inicio y usualmente son las que se redirigen desde el componente de la barra de tareas, "toolbar".
- [request-OBJECT] son componentes enfocados en el manejo de las peticiones de creación, modificación y eliminación de Tablas y Tablones, o de cualquier otro contenedor lógico que haga uso NEBULA. Estos tambíen realizan validaciones para asegurar la calidad de los datos.
- [OBJECT-search] son los componentes que se usan para la búsqueda de datos, ya sean namings, tablas, tablones o demas. Estas vistas no deberian tener ningun procesamiento de datos pero si filtros de busqueda y limpieza de datos para su visualización mas completa.

Estas vistas estan restringidas y controladas dependiendo del rol del usuario que accede a la plataforma, los roles actuales en uso son:
- 'L': "Invitado" este usuario tiene disponibilidad limitada y solo esta enfocado en consultar información.
- 'I': "Ingesta" o desarrollo, es el rol estandar para los usuarios desarrolladores de la herramienta. Con este rol se puede hacer uso de la mayoria de las caracteristicas de NEBULA con excepciones de datos mas sensibles.
- 'G': "Gobierno" son las personas con permisos elevados a los de Ingesta, con manejo de datos mas sensibles y de mayor impacto, en su mayoria en las vistas de edicion (edit) asi como el acceso al cuadro de mando, vista que permite editar datos libremente en caso que sea necesario.
- 'RN': "Visto Bueno Negocio" son los usuarios que realizan el paso de revision de diccionarios. Estos usuarios tienen acceso similar a Ingesta con una vista exclusiva para este rol [vobo-dictionary] en donde se realiza esta revisión. En toda la herramienta el nombre "VoBo" se usa como alternativa abreviada para este rol.
- 'PO': "Product Owner" son las personas que manejan los proyectos, casos de uso y solicitudes (requests) de la herramienta. Este rol tiene acceso similar a Ingesta pero con opciones adicionales en la vista de "Proyectos y Casos de Uso"  [backlog] en donde se administran las peticiones.
- 'SPO': "Super Product Owner" son los POs con permisos elevados. Poseen los mismos permisos que los POs pero con la opcion de aceptar o rechazar las solicitudes. Este rol debe manejarse con cuidado.
- 'A': "Admin" es el perfil de administracion el cual permite aceptar y agregar usuarios registrados, creación de los Proyectos lo cual no se puede realizar en ninguna otra parte, carga completa de Tablas y Tablones sin restricciones y otros detalles mas significativos. Este perfil es de suma importancia por
las validaciones que no realiza. Actualmente solo hay un usuario registrado con este rol el cual se usa por los miembros del equipo cuando se necesita.






